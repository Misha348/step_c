﻿
using System.Windows;

using Microsoft.Win32;

using System.IO;
using System.Windows.Forms;
using System.Threading.Tasks;

namespace Task_12._03._20
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Microsoft.Win32.OpenFileDialog openFileDialog;
        private FolderBrowserDialog folderBrowserDialog;
        

        public MainWindow()
        {
            InitializeComponent();
        }

        private void FILE_Click(object sender, RoutedEventArgs e)
        {
            openFileDialog = new Microsoft.Win32.OpenFileDialog();
            if (openFileDialog.ShowDialog() == true)
            {               
                FilePathSource.Text = Path.GetFullPath(openFileDialog.FileName);                
            }
        }


        private void TO_Bt_Click(object sender, RoutedEventArgs e)
        {
            folderBrowserDialog = new FolderBrowserDialog();
            DialogResult result = folderBrowserDialog.ShowDialog();
            if (result == System.Windows.Forms.DialogResult.OK)
            {              
                FilePathTarget.Text = folderBrowserDialog.SelectedPath;
            }
        }


        private void Copy_Bt_Click(object sender, RoutedEventArgs e)
        {
            var c = int.Parse(CopiesCount.Text);
            for (int i = 1; i <= c; i++)
            {              
                File.Copy(FilePathSource.Text, (FilePathTarget.Text + $"\\file_{i}.txt") );
            }            
            
            //Parallel.For(1, c, )
        }

        private void Cancel_Bt_Click(object sender, RoutedEventArgs e)
        {

        }

     
    }
}
