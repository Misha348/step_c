﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;


namespace Threads_04._03._20
{
	 public class AlhorithmConfigurator 
    {
        public string Name { get; set; }
        public long InputNumber { get; set; }
        public int Delay { get; set; }
        public ConsoleColor Color { get; set; }
    }
	
    class Program
    {
        public static void FiboNumbers(object parameters)
        {
            var config = parameters as AlhorithmConfigurator;
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine($"Start {config.Name}");
            int sum = 0;
            int _1st = 1;
            int _2nd = 1;
            while ((Int64)config.InputNumber > sum)
            {
                sum = _1st + _2nd;
                Console.ForegroundColor = config.Color;
                Console.WriteLine($"ThreadId : {Thread.CurrentThread.ManagedThreadId} {config.Name}: {sum}");
                _1st = _2nd;
                _2nd = sum;
                Thread.Sleep(config.Delay);
            }
        }

        public static void Facto(object parameters)
        {
            var config = parameters as AlhorithmConfigurator;
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine($"Start {config.Name}");
            int fact = 1;

            for (int i = 1; i <= (Int64)config.InputNumber; i++)
            {
                Console.ForegroundColor = config.Color;
                Console.WriteLine($"ThreadId : {Thread.CurrentThread.ManagedThreadId} {config.Name}: {fact}");
                fact *= i;
                Thread.Sleep(config.Delay);
            }
        }

        public static void Simple(object parameters)
        {
            var config = parameters as AlhorithmConfigurator;
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine($"Start {config.Name}");
            for (int i = 0; i <= (Int64)config.InputNumber; i++)
            {
                Console.ForegroundColor = config.Color;
                Console.WriteLine($"ThreadId : {Thread.CurrentThread.ManagedThreadId} {config.Name}: {i}");
                Thread.Sleep(500);
            }
        }

        static void Main(string[] args)
        {
            Console.Write("Fibo digit: ");
            Int64 digit_1 = Convert.ToInt32(Console.ReadLine());

            Console.Write("\nFacto digit: ");
            Int64 digit_2 = Convert.ToInt32(Console.ReadLine());

            Console.Write("\nSimple digit: ");
            Int64 digit_3 = Convert.ToInt32(Console.ReadLine());

            ParameterizedThreadStart threadstart1 = new ParameterizedThreadStart(FiboNumbers);
            ParameterizedThreadStart threadstart2 = new ParameterizedThreadStart(Facto);
            ParameterizedThreadStart threadstart3 = new ParameterizedThreadStart(Simple);

            Console.WriteLine($"Main thread id : {Thread.CurrentThread.ManagedThreadId}");

            Thread thread1 = new Thread(threadstart1);
            thread1.Start(new AlhorithmConfigurator
            {
                InputNumber = digit_1,
                Delay = 500,
                Name = "Fibo",
                Color = ConsoleColor.Red
            });

            Thread thread2 = new Thread(threadstart2);
            thread2.Start(new AlhorithmConfigurator
            {
                InputNumber = digit_2,
                Delay = 500,
                Name = "Facto",
                Color = ConsoleColor.Green
            });

            Thread thread3 = new Thread(threadstart3);
            thread3.Start(new AlhorithmConfigurator
            {
                InputNumber = digit_3,
                Delay = 500,
                Name = "Simple",
                Color = ConsoleColor.Blue
            });

            
            Console.WriteLine($"Main thread finished");

            var isInProgress = true;
            while (isInProgress)
            {
                var key = Console.ReadKey();
                switch (key.Key) 
                {
                    case ConsoleKey.NumPad1: thread1.Abort(); break;
                    case ConsoleKey.NumPad2: thread2.Abort(); break;
                    case ConsoleKey.NumPad3: thread3.Abort(); break;
                    default:
                        isInProgress = false; break;
                }
            }
            Console.ReadKey();
        }




        
    }
}
