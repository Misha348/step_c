﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Task_Manager
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private int _selectedItemId; 
        public MainWindow()
        {
            InitializeComponent();

            D_Grid.ItemsSource = Process.GetProcesses().ToList(); 
            D_Grid.Columns.Add(new DataGridTextColumn() { Header = "Id", Binding = new Binding("Id") });
            D_Grid.Columns.Add(new DataGridTextColumn() { Header = "Name", Binding = new Binding("ProcessName") });
            D_Grid.Columns.Add(new DataGridTextColumn() { Header = "PriorityId", Binding = new Binding("BasePriority") });
            D_Grid.Columns.Add(new DataGridTextColumn() { Header = "MachineName", Binding = new Binding("MachineName") });
            D_Grid.Columns.Add(new DataGridTextColumn() { Header = "MainWindowTitle", Binding = new Binding("MainWindowTitle") });

            DispatcherTimer timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromSeconds(1);
            timer.Tick += Timer_Tick;
            timer.Start();
        }
        
        private void Timer_Tick(object sender, EventArgs e)
        {           
            this.Dispatcher.Invoke( () => D_Grid.ItemsSource = Process.GetProcesses().ToList());          
        }

        private void Start_Click(object sender, RoutedEventArgs e)
        {
            Process customnProc = Process.Start(CustmnText.Text);
        }

        private void Info_Click(object sender, RoutedEventArgs e)
        {
            int id = _selectedItemId;
            var pr = Process.GetProcessById(id);
            MessageBox.Show($"{pr.ToString()}");

            //TextB.Text = ( $"{ (pr.Id).ToString()}  {(pr.ProcessName).ToString()} {(pr.BasePriority).ToString()}  {(pr.StartInfo).ToString()}  {(pr.Container).ToString()}  {(pr.ExitCode).ToString()} " +
            //    $"{ (pr.ExitTime).ToString()}  { (pr.MachineName).ToString()}  { (pr.MainWindowTitle).ToString()}"  );                    
        }

        private void End_Click(object sender, RoutedEventArgs e)
        {
            int id = _selectedItemId;
            Process proc = Process.GetProcessById(id);
            proc.Kill();
        }      

        private void D_Grid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            Process pr = (D_Grid.SelectedItem as Process);
            if (pr == null) return;
            _selectedItemId = pr.Id;
            MessageBox.Show(_selectedItemId.ToString());
        }
    }
}
