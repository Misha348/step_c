﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_Manager
{
    public class ProcessesList
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int PriorityId { get; set; }
        public string MachineName { get; set; }
        public string MainWindowTitle { get; set; }

        List<Process> Processes { get; set; }

        public ProcessesList()
        {
            List<Process> processesList = new List<Process>();
            Process[] proc = Process.GetProcesses();

            processesList.Add(proc[0]);
            processesList.Add(proc[1]);
            processesList.Add(proc[2]);
            processesList.Add(proc[3]);
            processesList.Add(proc[4]);


            //Id = proc.Id;
            //Name = proc.ProcessName;
            //PriorityId = proc.BasePriority;
            //MachineName = proc.MachineName;
            //MainWindowTitle = proc.MainWindowTitle;

        }
    }
}
