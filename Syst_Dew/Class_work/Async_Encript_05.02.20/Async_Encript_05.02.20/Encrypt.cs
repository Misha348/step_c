﻿
using System;
using System.Windows.Threading;

namespace Async_Encript_05._02._20
{
    public class XORCipher
    {
        public int TotalLength { get; set; }
        public int HasCompleted { get; set; }
        public double Ratio { get; set; }

        private DispatcherTimer Timer { get; set; }
       


        public XORCipher()
        {
           

            TotalLength = 0;
            HasCompleted = 0;
            Ratio = 0;

            Timer = new DispatcherTimer();
            Timer.Interval = TimeSpan.FromSeconds(0.25);
            Timer.Tick += Timer_Tick;
           
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            Ratio = (HasCompleted * 100) / TotalLength;
           
        }


        private string GetRepeatKey(string s, int n)
        {
            var r = s;
            while (r.Length < n)
            {
                r += r;
            }
            return r.Substring(0, n);
        }

         
      
        private string Cipher(string text, string secretKey)
        {
            TotalLength = text.Length;
            Timer.Start();

            var currentKey = GetRepeatKey(secretKey, text.Length);
            var res = string.Empty;
            for (var i = 0; i < TotalLength; i++)
            {
                HasCompleted = i;
                res += ((char)(text[i] ^ currentKey[i])).ToString();
            }

            return res;
        }

        public string Encrypt(string TextFromFile, string password)
        {
            return Cipher(TextFromFile, password);
        }

        public string Decrypt(string encryptedText, string password) => Cipher(encryptedText, password);
    }
}
