﻿using Microsoft.Win32;
using System;
using System.IO;
using System.Windows;
using System.Windows.Threading;

namespace Async_Encript_05._02._20
{   
    public partial class MainWindow : Window
    {
        public string TextFromFile { get; set; }
        private OpenFileDialog openFileDialog;        
        private DispatcherTimer Timer { get; set; }

        public int TotalLength { get; set; }
        public int HasCompleted { get; set; }
        public int I { get; set; }

     
        public string Encrypted { get; set; }
        public string Decrypted { get; set; }

        public MainWindow()
        {
            InitializeComponent();          
          

            Pr_Bar.Minimum = 0;
            Pr_Bar.Maximum = 1;

            Timer = new DispatcherTimer
            {
                Interval = TimeSpan.FromMilliseconds(10)
            };
            Timer.Tick += Timer_Tick;
          
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
           
            Pr_Bar.Value = I;
        }


        private void Button_Click(object sender, RoutedEventArgs e)
        {
            openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == true)
            {
                T_Box.Text = System.IO.Path.GetFullPath(openFileDialog.FileName);
                TextFromFile = File.ReadAllText(openFileDialog.FileName);                  
            }            
        }     

        private void Start_Click(object sender, RoutedEventArgs e)
        {
            if (Rb_1.IsChecked == true)
            {
                Pr_Bar.Maximum = TextFromFile.Length;
                Encrypted = Encrypt(TextFromFile, KeyText.Text);
              
                File.WriteAllText(openFileDialog.FileName, Encrypted);
            }

            if (Rb_2.IsChecked == true)
            {
                Decrypted = Decrypt(Encrypted, KeyText.Text);
                File.WriteAllText(openFileDialog.FileName, Decrypted);
            }          
        }

        private string GetRepeatKey(string s, int n)
        {
            var r = s;
            while (r.Length < n)
            {
                r += r;
            }
            return r.Substring(0, n);
        }

        private string Cipher(string text, string secretKey)
        {
            TotalLength = text.Length;
            Timer.Start();

            var currentKey = GetRepeatKey(secretKey, text.Length);
            var res = string.Empty;
            for (I = 0; I < TotalLength; I++)
            {               
                res += ((char)(text[I] ^ currentKey[I])).ToString();
                //Ratio = (i * 100) / TotalLength;
            }

            return res;
        }

        public string Encrypt(string TextFromFile, string password)
        {
            return Cipher(TextFromFile, password);
        }

        public string Decrypt(string encryptedText, string password) => Cipher(encryptedText, password);

      
    }
}
