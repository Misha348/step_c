﻿using System;

namespace TestLibrary
{
    public static class StaticClass
    {
        public static void DoSome_1()
        {
            Console.WriteLine("Executining action 1 ");
        }

        public static void DoSome_2()
        {
            Console.WriteLine("Executining action 2 ");
        }

        public static void DoSome_3()
        {
            Console.WriteLine("Executining action 3 ");
        }
    }

    public class NoStaticClass
    {
        private readonly string ctorParameter;
        public NoStaticClass() 
        { 
        }

        public NoStaticClass(string parameter) 
        {
            ctorParameter = parameter;
        }

        public string MethodWithNoParams()
        {
            return "MethodWithNoParams result";
        }

        public string MethodWithStringParams(string param1, string param2)
        {
            return $"MethodsWithStringParams. Param1: {param1}, Param2 {param2}";
        }

        public string MethodWithIntParams(int param1, int param2)
        {
            return $"MethodWithIntParams. Param1: {param1}, Param2 {param2}";
        }

        public string GetCtorParameter() 
        {
            return ctorParameter;
        }
    }
}
