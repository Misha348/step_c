﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;

namespace Domains_03._03._20
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            SelectedDomain = AppDomain.CreateDomain("New_Domain");
            ReadAllDlls();
        }

        private void ReadAllDlls() 
        {
            var targetDirectory = Directory.GetCurrentDirectory();
            string[] fileEntries = Directory.GetFiles(targetDirectory);
            var dllFiles = fileEntries.Where(i => i.Contains(".dll"));

            foreach (var dllFile in dllFiles)
            {
                //availableAssembliesComboBox.Items.Clear();
                //availableAssembliesComboBox.Items.Add(dllFile);
            }
        }

        private void AssemblySelected(object sender, SelectionChangedEventArgs e)
        {
           LoadAssembly(e.AddedItems[0].ToString());
        }

        public Type SelectedClassType { get; set; }

        private void ClassSelected(object sender, SelectionChangedEventArgs e)
        {
            SelectedClassType = SelectedDomain.GetAssemblies()
                .SelectMany(type => type.GetTypes())
                .FirstOrDefault(type => type.IsClass && type.FullName.Contains(e.AddedItems[0].ToString()));

            var methods = SelectedClassType.GetMethods();
            availableMethodsComboBox.Items.Clear();
            foreach (var method in methods) 
            {
                availableMethodsComboBox.Items.Add(method.Name);
            } 
        }

        public MethodInfo SelectedMethodInfo { get; set; }
        public List<TextBox> SelectedMethodParametersControls { get; set; }
        private void MethodSelected(object sender, SelectionChangedEventArgs e)
        {
           var methodName = e.AddedItems[0].ToString();
            SelectedMethodInfo = SelectedClassType.GetMethods().FirstOrDefault(i=>i.Name == methodName);
           var parameters = SelectedMethodInfo.GetParameters();
            parameterControls.Children.Clear();

            SelectedMethodParametersControls = new List<TextBox>();

            if (parameters.Any())
            {
                foreach (var parameter in parameters)
                {
                    var label = new TextBlock();
                    label.Text = parameter.Name;
                    parameterControls.Children.Add(label);

                    var textControl = new TextBox();
                    parameterControls.Children.Add(textControl);
                    SelectedMethodParametersControls.Add(textControl);
                }
            }
            else
            {
                var label = new TextBlock();
                label.Text = $"Method {methodName} has no params";
                parameterControls.Children.Add(label);
            }
        }

        public AppDomain SelectedDomain { get; set; }

        private void LoadAssembly(string fileName) 
        {
            var assemblyName = AssemblyName.GetAssemblyName(fileName);
            Assembly asm = SelectedDomain.Load(assemblyName);
            var name = assemblyName.Name;
            var assemblies = SelectedDomain.GetAssemblies()
                .SelectMany(type => type.GetTypes())
                .Where(type => type.IsClass && type.FullName.Contains(name));
            availableClassesComboBox.Items.Clear();
            foreach (var item in assemblies)
            {
                availableClassesComboBox.Items.Add(item.Name);
            }
        }

        private void InvokeSelectedMethod(object sender, RoutedEventArgs e)
        {
            var paramStringValues = SelectedMethodParametersControls.Select(i => i.Text).ToArray();
            var methodParameters = SelectedMethodInfo.GetParameters();
            var methodParams = new List<object>();

            for (var i = 0; i < methodParameters.Length; i++)
            {
                var parameterType = methodParameters[i].ParameterType;
                var paramValue = paramStringValues[i];
                if (parameterType == typeof(string))
                {
                    methodParams.Add(paramValue);
                }
                if (parameterType == typeof(int))
                {
                    methodParams.Add(int.Parse(paramValue));
                }
            }
            object methodResult;
            if (SelectedMethodInfo.IsStatic)
            {
                methodResult = SelectedMethodInfo.Invoke(null, methodParams.ToArray());
              
            } else 
            {
                methodResult = SelectedMethodInfo.Invoke(Activator.CreateInstance(SelectedClassType), methodParams.ToArray());
            }

            if (methodResult == null)
            {
                MessageBox.Show("Method has been executed with no result");
            }
            else {
                MessageBox.Show(methodResult.ToString());
            }
        }

        private void SelectDllFileButtonClicked(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == true)
            {
                LoadAssembly(openFileDialog.FileName);
            }
           
        }
    }
    
}