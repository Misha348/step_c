﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ThreadsSynchronization_09._03._20
{
    public class FiboCounter
    {
        public Int64 FirstNum { get; set; }
        public Int64 SecondNum { get; set; }
        public int PairCount { get; set; }
        public int IterationCount { get; set; }
        public Int64 Sum { get; set; }

        public FiboCounter()
        {           
           FirstNum = 1;
           SecondNum = 1;
           PairCount = 0;
           IterationCount = 1;
           Sum = 0;            
        }


        public void UpdateValue(object obj)
        {   
            for (int i = 0; i < 10; i++)
            {
                lock (this)
                {
                    Sum = FirstNum + SecondNum;
               
                    if (Sum % 2 == 0)
                    {
                        ++PairCount;
                    }

                    Console.ForegroundColor = ConsoleColor.White;
                    Console.Write($"{IterationCount}");
                    switch (obj)
                    {
                        case 0:
                            Console.ForegroundColor = ConsoleColor.Red;
                            break;
                        case 1:
                            Console.ForegroundColor = ConsoleColor.Blue;
                            break;
                        case 2:
                            Console.ForegroundColor = ConsoleColor.Green;
                            break;
                    }
                    Console.Write($"\t{new string('\t', (int)obj)}{Sum}  ");

                    Console.ForegroundColor = ConsoleColor.White;
                    Console.WriteLine($"[{PairCount}]");
                    FirstNum = SecondNum;
                    SecondNum = Sum;
                    IterationCount++;
                }               
            }
        }
    }

    class Program
    { 
        static void Main(string[] args)
        {           
            FiboCounter fc = new FiboCounter();
            Thread[] threads = new Thread[3];
           
            for (int i = 0; i < threads.Length; i++)
            {          
                
                threads[i] = new Thread(fc.UpdateValue);
                threads[i].Start(i);
            }
            for (int i = 0; i < threads.Length; ++i)
                threads[i].Join();

            Console.WriteLine($"pair sum results: {fc.PairCount}");
        }
        
    }
}
