﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Async_Encript_05._02._20
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public string TextFromFile { get; set; }
        private OpenFileDialog openFileDialog;
        private XORCipher xor;
        public string Encrypted { get; set; }
        public string Decrypted { get; set; }

        public MainWindow()
        {
            InitializeComponent();
            xor = new XORCipher();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == true)
            {
                T_Box.Text = System.IO.Path.GetFullPath(openFileDialog.FileName);
                TextFromFile = File.ReadAllText(openFileDialog.FileName);


                //TestTxt.Text = File.ReadAllText(openFileDialog.FileName);

            }
            
        }

        private void Rb_1_Checked(object sender, RoutedEventArgs e)
        {

        }

        private void Rb_2_Checked(object sender, RoutedEventArgs e)
        {

        }

        private void Start_Click(object sender, RoutedEventArgs e)
        {
            if (Rb_1.IsChecked == true)
            {               
                Encrypted = xor.Encrypt(TextFromFile, KeyText.Text);              
                File.WriteAllText(openFileDialog.FileName, Encrypted);
            }

            if (Rb_2.IsChecked == true)
            {
                Decrypted = xor.Decrypt(Encrypted, KeyText.Text);
                File.WriteAllText(openFileDialog.FileName, Decrypted);
            }          
        }
    }
}
