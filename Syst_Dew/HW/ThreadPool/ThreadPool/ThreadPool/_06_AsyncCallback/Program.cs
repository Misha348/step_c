﻿// Blablabla
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AsyncReadCallBack
{
    class MyState
    {
        public FileStream FS { get; set; }
        public Byte[] Data { get; set; }
    }
    class AsyncReadCallBackClass
    {
        static void Main(string[] args)
        {
            Byte[] data = new Byte[100];
            Byte[] data2 = new Byte[100];            

            Console.WriteLine("Primary thread ID = {0}", Thread.CurrentThread.ManagedThreadId);
            FileStream fs = new FileStream(@"../../Program.cs",
                                            FileMode.Open,
                                            FileAccess.Read, FileShare.Read, 1024,
                                            FileOptions.Asynchronous);

            fs.BeginRead(data, 0, data.Length, ReadIsComplete, new MyState { FS = fs, Data = data });

            FileStream fs2 = new FileStream(@"../../Properties/AssemblyInfo.cs",
                                            FileMode.Open,
                                            FileAccess.Read, FileShare.Read, 1024,
                                            FileOptions.Asynchronous);

            fs2.BeginRead(data2, 0, data.Length, ReadIsComplete, new MyState { FS = fs2, Data = data2 });
            Console.ReadLine();
        }
        // public delegate void AsyncCallback(IAsyncResult ar);
        private static void ReadIsComplete(IAsyncResult ar)
        {
            Console.WriteLine("Reading on thread {0} is finished", Thread.CurrentThread.ManagedThreadId);
            MyState state = (MyState)ar.AsyncState;
            FileStream fs = state.FS;
            Int32 bytesRead = fs.EndRead(ar);
            fs.Close();
            Console.WriteLine("The number of bytes read = {0}", bytesRead);
            Console.WriteLine(Encoding.UTF8.GetString(state.Data).Remove(0, 1));
        }
    }
}