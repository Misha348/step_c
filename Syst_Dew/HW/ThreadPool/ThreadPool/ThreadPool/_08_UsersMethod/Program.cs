﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace _08_UsersMethod
{
    class Program
    {
        private delegate UInt64 AsyncSumDelegate(UInt64 n);
        /*
            internal sealed class AsyncSumDelegate : MulticastDelegate
            {
                public SumDelegate(object object, IntPtr method);
                public UInt64 Invoke(UInt64 n);
                public IAsyncResult BeginInvoke(long n, AsyncCallback callback, object object);
                public UInt64 EndInvoke(IAsyncResult result);
            }
        */
        public static UInt64 Sum(UInt64 n)
        {
            UInt64 sum = 1;
            for (UInt64 i = 2; i < n; ++i)
                sum += i;
            return sum;
        }
        static void Main(string[] args)
        {
            Console.WriteLine(Thread.CurrentThread.ManagedThreadId);
            AsyncSumDelegate del = Sum;
            del.BeginInvoke(100000000, EndSum, del);
            Console.WriteLine("Waiting...");
            string str = Console.ReadLine();
            Console.WriteLine($"Hello, {str}");
            Console.ReadKey();
        }
        private static void EndSum(IAsyncResult ar)
        {
            Console.WriteLine(Thread.CurrentThread.ManagedThreadId);
            AsyncSumDelegate del = (AsyncSumDelegate)ar.AsyncState;
            UInt64 res = del.EndInvoke(ar);
            Console.WriteLine("Summa = " + res);
        }
    }
}