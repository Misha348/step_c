﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02_ReadAsync
{
    class AsyncWaitClass
    {
        static void Main(string[] args)
        {
            FileStream fs = new FileStream(@"../../Program.cs", FileMode.Open,
                                                                FileAccess.Read, 
                                                                FileShare.Read, 
                                                                1024,
                                                                FileOptions.Asynchronous);
            byte[] data = new byte[300];
            // Start asynchronous read from
            // file FileStream.
            // BegixXXX();
            // EndXXX();           
            IAsyncResult ar = fs.BeginRead(data, 0, data.Length, null, null);
            // Another code is executed here...
            // Pause this thread until completion
            // asynchronous operation            
            // and getting her result.
            int bytesRead = fs.EndRead(ar);

            // There are no other operations. Close the file.
            fs.Close();
            // Now you can turn to byte
            // array and print the result of the operation.
            Console.WriteLine("The number of bytes read = {0}", bytesRead);

            Console.WriteLine(Encoding.UTF8.GetString(data));
        }
    }
}
