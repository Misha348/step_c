﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace AppDomainDynamicUnload
{
    class Program
    {
        static void Main(string[] args)
        {
            // створюємо домен додатку з довільним ім'ям            
            AppDomain Domain = AppDomain.CreateDomain("Demo Domain");
            
            // завантажуємо в створений нами домен додатку
            // підготовлену dll бібліотеку
            Assembly asm = Domain.Load(AssemblyName.GetAssemblyName("SampleLibrary.dll"));
            // отримуємо модуль, з якого будемо здійснювати виклик
            Module module = asm.GetModule("SampleLibrary.dll");
            // отримуємо тип даних, що містить шуканий метод
            Type type = module.GetType("SampleLibrary.SampleClass");            

            // отримуємо метод з типу даних
            MethodInfo method = type.GetMethod("DoSome");
            
            // здійснюємо виклик методу
            method.Invoke(null, null);
            // лінійний варіант виклику того ж методу через
            // анонімні об'єкти
            asm.GetModule("SampleLibrary.dll").
                GetType("SampleLibrary.SampleClass").
                GetMethod("DoSome").
                Invoke(null, null);

            // відвантажуємо домен додатку
            AppDomain.Unload(Domain);
        }
    }
}
