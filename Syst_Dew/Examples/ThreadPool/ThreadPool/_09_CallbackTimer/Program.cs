﻿using System;
using System.Threading;

namespace TimerTest
{
    class Program
    {
        static void Main(string[] args)
        {
            Timer t = new Timer(TimerMethod, "Bob", 0, 500);
            Console.WriteLine("Main thread {0} continue...",
           
            Thread.CurrentThread.ManagedThreadId);
            Thread.Sleep(5000);
            t.Dispose();
        }
        static void TimerMethod(Object obj)
        {
            Console.WriteLine("Hello, " + obj);            
            Console.WriteLine("Thread {0} : {1}",
                Thread.CurrentThread.ManagedThreadId, DateTime.Now);
        }
    }
}