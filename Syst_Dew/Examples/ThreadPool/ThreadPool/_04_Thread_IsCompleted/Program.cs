﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AsyncRequest
{
    class AsyncRequestClass
    {
        static void Main(string[] args)

        {
            FileStream fs = new FileStream(@"../../Program.cs",
                                                            FileMode.Open,
                                                            FileAccess.Read, 
                                                            FileShare.Read, 
                                                            1024,
                                                            FileOptions.Asynchronous);
            Byte[] data = new byte[10000];
            IAsyncResult ar = fs.BeginRead(data, 0, data.Length, null, null);
            //IAsyncResult ar = fs.BeginRead(data, 0, data.Length, null, null);
            while (!ar.IsCompleted)
            {
                Console.WriteLine("Operation not completed, wait...");
               
                Thread.Sleep(10);
            }
            Int32 bytesRead = fs.EndRead(ar);
            fs.Close();
            Console.WriteLine("The number of bytes read = {0}", bytesRead);
            Console.WriteLine(Encoding.UTF8.GetString(data).Remove(0, 1));
        }
    }
}