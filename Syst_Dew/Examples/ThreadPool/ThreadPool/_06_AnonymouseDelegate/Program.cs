﻿using System;
using System.IO;
using System.Threading;
using System.Text;
namespace AsyncReadCallBack
{
    class AsyncReadCallBackClass
    {
        // private static Byte[] staticData = new Byte[100];
        static void Main(string[] args)
        {
            Byte[] data = new Byte[100];
            Console.WriteLine("Primary thread ID = {0}",
            Thread.CurrentThread.ManagedThreadId);
            FileStream fs = new FileStream(@"../../Program.cs",
                                                        FileMode.Open,
                                                        FileAccess.Read, FileShare.Read,
                                                        1024, FileOptions.Asynchronous);

            fs.BeginRead(data, 0, data.Length,
                delegate (IAsyncResult ar)
                {
                    Console.WriteLine("Reading on thread {0} is finished",


                    Thread.CurrentThread.ManagedThreadId);
                    Int32 bytesRead = fs.EndRead(ar);
                    fs.Close();
                    Console.WriteLine("The number of bytes read = {0}", bytesRead);

                    Console.WriteLine(Encoding.UTF8.GetString(data));
                    Console.ReadLine();
                }, null);

            Console.ReadLine();
        }
    }
}