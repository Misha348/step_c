﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace _01_ThreadPool_class
{
    // System.Threading.ThreadPool

    // void GetMaxThreads(out int workerThreads, 
    //                    out int completionPortThreads);
    // bool SetMaxThreads(int workerThreads, 
    //                    int completionPortThreads);
    // void GetMinThreads(out int workerThreads, 
    //                    out int completionPortThreads);
    // bool SetMinThreads(int workerThreads, 
    //                    int completionPortThreads);
    // void GetAvailableThreads(out int workerThreads, 
    //                    out int completionPortThreads);

    // static bool QueueUserWorkItem(WaitCallback callBack);
    // static bool QueueUserWorkItem(WaitCallback callBack, object state);
    // static bool UnsafeQueueUserWorkItem(WaitCallback callBack, object state);

    class PoolUsingClass
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Main thread: queue the work item");

            ThreadPool.GetAvailableThreads(out int work, out int comp);
            Console.WriteLine("Available threads: work - " + work + "completion: "  + comp);
            ThreadPool.GetMaxThreads(out work, out comp);
            Console.WriteLine("Max threads: work - " + work + "completion: " + comp);

            Random r = new Random();
            for (int i = 0; i < 10; ++i)
                ThreadPool.QueueUserWorkItem(WorkingElementMethod, r.Next(10));
            Console.WriteLine("Main stream: we perform other tasks");
           
            Thread.Sleep(1000);
            Console.WriteLine("Press any key to continue...");
           
            Console.ReadLine();
        }
        private static void WorkingElementMethod(object state)
        {
            Console.WriteLine("\tThread: {0} State = {1}",
                Thread.CurrentThread.ManagedThreadId, state);
            Thread.Sleep(1000);
        }
    }
}