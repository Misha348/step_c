﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace Domains_Loading
{
    class Program
    {
        static void PrintAllAssemblies(AppDomain appDomain)
        {
            string filler = new String('-', 20);
            Console.WriteLine("Start" + filler + $"{appDomain.FriendlyName}" + filler);
            var q = AppDomain.CurrentDomain.GetAssemblies().OrderBy(s => s.GetName().Name);
            int counter = 0;
            foreach (var item in q)
            {
                Console.WriteLine($"{++counter}. - " + item.GetName().FullName);
            }
            Console.WriteLine("End  " + filler + $"{appDomain.FriendlyName}" + filler);
            Console.WriteLine(Environment.NewLine);
        }
        static void Main(string[] args)
        {
            #region First domain with static method
            //Main Domain
            PrintAllAssemblies(AppDomain.CurrentDomain);

            //Child doman #1 with static method
            AppDomain secondaryDomain = AppDomain.CreateDomain("Child domain #1");
            secondaryDomain.AssemblyLoad += SecondaryDomain_AssemblyLoad;
            //secondaryDomain.AssemblyLoad += (s, e) => { Console.WriteLine("Domain LOADED"); };
            secondaryDomain.DomainUnload += (s, e) => { Console.WriteLine("Domain UNLOADED"); };

            Assembly asm = secondaryDomain.Load(AssemblyName.GetAssemblyName("SimpleLib.dll"));
            PrintAllAssemblies(secondaryDomain);

            Module module = asm.GetModule("SimpleLib.dll");
            Type type = module.GetType("SimpleLib.Class1");
            MethodInfo method = type.GetMethod("NothinDoingMethod");
            //void return, 1st param Instance of class, 2nd params obj[]
            method.Invoke(null, null);
            AppDomain.Unload(secondaryDomain);
            Console.ReadKey();
            #endregion
            #region Second domain with NON static method
            //Child doman #2 with non-static method
            AppDomain thirdDomain = AppDomain.CreateDomain("Child domain #2");
            thirdDomain.AssemblyLoad += (s, e) => { Console.WriteLine("Domain loaded"); };
            thirdDomain.DomainUnload += (s, a) => { Console.WriteLine("Domain UNLOADED"); };

            Assembly asm2 = thirdDomain.Load(AssemblyName.GetAssemblyName("ComplexLib.dll"));
            PrintAllAssemblies(thirdDomain);

            Module module2 = asm2.GetModule("ComplexLib.dll");
            Type type2 = module2.GetType("ComplexLib.Class2");

            //public string GetDomainName(int count)
            //MethodInfo method2 = type.GetMethod("GetDomainName");

            MethodInfo method2 = type2.GetMethod("GetDomainName", new Type[] { typeof(int) });
            if (method2 == null)
            {
                // never throw generic Exception - replace this with some other exception type
                Console.WriteLine("We DONT have such method!");
            }
            else
            {
                Console.WriteLine("We have such method!");
            }

            MethodInfo method3 = type2.GetMethod("GetStringResult");
            if (method3 == null)
            {
                // never throw generic Exception - replace this with some other exception type
                Console.WriteLine("We DONT have such method!");
            }
            else
            {
                Console.WriteLine("We have such method!");
            }

            //create object arr for parameterized constructor
            object[] constructorParameters = new object[2];
            constructorParameters[0] = 999;             // First parameter.
            constructorParameters[1] = "some value";   // Second parameter.

            //create an instance ob class for non static objects
            var o1 = Activator.CreateInstance(type2); //, constructorParameters);
            var o2 = Activator.CreateInstance(type2, constructorParameters);


            //create object array for method parameters
            var methodParams = new object[1];
            methodParams[0] = 100500;

            var result1 = method2.Invoke(o1, methodParams); // new object[] { 999 }
            Console.WriteLine(result1);

            //public string GetStringResult()
            var result2 = method3.Invoke(o2, null);
            Console.WriteLine(result2);
            Console.ReadKey();
            #endregion
            #region Get all methods with reflections
            var query = thirdDomain.GetAssemblies()
                                    .SelectMany(t => t.GetTypes())
                                    .Where(t => t.IsClass && t.Namespace == "ComplexLib")
                                    ;
            foreach (var item in query)
            {
                Console.WriteLine($"-------========{item.FullName}========-------");
                if (!item.IsPublic)
                {
                    continue;
                }

                MemberInfo[] members = item.GetMembers(BindingFlags.Public 
                                                      | BindingFlags.Instance
                                                      | BindingFlags.InvokeMethod);
                foreach (MemberInfo member in members)
                {
                    Console.WriteLine(item.Name + "." + member.Name);                           
                }
                Console.WriteLine();
            }
            AppDomain.Unload(thirdDomain);
            #endregion

        }

        private static void SecondaryDomain_AssemblyLoad(object sender, AssemblyLoadEventArgs args)
        {
            Console.WriteLine("Domain LOADED");
        }
    }
}