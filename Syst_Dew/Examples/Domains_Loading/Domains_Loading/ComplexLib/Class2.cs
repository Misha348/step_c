﻿using System;

namespace ComplexLib
{
    public class Class2
    {
        private int _code;
        private string _data;

        public Class2() { }
        public Class2(int code, string data)
        {
            _code = code;
            _data = data;
        }

        public string GetDomainName(int count)
        {            
            return $"Domain: {_code + count}";
        }
        public string GetDomainName()
        {
            return $"Domain: {_code}";
        }
        public string GetStringResult()
        {
            return $"Data:  {_data ?? "String is empty"}";
        }
    }
}
