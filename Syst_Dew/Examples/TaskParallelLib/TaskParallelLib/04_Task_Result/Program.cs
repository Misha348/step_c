﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04_Task_Result
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;

            Task<int> task1 = new Task<int>(() => Factorial(5));
            task1.Start();

            //task1.Wait();
            Console.WriteLine($"Фактоіал числа 5 = {task1.Result}");

            //Task<Book> task2 = new Task<Book>(() =>
            //{
            //    return new Book { Title = "Війні і мир", Author = "Л. Толстой" };
            //});

            string separator = new string('-', 10);
            Task<string> task2 = new Task<string>(
                delegate (object obj)
                {
                    return GetName("Tom\n" + separator);
                }, 
                new Book());
            task2.Start();

            Console.WriteLine(task2.Result);
            //Book b = task2.Result;  // ожидаем получение результата
            //Console.WriteLine($"Назва книги: {b.Title}, автор: {b.Author}");

            Console.ReadLine();
        }


        static int Factorial(int x)
        {
            int result = 1;

            for (int i = 1; i <= x; i++)
            {
                result *= i;
            }

            return result;
        }
        static public string GetName(string name)
        {
            return $"Name: {name}";
        }
    }


    public class Book
    {
        public string Title { get; set; }
        public string Author { get; set; }
    }
}