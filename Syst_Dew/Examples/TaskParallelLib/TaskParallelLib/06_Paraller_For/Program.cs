﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace _06_Paraller_For
{
    class Book
    {
        public string Name { get; set; }
    }
    class Program
    {
        static void Main(string[] args)
        {
            // itertions: [1...9]
            Parallel.For(1, 10, delegate (int i)
            {
                Factorial(i, new Book());
            });
            
            Console.ReadLine();
        }

        static void Factorial(int x, Book book)
        {
            int result = 1;

            for (int i = 1; i <= x; i++)
            {
                result *= i;               
            }
            Console.WriteLine($"Task executing {Task.CurrentId}");
            Console.WriteLine($"Factorial {x} = {result}");
            Thread.Sleep(3000);
        }
    }
}
