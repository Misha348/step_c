﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace _07_Parallel_ForEach
{
    class Program
    {
        static void Main(string[] args)
        {
            ParallelLoopResult result = Parallel.ForEach<int>(
                new List<int>() { 1, 3, 5, 8 },
                Factorial);

            Console.ReadLine();
        }
        static void Factorial(int x)
        {
            int result = 1;

            for (int i = 1; i <= x; i++)
            {
                result *= i;               
            }
            Console.WriteLine($"Task executing {Task.CurrentId}");
            Console.WriteLine($"Factorial {x} = {result}");
            Thread.Sleep(3000);
        }
    }
}
