﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace _03_monitor
{
    /*
        static void Enter(object obj);

        static bool TryEnter(object obj);
        static bool TryEnter(object obj, int millisecondsTimeout);
        static bool TryEnter(object obj, TimeSpan timeout);
     
        static void Exit(object obj); SynchronizationLockException
     */
    class Program
    {
        #region Sync with Interlocker
        class InterlockedCounter
        {
            int field1;
            int field2;
            public int Field1
            {
                get { return field1; }
            }
            public int Field2
            {
                get { return field2; }
            }
            public void UpdateFields()
            {
                for (int i = 0; i < 1000000; ++i)
                {
                    Interlocked.Increment(ref field1);
                    if (field1 % 2 == 0)
                        Interlocked.Increment(ref field2);
                }
            }
        }
        /*
       ■ Thread 1 reads count into register → 1
       ■ Thread 1 increments the register value → 2
       ■ Thread 1 saves the value to memory → 2
       ■ Scheduler disables Thread 1
       ■ Scheduler connects Thread 2
       ■ Thread 2 reads count into register → 0
       ■ Thread 2 increases the value of the register → 1
       ■ Thread 2 stores the value in memory → 1
       ■ Scheduler disables Thread 2
       ■ Scheduler connects Thread 1
       */
        private static void BadAsync()
        {
            Console.WriteLine("Sync Interlocked-methods:");
            InterlockedCounter c = new InterlockedCounter();
            Thread[] threads = new Thread[5];
            for (int i = 0; i < threads.Length; ++i)
            {
                threads[i] = new Thread(c.UpdateFields);
                threads[i].Start();
            }
            for (int i = 0; i < threads.Length; ++i)
                threads[i].Join();
            Console.WriteLine("Field1: {0}, Field2: {1}\n\n", c.Field1, c.Field2);
        }
        #endregion
        #region Sync with Monitor
        class MonitorLockCounter
        {
            int field1;
            int field2;
            public int Field1 { get { return field1; } }
            public int Field2 { get { return field2; } }
            public void UpdateFields()
            {
                for (int i = 0; i < 1000000; ++i)
                {
                    Monitor.Enter(this);
                    try
                    {
                        ++field1;
                        if (field1 % 2 == 0)
                            ++field2;
                    }
                    finally
                    {
                        Monitor.Exit(this);
                    }
                }
            }
        }
        private static void GoodAsync()
        {
            Console.WriteLine("Sync Monitor-methods:");
            MonitorLockCounter c = new MonitorLockCounter();
            Thread[] threads = new Thread[5];
            for (int i = 0; i < threads.Length; ++i)
            {
                threads[i] = new Thread(c.UpdateFields);
                threads[i].Start();
            }
            for (int i = 0; i < threads.Length; ++i)
                threads[i].Join();
            Console.WriteLine("Field1: {0}, Field2: {1}\n\n",
            c.Field1, c.Field2);
        }
        #endregion
        static void Main(string[] args)
        {
            //BadAsync();
            GoodAsync();
        }
    }
}
