﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace _23._09._19_Delegates
{
	public class Array
	{
		public int[] arr;
		public Array(int [] newArr)
		{
			arr = newArr;
			Random rnd = new Random();
			for (int i = 0; i < arr.Length; i++)
			{
				arr[i] = rnd.Next(-25, 30);
			}
		}

		public int GetQuantityOFNegatives()
		{
			var k = 0;
			for (int i = 0; i < arr.Length; i++)
			{
				if (arr[i] < 0)
					k++;
			}
			return k;
		}

		public int GetSumOfElements()
		{
			var sum = 0;
			for (int i = 0; i < arr.Length; i++)
			{
				sum += arr[i];
			}
			return sum;
		}

		public int GetQuantityOfSimpleDigits()
		{
			var k = 0;
			for (int i = 0; i < arr.Length; i++)
			{
				if (arr[i] % 2 != 0)
					k++;
			}
			return k;
		}

		public void ChangeNegative()
		{
			for (int i = 0; i < arr.Length; i++)
			{
				if (arr[i] < 0)
					arr[i] = 0;
			}
		}

		public void SortArray()
		{
			System.Array.Sort(arr); 
		}

		public void MovePairElToBegin()
		{
			int j = 0;
			for (int i = 0; i < arr.Length; i++)
			{
				if (arr[i] % 2 == 0)
				{
					var tmp = arr[i];
					arr[i] = arr[j];
					arr[j] = tmp;
					j++;
				}
			}
		}

		public void PrintArray()
		{
			for (int i = 0; i < arr.Length; i++)
			{
				Console.Write($"{arr[i]} ");
			}
			Console.WriteLine("\n");
		}
	}

	public delegate int GetSomeDataFromArrayDelegate();
	public delegate void DoSomeactionsWitharraydDelegate();

	class Program
	{
		static void Main(string[] args)
		{
			int[] myArr = new int[10];
			Array arr = new Array(myArr);

			GetSomeDataFromArrayDelegate[] getdata = new GetSomeDataFromArrayDelegate[3];
			getdata[0] = new GetSomeDataFromArrayDelegate(arr.GetQuantityOFNegatives);
			getdata[1] = new GetSomeDataFromArrayDelegate(arr.GetQuantityOfSimpleDigits);
			getdata[2] = new GetSomeDataFromArrayDelegate(arr.GetSumOfElements);

			DoSomeactionsWitharraydDelegate[] action = new DoSomeactionsWitharraydDelegate[3];
			action[0] = new DoSomeactionsWitharraydDelegate(arr.MovePairElToBegin);
			action[1] = new DoSomeactionsWitharraydDelegate(arr.SortArray);
			action[2] = new DoSomeactionsWitharraydDelegate(arr.ChangeNegative);			

			bool flag = false;
			while (!flag)
			{
				arr.PrintArray();
				Console.WriteLine("[ 1 ] - get some data from array.\n[ 2 ] - do some changes in array.");

				bool flag_ = false;
				int choise = 0;
				while (!flag_)
				{
					if (int.TryParse(Console.ReadLine(), out choise) && ((choise == 1) || (choise == 2)))
						break;
					else
						Console.WriteLine("wrong choise");				
				}				
				
				switch (choise)
				{
					case 1:
						Console.WriteLine("[ 1 ] - get quantity oF negatives.\n[ 2 ] - get quantity of simple digits.\n" +
							"[ 3 ] - get sum of elements.\n");
						var _choise = int.Parse(Console.ReadLine());
						Console.WriteLine(getdata[_choise - 1]());
						break;
					case 2:
						Console.WriteLine("[ 1 ] - move pair digits to begining.\n[ 2 ] - sort array.\n" +
							"[ 3 ] - change negative digits with 0.\n");
						var _choise_ = int.Parse(Console.ReadLine());
						action[_choise_ - 1]();
						arr.PrintArray();
						break;
					default:
						break;
				}
				Console.WriteLine("continue? [ y ] - yes.\t[ n ] - no");
				string myChoise = Console.ReadLine();
				if (myChoise == "y")
				{
					Console.Clear();
					continue;					
				}					
				else if (myChoise == "n")
					break;
			}			
		}
	}
}
