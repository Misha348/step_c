﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test
{
	class Program
	{
		static void Main(string[] args)
		{
			Random rnd = new Random();
			int[] arr = new int[10];
			for (int i = 0; i < arr.Length; i++)
			{
				arr[i] = rnd.Next(-20, 20);

			}

			for (int i = 0; i < arr.Length; i++)
			{
				Console.Write($"{arr[i]} ");
			}
			Console.WriteLine();
			Console.WriteLine(new string('=', 30));

			int j = 0;
			for (int i = 0; i < arr.Length; i++)
			{
				if (arr[i] % 2 == 0)
				{
					var tmp = arr[i];
					arr[i] = arr[j];
					arr[j] = tmp;
					j++;
				}
			}

			for (int i = 0; i < arr.Length; i++)
			{
				Console.Write($"{arr[i]} ");
			}
			Console.WriteLine();

		}
	}
}
