﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _07._10._19_LINQ_I
{
	class Program
	{
		static void Main(string[] args)
		{
			Random rnd = new Random();
			int[] arr = new int[15];
			for (int i = 0; i < arr.Length; i++)
			{
				arr[i] = rnd.Next(-100, 100);
				Console.Write($"{arr[i]} ");
			}
			Console.WriteLine();
			Console.WriteLine(new string('=', 50));


			var sample = from d in arr
						 where ((d % 1 == 0) && d > 0)
						 orderby d
						 select d;

			foreach (var digit in sample)
			{
				Console.Write($"{digit} ");
			}
			Console.WriteLine();

		}
	}
}
