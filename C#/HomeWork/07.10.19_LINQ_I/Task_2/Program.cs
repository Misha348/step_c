﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_2
{
	class Program
	{
		static void Main(string[] args)
		{
			Random rnd = new Random();
			int[] arr = new int[15];
			for (int i = 0; i < arr.Length; i++)
			{
				arr[i] = rnd.Next(-100, 100);
				Console.Write($"{arr[i]} ");
			}
			Console.WriteLine();
			Console.WriteLine(new string('=', 50));

			var sample = from d in arr
						 where d >= 10 && d <= 99
						 select d;
			var count = sample.Count();
			Console.Write($"quantity of specified el: {count}\n");
			var aver = sample.Average();
			Console.Write($"average of specified el: {aver}\n");
		}
	}
}
