﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_3
{
	class Program
	{
		static void Main(string[] args)
		{
			string[] arr = new string[] { "ihoui", "lihyf", "Ouhuyf", "ouhutof", "ugydiyg", "Fpiytud", "Ujkgfrjf" };
			//var newStr = from w in arr
			//			 from s in w
			//			 where s == w[0]
			//			 select s;

			//foreach (var symb in newStr)
			//{
			//	Console.Write($"{symb}");
			//}
			//Console.WriteLine();

			//string newString = arr.Aggregate((str1, str2) => (str1 + str2));
			//Console.WriteLine(newString);

			string newString = arr.Where(s => !string.IsNullOrEmpty(s))
						  .Aggregate("", (sb1, sb2) => sb1 + sb2.First());
			Console.WriteLine(newString);
		}
	}
}
