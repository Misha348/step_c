﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_4
{
	class Program
	{
		static void Main(string[] args)
		{
			string[] arr = new string[] 
			{
				"A_",
				"B_",
				"C_",
				"D_",
				"E_",
				"F_",
				"G_"
			};
			string newString = arr.Where(s => !string.IsNullOrEmpty(s))
						  .Aggregate("", (sb1, sb2) => sb1 + sb2.First());

			char[] arr1 = newString.ToCharArray();
			Array.Reverse(arr1);

			string newString1 = new string(arr1);
			Console.WriteLine(newString1);
		}
	}
}
