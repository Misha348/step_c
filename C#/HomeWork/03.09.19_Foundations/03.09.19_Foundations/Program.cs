﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _03._09._19_Foundations
{
	class Program
	{
		public static string StringGenerator1(int length)
		{
			Random random = new Random();
			//const string chars = "abcdefghijklmnopqrstuvwxyz          ";
			//const string chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789          ";
			const string chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()[]{}|,.<>/?          ";

			string str = new string(Enumerable.Repeat(chars, length).Select(s => s[random.Next(s.Length)]).ToArray());

			//var randomizer = new Random();
			//var final = str.Select(x => randomizer.Next() % 2 == 0 ? (char.IsUpper(x) ? x.ToString().ToLower().First() : x.ToString().ToUpper().First()) : x);
			//var randomUpperLower = new string(final.ToArray());
			return str;
		}
		

		static void Main(string[] args)
		{
			string str = StringGenerator1(160);
			Console.WriteLine(str);
			//var spaceQuantity = 0;
			//Console.WriteLine("Enter some line: ");
			//char input;
			//do
			//{
			//	input = Console.ReadKey().KeyChar;
			//	if (input == ' ')
			//		spaceQuantity++;
			//}
			//while (input != '.');
			//Console.WriteLine();
			//Console.WriteLine($"space symbol quantity = {spaceQuantity}");

			//StringBuilder builder = new StringBuilder();
			//Random random = new Random();
			//char ch;
			//for (int i = 0; i < 50; i++)
			//{
			//	ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
			//	builder.Append(ch);
			//}
			//Console.WriteLine(builder.ToString().ToLower());
			//Console.WriteLine("=================================");

			//	var someString = builder.ToString();

			//var randomizer = new Random();
			//var final = someString.Select(x => randomizer.Next() % 2 == 0 ? (char.IsUpper(x) ? x.ToString().ToLower().First() : x.ToString().ToUpper().First()) : x);
			//var randomUpperLower = new string(final.ToArray());
			//Console.WriteLine(randomUpperLower);
			//Console.WriteLine("=================================");











			Console.ReadKey();
		}
	}
}
