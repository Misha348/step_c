﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_2
{
	class Program
	{
		static void Main(string[] args)
		{
			Console.WriteLine("Enter some digit: ");
			string digit = Console.ReadLine();			

			if (digit.Count() == 6)
			{				
				var sb_1 = digit.Substring(0, 1);
				int a = int.Parse(sb_1);

				var sb_2 = digit.Substring(1, 1);
				int b = int.Parse(sb_2);

				var sb_3 = digit.Substring(2, 1);
				int c = int.Parse(sb_3);

				var sb_4 = digit.Substring(3, 1);
				int d = int.Parse(sb_4);

				var sb_5 = digit.Substring(4, 1);
				int e = int.Parse(sb_5);

				var sb_6 = digit.Substring(5, 1);
				int f = int.Parse(sb_6);

				var sum1 = a + b + c;
				var sum2 = d + e + f;

				if (sum1 == sum2)
					Console.WriteLine( $"digit {digit} is lucky." );
				else
					Console.WriteLine($"digit {digit} is'n lucky.");
			}
			else
				Console.WriteLine($"digit {digit} is'n lucky.");

			Console.ReadKey();
		}
		
	}
}
