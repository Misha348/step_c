﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_3
{
	class Program
	{
		static void Main(string[] args)
		{
			Console.WriteLine("Enter some digit: ");
			int digit1 = int.Parse(Console.ReadLine());

			Console.WriteLine("Enter some another digit: ");
			int digit2 = int.Parse(Console.ReadLine());
			Console.WriteLine();

			int counter = digit1;
			while (counter <= digit2)
			{
				for (int j = 0; j < counter; j++)
				{
					Console.Write($"{counter} ");
				}
				counter++;
				Console.WriteLine();
			}
			Console.ReadKey();
		}
	}
}
