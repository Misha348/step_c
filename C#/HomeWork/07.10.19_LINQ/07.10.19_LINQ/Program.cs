﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _07._10._19_LINQ
{
	class Program
	{
		static void Main(string[] args)
		{
			string[] str_1 = new string[] { "One", "two", "Three", "four", "Five", "six", "Seven", "eight" };
			string[] str_2 = new string[] { "One", "two", "Three", "four", "Five", "six", "Seven", "eight" };

            //var matches = from w1 in str_1
            //			  join w2 in str_2
            //			  where char.IsUpper(w1[0]) equals
            //			  char.IsUpper(w2[0])

            var q = (from w1 in str_1 where char.IsUpper(w1[0]) select w1)
                .Union(from w2 in str_2 where char.IsUpper(w2[0]) select w2);


            //var matches = from w1 in str_1 where char.IsUpper(w1[0])
			//			  join w2 in str_2 on char.IsUpper(w2[0]);

            foreach (var item in q)
            {
                Console.WriteLine(item);
            }

            //var matches = str_1.Intersect(str_2);
            //foreach (var item in matches)
            //{
            //	Console.WriteLine(item);
            //}


        }
	}
}
