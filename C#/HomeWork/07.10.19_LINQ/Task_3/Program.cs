﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using static System.Console;

namespace Task_2
{
    public class Category
    {
        public int CategoryId { get; set; }
        public string CatName { get; set; }
        public string CatDescription { get; set; }

		public override string ToString()
		{
			return $"{CategoryId}\tCat Name: {CatName}\t Cat Descript: {CatDescription}";
		}

	}

    public class Product
    {      
		public Category CategoryId { get; set; }
        public int ProdId { get; set; }       
        public string ProdName { get; set; }       
        public decimal Price { get; set; }       
        public DateTime Date { get; set; }      
        public string ProdCountry { get; set; }      
        //public int CategoryId { get; set; }      
        public int? Discount = null;
        public override string ToString()
        {
            return $"categ ID: {CategoryId}\nprod ID:  {ProdId}.\tprod name: {ProdName}\tprice: {Price}\tdate: {(Date.ToShortDateString()).ToString()}\tcountry: {ProdCountry}";
        }
    }

    public class Order
    {
        public int OrderId { get; set; }
        public int ProdId { get; set; }
        public DateTime OrderDate { get; set; }
        public int Quantity { get; set; }

    }

    class Program
    {
        static void Main(string[] args)
        {
            List<Category> categories = new List<Category>()
            {
                 new Category { CategoryId = 1, CatName = "Electronic", CatDescription = "work by electricity" },
                 new Category { CategoryId = 2, CatName = "Health", CatDescription = "improve health"},
                 new Category { CategoryId = 3, CatName = "Book", CatDescription = "contain information"},
                 new Category { CategoryId = 4, CatName = "Home", CatDescription = "is using in household"},
                 new Category { CategoryId = 5, CatName = "Sport", CatDescription = "is using in sport activities"}
            };

            List<Product> products = new List<Product>()
            {
                new Product {ProdId = 1, CategoryId = categories[0], ProdName = "Pneumoaxe", Date = new DateTime(2014, 05, 08), Price = 850, ProdCountry = "Poland"},
                new Product {ProdId = 2, CategoryId = categories[1], ProdName = "Cream", Date = new DateTime(2015, 01, 18), Price = 85, ProdCountry = "Greece"},
                new Product {ProdId = 3, CategoryId = categories[2], ProdName = "Science", Date = new DateTime(2010, 12, 12), Price = 155, ProdCountry = "USA" },
                new Product {ProdId = 4, CategoryId = categories[3], ProdName = "siting sofa", Date = new DateTime(2013, 02, 13), Price = 745, ProdCountry = "Hungary"},
                new Product {ProdId = 5, CategoryId = categories[4], ProdName = "Horizont bar", Date = new DateTime(2019, 03, 10), Price = 900, ProdCountry = "Ukraine"},

                new Product {ProdId = 6, CategoryId = categories[0], ProdName = "Electro saw", Date = new DateTime(2016, 07, 08), Price = 400, ProdCountry = "Germany"},
                new Product {ProdId = 7, CategoryId = categories[1], ProdName = "Blood checker", Date = new DateTime(2015, 09, 19), Price = 285, ProdCountry = "Poland"},
                new Product {ProdId = 8, CategoryId = categories[2], ProdName = "Fantasy", Date = new DateTime(2019, 12, 12), Price = 120, ProdCountry = "Ukraine" },
                new Product {ProdId = 9, CategoryId = categories[3], ProdName = "Garden set", Date = new DateTime(2019, 02, 25), Price = 345, ProdCountry = "Ukraine"},
                new Product {ProdId = 10, CategoryId = categories[4], ProdName = "barbelbell", Date = new DateTime(2018, 01, 08), Price = 550, ProdCountry = "Poland"}
            };

            foreach (var product in products)
            {
                Console.WriteLine(product.ToString());
                Console.WriteLine();
            }

			//Console.Write("\nenter lowwer diapasone of price: ");
			//var lowwer = Convert.ToUInt32(Console.ReadLine());
			//Console.Write("\nenter upper diapasone of price: ");
			//var upperer = Convert.ToUInt32(Console.ReadLine());

			//var query1 = from prod in products
			//             where (prod.Price > lowwer && prod.Price < upperer)
			//             select prod.Name;
			//var query_1 = products.Where(i => i.Price > lowwer && i.Price < upperer);

			//foreach (var item in query1)
			//{
			//    Console.Write($"{item}\t");
			//}
			//Console.WriteLine();
			//Console.WriteLine(new string('=', 130));
			////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

			//var query2 = from prod in products
			//             where (((DateTime.Now.Year - prod.Date.Year) <= 2) && (prod.ProdCountry == "Ukraine"))
			//             select prod.Name;
			//var query_2 = products.Where(i => (DateTime.Now.Year - i.Date.Year) <= 2 && (i.ProdCountry == "Ukraine"));

			//foreach (var item in query2)
			//{
			//    Console.Write($"{item}\t");
			//}
			//Console.WriteLine();
			//Console.WriteLine(new string('=', 130));
			////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


			//Console.WriteLine("enter name of product/part of name");
			//         var prName = Console.ReadLine();
			//         var query3 = from prod in products
			//                      where (prod.Name == prName || (prod.Name).Contains(prName))
			//                      select prod.Name;

			//         var query_3 = products.Where(i => i.Name == prName || i.Name.Contains(prName));
			//         foreach (var item in query3)
			//         {
			//             Console.Write($"{item}\t");
			//         }
			//         Console.WriteLine();
			//         Console.WriteLine(new string('=', 130));
			////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


			//var query4 = from prod in products
			//                      orderby prod.Price
			//                      select prod;

			//         Console.WriteLine(query4.First());
			//         Console.WriteLine(query4.Last());
			//			Console.WriteLine();
			//         Console.WriteLine(new string('=', 130));

			//         decimal max = products.Max(p => p.Price);
			//         decimal min = products.Min(p => p.Price);
			////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


			//var qu = from prod in products
			//                  where prod.ProdCountry == "Ukraine"
			//                  select prod;

			//         Console.WriteLine(qu.Average(p => p.Price));
			//Console.WriteLine();
			Console.WriteLine(new string('=', 130));
			////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

			var sample_1 = from prod in products
						   where prod.Date.Year == DateTime.Now.Year
						   orderby prod.Price descending
						   select prod;

			foreach (var prod in sample_1)
			{
				Console.WriteLine(prod);
				Console.WriteLine();
			}
			Console.WriteLine();
			Console.WriteLine(new string('=', 130));
			////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

			Console.Write("enter country for searching products: ");
			string country = Console.ReadLine();
			var sample_2 = from prod in products
						   where prod.ProdCountry == country
						   select prod;
			foreach (var prod in sample_2)
			{
				Console.WriteLine(prod);
			}

			Console.WriteLine($" -= {sample_2.Count(p => p.ProdCountry == country)} =- ");
			Console.WriteLine();
			Console.WriteLine(new string('=', 130));
			////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

			Console.WriteLine("enter category for searching products");
			string categor = Console.ReadLine();
			var sample_3 = from prod in products
						   where prod.CategoryId.CatName == categor
						   select prod;

			//decimal max = sample_3.Max(p => p.Price);
			//decimal min = sample_3.Min(p => p.Price);

			Console.WriteLine(sample_3.First());
			Console.WriteLine(sample_3.Last());
			
			Console.WriteLine(new string('=', 130));
			////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

			var sample_4 = from prod in products
						   where prod.ProdCountry != "Ukraine"
						   select prod;
			foreach (var prod in sample_4)
			{
				Console.WriteLine($"{prod.CategoryId.CatName.ToString()}\t {prod.ProdCountry.ToString()}");
			}
			Console.WriteLine();
			Console.WriteLine(new string('=', 130));
			////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

			var sample_5 = from prod in products
						   group prod by prod.CategoryId.CatName;

			foreach (IGrouping<string, Product> p in sample_5)
			{
				Console.WriteLine(p.Key);				
				foreach (var item in p)
				{
					Console.WriteLine($"   {item.ProdName.ToString()}\t {item.ProdCountry.ToString()}\t {item.Date.ToShortDateString()}");
				}
				Console.WriteLine();
			}
		}		
	}
}

