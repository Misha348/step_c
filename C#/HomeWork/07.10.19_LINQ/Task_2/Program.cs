﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using static System.Console;

namespace Task_2
{
    public class Category
    {
        public int CategoryId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }       
    }

    public class Product
    {
        [Required(ErrorMessage = "Id must be inputed")]
        [Range(1000, 9999, ErrorMessage = "uncorrect range")]
        public int ProdId { get; set; }

        [Required(ErrorMessage = "name must be inputed")]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "to short name")]
        [RegularExpression("^A-Z", ErrorMessage = "name must begin from Upper letter")]
        public string Name { get; set; }

        [Required(ErrorMessage = "price must be defenited")]
        [Range(0.0000001, 9999999999, ErrorMessage = "price must be > 0")]
        public decimal Price { get; set; }

        [Required(ErrorMessage = "date must be defenited")]
        [DataType("1950.01.01", ErrorMessage = "data must be in scope (1950 - now)")]
        [Range(typeof(DateTime), "1950.01.01", "2019.12.31")]
        public DateTime Date { get; set; }

        [Required(ErrorMessage = "country must be inputed")]
        [StringLength(100, MinimumLength = 2, ErrorMessage = "to short name")]
        public string ProdCountry { get; set; }

        [Required(ErrorMessage = "Id must be inputed")]
        [Range(1, 10, ErrorMessage = "uncorrect range")]
        public int CategoryId { get; set; }

        [Range(0, 100, ErrorMessage = "uncorrect range of discount")]
        public int? Discount = null;


        public Product() { }
        public Product(int prID, string prName, decimal prPrice, DateTime prDate, string prCountr, int categorID, int prDisc)
        {
            ProdId = prID;
            Name = prName;
            Price = prPrice;
            Date = prDate;
            ProdCountry = prCountr;
            CategoryId = categorID;
            Discount = prDisc;
        }

        public override string ToString()
        {
            return $"categor ID: {CategoryId}\tprod ID: {ProdId}.\tprod name: {Name}\t\tprice: {Price}\tdate: {(Date.ToShortDateString()).ToString()}\tcountry: {ProdCountry}";
        }
    }

    public class Order
    {
        public int OrderId { get; set; }
        public int ProdId { get; set; }
        public DateTime OrderDate { get; set; }
        public int Quantity { get; set; }
    }

    class Program
    {
        static void Main(string[] args)
        {
            //List<Category> categories = new List<Category>()
            //{
            //     new Category { CategoryId = 1, Name = "Electronic", Description = "work by electricity" },
            //     new Category { CategoryId = 2, Name = "Health", Description = "improve health"},
            //     new Category { CategoryId = 3, Name = "Book", Description = "contain information"},
            //     new Category { CategoryId = 4, Name = "Home", Description = "is using in household"},
            //     new Category { CategoryId = 5, Name = "Sport", Description = "is using in sport activities"}
            //};

            Product product = new Product();
            bool IsValid;
            do
            {
                Console.Write("Product ID: ");
				bool notSucces = true;
				var _prodID = 0;
				while (notSucces)
				{
					if (int.TryParse(Console.ReadLine(), out _prodID))
						notSucces = false;
					else
						Console.WriteLine("input data in range 1000 - 9999");

				}				
				product.ProdId = _prodID;              

                var results = new List<ValidationResult>();
				var context = new ValidationContext(product)
				{
					MemberName = "ProdId"
				};
				if (!(IsValid = Validator.TryValidateProperty(product.ProdId, context, results)))
                {
                    foreach (ValidationResult error in results)
                    {
                        Console.WriteLine($"{error.MemberNames.FirstOrDefault()} : {error.ErrorMessage}");
                    }
                }
            } while (!IsValid);

			
			do
			{
				Console.Write("Category ID: ");
				bool notSucces = true;
				var _categID = 0;
				while (notSucces)
				{
					if (int.TryParse(Console.ReadLine(), out _categID))
						notSucces = false;
					else
						Console.WriteLine("input data in range 1 - 10");

				}
				product.CategoryId = _categID;

				var results = new List<ValidationResult>();
				var context = new ValidationContext(product)
				{
					MemberName = "CategoryId"
				};
				if (!(IsValid = Validator.TryValidateProperty(product.CategoryId, context, results)))
				{
					foreach (ValidationResult error in results)
					{
						Console.WriteLine($"{error.MemberNames.FirstOrDefault()} : {error.ErrorMessage}");
					}
				}
			} while (!IsValid);


			
            //var _categID = int.Parse(Console.ReadLine());



            Console.Write("Product name: ");
            var _prodName = Console.ReadLine();

            Console.Write("Date of produce: ");
            var testDate = Console.ReadLine();
            var _date = Convert.ToDateTime(testDate);

            Console.Write("Product price: ");
            var _prodPrice = int.Parse(Console.ReadLine());

            Console.Write("Produce country: ");
            var _prodCounty = Console.ReadLine();

            Console.Write("Produce discount: ");
            var _prDisc = Console.ReadLine();

            Product p = new Product();

            


            List<Product> products1 = new List<Product>()
            {
                new Product {ProdId = 1, CategoryId = 1, Name = "Pneumoaxe", Date = new DateTime(2014, 05, 08), Price = 850, ProdCountry = "Poland"},
                new Product {ProdId = 2, CategoryId = 2, Name = "Cream", Date = new DateTime(2015, 01, 18), Price = 85, ProdCountry = "Greece"},
                new Product {ProdId = 3, CategoryId = 3, Name = "Science", Date = new DateTime(2010, 12, 12), Price = 155, ProdCountry = "USA" },
                new Product {ProdId = 4, CategoryId = 4, Name = "siting sofa", Date = new DateTime(2013, 02, 13), Price = 745, ProdCountry = "Hungary"},
                new Product {ProdId = 5, CategoryId = 5, Name = "Horizont bar", Date = new DateTime(2012, 03, 10), Price = 900, ProdCountry = "Ukraine"},

                new Product {ProdId = 6, CategoryId = 1, Name = "Electro saw", Date = new DateTime(2016, 07, 08), Price = 400, ProdCountry = "germany"},
                new Product {ProdId = 7, CategoryId = 2, Name = "Blood checker", Date = new DateTime(2015, 09, 19), Price = 285, ProdCountry = "Poland"},
                new Product {ProdId = 8, CategoryId = 3, Name = "Fantasy", Date = new DateTime(2011, 12, 12), Price = 120, ProdCountry = "Ukraine" },
                new Product {ProdId = 9, CategoryId = 4, Name = "Garden set", Date = new DateTime(2018, 02, 25), Price = 345, ProdCountry = "Ukraine"},
                new Product {ProdId = 10, CategoryId = 5, Name = "barbelbell", Date = new DateTime(2018, 01, 08), Price = 550, ProdCountry = "Poland"}
            };




        }
    }
}
