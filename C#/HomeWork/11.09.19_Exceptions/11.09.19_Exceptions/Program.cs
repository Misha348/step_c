﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace _11._09._19_Exceptions
{
    class WrongFormatException : FormatException
    {
        public void WrongFormatExceptionMessage()
        {
            Console.WriteLine("wrong date format.");
        }
    }

    public class Worker
    {
        public string Name { get; set; }
        public string Position { get; set; }
        public string hireingData;
		public string HireingData
        {
            get
            {
                return hireingData;
            }

            set
            {
                if (DateTime.TryParse(value, out DateTime dDate))
                {
                    String.Format("{0:d/mm/yyyy}", dDate);
                    hireingData = value;
                }
                else
                {
                    throw new WrongFormatException();
                }
            }
        }

        public Worker()
        {
            SetWorkersData(out string name, out string position, out string hireingData);
            Name = name;
            Position = position;
            HireingData = hireingData;
        }

        public void SetWorkersData(out string name, out string position, out string hireingData)
        {
            Console.Write("enter name: ");
            name = Console.ReadLine();

            Console.Write("enter position: ");
            position = Console.ReadLine();

            Console.Write("enter hired date [ format - dd.mm.yyyy]: ");
            hireingData = Console.ReadLine();
        }

        public void ShowWorker()
        {
            Console.WriteLine($"name: {Name}\n" +
                $"position: {Position}\n" +
                $"hired date: {hireingData}");
        }

        public override string ToString()
        {
        	return $"name: {Name}\n" +
        		$"position: {Position}\n" +
        		$"hired date: {hireingData}";
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Worker[] worker = new Worker[4];

            for (int i = 0; i < worker.Length; i++)
            {
                try
                {
                    worker[i] = new Worker();
                    Console.WriteLine();
                }
                catch (WrongFormatException ex)
                {
                    ex.WrongFormatExceptionMessage();
                    --i;
                }
            }


            Array.Sort(worker, delegate (Worker worker1, Worker worker2)
            {
                return worker1.Name.CompareTo(worker2.Name);
            });

            for (int i = 0; i < worker.Length; i++)
            {
                worker[i].ShowWorker();
                Console.WriteLine(new string('=', 25));
            }           

            DateTime dateNow = DateTime.Today;
            Console.WriteLine($"dateNow = {dateNow.ToShortDateString()}");
            Console.WriteLine("enter expierience years quantuty: ");
            int expYearsQuant = int.Parse(Console.ReadLine());

            for (int i = 0; i < worker.Length; i++)
            {
                DateTime date;
                bool success = DateTime.TryParseExact(worker[i].hireingData, "dd.MM.yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date);              
				if ((expYearsQuant - (dateNow.Year - date.Year) < 0))
 					 Console.WriteLine($"worker {worker[i].Name} has more experience than entered value.");			   
            }       
                 
        }
    }
}

/////////////////////////////////////
//DateTime dt = DateTime.ParseExact(worker[i].hireingData, "0:d/MM/yyyy", CultureInfo.InvariantCulture);     
//public void SetWorkersData(out string name, out string position, out DateTime userDate)
//{
//	Console.WriteLine("enter name: ");
//	name = Console.ReadLine();

//	Console.WriteLine("enter position: ");
//	position = Console.ReadLine();

//	var usCulture = new System.Globalization.CultureInfo("en-US");
//	Console.WriteLine($"enter hired date.  (format: {usCulture.DateTimeFormat.ShortDatePattern})");
//	string dateString = Console.ReadLine();
//	DateTime userDate;

//	if (DateTime.TryParse(dateString, usCulture.DateTimeFormat, System.Globalization.DateTimeStyles.None, out userDate))
//		Console.WriteLine("Valid date entered (short date format):" + userDate.ToShortDateString());

//Console.WriteLine("enter hired date: ");
//hireingData = new DateTime(int.Parse(Console.ReadLine()), int.Parse(Console.ReadLine()), int.Parse(Console.ReadLine()));
//}