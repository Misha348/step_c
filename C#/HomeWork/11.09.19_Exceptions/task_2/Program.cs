﻿using System;

namespace task_2
{

	class SimpleCalculator
	{
		public double A { get; set; }
		public double B { get; set; }
		private char mathOperator;
		public char MathOperator
		{
			get
			{
				return mathOperator;
			}
			set
			{
				if (value == '+' || value == '-' || value == '*' || value == '/')
					mathOperator = value;
				else
				throw new WrongOperatorFormatExeption();
				
			}
		}

		public SimpleCalculator(double a, char mOperator, double b)
		{
			A = a;
			B = b;
			MathOperator = mOperator;
		}

		public double Addition(double A, double B)
		{
			return  A + B;			
		}

		public double Substruct(double A, double B)
		{
			return A - B;
		}

		public double Multiply(double A, double B)
		{
			return A * B;
		}

		public double Dividion(double A, double B)
		{
			try
			{
				if (B == 0)
					throw new DivideByZeroException();
			}
			catch (DivideByZeroException ex)
			{
				Console.WriteLine(ex.Message);				
			}
			
			return A / B;
		}

		public double GetResult()
		{
			switch (MathOperator)
			{
				case '+': return Addition(A, B);  
				case '-': return Substruct(A, B);
				case '*': return Multiply(A, B);
				case '/': return Dividion(A, B);
				default: return 0;					
			}
		}
	}

	class Program
	{
		static void Main(string[] args)
		{
			var notSucces = true;

			
			double a = 0;
			while (notSucces)
			{
				Console.Write("enter digit: ");
				try
				{
					if (!double.TryParse(Console.ReadLine(), out a))
						throw new FormatException();
					else
						notSucces = false;
				}
				catch (FormatException ex)
				{
					Console.WriteLine(ex.Message);
				}
				
			}

			notSucces = true;
			char sb = ' ';					
			do
			{
				Console.Write("enter op [+, -, *, /,]: ");
				sb = char.Parse(Console.ReadLine());			
				if (sb != '+' && sb != '-' && sb != '*' && sb != '/')
				{
					Console.WriteLine("wrong operator format");
				}			
			} while( !(sb == '+' || sb == '-' || sb == '*' || sb == '/') );

			double b = 0;
			while (notSucces)
			{
				Console.Write("enter digit: ");
				try
				{
					if (!double.TryParse(Console.ReadLine(), out b))
						throw new FormatException();
					else
						notSucces = false;
				}
				catch (FormatException ex)
				{
					Console.WriteLine(ex.Message);
				}
			}		

			SimpleCalculator calc = new SimpleCalculator(a, sb, b);
			double res = calc.GetResult();
			Console.WriteLine($"result = {res}");

		}
	}
}
