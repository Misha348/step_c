﻿
//									===   DONT RUN  ===
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading;

//namespace task_1._1
//{
//	class WrongFormatException : FormatException
//	{
//		public void WrongFormatExceptionMessage()
//		{
//			Console.WriteLine("wrong date format.\n");
//		}
//	}

//	public class Worker
//	{
//		public string Name { get; set; }
//		public string Position { get; set; }
//		public int hireingYear;
//		public int HireingYear
//		{
//			get
//			{
//				return hireingYear;
//			}

//			set
//			{
//				if (value > currentYear || value < 2000)
//				{
//					throw new WrongFormatException();
//				}
//				else
//				{
//					hireingYear = value;
//				}
//			}
//		}
//		public static readonly int currentYear = 2019;

//		public Worker()
//		{
//			SetWorkersData(out string name, out string position, out int hireingYear);
//			Name = name;
//			Position = position;
//			HireingYear = hireingYear;
//		}

//		public void SetWorkersData(out string name, out string position, out int hireingYear)
//		{
//			Console.Write("enter name: ");
//			name = Console.ReadLine();

//			Console.Write("enter position: ");
//			position = Console.ReadLine();

//			Console.Write("enter hired year:  ");
//			if ( !int.TryParse(Console.ReadLine(), out hireingYear))
//			{
//				throw new WrongFormatException();
//			}
//		}

//		public void ShowWorker()
//		{
//			Console.WriteLine($"name: {Name}\n" +
//				$"position: {Position}\n" +
//				$"hired date: {HireingYear}");
//		}
//		//public override string ToString()
//		//{
//		//	return $"name: {Name}\n" +
//		//		$"position: {Position}\n" +
//		//		$"hired date: {hireingData}";
//		//}
//	}

//	class Program
//	{
//		static void Main(string[] args)
//		{
//			Worker[] worker = new Worker[2];

//			for (int i = 0; i < worker.Length; i++)
//			{
//				try
//				{
//					worker[i] = new Worker();
//					Console.WriteLine();
//				}
//				catch (WrongFormatException ex)
//				{
//					ex.WrongFormatExceptionMessage();
//					--i;
//				}
//			}

//			Array.Sort(worker, delegate (Worker worker1, Worker worker2)
//			{
//				return worker1.Name.CompareTo(worker2.Name);
//			});

//			for (int i = 0; i < worker.Length; i++)
//			{
//				worker[i].ShowWorker();
//				Console.WriteLine(new string('=', 25));
//			}

//			Console.WriteLine("enter expierience years value: ");
//			var expYearsQuant = int.Parse(Console.ReadLine());

//			for (int i = 0; i < worker.Length; i++)
//			{
//				if( (Worker.currentYear - worker[i].hireingYear) > expYearsQuant)
//					Console.WriteLine(worker[i].Name);
//			}
//		}
//	}
//}
