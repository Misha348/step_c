﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_3
{
	class Program
	{
		static void Main(string[] args)
		{
			Random rnd = new Random();
			int[,] arr = new int[5, 5];
			for (int i = 0; i < arr.GetUpperBound(0) + 1; i++)
			{
				for (int j = 0; j < arr.GetUpperBound(1)+1; j++)
				{
					arr[i, j] = rnd.Next(-100, 100);
				}
			}

			int maxEl = 0;
			int minEl = 0;
			for (int i = 0; i < arr.GetUpperBound(0) + 1; i++)
			{
				for (int j = 0; j < arr.GetUpperBound(1) + 1; j++)
				{
					maxEl = arr.Cast<int>().Min();
					minEl = arr.Cast<int>().Max();					
				}				
			}

			for (int i = 0; i < arr.GetUpperBound(0) + 1; i++)
			{
				for (int j = 0; j < arr.GetUpperBound(1) + 1; j++)
				{
					if (arr[i, j] == maxEl || arr[i, j] == minEl)
					{
						Console.ForegroundColor = ConsoleColor.DarkGreen;
						Console.Write($"{arr[i, j]} ");
						Console.ForegroundColor = ConsoleColor.White;

					}
					else
						Console.Write($"{arr[i, j]} ");
				}
				Console.WriteLine();
			}

			int[] arr1 = new int[25];
			int c = 0;
			for (int i = 0; i < 5; i++)
				for (int j = 0; j < 5; j++)
				{
					arr1[c] = arr[i, j];
					c++;
				}
			Console.WriteLine();			

			int maxIndex = Array.IndexOf(arr1, arr1.Max());
			int minIndex = Array.IndexOf(arr1, arr1.Min());

			for (int i = 0; i < arr1.Length; i++)
			{
				if (arr1[i] == arr1[minIndex] || arr1[i] == arr1[maxIndex])
				{
					Console.ForegroundColor = ConsoleColor.DarkGreen;
					Console.Write($"{arr1[i]} ");
					Console.ForegroundColor = ConsoleColor.White;
				}
				else
				Console.Write($"{arr1[i]} ");
			}				

			var sum = 0;
			//sum = (arr1.Where((elem, index) => (minIndex < maxIndex) ? index > minIndex && index < maxIndex : index > maxIndex && index < minIndex)).Sum();
			if (maxIndex < minIndex)
			{
				for (int i = maxIndex; i < (minIndex - 1); i++)				
					sum += arr1[i + 1];
				
			}
			else if (maxIndex > minIndex)
			{
				for (int i = minIndex; i < (maxIndex - 1); i++)				
					sum += arr1[i + 1];				
			}

			Console.WriteLine($"\n\nmin Indx = {minIndex}.\nmax Indx = {maxIndex}.");
			Console.WriteLine($"res = {sum}");
			//Console.WriteLine($"res = {sum}");
		}
	}
}
