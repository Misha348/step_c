﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_2
{
	class Program
	{
		static void Main(string[] args)
		{
			Console.WriteLine("Enter some sentece: ");
			var sentence = Console.ReadLine();

			var letters = 0;
			var words = 0;

			while (letters <= sentence.Length - 1)
			{
				if (sentence[letters] == ' ' || sentence[letters] == '\n' ||
					sentence[letters] == '\t' || sentence[letters] == '.') 
					words++;
				letters++;
			}
			Console.Write($"Total number of words in the string is : {words}\n");
		}
	}
}
