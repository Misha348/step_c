﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_1
{
	class Program
	{
		static bool CheckPalindrom(string string_1, string string_2)
		{
			var result = string_1 == string_2 ? true : false;
			return result;
		}

		static void Main(string[] args)
		{
			Console.WriteLine("Enter some line: ");
			var line_1 = Console.ReadLine();
			Console.WriteLine($"line = {line_1}");

			var line_2 = string.Concat(line_1.Reverse());
			var isPalindrom = CheckPalindrom(line_1, line_2);
			Console.WriteLine($"line: {(isPalindrom ? "is palindrom":"not palindrom")}");   
			
		}
	}
}
