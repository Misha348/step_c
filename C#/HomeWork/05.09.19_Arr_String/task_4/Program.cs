﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_4
{
	class Program
	{
		static void Main(string[] args)
		{
			Console.WriteLine("\tI ARR");
			Random rnd = new Random();
			var m = 9;
			int[] arr1 = new int[m];
			for (int i = 0; i < arr1.Length; i++)
			{
				arr1[i] = rnd.Next(-50, 50);
				Console.Write($"{arr1[i]} ");
			}
			Console.WriteLine();

			Console.WriteLine("\n\tII ARR");
			var n = 13;
			int[] arr2 = new int[n];
			for (int i = 0; i < arr2.Length; i++)
			{
				arr2[i] = rnd.Next(-50, 50);
				Console.Write($"{arr2[i]} ");
			}
			Console.WriteLine();

			Console.WriteLine("\n\tIII ARR(without duplets)");
			//int[] arr3 = arr1.Concat(arr2).ToArray();
			int[] arr3 = arr1.Union(arr2).ToArray();
			Console.WriteLine();

			for (int i = 0; i < arr3.Length; i++)
			{
				Console.Write($"{arr3[i]} ");
			}



			//string sentence1 = "som";
			//char[] charArr1 = sentence1.ToCharArray();
			//foreach (char ch in charArr1)
			//{
			//	Console.Write($"{ch}");
			//}
			//Console.WriteLine();

			//string sentence2 = "sombodbok";
			//char[] charArr2 = sentence2.ToCharArray();
			//foreach (char ch in charArr2)
			//{
			//	Console.Write($"{ch}");
			//}
			//Console.WriteLine();			

			//char[] charArr3 = charArr1.Concat(charArr2).ToArray();
			//char[] charArr4 = charArr3.Union(charArr3).ToArray();
			//foreach (char ch in charArr4)
			//{
			//	Console.Write($"{ch}");
			//}
			//Console.WriteLine();


		}
	}
}
