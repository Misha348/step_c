﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/// <summary>
/// /////////////////////////////////////////////////
/// </summary>
namespace Task_4
{
	public class Card
	{
		public Suit _Suit { get; set; }
		public Rank _Rank { get; set; }

		public Card() { }
		public Card(Suit suit, Rank rank)
		{
			_Suit = suit;
			_Rank = rank;
		}

		public override string ToString()
		{
			if (_Suit == Suit.Club)
				Console.ForegroundColor = ConsoleColor.DarkGray;
			else if (_Suit == Suit.Diamond)
				Console.ForegroundColor = ConsoleColor.DarkRed;
			else if (_Suit == Suit.Heart)
				Console.ForegroundColor = ConsoleColor.Red;
			else if (_Suit == Suit.Spade)
				Console.ForegroundColor = ConsoleColor.Gray;
			if (_Suit == Suit.Club)
				return $" << \u2663 {_Rank} >>";
			if (_Suit == Suit.Diamond)
				return $" << \u2666 {_Rank} >>";
			if (_Suit == Suit.Heart)
				return $" << \u2665 {_Rank} >>";
			if (_Suit == Suit.Spade)
				return $" << \u2660 {_Rank} >>";
			else
				return $" << error >>";
		}
	}


	public class Player
	{
		public int PlayerNumb { get; set; }
		Queue<Card> cardSet = new Queue<Card>();

		public Player(int nplNumb, params Card[] cardArray)
		{
			PlayerNumb = nplNumb;
			for (int i = 0; i < cardArray.Length; i++)
			{
				cardSet.Enqueue(cardArray[i]);
			}
		}

		public void ShowPlayercardset()
		{
			Console.WriteLine($"Player {PlayerNumb} has:\n");
			foreach (Card card in cardSet)
			{
				card = cardSet.Peek();
				Console.WriteLine(card.ToString());
			}
		}
	}

	class Program
	{
		static void Main(string[] args)
		{
			Console.OutputEncoding = System.Text.Encoding.UTF8;

			List<Card> cards = new List<Card>();

			for (int suit = (int)Suit.Club; suit <= (int)Suit.Spade; suit++)
			{
				for (int rank = (int)Rank.Two; rank <= (int)Rank.Jouker; rank++)
				{
					Card newCard = new Card((Suit)suit, (Rank)rank);
					cards.Add(newCard);
				}
			}

			Console.WriteLine("\tDECK OF CARDS");
			Console.WriteLine(new string('=', 20));
			foreach (Card card in cards)
			{
				Console.WriteLine(card.ToString());
			}

			List<Card> tmpdeck = new List<Card>();
			for (int i = 0; i < cards.Count; i++)
			{
				tmpdeck.Add(cards[i]);
			}
			for (int i = 0; i < cards.Count; i++)
			{
				Card tmpCard = tmpdeck[i];
				Random rnd = new Random();
				int r = rnd.Next(i, cards.Count);
				tmpdeck[i] = tmpdeck[r];
				tmpdeck[r] = tmpCard;
			}
			Console.ForegroundColor = ConsoleColor.White;
			Console.WriteLine("\n\t\tSHUFFLE");
			Console.WriteLine(new string('=', 30));
			Console.WriteLine("\n");

			Queue<Card> mixedDeck = new Queue<Card>();
			foreach (Card card in tmpdeck)
			{
				mixedDeck.Enqueue(card);
			}

			foreach (Card card in tmpdeck)
			{
				Console.WriteLine(card.ToString());
			}

			Player[] players = new Player[4];
			Card[] cardSet = new Card[6];

			for (int j = 0; j < players.Length; j++)
			{
				for (int i = 0; i < cardSet.Length; i++)
				{
					cardSet[i] = mixedDeck.Dequeue();
				}
				players[j] = new Player((j + 1), cardSet);
			}

			foreach (Player player in players)
			{
				Console.ForegroundColor = ConsoleColor.Yellow;
				Console.WriteLine();
				player.ShowPlayercardset();
				Console.WriteLine(new string('=', 14));
			}



		}
	}
}
