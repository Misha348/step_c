﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_5
{
	public class Ring
	{
		List<char> TowerRing = new List<char>();
		public static char sb = '#';
		public int RingIndex { get; set; }

		public static int ringSize_1 = 7;
		public static int ringSize_2 = 5;
		public static int ringSize_3 = 3;
		public static int ringSize_4 = 1;

		public Ring(int ringIdex)
		{
			RingIndex = ringIdex;
			switch (ringIdex)
			{
				case 0:
					for (int i = 0; i < ringSize_1; i++)
						TowerRing.Add(sb);						
					break;
				case 1:					
					for (int i = 0; i < ringSize_2; i++)
						TowerRing.Add(sb);
					break;
				case 2:					
					for (int i = 0; i < ringSize_3; i++)
						TowerRing.Add(sb);
					break;
				case 3:					
					for (int i = 0; i < ringSize_4; i++)
						TowerRing.Add(sb);
					break;
			}
		}

		public int GetRingSize()
		{
			var rSize = 0;
			rSize = TowerRing.Count;

			return rSize;
		}

		public void ShowRing()
		{			
			for (int i = 0; i < TowerRing.Count; i++)
			{						
					Console.Write(TowerRing[i]);					
			}				
		}		
	}

	public class Tower
	{
		public Stack<Ring> tower = new Stack<Ring>();

		public Tower()	{}

		public Tower(int ringQuant)
		{			
			for (int i = 0; i < ringQuant; i++)
			{
				Ring ring = new Ring(i);
				tower.Push(ring);
			}
		}

		public int GetRingsQuant()
		{
			return tower.Count;
		}

		public void ShowTower()
		{
			if (tower.Count == 0)
			{
				Console.ForegroundColor = ConsoleColor.White;
				Console.WriteLine("\t--");
			}
				
			else
			{
				foreach (Ring ring in tower)
				{
					switch (ring.RingIndex)
					{
						case 0:
							Console.ForegroundColor = ConsoleColor.Red;
							Console.Write("\t"); ring.ShowRing(); Console.WriteLine(); break;
						case 1:
							Console.ForegroundColor = ConsoleColor.Magenta;
							Console.Write("\t "); ring.ShowRing(); Console.WriteLine(); break;
						case 2:
							Console.ForegroundColor = ConsoleColor.Yellow;
							Console.Write("\t  "); ring.ShowRing(); Console.WriteLine(); break;
						case 3:
							Console.ForegroundColor = ConsoleColor.Gray;
							Console.Write("\t   "); ring.ShowRing(); Console.WriteLine(); break;
					}						
				}
				Console.WriteLine();

			}
			
		}
	}

	class Program
	{
		public static void ShowTowers(ref Tower _tover1, ref Tower _tover2, ref Tower _tover3)
		{
			Tower tower1 = _tover1;
			Console.ForegroundColor = ConsoleColor.White;
			Console.WriteLine("FIRST TOWER\n");
			tower1.ShowTower();

			Tower tower2 = _tover2;
			Console.ForegroundColor = ConsoleColor.White;
			Console.WriteLine("SECOND TOWER\n");
			tower2.ShowTower();

			Tower tower3 = _tover3;
			Console.ForegroundColor = ConsoleColor.White;
			Console.WriteLine("THIRD TOWER\n");
			tower3.ShowTower();

			Console.ReadKey();
			Console.Clear();
		}

		static void Main(string[] args)
		{			
			Ring ring_1;
			Ring ring_2;			

			Tower tower1 = new Tower(4);			
			Tower tower2 = new Tower();			
			Tower tower3 = new Tower();

			ShowTowers(ref tower1, ref tower2, ref tower3);
			{
				//Console.ForegroundColor = ConsoleColor.White;
				//Console.WriteLine("FIRST TOWER\n");
				//tower1.ShowTower();

				//Console.ForegroundColor = ConsoleColor.White;
				//Console.WriteLine("SECOND TOWER\n");
				//tower2.ShowTower();

				//Console.ForegroundColor = ConsoleColor.White;
				//Console.WriteLine("THIRD TOWER\n");
				//tower3.ShowTower();

				//Console.ReadKey();
				//Console.Clear();
			}
			if (tower1.GetRingsQuant() > 0)
			{
				ring_1 = tower1.tower.Pop();
				if (tower2.tower.Count == 0)
					tower2.tower.Push(ring_1);
			}
			ShowTowers(ref tower1, ref tower2, ref tower3);

			if (tower1.GetRingsQuant() > 0)
			{
				ring_2 = tower1.tower.Pop();
				if (tower3.tower.Count == 0)
					tower3.tower.Push(ring_2);
			}
			ShowTowers(ref tower1, ref tower2, ref tower3);		

			if (tower2.GetRingsQuant() > 0)
			{
				ring_1 = tower2.tower.Pop();
				ring_2 = tower3.tower.Peek();
				if (ring_1.GetRingSize() < ring_2.GetRingSize() )
				{
					tower3.tower.Push(ring_1);
				}
			}
			ShowTowers(ref tower1, ref tower2, ref tower3);

			if (tower1.GetRingsQuant() > 0)
			{
				ring_1 = tower1.tower.Pop();
				if (!tower2.tower.Any())
					tower2.tower.Push(ring_1);
			}
			ShowTowers(ref tower1, ref tower2, ref tower3);

			if (tower3.GetRingsQuant() > 0)
			{
				ring_1 = tower3.tower.Pop();
				ring_2 = tower1.tower.Peek();
				if (ring_1.GetRingSize() < ring_2.GetRingSize())
				{
					tower1.tower.Push(ring_1);
				}
			}
			ShowTowers(ref tower1, ref tower2, ref tower3);

			if (tower3.GetRingsQuant() > 0)
			{
				ring_1 = tower3.tower.Pop();
				ring_2 = tower2.tower.Peek();
				if (ring_1.GetRingSize() < ring_2.GetRingSize())
				{
					tower2.tower.Push(ring_1);
				}
			}
			ShowTowers(ref tower1, ref tower2, ref tower3);

			if (tower1.GetRingsQuant() > 0)
			{
				ring_1 = tower1.tower.Pop();
				ring_2 = tower2.tower.Peek();
				if (ring_1.GetRingSize() < ring_2.GetRingSize())
				{
					tower2.tower.Push(ring_1);
				}
			}
			ShowTowers(ref tower1, ref tower2, ref tower3);

			if (tower1.GetRingsQuant() > 0)
			{
				ring_1 = tower1.tower.Pop();
				if (!tower3.tower.Any())
					tower3.tower.Push(ring_1);
			}		
			ShowTowers(ref tower1, ref tower2, ref tower3);

			if (tower2.GetRingsQuant() > 0)
			{
				ring_1 = tower2.tower.Pop();
				ring_2 = tower3.tower.Peek();
				if (ring_1.GetRingSize() < ring_2.GetRingSize())
				{
					tower3.tower.Push(ring_1);
				}
			}
			ShowTowers(ref tower1, ref tower2, ref tower3);

			if (tower2.GetRingsQuant() > 0)
			{
				ring_1 = tower2.tower.Pop();
				if (!tower1.tower.Any())
					tower1.tower.Push(ring_1);
			}
			ShowTowers(ref tower1, ref tower2, ref tower3);

			if (tower3.GetRingsQuant() > 0)
			{
				ring_1 = tower3.tower.Pop();
				ring_2 = tower1.tower.Peek();
				if (ring_1.GetRingSize() < ring_2.GetRingSize())
				{
					tower1.tower.Push(ring_1);
				}
			}
			ShowTowers(ref tower1, ref tower2, ref tower3);

			if (tower2.GetRingsQuant() > 0)
			{
				ring_1 = tower2.tower.Pop();
				ring_2 = tower3.tower.Peek();
				if (ring_1.GetRingSize() < ring_2.GetRingSize())
				{
					tower3.tower.Push(ring_1);
				}
			}
			ShowTowers(ref tower1, ref tower2, ref tower3);

			if (tower1.GetRingsQuant() > 0)
			{
				ring_1 = tower1.tower.Pop();
				if (!tower2.tower.Any())
					tower2.tower.Push(ring_1);
			}
			ShowTowers(ref tower1, ref tower2, ref tower3);

			if (tower1.GetRingsQuant() > 0)
			{
				ring_1 = tower1.tower.Pop();
				ring_2 = tower3.tower.Peek();
				if (ring_1.GetRingSize() < ring_2.GetRingSize())
				{
					tower3.tower.Push(ring_1);
				}
			}
			ShowTowers(ref tower1, ref tower2, ref tower3);

			if (tower2.GetRingsQuant() > 0)
			{
				ring_1 = tower2.tower.Pop();
				ring_2 = tower3.tower.Peek();
				if (ring_1.GetRingSize() < ring_2.GetRingSize())
				{
					tower3.tower.Push(ring_1);
				}
			}
			ShowTowers(ref tower1, ref tower2, ref tower3);
			Console.WriteLine("\n\tFINISH");

		}
	}
}
