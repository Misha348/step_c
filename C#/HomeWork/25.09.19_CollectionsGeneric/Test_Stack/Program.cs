﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test_Stack
{

	public class Ring
	{	
		List<char> TowerRing = new List<char>();
			

		public static char sb = '#';
		public int RingIndex { get; set; }
		public int RingQuant { get; set; }
		public int RingSize { get; set; }

		public Ring() { }

		public Ring(int ringIndex, int ringQuant, int ringSise)
		{
			RingIndex = ringIndex;
			RingQuant = ringQuant;

			if (ringIndex == 1)
			{
				for (int j = ringIndex; j <= ringIndex; j++)
				{
					for (int i = 0; i < (ringQuant + (ringQuant - 1)); i++)
					{
						TowerRing.Add(sb);
					}					
				}					
			}
			else
			{
				RingSize = ringSise;
				for (int i = 0; i < ringSise; i++)
				{
					TowerRing.Add(sb);
				}
			}	

		}		

		public int GetRingSize()
		{
			var rSize = 0;
			rSize = TowerRing.Count;

			return rSize;
		}

		public void ShowRing()
		{
			for (int i = 0; i < TowerRing.Count; i++)
			{
				Console.Write(TowerRing[i]);
			}
		}
	}

	public class Tower
	{
		public Stack<Ring> tower = new Stack<Ring>();
		public int RingQuantity { get; set; }
		
		public Tower() { }

		public Tower(int ringQuant)
		{
			RingQuantity = ringQuant;
			for (int i = 1; i <= ringQuant; i++)
			{
				if (!tower.Any())
				{
					Ring ring = new Ring(i, ringQuant, 0);
					tower.Push(ring);
				}
				else
				{
					Ring tmpRing = tower.Peek();
					var ringSise = tmpRing.GetRingSize();
					Ring newRing = new Ring(i, ringQuant, ringSise - 2);
					tower.Push(newRing);
				}
			}
		}

		public int GetRingsQuant()
		{			
			return tower.Count;
		}

		public void ShowTower()
		{
			if (tower.Count == 0)
			{
				Console.ForegroundColor = ConsoleColor.White;
				Console.WriteLine("\t--\n");
			}
			else
			{
				foreach (Ring ring in tower)
				{
					switch (ring.RingIndex)
					{
						case 1:
							Console.ForegroundColor = ConsoleColor.Red;
							Console.Write("\t"); ring.ShowRing(); Console.WriteLine(); break;
						case 2:
							Console.ForegroundColor = ConsoleColor.Magenta;
							Console.Write("\t "); ring.ShowRing(); Console.WriteLine(); break;
						case 3:
							Console.ForegroundColor = ConsoleColor.Yellow;
							Console.Write("\t  "); ring.ShowRing(); Console.WriteLine(); break;
						case 4:
							Console.ForegroundColor = ConsoleColor.Gray;
							Console.Write("\t   "); ring.ShowRing(); Console.WriteLine(); break;
						//case 5:
						//	Console.ForegroundColor = ConsoleColor.Cyan;
						//	Console.Write("\t    "); ring.ShowRing(); Console.WriteLine(); break;
						//case 6:
						//	Console.ForegroundColor = ConsoleColor.Blue;
						//	Console.Write("\t     "); ring.ShowRing(); Console.WriteLine(); break;
						//case 7:
						//	Console.ForegroundColor = ConsoleColor.Green;
						//	Console.Write("\t      "); ring.ShowRing(); Console.WriteLine(); break;
						//case 8:
						//	Console.ForegroundColor = ConsoleColor.White;
						//	Console.Write("\t       "); ring.ShowRing(); Console.WriteLine(); break;
						//case 9:
						//	Console.ForegroundColor = ConsoleColor.DarkMagenta;
						//	Console.Write("\t        "); ring.ShowRing(); Console.WriteLine(); break;
						//case 10:
						//	Console.ForegroundColor = ConsoleColor.Yellow;
						//	Console.Write("\t         "); ring.ShowRing(); Console.WriteLine(); break;
					}
				}

				Console.WriteLine();

			}			
		}
	}

	class Program
	{
		public static void ShowTowers(ref Tower _tover1, ref Tower _tover2, ref Tower _tover3)
		{
			Tower tower1 = _tover1;
			Console.ForegroundColor = ConsoleColor.White;
			Console.WriteLine("FIRST TOWER\n");
			tower1.ShowTower();

			Tower tower2 = _tover2;
			Console.ForegroundColor = ConsoleColor.White;
			Console.WriteLine("SECOND TOWER\n");
			tower2.ShowTower();

			Tower tower3 = _tover3;
			Console.ForegroundColor = ConsoleColor.White;
			Console.WriteLine("THIRD TOWER\n");
			tower3.ShowTower();

			Console.ReadKey();
			Console.Clear();
		}


		//static void MoveRing(ref Tower tower_1, ref Tower tower_2, ref Tower tower_3)
		//{
		//	Ring ring_1;
		//	Ring ring_2;
		//	Ring tmpRing;

		//	if (tower_1.GetRingsQuant() > 0)
		//	{

		//		if (!tower_2.tower.Any())
		//		{
		//			ring_1 = tower_1.tower.Pop();
		//			tower_2.tower.Push(ring_1);     //1	4
		//		}
		//		else if (!tower_3.tower.Any())
		//		{
		//			ring_1 = tower_1.tower.Pop();
		//			tower_3.tower.Push(ring_1);     //2
		//		}
		//		else if (tower_2.tower.Any() && tower_3.tower.Any())
		//		{
		//			ring_1 = tower_2.tower.Peek();
		//			ring_2 = tower_3.tower.Peek();
		//			if (ring_1.RingSize < ring_2.RingSize)
		//			{
		//				tmpRing = tower_2.tower.Pop();
		//				tower_3.tower.Push(tmpRing);    //3
		//			}
		//		}
		//		else if (tower_1.tower.Any() || !tower_1.tower.Any())
		//		{
		//			if (tower_1.tower.Any())
		//			{
		//				ring_1 = tower_1.tower.Peek();
		//				ring_2 = tower_3.tower.Peek();
		//				if (ring_1.RingSize > ring_2.RingSize)
		//				{
		//					tmpRing = tower_3.tower.Pop();
		//					tower_1.tower.Push(tmpRing);    //3
		//				}
		//			}
		//			else if (!tower_1.tower.Any())
		//			{
		//				tmpRing = tower_3.tower.Pop();
		//				tower_1.tower.Push(tmpRing);
		//			}
		//		}
		//	}			
		//	ShowTowers(ref tower_1, ref tower_2, ref tower_3);
		//	MoveRing(ref tower_1, ref tower_2, ref tower_3);
		//}
				

			// є такий алгоритм по перекиданню кілець

			// яким чином мої об"єкти [Tower] з їх властивостями й методами (+ методи стеку)
			// імплементувати в цю рекурсію ???
		static void MoveRing(int rQuant, ref Tower tower_1, ref Tower tower_2, ref Tower tower_3)
		{
			if (rQuant > 1)
				MoveRing(rQuant - 1, ref tower_1, ref tower_3, ref tower_2);    //1
			ShowTowers(ref tower_1, ref tower_2, ref tower_3);   //2
			if (rQuant > 1)
				MoveRing(rQuant - 1, ref tower_2, ref tower_1, ref tower_3);    //3
		}


		static void Main(string[] args)
		{
			var rQuant = 0;
			Console.Write("quantity of rings: ");
			rQuant = Convert.ToInt16(Console.ReadLine());
			Console.Clear();

			Tower tower1 = new Tower(rQuant);
			Tower tower2 = new Tower();
			Tower tower3 = new Tower();

			ShowTowers(ref tower1, ref tower2, ref tower3);
			MoveRing(rQuant, ref tower1, ref tower3, ref tower2);


		}
	}
}
