﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_2
{
	class Program
	{
		public static string RandomString()
		{
			Random rnd = new Random();
			var size = rnd.Next(8, 30);

			StringBuilder bld = new StringBuilder();
			Random rand = new Random();
			char sb;
			for (int i = 0; i < size; i++)
			{
				sb = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * rand.NextDouble() + 65)));
				bld.Append(sb);
			}
			return bld.ToString().ToLower();
		}

		static void Main(string[] args)
		{
			List<string> strings = new List<string>();

			var size = 3;
			string[] str1 = new string[size];
			for (int i = 0; i < size; i++)
			{
				Console.WriteLine($"enter {i+1} line: ");
				str1[i] = Console.ReadLine();
				strings.Add(str1[i]);
				Console.Clear();
			}

			foreach (var str in strings)
			{				
				Console.WriteLine($"{str} LENGTH = {str.Length}");
			}
			Console.WriteLine("\n\n");

			
			var maxLength = 0;
			for (int i = 0; i < strings.Count; i++)
			{
				if (i == 1)
				{
					if (strings[i].Length > strings[i - 1].Length)
						maxLength = strings[i].Length;
					else
						maxLength = strings[i - 1].Length;
				}				
			}

			List<string> strings1 = new List<string>();
			foreach (var str in strings)
			{
				if (str.Length == maxLength)
					strings1.Add(str);
			}
			strings1.Sort();
			for (int i = 0; i < strings1.Count; i++)
			{
				Console.WriteLine($"{ strings1[i]}  LENGTH = {strings1[i].Length}");
				break;
			}
		}
	}
}
