﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test_Deck
{
	public enum Suit
	{
		Club,
		Diamond,
		Heart,
		Spade,
	}

	public enum Rank
	{
		Two,
		Three,
		Four,
		Five,
		Six,
		Seven,
		Eight,
		Nine,
		Ten,
		J,
		Q,
		K,
		A,
		Jouker,
	}
}
