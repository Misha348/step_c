﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace Test_Deck
{
	public class Card
	{
		public Suit _Suit { get; set; }
		public Rank _Rank { get; set; }

		public Card() { }
		public Card(Suit suit, Rank rank)
		{
			_Suit = suit;
			_Rank = rank;
		}

		public override string ToString()
		{
			if (_Suit == Suit.Club)
			{
				Console.ForegroundColor = ConsoleColor.DarkGray;
				return $" << \u2663 {_Rank} >>";
			}
			if (_Suit == Suit.Diamond)
			{
				Console.ForegroundColor = ConsoleColor.DarkRed;
				return $" << \u2666 {_Rank} >>";
			}
			if (_Suit == Suit.Heart)
			{
				Console.ForegroundColor = ConsoleColor.Red;
				return $" << \u2665 {_Rank} >>";
			}
			if (_Suit == Suit.Spade)
			{
				Console.ForegroundColor = ConsoleColor.Gray;
				return $" << \u2660 {_Rank} >>";
			}
			else
				return $" << error >>";
		}
	}

	public class Deck
	{
		public Queue<Card> cards = new Queue<Card>();	

		public Deck()
		{			
			for (int suit = (int)Suit.Club; suit <= (int)Suit.Spade; suit++)
			{
				for (int rank = (int)Rank.Two; rank <= (int)Rank.Jouker; rank++)
				{
					Card newCard = new Card((Suit)suit, (Rank)rank);
					cards.Enqueue(newCard);
				}
			}
		}		

		public void  ShowDeck()
		{
			foreach (Card card in cards)
			{				
				Console.WriteLine(card.ToString());
				Thread.Sleep(25);
			}			
		}	

		public void ShufleDeck()
		{
			List<Card> _cards = new List<Card>();
			for (int i = 0; i < cards.Count; i++)
			{			
				Card tmpCard = cards.Dequeue();
				_cards.Add(tmpCard);
			}
			for (int i = 0; i < _cards.Count; i++)
			{
				Card tmpCard = _cards[i];
				Random rnd = new Random();
				int r = rnd.Next(i, _cards.Count);
				_cards[i] = _cards[r];
				_cards[r] = tmpCard;
			}
			foreach (Card card in _cards)
			{
				cards.Enqueue(card);
			}
		}

		public Card[] GetCardSet()
		{
			Card[] Cardset;
			Cardset = new Card[6];
			for (int i = 0; i < Cardset.Length; i++)
			{
				Cardset[i] = cards.Dequeue();
			}
			return Cardset;
		}
	}

	public class Player
	{
		public int PlayerNumb { get; set; }
		Card[] cardSet = new Card[6];

		public Player(int nplNumb, ref Deck _deck)
		{
			PlayerNumb = nplNumb;
			cardSet = _deck.GetCardSet();
		}

		public void ShowPlayercardset()
		{
			Console.WriteLine($"Player {PlayerNumb} has:\n");
			foreach (Card card in cardSet)
			{				
				Console.WriteLine(card.ToString());
				Thread.Sleep(25);
			}
		}
	}

	class Program
	{		
		static void Main(string[] args)
		{
			Console.OutputEncoding = System.Text.Encoding.UTF8;
			Deck deck = new Deck();

			Console.WriteLine("\tDECK OF CARDS");
			Console.WriteLine(new string('=', 20));
			deck.ShowDeck();

			Console.ForegroundColor = ConsoleColor.White;
			Console.WriteLine("\n\t\tSHUFFLE");
			Console.WriteLine(new string('=', 30));
			Console.WriteLine("\n");

			var sh = 3;
			do {  deck.ShufleDeck(); sh--; }
			while (sh > 0);			
			deck.ShowDeck();

			Player[] players = new Player[4];
			for (int i = 0; i < players.Length; i++)
			{
				players[i] = new Player((i + 1), ref deck);
			}

			foreach (Player player in players)
			{
				Console.ForegroundColor = ConsoleColor.Yellow;
				Console.WriteLine();
				player.ShowPlayercardset();
				Console.WriteLine(new string('=', 14));
			}
			Console.ForegroundColor = ConsoleColor.White;
			

		}
	}
}
