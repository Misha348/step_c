﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_3
{
	class Program
	{
		static void Main(string[] args)
		{
			ArrayList list = new ArrayList();
			list.Add(25);
			list.Add(-4);
			list.Add(3.56);
			list.Add(-4.65);
			list.Add(true);
			list.Add(false);

			List<int> integers = new List<int>();
			List<double> doubles = new List<double>();
			List<bool> booleans = new List<bool>();

			foreach (var obj in list)
			{
				if (obj is int)
					integers.Add((int)obj);
				else if (obj is double)
					doubles.Add((double)obj);
				else if (obj is bool)
					booleans.Add((bool)obj);
			}

			foreach (var obj in integers)
			{
				Console.WriteLine(obj);
			}
			Console.WriteLine();

			foreach (var obj in doubles)
			{
				Console.WriteLine(obj);
			}
			Console.WriteLine();

			foreach (var obj in booleans)
			{
				Console.WriteLine(obj.ToString());
			}
			Console.WriteLine();
		}
	}
}
