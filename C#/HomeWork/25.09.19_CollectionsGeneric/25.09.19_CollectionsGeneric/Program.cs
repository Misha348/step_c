﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _25._09._19_CollectionsGeneric
{
	public class Contact
	{
		public string Name { get; set; }
		public string Surname { get; set; }


		public Contact()
		{
			SetAbonentData(out string name, out string surname);
			Name = name;
			Surname = surname;
		}
		public Contact( string name, string surname)
		{
			Name = name;
			Surname = surname;
		}	

		public void SetAbonentData(out string name, out string surname)
		{
			Console.Write("name: ");
			name = Console.ReadLine();

			Console.Write("surname: ");
			surname = Console.ReadLine();
		}

		public override string ToString()
		{
			return $"[ name: {Name}\tsurname: {Surname} ]";
		}
	}

	class Program
	{
		static void Main(string[] args)
		{
			string tel;
			Dictionary<string, Contact> phoneBook = new Dictionary<string, Contact>();

			bool flag = false;
			while (!flag)
			{
				Console.WriteLine("  add new cont? [ y ] - yes.\t[ n ] - no");
				string myChoise = Console.ReadLine();
				if (myChoise == "y")
				{
					Console.Write("tel: ");					
					phoneBook.Add(tel = Console.ReadLine(), new Contact());
					Console.Clear();
				}
				if (myChoise == "n")
					break;
			}		

			foreach (var eachContact in phoneBook)
			{
				Console.WriteLine($"tel: {eachContact.Key}  -  {eachContact.Value}");
			}
			Console.WriteLine(new string('=', 50));
           
			Contact testContact = new Contact("Bill", "Billson");	
			foreach (KeyValuePair<string, Contact> pair in phoneBook)
			{
				if (pair.Value.Name == testContact.Name)
				{                    
					Console.WriteLine($" {pair.Key} - {pair.Value.Name} {pair.Value.Surname}");
					break;
				}
			}
			Console.WriteLine(new string('=', 30));

			Console.Write("enter tel for searching: ");
			string searchTelContact = Console.ReadLine();
			foreach (var eachContact in phoneBook)
			{
				if (phoneBook.ContainsKey(searchTelContact))
				{
					Console.WriteLine(phoneBook[searchTelContact]);
					break;
				}					
			}
            Console.WriteLine(new string('=', 30));

            Console.Write("enter tel for chanching: ");
            string TelContact = Console.ReadLine();
            foreach (var eachContact in phoneBook)
            {
                if (phoneBook.ContainsKey(searchTelContact))
                {
                    Contact testContact1 = new Contact();
                    phoneBook[TelContact] = testContact1;
                    break;
                }
            }
            Console.WriteLine(new string('=', 30));
            foreach (var eachContact in phoneBook)
            {
                Console.WriteLine($"tel: {eachContact.Key}  -  {eachContact.Value}");
            }
            Console.WriteLine(new string('=', 50));


            Console.Write("enter tel for deleting: ");
            string TelContact2 = Console.ReadLine();
            foreach (var eachContact in phoneBook)
            {
                if (phoneBook.ContainsKey(TelContact2))
                {
                    phoneBook.Remove(TelContact2);
                    break;
                }
            }
            Console.WriteLine(new string('=', 30));
            foreach (var eachContact in phoneBook)
            {
                Console.WriteLine($"tel: {eachContact.Key}  -  {eachContact.Value}");
            }
            Console.WriteLine(new string('=', 50));
        }
	}
}
