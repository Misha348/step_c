﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calorie_Tracker
{
	public class AccountMenuItems
	{
		readonly List<string> accountMenuItems = new List<string>()
		{
			"CREATE ACCOUNT",
			"DELETE ACCOUNT",
			"CHECK ALL ACCOUNTS"
		};

		public void AccountMenuManipulation()
		{
			MenuDraughter menuDraughter = new MenuDraughter();

			Console.CursorVisible = false;

			while (true)
			{					
				string selectedMenuItem = menuDraughter.DrawMenu(accountMenuItems);

				if (selectedMenuItem == "CREATE ACCOUNT")
				{
					AccountMenuItemCreateAccount();
				}
				else if (selectedMenuItem == "DELETE ACCOUNT")
				{
				}
				else if (selectedMenuItem == "CHECK ALL ACCOUNTS")
				{
				}
			}			
		}

		public void AccountMenuItemCreateAccount()
		{
			AccountCreator accountCreator = new AccountCreator();
			AccountDataSaver accountDataSaver = new AccountDataSaver();

			Account newAccount = accountCreator.GetNewAccount();
			accountDataSaver.SaveAccountToFile(newAccount);
		}

	}
}
