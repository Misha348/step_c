﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calorie_Tracker
{
	class AppMenu
	{		
		readonly List<string> menuItem = new List<string>()
		{
			"ACCOUNT'S SECTION",
			"SPORT PROGRRAM'S SECTION"
		};

		public void MenuManipulation()
		{
			MenuDraughter menuDraughter = new MenuDraughter();			

			Console.CursorVisible = false;
			while (true)
			{
				string selectedMenuItem = menuDraughter.DrawMenu(menuItem);

				if (selectedMenuItem == "ACCOUNT'S SECTION")
				{
					AccountMenuItems accMenu = new AccountMenuItems();
					accMenu.AccountMenuManipulation();					
				}
				else if (selectedMenuItem == "SPORT PROGRRAM'S SECTION")
				{					
				}				
			}
		}		
	}
}
