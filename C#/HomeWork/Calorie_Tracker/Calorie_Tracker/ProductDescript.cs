﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calorie_Tracker
{
	public class Product
	{
		public string ProdName { get; set; }
		public double Calories_In_100_Gr { get; set; }
		public double Proteins { get; set; }
		public double Fats { get; set; }
		public double Carbohydrates { get; set; }
		

		public Product()
		{
			//ProdName = prName;
			//Calories_In_100_Gr = calor;
			//Proteins = prot;
			//Fats = fat;
			//Carbohydrates = carbohyd;
		}

		public override string ToString()
		{
			return $"PROD. NAME: {ProdName, 16}.\ncalories / 100 gr: {Calories_In_100_Gr, 3}.\n" + 
				$"proteins: {Proteins, 11}.\nfats: {Fats, 15}.\ncarbohydrates: {Carbohydrates, 7}";
		}
	}

	public class Potatous : Product
	{
		public Potatous():	base()
		{
			ProdName = "potatous";
			Calories_In_100_Gr = 80;
			Proteins = 2;
			Fats = 0;
			Carbohydrates = 16;
		}
	}

	public class Tomatous : Product
	{
		public Tomatous() : base()
		{
			ProdName = "tomatous";
			Calories_In_100_Gr = 19;
			Proteins = 0.8;
			Fats = 0.3;
			Carbohydrates = 3.5;
		}
	}

	public class RyeBread : Product
	{
		public RyeBread() : base()
		{
			ProdName = "rye bread";
			Calories_In_100_Gr = 223;
			Proteins = 5.6;
			Fats = 1.7;
			Carbohydrates = 51.5;
		}
	}

	public class Salmon : Product
	{
		public Salmon() : base()
		{
			ProdName = "salmon";
			Calories_In_100_Gr = 201;
			Proteins = 19.9;
			Fats = 13.6;
			Carbohydrates = 0;
		}
	}

	public class Buckwheat : Product
	{
		public Buckwheat() : base()
		{
			ProdName = "buckweat";
			Calories_In_100_Gr = 300;
			Proteins = 9.5;
			Fats = 2.3;
			Carbohydrates = 48.1;
		}
	}

}
