﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calorie_Tracker
{
	class MenuDraughter
	{
		public static int index = 0;
		public string DrawMenu(List<string> menuItems)
		{
			Console.Clear();
			Console.ForegroundColor = ConsoleColor.Yellow;
			Console.WriteLine("\t\t\t\t======================");
			Console.WriteLine("\t\t\t\t|  CALORIES TRACKER  |");
			Console.WriteLine("\t\t\t\t======================\n\n");
			Console.ResetColor();

			for (int i = 0; i < menuItems.Count; i++)
			{
				if (i == index)
				{
					Console.ForegroundColor = ConsoleColor.Magenta;
					Console.Write($"\t{menuItems[i]}\t\t");
				}
				else
				{
					Console.Write($"\t{menuItems[i]}\t\t");
				}
				Console.ResetColor();

			}
			ConsoleKeyInfo ckey = Console.ReadKey();
			if (ckey.Key == ConsoleKey.DownArrow || ckey.Key == ConsoleKey.RightArrow)
			{
				if (index == menuItems.Count - 1)
				{
					index = 0; // overscroll throught up
				}
				else
					index++;
			}
			else if (ckey.Key == ConsoleKey.UpArrow || ckey.Key == ConsoleKey.LeftArrow)
			{
				if (index <= 0)
				{
					index = menuItems.Count - 1; // overscroll throught bottom
				}
				else
					index--;
			}
			else if (ckey.Key == ConsoleKey.Enter)
			{
				return menuItems[index];
			}
			else
			{
				return "";
			}
			Console.Clear();
			return "";

		}
	}
}
