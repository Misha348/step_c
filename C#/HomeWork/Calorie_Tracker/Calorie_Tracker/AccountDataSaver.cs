﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calorie_Tracker
{
	public class AccountDataSaver
	{
		BinaryFormatter formater = new BinaryFormatter();
		Dictionary<string, Account> accounts = new Dictionary<string, Account>();

		public void SaveAccountToFile(Account account)
		{
			foreach (var eachAccount in accounts)
			{
				if (!accounts.ContainsKey(account.Login))
				{
					accounts.Add(account.Login, account);
					using (Stream fstream = File.Create("test.bin"))
						formater.Serialize(fstream, accounts);
				}
				else
					Console.WriteLine("account with such login already exist");					
			}
			
		}
	}
}
