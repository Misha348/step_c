﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calorie_Tracker
{
	public class PhysicalActivitie
	{
		public string Name { get; set; }
		public int Duration { get; set; }		
	}

	public class PhysicalActivitieExecuting
	{
		public PhysicalActivitie Activitie {get; set;}
		public Account Account { get; set; }
	}
}
