﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calorie_Tracker
{
	[Serializable]
	public class Account
	{
		[Required(ErrorMessage = "login must be inputed")]
		[StringLength(30, MinimumLength = 5, ErrorMessage = "to short login")]
		[RegularExpression("^[._a-zA-Z0-9]{5,}$", ErrorMessage = "invalid login (only . _ a-z A-Z 0-9)")]
		public string Login { get; set; }

		[Required(ErrorMessage = "gender must be inputed")]
		[StringLength(10, MinimumLength = 4, ErrorMessage = "to short gender name")]
		[RegularExpression("^[a-zA-Z]{4,}$", ErrorMessage = "invalid gender name (only a-z A-Z)")]
		public string Gender { get; set; }

		[Required(ErrorMessage = "age must be inputed")]
		[Range(10, 100, ErrorMessage = "invalid age range")]
		public int Age { get; set; }

		[Required(ErrorMessage = "name must be inputed")]
		[StringLength(20, MinimumLength = 2, ErrorMessage = "to short name")]
		[RegularExpression("^[a-zA-Z]{2,}$", ErrorMessage = "invalid name (only a-z A-Z)")]
		public string Name { get; set; }

		[Required(ErrorMessage = "surname must be inputed")]
		[StringLength(20, MinimumLength = 2, ErrorMessage = "to short surname")]
		[RegularExpression("^[a-zA-Z]{2,}$", ErrorMessage = "invalid surname (only a-z A-Z)")]
		public string Surname { get; set; }

		[Required(ErrorMessage = "height must be inputed")]
		[Range(120, 230, ErrorMessage = "invalid height range")]
		public int Height { get; set; }

		[Required(ErrorMessage = "weight must be inputed")]
		[Range(30, 230, ErrorMessage = "invalid weight range")]
		public int Weight { get; set; }		

		public Account() { }

		public override string ToString()
		{
			return $"login: {Login}.\ngender: {Gender}.\nage: {Age}.\nname: {Name}.\n" +
					$"surname: {Surname}.\nheight: {Height}.\nweight {Weight}.";
		}
	}
}
