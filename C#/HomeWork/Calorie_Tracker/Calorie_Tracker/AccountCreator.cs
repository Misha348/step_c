﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calorie_Tracker
{
	public class AccountCreator
	{
		public Account GetNewAccount()
		{
			Account newAccount = new Account();
			bool IsValid;

			do
			{
				Console.Write("\n\n\tuser login: ");
				var _login = Console.ReadLine();
				newAccount.Login = _login;

				var results = new List<ValidationResult>();
				var context = new ValidationContext(newAccount)
				{
					MemberName = "Login"
				};
				if (!(IsValid = Validator.TryValidateProperty(newAccount.Login, context, results)))
				{
					foreach (ValidationResult error in results)
					{
						Console.WriteLine($"{error.MemberNames.FirstOrDefault()} : {error.ErrorMessage}");
					}
				}
			}
			while (!IsValid);

			do
			{
				Console.Write("user gender: ");
				string _gender = Console.ReadLine();
				newAccount.Gender = _gender;

				var results = new List<ValidationResult>();
				var context = new ValidationContext(newAccount)
				{
					MemberName = "Gender"
				};
				if (!(IsValid = Validator.TryValidateProperty(newAccount.Gender, context, results)))
				{
					foreach (ValidationResult error in results)
					{
						Console.WriteLine($"{error.MemberNames.FirstOrDefault()} : {error.ErrorMessage}");
					}
				}
			}
			while (!IsValid);

			do
			{
				Console.Write("user age: ");
				bool notSucces = true;
				int _age = 0;

				while (notSucces)
				{
					if (int.TryParse(Console.ReadLine(), out _age))
						notSucces = false;
					else
						Console.WriteLine("input data in range 10 - 100");
				}
				newAccount.Age = _age;

				var results = new List<ValidationResult>();
				var context = new ValidationContext(newAccount)
				{
					MemberName = "Age"
				};
				if (!(IsValid = Validator.TryValidateProperty(newAccount.Age, context, results)))
				{
					foreach (ValidationResult error in results)
					{
						Console.WriteLine($"{error.MemberNames.FirstOrDefault()} : {error.ErrorMessage}");
					}
				}
			}
			while (!IsValid);

			do
			{
				Console.Write("user name: ");
				string _name = Console.ReadLine();
				newAccount.Name = _name;

				var results = new List<ValidationResult>();
				var context = new ValidationContext(newAccount)
				{
					MemberName = "Name"
				};
				if (!(IsValid = Validator.TryValidateProperty(newAccount.Name, context, results)))
				{
					foreach (ValidationResult error in results)
					{
						Console.WriteLine($"{error.MemberNames.FirstOrDefault()} : {error.ErrorMessage}");
					}
				}
			}
			while (!IsValid);

			do
			{
				Console.Write("user surname: ");
				string _surname = Console.ReadLine();
				newAccount.Surname = _surname;

				var results = new List<ValidationResult>();
				var context = new ValidationContext(newAccount)
				{
					MemberName = "Surname"
				};
				if (!(IsValid = Validator.TryValidateProperty(newAccount.Surname, context, results)))
				{
					foreach (ValidationResult error in results)
					{
						Console.WriteLine($"{error.MemberNames.FirstOrDefault()} : {error.ErrorMessage}");
					}
				}
			}
			while (!IsValid);

			do
			{
				Console.Write("user height: ");
				bool notSucces = true;
				int _height = 0;

				while (notSucces)
				{
					if (int.TryParse(Console.ReadLine(), out _height))
						notSucces = false;
					else
						Console.WriteLine("input data in range 120 - 230");
				}
				newAccount.Height = _height;

				var results = new List<ValidationResult>();
				var context = new ValidationContext(newAccount)
				{
					MemberName = "Height"
				};
				if (!(IsValid = Validator.TryValidateProperty(newAccount.Height, context, results)))
				{
					foreach (ValidationResult error in results)
					{
						Console.WriteLine($"{error.MemberNames.FirstOrDefault()} : {error.ErrorMessage}");
					}
				}
			}
			while (!IsValid);

			do
			{
				Console.Write("user weight: ");
				bool notSucces = true;
				int _weight = 0;

				while (notSucces)
				{
					if (int.TryParse(Console.ReadLine(), out _weight))
						notSucces = false;
					else
						Console.WriteLine("input data in range 30 - 230");
				}
				newAccount.Weight = _weight;

				var results = new List<ValidationResult>();
				var context = new ValidationContext(newAccount)
				{
					MemberName = "Weight"
				};
				if (!(IsValid = Validator.TryValidateProperty(newAccount.Weight, context, results)))
				{
					foreach (ValidationResult error in results)
					{
						Console.WriteLine($"{error.MemberNames.FirstOrDefault()} : {error.ErrorMessage}");
					}
				}
			}
			while (!IsValid);
			return newAccount;
		}
	}
}
