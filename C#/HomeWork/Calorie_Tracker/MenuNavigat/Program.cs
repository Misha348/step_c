﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MenuNavigat
{
	class Program
	{
		static int index = 0;
		static void Main(string[] args)
		{			
			List<string> menuItem = new List<string>()
			{
				"one",
				"two",
				"Exit"
			};

			Console.CursorVisible = false;
			while (true)
			{
				string selectedMenuItem = DrawMenu(menuItem);
				if (selectedMenuItem == "one")
				{
					Console.Clear();
					Console.WriteLine("message from [ONE] choise");
					Console.Read();
				}
				else if (selectedMenuItem == "two")
				{
					Console.Clear();
					Console.WriteLine("message from [TWO] choise");
					Console.Read();
				}
				else if (selectedMenuItem == "Exit")
					Environment.Exit(0);
			}
		}

		private static string DrawMenu(List<string> items)
		{
			for (int i = 0; i < items.Count; i++)
			{
				if (i == index)
				{
					Console.BackgroundColor = ConsoleColor.Gray;
					Console.ForegroundColor = ConsoleColor.Black;

					Console.WriteLine(items[i]);
				}
				else
				{
					Console.WriteLine(items[i]);
				}
				Console.ResetColor();

			}
			ConsoleKeyInfo ckey = Console.ReadKey();
			if (ckey.Key == ConsoleKey.DownArrow)
			{
				if (index == items.Count - 1)
				{
					index = 0; // overscroll throught up
				}

				else
					index++;
			}
			else if (ckey.Key == ConsoleKey.UpArrow)
			{
				if (index <= 0)
				{
					index = items.Count - 1; // overscroll throught bottom
				}

				else
					index--;
			}
			else if (ckey.Key == ConsoleKey.Enter)
			{
				return items[index];
			}
			else
			{
				return "";
			}					
			Console.Clear();
			return "";

		}
	}
}
