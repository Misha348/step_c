﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Army	
{
	public class OutOfRangeWeaponryGeneration: Exception
	{
		public void OutOfRangeWeaponryGenerationMessage()
		{
			Console.WriteLine("technologies doesnt allow get to the 8-th weaponry generation");
		}
	}

	public enum ArmedForceType : byte
	{
		infantry_force = 1,
		air_force,
		sea_force,
		airBorn_force,
		special_forces
	}

	public class ArmyForce
	{
		public ArmedForceType ForceType { get; set; }
		public int Quantity { get; set; }
		public int Reserve { get; set; }
		public string MainPurpose { get; set; }
		public int ForceTechnique { get; set; }
		private int budget;
		public int Budget
		{
			get
			{
				if (budget < 10)
				{
					Console.WriteLine("not enought budget: ");
					return budget;
				}
				else
					return budget;
			}

			set
			{				
				budget = value;				
			}
		}

		public static string MainCommander { get; private set; }
		private static int _weaponryGeneration;
		public static int WeaponryGeneration
		{
			get
			{
				return _weaponryGeneration;
			}
			set
			{
				if (value > 6)
					throw new OutOfRangeWeaponryGeneration();
				else
				{
					_weaponryGeneration = value;					
				}
			}
		}

		static ArmyForce()
		{
			MainCommander = "president";
			_weaponryGeneration = 4;
		}

		public ArmyForce() 
		{
			SetforceData(out ArmedForceType forceType, out int budget, out int quantity, out int reserve, out int forceTechnique, out string mainPurpose);
			ForceType = forceType;
			Budget = budget;
			Quantity = quantity;
			Reserve = reserve;
			ForceTechnique = forceTechnique;
			MainPurpose = mainPurpose;
		}

		public ArmyForce(ArmedForceType forceType, string mainPurpose): this()
		{
			ForceType = forceType;
			MainPurpose = mainPurpose;			
		}

		//public ArmyForce(ArmedForceType forceType, long budget, int quantity, int reserve,  int forceEquipmentAmount, string mainPurpose): this ()
		//{
		//	ForceType = forceType;
		//	Budget = budget;
		//	Quantity = quantity;
		//	Reserve = reserve;			
		//	ForceTechnique = forceEquipmentAmount;
		//	MainPurpose = mainPurpose;
			
		//}

		public void SetforceData(out ArmedForceType forceType, out int budget, out int quantity, out int reserve, out int forceTechnique, out string mainPurpose)
		{
			
			foreach (var _forceType in Enum.GetNames(typeof(ArmedForceType)))
			{				
				Console.WriteLine($"  * {_forceType}");
				
			}
			Console.WriteLine();			

			Console.Write($"\nenter  force type: ");
			while (!ArmedForceType.TryParse(Console.ReadLine(), out forceType))
			{
				Console.WriteLine("unexistiable type, try again: ");
			}

			Console.Write("enter budget (not less than 40): ");
			budget = Convert.ToInt16(Console.ReadLine());
			

			Console.Write("enter quantity of forces: ");
			quantity = Convert.ToInt16(Console.ReadLine());
			

			Console.Write("enter quantity of reserve: ");
			reserve = int.Parse(Console.ReadLine());

			Console.Write("enter quantity of forceTechnique: ");
			forceTechnique = int.Parse(Console.ReadLine());

			Console.Write("detrmine main purpose of forse type: ");
			mainPurpose = Console.ReadLine();
		}

		public void ReformWeaponryGeneration()
		{
			Console.WriteLine($"now weaponry generation equal - {_weaponryGeneration++} generation.");
			budget -= 20;
		}

		public void ChekBudget()
		{
			Console.WriteLine($"budget: {Budget}");
		}

		//public override string ToString()
		//{
		//	return $"-   Main commander: {MainCommander}\n" +
		//			$"- Force type: {ForceType}\n" +
		//			$"- weapon.generat: {WeaponryGeneration}\n" +
		//			$"- Purpose: {MainPurpose}" +
		//			$"- Budget: {Budget}\n" +
		//			$"- Quant: {Quantity}\n" +
		//			$"- Reserve: {Reserve}\n" +
		//			$"- Technique: {ForceTechnique}\n";
		//}

		public void ShowForce()
		{
			Console.ForegroundColor = ConsoleColor.DarkGreen;
			Console.WriteLine($"\n\n   Main commander: {MainCommander}\n" +
					$"- Force type: {ForceType}\n" +
					$"- weapon.generat: {WeaponryGeneration}\n" +
					$"- Purpose: {MainPurpose}\n" +
					$"- Budget: {Budget} bill\n" +
					$"- Quant: {Quantity}\n" +
					$"- Reserve: {Reserve}\n" +
					$"- Technique: {ForceTechnique}");
			Console.ForegroundColor = ConsoleColor.White;
		}
	}
}
