﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Army
{
	class Program
	{
		static void Main(string[] args)
		{
			Console.WriteLine("\t\tCREATE ARMY");
			ArmyForce[] armyForces = new ArmyForce[5];
			for (int i = 0; i < armyForces.Length; i++)
			{
				armyForces[i] = new ArmyForce();
				Console.Clear();
			}

			foreach (var force in armyForces)
			{
				force.ShowForce();
				Console.WriteLine(new string('=', 35));
			}
			Thread.Sleep(7000);

			Console.Write($"choose forse for reforming: ");
			ArmedForceType type;			
			var choise = ArmedForceType.TryParse(Console.ReadLine(), out type);

			try
			{
				foreach (var force in armyForces)
				{
					if (force.ForceType == type)
					{
						force.ReformWeaponryGeneration();
						force.ShowForce();
						force.ChekBudget();
					}
				}
			}
			catch (OutOfRangeWeaponryGeneration ex)
			{
				ex.OutOfRangeWeaponryGenerationMessage();
			}
			
		}
	}
}
