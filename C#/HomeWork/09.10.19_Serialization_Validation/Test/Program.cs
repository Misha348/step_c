﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace Test
{
	class Program
	{
		static void Main(string[] args)
		{
			string pattern = @"[._a-zA-Z0-9]";
			var regex = new Regex(pattern);
			var mystr = Console.ReadLine();
			Console.WriteLine(
				regex.IsMatch(mystr)
						? $"String \"{mystr}\" matched \"{pattern}\""
						: $"String \"{mystr}\" NOT matched \"{pattern}\"");



		}
	}
}
