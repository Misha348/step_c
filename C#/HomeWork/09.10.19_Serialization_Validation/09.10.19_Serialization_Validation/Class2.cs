﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _09._10._19_Serialization_Validation
{
	public class UserRegistrator
	{
		public User GetNewUser()
		{
			User newUser = new User();
			bool IsValid;
			do
			{
				Console.Write("user ID: ");
				bool notSucces = true;
				var _ID = 0;
				while (notSucces)
				{
					if (int.TryParse(Console.ReadLine(), out _ID))
						notSucces = false;
					else
						Console.WriteLine("input data in range 1000 - 9999");			
				}
				newUser.UserID = _ID;

				var results = new List<ValidationResult>();
				var context = new ValidationContext(newUser)
				{
					MemberName = "UserID"
				};
				if (!(IsValid = Validator.TryValidateProperty(newUser.UserID, context, results)))
				{
					foreach (ValidationResult error in results)
					{
						Console.WriteLine($"{error.MemberNames.FirstOrDefault()} : {error.ErrorMessage}");
					}
				}
			}
			while (!IsValid);

			do
			{
				Console.Write("enter login: ");
				var _login = Console.ReadLine();
				newUser.Login = _login;

				var results = new List<ValidationResult>();
				var context = new ValidationContext(newUser)
				{
					MemberName = "Login"
				};
				if (!(IsValid = Validator.TryValidateProperty(newUser.Login, context, results)))
				{
					foreach (ValidationResult error in results)
					{
						Console.WriteLine($"{error.MemberNames.FirstOrDefault()} : {error.ErrorMessage}");
					}
				}
			}
			while (!IsValid);

			do
			{
				Console.Write("enter password: ");
				var _password = Console.ReadLine();
				newUser.Password = _password;

				var results = new List<ValidationResult>();
				var context = new ValidationContext(newUser)
				{
					MemberName = "Password"
				};
				if (!(IsValid = Validator.TryValidateProperty(newUser.Password, context, results)))
				{
					foreach (ValidationResult error in results)
					{
						Console.WriteLine($"{error.MemberNames.FirstOrDefault()} : {error.ErrorMessage}");
					}
				}
			}
			while (!IsValid);

			do
			{
				Console.Write("enter password confirmation: ");
				var _confirmation = Console.ReadLine();
				newUser.Conf_Passwd = _confirmation;

				var results = new List<ValidationResult>();
				var context = new ValidationContext(newUser)
				{
					MemberName = "Conf_Passwd"
				};
				if (!(IsValid = Validator.TryValidateProperty(newUser.Conf_Passwd, context, results)))
				{
					foreach (ValidationResult error in results)
					{
						Console.WriteLine($"{error.MemberNames.FirstOrDefault()} : {error.ErrorMessage}");
					}
				}
			}
			while (!IsValid);			

			do
			{
				Console.Write("enter email: ");
				var _email = Console.ReadLine();
				newUser.Email = _email;

				var results = new List<ValidationResult>();
				var context = new ValidationContext(newUser)
				{
					MemberName = "Email"
				};
				if (!(IsValid = Validator.TryValidateProperty(newUser.Email, context, results)))
				{
					foreach (ValidationResult error in results)
					{
						Console.WriteLine($"{error.MemberNames.FirstOrDefault()} : {error.ErrorMessage}");
					}
				}
			}
			while (!IsValid);

			do
			{
				Console.Write("enter cred.card: ");
				var _credcard = Console.ReadLine();
				newUser.CredCard = _credcard;

				var results = new List<ValidationResult>();
				var context = new ValidationContext(newUser)
				{
					MemberName = "CredCard"
				};
				if (!(IsValid = Validator.TryValidateProperty(newUser.CredCard, context, results)))
				{
					foreach (ValidationResult error in results)
					{
						Console.WriteLine($"{error.MemberNames.FirstOrDefault()} : {error.ErrorMessage}");
					}
				}
			}
			while (!IsValid);

			do
			{
				Console.Write("enter phone numb: ");
				var _phNumb = Console.ReadLine();
				newUser.PhoneNo = _phNumb;

				var results = new List<ValidationResult>();
				var context = new ValidationContext(newUser)
				{
					MemberName = "PhoneNo"
				};
				if (!(IsValid = Validator.TryValidateProperty(newUser.PhoneNo, context, results)))
				{
					foreach (ValidationResult error in results)
					{
						Console.WriteLine($"{error.MemberNames.FirstOrDefault()} : {error.ErrorMessage}");
					}
				}
			}
			while (!IsValid);			
			return newUser;
		}
	}
}
