﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _09._10._19_Serialization_Validation
{
	class WorkWithMember
	{
		BinaryFormatter formater = new BinaryFormatter();
		UserRegistrator us = new UserRegistrator();
		Dictionary<int, User> members = new Dictionary<int, User>();

		public void RegistrateNewMember()
		{
			User user = us.GetNewUser();
			members.Add(user.UserID, user);
		}

		public void SaveNewMemberDataInFile()
		{			
			using (Stream fstream = File.Create("test.bin"))
				formater.Serialize(fstream, members);
			Console.WriteLine("saved...\n");
		}

		public void DownloadMembersList()
		{
			members = null;
			using (Stream fstream = File.OpenRead("test.bin"))
				members = (Dictionary<int, User>)formater.Deserialize(fstream);
			Console.WriteLine("downloaded...\n");
		}

		public void ShowMembersList()
		{
			if (members.Count == 0)
				Console.WriteLine("no members on  registration");
			foreach (var member in members)
			{				
				Console.WriteLine($"{member.Value}");
				Console.WriteLine(new string('=', 30));
			}
		}
	}
}

/*  BinaryFormatter binFormater = new BinaryFormatter();
            using (Stream fstream = File.Create("test.bin"))
                binFormater.Serialize(fstream, user);
            Console.WriteLine("BinarySerialize OK!\n");

            User user2 = null;
            using (Stream fstream = File.OpenRead("test.bin"))
                user2 = (User)binFormater.Deserialize(fstream);
            Console.WriteLine(user2);*/
