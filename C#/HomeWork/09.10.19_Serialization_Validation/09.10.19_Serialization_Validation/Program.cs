﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _09._10._19_Serialization_Validation
{
	class Program
	{
		static void Main(string[] args)
		{
			WorkWithMember manipulation = new WorkWithMember();

			Console.WriteLine("[ 1 - REGISTRATE NEW MEMBER]");
			Console.WriteLine("[ 2 - SAVE NEW MEMBER]");
			Console.WriteLine("[ 3 - DOWNLOAD MEMBERS LIST]");
			Console.WriteLine("[ 4 - WATCH MEMBERS LIST]");
			Console.WriteLine("[ 5 - EXIT REGISTRATOR]");
			bool exit = true;			
			while (exit == true)
			{
				Console.Write("____");
				var choise = int.Parse(Console.ReadLine());
				switch (choise)
				{
					case 1:
						manipulation.RegistrateNewMember();
						break;
					case 2:
						manipulation.SaveNewMemberDataInFile();
						break;
					case 3:
						manipulation.DownloadMembersList();
						break;
					case 4:
						manipulation.ShowMembersList();
						break;
					case 5:
						exit = false;
						break;
				}
			}			
		}
	}
}
