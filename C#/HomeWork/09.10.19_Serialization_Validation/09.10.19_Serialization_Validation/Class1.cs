﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _09._10._19_Serialization_Validation
{
	[Serializable]
	public class User
	{
		[Required(ErrorMessage = "ID must be inputed")]
		[Range(1000, 9999, ErrorMessage = "invalid id range")]
		public int UserID { get; set; }

		[Required(ErrorMessage = "LOGIN must be inputed")]
		[StringLength(30, MinimumLength = 5, ErrorMessage = "to short login")]
		[RegularExpression("^[._a-zA-Z0-9]{5,}$", ErrorMessage = "invalid login (only . _ a-z A-Z 0-9)")]
		public string Login { get; set; }

		[Required(ErrorMessage = "PASSWORD must be inputed")]
		[StringLength(30, MinimumLength = 8, ErrorMessage = "to short passwor")]
		[RegularExpression(@"^[._a-zA-Z0-9]{8,}$", ErrorMessage = "invalid password (only . _ a-z A-Z 0-9)")]
		public string Password { get; set; }

		[Required]
		[Compare("Password", ErrorMessage = "confirmation FAILED")]
		public string Conf_Passwd { get; set; }

		[Required(ErrorMessage = "EMAIL must be inputed")]
		[StringLength(30, MinimumLength = 6, ErrorMessage = "to short email")]
		[RegularExpression("^[.a-z0-9]*@[a-z0-9.]*[a-z]*$", ErrorMessage = "invalid email")]
		public string Email { get; set; }
		//^[.\-_a-z0-9]+@([a-z0-9][\-a-z0-9]+\.)+[a-z]$

		//[Required(ErrorMessage = "cred card No. not inputed"/*, AllowEmptyStrings = true*/)]
		[StringLength(20, MinimumLength = 20, ErrorMessage = "to short card numb")]
		[RegularExpression(@"^(\d+\s)(\d+\s)(\d+\s)(\d+\s)$", ErrorMessage = "invalid format: xxxx xxxx xxxx xxxx ")]
		public string CredCard { get; set; }

		[Required(ErrorMessage = "PHONE NUMBER must be inputed")]
		[RegularExpression(@"^(\+)([1-9]{2})(-|\s)0(\d{2})(-|\s)(\d{3})(-|\s)(\d{4})$", ErrorMessage = "invalid format: +38 0xx xxx xxxx")]
		public string PhoneNo { get; set; }

		
		public User()
		{			
		}

		public User(int usID, string usLog, string usPasswd, string paswConf, string usEmail, string usCrCard, string No)
		{
			UserID = usID;
			Login = usLog;
			Password = usPasswd;
			Conf_Passwd = paswConf;
			Email = usEmail;
			CredCard = usCrCard;
			PhoneNo = No;
		}

		public override string ToString()
		{
			return $"id: {UserID}.\nlogin: {Login}.\npasswd: {Password}.\nconf passwd: {Conf_Passwd}.\n" +
					$"email: {Email}.\ncred card: {CredCard}.\nphone: {PhoneNo}.";
		}
		
	}
}
