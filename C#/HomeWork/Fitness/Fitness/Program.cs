﻿using FitnessBL.Controller;
using FitnessBL.Model;
using System;

namespace Fitness
{
	class Program
	{
		static void Main(string[] args)
		{
			Console.WriteLine(Languages.Messages.Title);

			Console.WriteLine(Languages.Messages.FillingInName);
			var name = Console.ReadLine();			

			var userController = new UserController(name);
			var eatingController = new EatingController(userController.CurrentUser);
			var exerciseController = new ExerciseController(userController.CurrentUser);

			if (userController.IsNewUser)
			{
				Console.Write("enter gender: ");
				var gender = Console.ReadLine();
				var birthDate = ParseDateTime("birth date");
				var weight = ParseDouble("weight");
				var height = ParseDouble("streight");
				
				userController.SetNewUsersData(gender, birthDate, weight, height);
			}
			Console.WriteLine(userController.CurrentUser);

			

			while (true)
			{
				Console.WriteLine("choose action: ");
				Console.WriteLine("E - enter meal consummering");
				Console.WriteLine("A - enter activity");
				Console.WriteLine("Q - exit");
				var key = Console.ReadKey();
				Console.WriteLine();

				switch (key.Key)
				{
					case ConsoleKey.E:
						var foods = EnterEating();
						eatingController.Add(foods.Food, foods.Weight);

						foreach (var item in eatingController.Eating.Foods)
						{
							Console.WriteLine($"\t{item.Key} - {item.Value}");
						}
						break;
					case ConsoleKey.A:
						var exe = EnterExercise();
						exerciseController.Add(exe.Activity, exe.Begin, exe.End);
						foreach (var item in exerciseController.Exercises)
						{
							Console.WriteLine($"{item.Activity} from {item.Start.ToShortTimeString()} to {item.Finish.ToShortTimeString()}");
						}
						break;
					case ConsoleKey.Q:
						Environment.Exit(0);
						break;
				}
				Console.WriteLine();
			}		
		}

		private static (DateTime Begin, DateTime End, Activity Activity) EnterExercise()
		{
			Console.WriteLine("enter name of exercise");
			var name = Console.ReadLine();
			var energy = ParseDouble("energy consumption");
			var begin = ParseDateTime("start of exercise");
			var end = ParseDateTime("end of exercise");
			var activity = new Activity(name, energy);
			return (begin, end, activity);
		}

		private static (Food Food, double Weight) EnterEating()
		{
			Console.Write("Enter products name: ");
			var food = Console.ReadLine();

			//Console.Write("enter caloricity: ");
			var calories = ParseDouble("caloricity");
			var proteins = ParseDouble("proteins");
			var fats = ParseDouble("fats");
			var carboHydrates = ParseDouble("carboHydrates");
			
			var weight = ParseDouble("weight of batch");
			var product = new Food(food);

			return (Food:product, Weight:weight);
		}

		private static DateTime ParseDateTime(string value)
		{
			DateTime birthDate;
			while (true)
			{
				Console.Write($"enter {value} (dd.MM.yyyy): ");
				if (DateTime.TryParse(Console.ReadLine(), out birthDate))
				{
					break;
				}
				else
					Console.WriteLine($"wrong {value} format");
			}

			return birthDate;
		}

		private static double ParseDouble(string name)
		{
			while (true)
			{
				Console.Write($"enter {name}: ");
				if (double.TryParse(Console.ReadLine(), out double value))
				{
					return value;
				}
				else
					Console.WriteLine($"wrong format of {name}");
			}
		}
	}
}
