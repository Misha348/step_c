﻿using System;

namespace FitnessBL.Model
{
	[Serializable]
	public class User
	{
		public int Id { get; set; }
		//users Prop
		#region users Prop
		public string Name { get; }
		public Gender Gender { get; set; }
		public DateTime BirthDate { get; set; }
		public double Weight { get; set; }
		public double Height { get; set; }
		public int Age { get { return DateTime.Now.Year - BirthDate.Year; } }
		#endregion

		// users constructor (create new user)
		public User(string name, Gender gender, DateTime birthDate, double weight, double height)
		{
			#region checking users parameters
			if (string.IsNullOrWhiteSpace(name))
			{
				throw new ArgumentNullException("users name cant be empty or null.", nameof(name));
			}
			if (gender == null)
			{
				throw new ArgumentNullException("gender cant be null.", nameof(gender));
			}
			if (birthDate < DateTime.Parse("01.01.1900") || birthDate >= DateTime.Now)
			{
				throw new ArgumentException("invalid bith date.", nameof(birthDate));
			}
			if (weight <= 0)
			{
				throw new ArgumentException("weight cant be less or equals zero.", nameof(weight));
			}
			if (height <= 0)
			{
				throw new ArgumentException("height cant be less or equals zero.", nameof(height));
			}
			#endregion

			Name = name;
			Gender = gender;
			BirthDate = birthDate;
			Weight = weight;
			Height = height;
		}

		public User(string name)
		{
			if (string.IsNullOrWhiteSpace(name))
			{
				throw new ArgumentNullException("users name cant be empty or null.", nameof(name));
			}
			Name = name;
		}

		public override string ToString()
		{
			return Name + " " + Age;
		}
	}
}
