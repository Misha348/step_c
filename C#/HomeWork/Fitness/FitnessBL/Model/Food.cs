﻿using System;

namespace FitnessBL.Model
{
	[Serializable]
	public class Food
	{
		public int Id { get; set; }
		public string Name { get; }
		public double Callories { get; }
		public double Proteins { get; set; }
		public double Fats { get; set; }
		public double Carbohydrates { get; }
		public double Calories { get; }

		private double CalloriesOneGramm { get { return Calories / 100.0; } }
		public double ProteinsOneGramm { get { return Proteins / 100.0; } }
		public double FatsOneGramm { get { return Fats / 100.0; } }
		public double CArbohydratesGramm { get { return Carbohydrates / 100.0; } }

		public Food(string name): this(name, 0, 0, 0, 0) { }

		public Food(string name, double callories, double proteins, double fats, double carbohydrates)
		{
			Name = name;
			Callories = callories / 100.0;
			Proteins = proteins / 100.0;
			Fats = fats / 100.0;
			Carbohydrates = carbohydrates;
		}

		public override string ToString()
		{
			return Name;
		}

	}
}
