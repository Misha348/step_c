﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace FitnessBL.Model
{
	/// <summary>
	/// procces of consummering food 
	/// </summary>
	[Serializable]
	public class Eating
	{
		public int Id { get; set; }
		public DateTime Moment { get; }
		// its better to use ref type like VALUE!!!
		public Dictionary<Food, double> Foods { get; }
		public User User { get; }

		public Eating(User user)
		{
			// verification with null
			User = user ?? throw new ArgumentNullException("users name cant be empty or null.", nameof(user));
			Moment = DateTime.UtcNow;
			Foods = new Dictionary<Food, double>();
		}

		public void Add(Food food, double weight)
		{

			// checking if there was shuch product??
			var product = Foods.Keys.FirstOrDefault(f => f.Name.Equals(food.Name));

			if (product == null)
				Foods.Add(food, weight);
			else
				Foods[product] += weight;

		}
	}
}
