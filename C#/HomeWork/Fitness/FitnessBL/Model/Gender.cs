﻿using System;

namespace FitnessBL.Model
{
	[Serializable]
	public class Gender
	{
		public int Id { get; set; }
		public string Name { get; }

		/// <summary>
		/// create gender
		/// </summary>
		/// <param name="name"></param>
		public Gender(string name)
		{
			if (string.IsNullOrWhiteSpace(name))
			{
				throw new ArgumentNullException("genders name cant be empty or null", nameof(name));
			}
			Name = name;
		}

		public override string ToString()
		{
			return Name;
		}
	}
}
