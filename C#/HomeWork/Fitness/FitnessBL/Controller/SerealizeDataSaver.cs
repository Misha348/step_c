﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace FitnessBL.Controller
{
	public class SerealizeDataSaver : IDataSaver
	{
		public T Load<T>(string fileName)
		{
			var formater = new BinaryFormatter();
			using (var fs = new FileStream(fileName, FileMode.OpenOrCreate))
			{
				if (fs.Length > 0 && formater.Deserialize(fs) is T items)
				{
					return items;
				}
				else
					return default(T);
			}
		}

		public void Save(string fileName, object item)
		{
			var formater = new BinaryFormatter();
			using (var fs = new FileStream(fileName, FileMode.OpenOrCreate))
			{
				formater.Serialize(fs, item);
			}
		}
	}
}
