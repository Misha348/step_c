﻿using FitnessBL.Model;
using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Collections.Generic;
using System.Linq;

namespace FitnessBL.Controller
{
	/// <summary>
	/// users controller
	/// </summary>
	public class UserController: ControllerBase
	{
		private const string USERS_FILE_NAME = "users.dat";
		public List<User> Users { get; }
		public User CurrentUser { get; }
		public bool IsNewUser { get; } = false;
		/// <summary>
		/// creating new users controller(which manages creating of users)
		/// </summary>
		/// <param name="user"></param>
		public UserController(string userName)
		{
			if (string.IsNullOrWhiteSpace(userName))
			{
				throw new ArgumentNullException("users name cant be empty", nameof(userName));
			}
			Users = GetUsersData();
			// Get such single user with such name, or if it was unmanaged find such utser we get null
			CurrentUser = Users.SingleOrDefault(u => u.Name == userName);

			if (CurrentUser == null)
			{
				CurrentUser = new User(userName);
				Users.Add(CurrentUser);
				IsNewUser = true;
				Save();
			}
		}

		/// <summary>
		/// Get saved list of users
		/// </summary>
		/// <returns></returns>
		private List<User> GetUsersData()
		{
			return Load<List<User>>(USERS_FILE_NAME) ?? new List<User>();
		}

		public void SetNewUsersData(string genderName, DateTime birthDate, double weight, double height)
		{
			// Verification
			CurrentUser.Gender = new Gender(genderName);
			CurrentUser.BirthDate = birthDate;
			CurrentUser.Weight = weight;
			CurrentUser.Height = height;
			Save();
		}

		/// <summary>
		/// save users data
		/// </summary>
		public void Save()
		{
			Save(USERS_FILE_NAME, Users);			
		}		
	}
}
