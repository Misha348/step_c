﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace FitnessBL.Controller
{
	public abstract class ControllerBase
	{
		protected IDataSaver saver = new SerealizeDataSaver();


		protected void Save(string fileName, object item)
		{
			saver.Save(fileName, item);
		}

		protected T Load<T>(string fileName)
		{
			return saver.Load<T>(fileName);
		}
	}
}
