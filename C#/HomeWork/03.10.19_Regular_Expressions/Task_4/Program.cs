﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_4
{
	class Program
	{
		static void Main(string[] args)
		{
			Console.WriteLine("Enter some line: ");
			var line = Console.ReadLine();

			FileStream file = File.Create(@"test.txt");
			var writer = new StreamWriter(file);
			writer.WriteLine(line);
			writer.Close();
			file.Close();

			//
			string path = @"test.txt";
			string textFromFile;
			using (StreamReader sReader = new StreamReader(path))
			{
				textFromFile = sReader.ReadToEnd();
			}


			string pattern = @"[+-]?[0-9][0-9]*";

			Match match = Regex.Match(textFromFile, pattern);
			List<int> integers = new List<int>();


			while (match.Success)
			{
				var dig = int.Parse(match.Value);
				match = match.NextMatch();
				integers.Add(dig);
			}

			foreach (var digit in integers)
			{
				Console.WriteLine(digit);
			}

			// @"[^\.] ([0 - 9]+)\s";
		}
	}
}
