﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Regular_Expressions
{
	class Program
	{
		static void Main(string[] args)
		{
			Console.WriteLine("Enter some line: ");
			var line = Console.ReadLine();

			FileStream file = File.Create(@"test.txt");
			var writer = new StreamWriter(file);
			writer.WriteLine(line);
			writer.Close();
			file.Close();

			string path = @"test.txt";
			string textFromFile;			
			using (StreamReader sReader = new StreamReader(path))
			{
				textFromFile = sReader.ReadToEnd();
			}

			string pattern = "[0-9]*[.,][0-9]+";			
			Match match = Regex.Match(textFromFile, pattern);			


			while (match.Success)
			{
				Console.WriteLine(match.Value);
				match = match.NextMatch();
			}


		}
	}
}
