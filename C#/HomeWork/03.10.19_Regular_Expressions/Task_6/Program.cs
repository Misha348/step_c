﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_6
{
	class Program
	{
		static void Main(string[] args)
		{
			string path = @"test.txt";
			string textFromFile;
			using (StreamReader sReader = new StreamReader(path))
			{
				textFromFile = sReader.ReadToEnd();
			}

		
			string datePatern = @"[1-2]{1}[0-9]{1}[0-9]{1}[0-9]{1}(\s|:|-|_)" + 
								@"([1-9]|0[1-9]|1[0-2])(\s|:|-|_)" +
								@"([1-9]|0[1-9]|1[0-9]|2[0-9]|3[0-1])(\s|:|-|_)" +
								@"(0[1-9]|1[1-9]|2[0-4])(\s|:|-|_)" +
								@"(0[1-9]|1[1-9]|2[0-9]|3[0-9]|4[0-9]|5[0-9])";

			Match match = Regex.Match(textFromFile, datePatern);
			

			while (match.Success)
			{
				Console.WriteLine(match.Value);
				match = match.NextMatch();
			}
		}
	}
}
