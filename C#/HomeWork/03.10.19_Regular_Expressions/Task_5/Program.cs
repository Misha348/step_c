﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_5
{
	class Program
	{
		static void Main(string[] args)
		{
			bool notValid1 = true;
			while (notValid1)
			{
				Console.WriteLine("Enter email addr: ");
				var line = Console.ReadLine();

				string pattern = @"[.\-_a-z0-9]+@([a-z0-9][\-a-z0-9]+\.)+[a-z]{2,6}";
				var regex = new Regex(pattern);
				if (regex.IsMatch(line))
				{
					Console.WriteLine($"email is VALID");
					notValid1 = false;
				}
				else
					Console.WriteLine($"email is INVALID");
			}		
			Console.WriteLine(new string('=', 20));


			bool notValid = true;
			while (notValid)
			{
				Console.WriteLine("Enter password: ");
				var line1 = Console.ReadLine();

				string pattern1 = @"[.\-_a-zA-Z0-9]{6,50}";
				var regex1 = new Regex(pattern1);
				if (regex1.IsMatch(line1))
				{
					Console.WriteLine($"password is VALID");
					notValid = false;
				}
				else
					Console.WriteLine($"password is INVALID");						
			}
			

		}
	}
}
