﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _19._09._19_Interfaces
{
	public interface IRuning
	{
		string AbillityToMoveRuningly();
	}

	public interface ISweaming
	{
		string AbillityToMoveSweamingly();
	}

	public interface IFlying
	{
		void AbillityToMoveFlyingly();
	}

	public interface ICrawling
	{
		string AbillityToMoveCrawlingly();
	}

}
