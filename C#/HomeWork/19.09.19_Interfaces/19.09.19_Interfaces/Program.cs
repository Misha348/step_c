﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _19._09._19_Interfaces
{
	public abstract class AliveOrganism
	{
		public AliveOrganism() { }
		public abstract string HasSomePhisicForm();
		public abstract string AbilityMoveActivelly();
	}

	public class Animal : AliveOrganism
	{
		protected string AreaOfBeing { get; set; }
		protected string TypeName    { get; set; }
		protected string Species     { get; set; }

		public Animal(string typeName, string species, string areaOfBeing): base()
		{
			Species = species;
			TypeName = typeName;
			AreaOfBeing = areaOfBeing;
		}

		public virtual string ConsumeringNutries()
		{
			return "consume nutries.";
		}

		public virtual string BirthForm()
		{
			return "different types has different form of birth.";
		}

		public virtual string Migration()
		{
			return "some animal types migrate.";
		}

		public override string HasSomePhisicForm()
		{
			return "creature has animal characteristic form.";
		}

		public override string AbilityMoveActivelly()
		{
			return "specifid animals characteristic is ability to move activelly.";
		}
	}

	class Program
	{
		static void Main(string[] args)
		{
			Animal[] zoo = new Animal[]
			{
				new Bear("bear", true, true, "mammal", "arctic", "ursus", "white bear"),
				new Salmon("salmo", "salmonidae", true, true, "fresh and solt water", "fish", "salmonidae"),
				new Chameleon("reptile", "tropic Africa + Madagaskar", "chameleon", "cold", "acient creatures",
				"ability to watch in different directions", "chameleon simple"),
			};

			//"Salmonidae", true, true, "fish", "fresh and solt water", "Salmo"

			foreach (var animal in zoo)
			{
				Console.WriteLine(animal.ToString()); 
				Console.WriteLine(new string('=', 50));
			}


		}
	}
}
