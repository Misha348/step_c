﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _19._09._19_Interfaces
{
	public class Mammal : Animal
	{
		protected string Breed { get; set; }
		protected bool FurAvailability { get; set; }
		protected bool MammaryGlandAvailability { get; set; }


		public Mammal(bool furAvailability, bool mammaryGlandAvailability, string typeName, string areaOfBeing, string species, string breed) :
			base(typeName, areaOfBeing, species)
		{
			FurAvailability = furAvailability;
			MammaryGlandAvailability = mammaryGlandAvailability;
			Breed = breed;
		}

		public override string ConsumeringNutries()
		{
			return "MAMMALS consume nutries character for MAMMALS.";
		}

		public override string BirthForm()
		{
			return "MAMMALS birth alive posterities at once. ";
		}

		public override string Migration()
		{
			return "MAMMALS has ability to migrate.";
		}

		public virtual string ProgenyFeeding()
		{
			return "MAMMALS progeny firstly are feeded with milk. ";
		}
	}

	public class Bear : Mammal, IRuning, ISweaming
	{
		public string Name { get; set; }

		public Bear(string name, bool furAvailability, bool mammaryGlandAvailability, string typeName, string areaOfBeing, string species, string breed) :
			base(furAvailability, mammaryGlandAvailability, typeName, areaOfBeing, species, breed)
		{
			Name = name;
		}

		public string AnabiozAvailability()
		{
			return "BEARS has ability get into anabolism.";
		}

		public override string ConsumeringNutries()
		{
			return "BEARS consum meat, fish, honey, fruits, vegetables";
		}

		public override string BirthForm()
		{
			return "BEARS birth alive posterities at once. ";
		}

		public override string Migration()
		{
			return "BEARS has ability to migrate.";
		}

		public override string ProgenyFeeding()
		{
			return "BEARS progeny firstly are feeded with milk. ";
		}

		public string AbillityToMoveRuningly()
		{
			return "BEARS has ability to run fast.";
		}

		public string AbillityToMoveSweamingly()
		{
			return "BEARS has ability to sweam.";
		}

		public override string ToString()
		{
			return  $"Mammary Gland: {(MammaryGlandAvailability ? "present" : "apsent")}.\nFur: {(FurAvailability ? "present" : "apsent")}.\n" +
					$"Type: {TypeName}.\nSpecies: {Species}.\nBreed: {Breed}.\nAreaOfBeing: {AreaOfBeing}.\nName:{Name}.\n\n" +
					$"{AnabiozAvailability()}\n{BirthForm()}\n{ProgenyFeeding()}\n{ConsumeringNutries()}\n{Migration()}\n{AbillityToMoveRuningly()}\n{AbillityToMoveSweamingly()}";
		}

	}
}
