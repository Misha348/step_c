﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _19._09._19_Interfaces
{
	class Fish: Animal
	{
		protected string Family { get; set; }
		protected bool GillAvailability { get; set; }
		protected bool Scalesavailability { get; set; }

		public Fish(string family, bool gillAvailability, bool scalesavailability, string typeName, string areaOfBeing, string species) :
			base(typeName, areaOfBeing, species)
		{
			Family = family;
			GillAvailability = gillAvailability;
			Scalesavailability = scalesavailability;
		}

		public override string ConsumeringNutries()
		{
			return "FISH consume nutries character for FISH.";
		}

		public override string BirthForm()
		{
			return "FISH lay caviar.";
		}

		public override string Migration()
		{
			return "FISH has ability to migrate.";
		}

		public virtual string UnderWaterBreath()
		{
			return "FISH has ability to breath under water.";
		}
	}	
	

	class Salmon : Fish, ISweaming
	{
		public string Name { get; set; }

		public Salmon(string name, string family, bool gillAvailability, bool scalesavailability, string areaOfBeing, string typeName, string species):
			base(family, gillAvailability, scalesavailability, areaOfBeing, typeName, species)
		{
			Name = Name;
		}

		public string IndustryValue()
		{
			return "SALMON has huge industry value."; 
		}

		public override string ConsumeringNutries()
		{
			return "SALMON consum pidlling fish and crustaceans";
		}

		public override string BirthForm()
		{
			return "SALMON lay caviar. ";
		}

		public override string Migration()
		{
			return "SALMON has ability to migrate.";
		}

		public string AbillityToMoveSweamingly()
		{
			return "SALMON has ability to sweam";
		}

		public override string ToString()
		{
			return  $"Gill: {(GillAvailability ? "present" : "absent")}\nScale: {(Scalesavailability ? "present" : "absent")}\n" +
					$"Type: {TypeName}.\nSpecies: {Species}.\nFamily: {Family}.\nAreaOfBeing: {AreaOfBeing}.\nName:{Name}.\n\n" +
					$"{ConsumeringNutries()}\n{BirthForm()}\n{Migration()}\n{AbillityToMoveSweamingly()}\n{IndustryValue()}";
		}
	}
}
