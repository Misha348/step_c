﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _19._09._19_Interfaces
{
	public class Reptile: Animal
	{
		protected string Family { get; set; }
		protected string BloodTemperatureType { get; set; }
		protected string AncientnessOfBeing { get; set; }

		public Reptile(string typeName, string areaOfBeing, string species, string bloodTemperatureType, string ancientnessOfBeing) :
			base(typeName, areaOfBeing, species)
		{
			BloodTemperatureType = bloodTemperatureType;
			AncientnessOfBeing = ancientnessOfBeing;
		}

		public override string ConsumeringNutries()
		{
			return "REPTILES consume nutries character for REPTILES.";
		}

		public override string BirthForm()
		{
			return "REPTILES lay egs. ";
		}

		public override string Migration()
		{
			return "REPTILES has ability to migrate.";
		}

		public virtual string FeaturesOfProtectuon()
		{
			return $"some representers have features for protection";
		}
	}

	public class Chameleon : Reptile, ICrawling
	{
		public string EyesFeature { get; set; }
		public string Name { get; set; }

		public Chameleon(string typeName, string areaOfBeing, string species, string bloodTemperatureType, string ancientnessOfBeing, string eyesFeature, string name) :
			base(typeName, areaOfBeing, species, bloodTemperatureType, ancientnessOfBeing)
		{
			EyesFeature = eyesFeature;
			Name = name;
		}

		public override string ConsumeringNutries()
		{
			return "CHAMELEONS consume bugs, small boneless, small lizards.";
		}

		public override string BirthForm()
		{
			return "CHAMELEONS lay egs and some species иупуе alive posterities. ";
		}

		public override string Migration()
		{
			return "CHAMELEONS allmost dont migrate.";
		}

		public override string FeaturesOfProtectuon()
		{
			return $"CHAMELEONS for protection might to chenge skin colour";
		}

		public string AbillityToMoveCrawlingly()
		{
			return $"CHAMELEONS has ability to move crawlingly";
		}

		public override string ToString()
		{
			return $"AncientnessOfBeing: {AncientnessOfBeing}.\nBloodTemperatureType{BloodTemperatureType}" +
				   $"Type: {TypeName}.\nSpecies: { Species}.\nFamily: { Family}.\nAreaOfBeing: { AreaOfBeing}.\nName: { Name}.\n\n" +
				   $"{ConsumeringNutries()}.\n{BirthForm()}\n" +
				   $"{Migration()}.\n{FeaturesOfProtectuon()}.\n" +
				   $"{AbillityToMoveCrawlingly()}";
		}
	}
}
