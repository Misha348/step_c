﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _18._09._19
{
	public abstract class Figure
	{
		protected string Name { get; set; }		

		public abstract double GetFigureSquare();
		public abstract double GetFigurePerimetr();
		public abstract string GetFiguresName();

		public Figure(string name)
		{
			Name = name;
		}

		public override string ToString()
		{
			return "some abstract figure";
		}

		public virtual void ShowFigure()
		{
			Console.WriteLine("some abstract figure");
		}
	}

	public class RightangledTriangle: Figure
	{
		double Gipothenusa { get; set; }
		double Cathet_a { get; set; }
		double Cathet_b { get; set; }


		public RightangledTriangle(double cathet_a, double cathet_b, double newGipothenusa, string newName): base(newName)
		{			
			Cathet_a = cathet_a;
			Cathet_b = cathet_b;
			Gipothenusa = newGipothenusa;
		}

		public override string GetFiguresName()
		{
			return Name;
		}

		public override double GetFigureSquare()
		{
			double square = 0;
			return square = (Cathet_a * Cathet_b) / 2;
		}

		public override double GetFigurePerimetr()
		{
			double perim = 0;
			return perim = Cathet_a + Cathet_b + Gipothenusa;
		}

		public override string ToString()
		{
			return $"cathe a: {Cathet_a}\ncathet b: {Cathet_b}\ngipothen: {Gipothenusa}" +
				$"\nperimetr: {GetFigurePerimetr()}\nsquare: {GetFigureSquare()}.";
		}

		public override void ShowFigure()
		{
			Console.WriteLine($"name: {Name}\ncathe a: {Cathet_a}\ncathet b: {Cathet_b}\ngipothen: {Gipothenusa}" +
				$"\nperimetr: {GetFigurePerimetr()}\nsquare: {GetFigureSquare()}.");
		}
	}

	public class Quadrat: Figure
	{
		public double Side_a { get; set; }

		public Quadrat(double side_a, string newName) : base(newName)
		{
			Side_a = side_a;
		}

		public override string GetFiguresName()
		{
			return Name;
		}

		public override double GetFigureSquare()
		{
			double square = 0;
			return square = Math.Pow(Side_a, 2);
		}

		public override double GetFigurePerimetr()
		{
			double perim = 0;
			return perim = (Side_a * 4);
		}

		public override string ToString()
		{
			return $"side a: {Side_a}\nperimetr: {GetFigurePerimetr()}\nsquare: {GetFigureSquare()}";
		}

		public override void ShowFigure()
		{
			Console.WriteLine($"name: {Name}\nside a: {Side_a}\nperimetr: {GetFigurePerimetr()}\nsquare: {GetFigureSquare()}");
		}
	}

	public class Circle: Figure
	{
		public double Radius { get; set; }

		public Circle(double rad, string name): base(name) 
		{
			Radius = rad;
			Name = name;
		}

		public override string GetFiguresName()
		{
			return Name;
		}

		public override double GetFigureSquare()
		{
			double square = 0;
			return square = Math.PI * Math.Pow(Radius, 2);
		}

		public override double GetFigurePerimetr()
		{
			double perim = 0;
			return perim = Math.PI * (2 * Radius);
		}

		public override void ShowFigure()
		{
			Console.WriteLine($"name: {Name}\nradius: {Radius}\nlength: {GetFigurePerimetr()}\nsquare: {GetFigureSquare()}");
		}
	}

	public class CompositFigure
	{
		public List<Figure> figures;
		public double TotalSquare { get; set; }

		public CompositFigure(params Figure[] newFigures )
		{
			figures = new List<Figure>();
			foreach (var figure in newFigures)
			{
				figures.Add(figure);
			}
		}

		public void GetPartsOfFigure()
		{
			foreach (var figure in figures)
			{
				Console.WriteLine(figure.GetFiguresName());
			}
		}

		public double GetTotalSquare()
		{
			foreach (var figure in figures)
			{
				TotalSquare += figure.GetFigureSquare();
			}
			return TotalSquare;
		}

		public override string ToString()
		{
			return $"total sqr: {GetTotalSquare()}";
		}
		public void ShowCompositFigureInfo()
		{
			Console.WriteLine("composit figure:\n".ToUpper());
			GetPartsOfFigure();
			Console.WriteLine($"\ntotal sqr: {GetTotalSquare()}");
		}
	}

	class Program
	{
		static void Main(string[] args)
		{
			Figure[] figures = new Figure[]
			{
				new RightangledTriangle(5, 4, 7, "RightangledTriangle".ToUpper()),
				new Quadrat(8, "quadrat".ToUpper()),
				new Circle(5.5, "circle".ToUpper()),
				new Quadrat(10, "quadrat".ToUpper()),

			};

			foreach (var figure in figures)
			{
				figure.ShowFigure();
				Console.WriteLine(new string('=', 13));
				Console.WriteLine();
			}

			CompositFigure composFigure = new CompositFigure(figures);
			composFigure.ShowCompositFigureInfo();
		}
	}
}
