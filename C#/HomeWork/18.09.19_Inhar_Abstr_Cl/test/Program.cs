﻿//using System;
//using System.Collections.Generic;

//namespace SnakeGame
//{
//	class Point
//	{
//		public int X { get; set; }
//		public int Y { get; set; }
//		public Point(int x, int y)
//		{
//			X = x;
//			Y = y;
//		}
//		public override bool Equals(object obj)
//		{
//			Point point = obj as Point;
//			return point != null &&
//				   X == point.X &&
//				   Y == point.Y;
//		}
//	}

//	class Snake
//	{
//		public ConsoleKey Direction { get; set; }
//		public List<Point> body;
//		private bool IsNewApple = false;


//		public Snake()
//		{
//			body = new List<Point>();
//			body.Add(new Point(9, 5));
//			body.Add(new Point(8, 5));
//			body.Add(new Point(7, 5));
//			body.Add(new Point(6, 5));
//			Direction = ConsoleKey.D;
//        }

//		public void SetDirection()
//		{
//			if (Console.KeyAvailable)
//				Direction = Console.ReadKey().Key;
//		}
		
//		public void Update()
//		{
//			SetDirection();
//			if (!IsNewApple)
//				body.RemoveAt(body.Count - 1);
//			else
//				IsNewApple = false;

//			Point point = new Point(body[0].X, body[0].Y);
//            switch (Direction)
//			{
//				case ConsoleKey.W:
//					point.Y--;
//					break;
//				case ConsoleKey.D:
//					point.X++;
//					break;
//				case ConsoleKey.S:
//					point.Y++;
//					break;
//				case ConsoleKey.A:
//					point.X--;
//					break;
//				default:
//					break;
//			}

//            if (body.Contains(point))
//			{
//				Console.SetCursorPosition(10, 10);
//				Console.WriteLine("Fail...");
//				Console.ReadLine();
//			}
//			body.Insert(0, point);
//        }

//		public void SetPixel(int x, int y)
//		{
//			Console.SetCursorPosition(x + 1, y + 1);
//			Console.WriteLine('#');
//		}
//		public void Render()
//		{
//			Console.Clear();
//			Console.WriteLine("Points: " + body.Count);
//			foreach (var point in body)
//				SetPixel(point.X, point.Y);
//		}
//		public void NewApple()
//		{
//			IsNewApple = true;
//		}
//	}

//	class Game
//	{
//		public List<Point> apples;
//		public Snake snake;
//		private Random random;
//		public Game()
//		{
//			random = new Random();
//			apples = new List<Point>();
//			apples.Add(new Point(10, 10));
//		}

//		private void AppleToSnake()
//		{
//			for (int i = 0; i < apples.Count; i++)
//			{
//				if (apples[i].Equals(snake.body[0]))
//				{
//					apples.RemoveAt(i);
//					snake.NewApple();
//                    apples.Add(new Point(random.Next(0, 19), random.Next(0, 19)));
//				}
//			}
//        }

//		public void run()
//		{
//			snake = new Snake();
//            while (true)
//			{
//				System.Threading.Thread.Sleep(180 - (snake.body.Count * 4));
//				snake.Update();
//				snake.Render();
//                AppleToSnake();
//				foreach (var point in apples)
//					snake.SetPixel(point.X, point.Y);
//			}
//		}
//	}

//	class Program
//	{
//		static void Main(string[] args)
//		{
//			Console.SetWindowSize(40, 40);
//			Console.SetBufferSize(40, 40);
//			Game game = new Game();
//			game.run();
//		}
//	}



//}