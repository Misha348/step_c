﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _17._09._19_Indexer
{
    class MyOutOfRangeExeption: Exception
    {
        public void OutOfRangeExeptionMessage()
        {
            Console.WriteLine("index out of range");
        }
    }

    class RangeOfArray
    {
        private int _upperIndex;
        public int UpperIndex { get; set; }
        private int _lowerIndex;
        public int LowerIndex { get; set; }

        private int _size;
        public int Size
        {
            get
            {
                return _size;
            }
        }

        private int[] _arr;
        public int this[int index]
        {
            get
            {
                if (index >= LowerIndex && index <= UpperIndex)
                    return _arr[index - LowerIndex];
                else
                    throw new MyOutOfRangeExeption();                   
            }

            set
            {
                if (index >= LowerIndex && index <= UpperIndex)
                    _arr[index - LowerIndex] = value;
            }
        }      

        public RangeOfArray()
        {
            LowerIndex = 0;
            UpperIndex = 10;
        }

        public RangeOfArray(int lowerIndex, int upperIndex)
        {
            UpperIndex = upperIndex;
            LowerIndex = lowerIndex;

            if(LowerIndex < 0 && UpperIndex > 0)
            _arr = new int[UpperIndex + (-LowerIndex)];

            else if(LowerIndex > 0 && UpperIndex > 0)
                _arr = new int[UpperIndex - LowerIndex];
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("enter lower array index: ");
            var lowIndex = int.Parse(Console.ReadLine());

            Console.Write("enter upper array index: ");
            var uppIndex = int.Parse(Console.ReadLine());

            RangeOfArray arr = new RangeOfArray(lowIndex, uppIndex);


            for (int i = lowIndex; i < uppIndex; i++)
            {
                arr[i] = int.Parse(Console.ReadLine());
                Console.WriteLine($"{i} el = {arr[i]}");
            }  
        }
    }
}
