﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace _1._10._19_FileSystem
{
	class Program
	{
		public static void Menu()
		{
			Console.WriteLine("\t\tFILE MANAGER\n");
			Console.ForegroundColor = ConsoleColor.Yellow;
			Console.WriteLine("FOLDERS");
			Console.ForegroundColor = ConsoleColor.White;
			Console.WriteLine("[ 1 ] - CREATE FOLDER.\n[ 2 ] - DELETE FOLDER.\n[ 3 ] - WATCH FOLDER.\n" + 
							  "[ 4 ] - WATCH FILES IN FOLDER.\n[ 5 ] - WATCH ALL FILES IN FOLDER\n");
			Console.ForegroundColor = ConsoleColor.Yellow;
			Console.WriteLine("\nFILES");
			Console.ForegroundColor = ConsoleColor.White;
			Console.WriteLine("[ 6 ] - CREATE FILE.\n[ 7 ] - DELETE FILE.\n[ 8 ] - REPLACE FILE.\n" +
							  "[ 9 ] - WATCH FILES INFO.\n[ 0 ] - EXIT MANAGER\n");

			Console.ForegroundColor = ConsoleColor.Red;
			Console.WriteLine("\nMOVE");
			Console.ForegroundColor = ConsoleColor.White;
			Console.WriteLine("[ 10 ] - GET CURRENT DIRECTORY.\n[ 11 ] - MOVING BY FOLDERS");
		}

		static void Main(string[] args)
		{
			Console.OutputEncoding = System.Text.Encoding.UTF8;
			Menu();
			bool exit = false;
			while (exit == false)
			{
				LINK_2:
				Console.WriteLine("\naction: ");
				var action = Convert.ToInt16(Console.ReadLine());
				switch (action)
				{
					case 1:
						Console.WriteLine("enter path and name for folder: ");						
						string path = Console.ReadLine();
						DirectoryInfo folder = new DirectoryInfo(path);
						if (!folder.Exists)
						{
							folder.Create();
							Console.WriteLine($"folder {folder.Name} created");
						}
						else
							Console.WriteLine($"folder {folder.Name} already exist");
						Console.WriteLine(new string('=', 30));
						break;
					case 2:
						Console.WriteLine("enter path and name for deleting folder: ");					
						string path_1 = Console.ReadLine();

						DirectoryInfo folderDel = new DirectoryInfo(path_1);
						folderDel.Delete();

						Console.WriteLine("folder deleted.");
						Console.WriteLine(new string('=', 30));
						break;
					case 3:
						Console.WriteLine("enter path you want to chek:");
						string path_2 = Console.ReadLine();
						DirectoryInfo watcher = new DirectoryInfo(path_2);

						foreach (var _folder in watcher.GetDirectories())
						{
							Console.WriteLine($"Folder: {_folder.Name}");
						}
						break;
					case 4:
						Console.WriteLine("enter path you want to chek:");
						string path_3 = Console.ReadLine();
						DirectoryInfo watcher1 = new DirectoryInfo(path_3);

						foreach (var _file in watcher1.GetFiles())
						{
							Console.WriteLine($"File: {_file.Name}");
						}

						break;
					case 5:
						Console.WriteLine("enter path you want to chek:");
						string path_4 = Console.ReadLine();
						DirectoryInfo watcher2 = new DirectoryInfo(path_4);

						foreach (var _file in watcher2.GetFileSystemInfos())
						{
							Console.WriteLine($"File: {_file.Name}");
						}

						break;
					case 6:
						Console.WriteLine("enter path, name, extension for file: ");
						string filePath = Console.ReadLine();
						FileInfo file_ = new FileInfo(filePath);
						if (!file_.Exists)
						{
							file_.Create();
							Console.WriteLine($"file {file_.Name} created");
						}
						else
							Console.WriteLine($"folder {file_.Name} already exist");
						Console.WriteLine(new string('=', 30));
						break;
					case 7:
						Console.WriteLine("enter path, name, extension for deleting file: ");
						string _filePath = Console.ReadLine();

						FileInfo delFile = new FileInfo(_filePath);
						delFile.Delete();

						Console.WriteLine("file deleted.");
						Console.WriteLine(new string('=', 30));
						break;
					case 8:
						Console.WriteLine("enter path, name, extension of file for moving: ");
						string fileMovingFrom = Console.ReadLine();
						Console.WriteLine("enter path, name, extension of file in new place: ");
						string fileMovingTo = Console.ReadLine();

						FileInfo moveFile = new FileInfo(fileMovingFrom);
						if (moveFile.Exists)
						{
							moveFile.MoveTo(fileMovingTo);
							Console.WriteLine($"file {moveFile.Name} removed");
						}
						break;
					case 9:
						Console.WriteLine("enter path, name, extension of file: ");
						string fileData = Console.ReadLine();
						FileInfo file = new FileInfo(fileData);
						if (file.Exists)
						{
							Console.WriteLine($"FileName : {file.Name}");
							Console.WriteLine($"Path     : {file.FullName}");
							Console.WriteLine($"FOLder   : {file.Directory}");
						}
						else
						{
							Console.WriteLine("File not existed.");
						}
						break;
					case 10:
						DirectoryInfo curFold = new DirectoryInfo(Directory.GetCurrentDirectory());
						Console.WriteLine($"cur fold: { curFold.Name}" );
						break;
					case 11:
						DirectoryInfo currFold = new DirectoryInfo(Directory.GetCurrentDirectory());
					
						string oneMoveUp = "..\\";
						string newPath = null;
					LINK_1:
						Console.WriteLine("press [ \u2191 ] to move up / press [ ESC ] to escape");
						ConsoleKeyInfo keyInfo = Console.ReadKey();
						switch (keyInfo.Key)
						{
							case ConsoleKey.UpArrow:
								oneMoveUp += "..\\";
								newPath = Path.GetFullPath(Path.Combine(currFold.ToString(), oneMoveUp));
								break;
							case ConsoleKey.Escape:
								goto LINK_2;
							default:
								Console.WriteLine("uncorrect input");
								break;							
						}
						

						Console.ForegroundColor = ConsoleColor.Magenta;						
						Console.WriteLine(newPath.ToString());
						Console.ForegroundColor = ConsoleColor.White;

						DirectoryInfo _watcher = new DirectoryInfo(newPath);

                        foreach (var _folder in _watcher.GetDirectories())
                        {
                            Console.WriteLine($"Folder: {_folder.Name}");
                        }						
						goto LINK_1;						
					case 0:
						Console.WriteLine("end of work.");
						exit = true;
						break;
				}
			}
		}
	}
}
