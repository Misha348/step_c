﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _10._09._19_Class_Creating
{
	partial class Store
    {
        public void AddProd(Product newProd)
        {
			if (newProd == null)
				throw new ArgumentNullException();
			else
            products.Add(newProd);      // ArgumentNullExeption
        }

        public Product GetProduct(int prodID)
        {
			if(products == null)
				throw new StoreEmptyExeption();
			foreach (var product in products)
            {
                if (product.ProductInfo.prodID == prodID)   // ProductNotFoundExeption + StoreEmptyExeption
                    return product;				

			}
			throw new ProductNotFoundExeption();
			
        }

        public List<Product> DelProduct(int prodID)   // ProductNotFoundExeption(specified prod ) + StoreEmptyExeption(no any prod)
        {
			
            if (products == null)
				throw new StoreEmptyExeption();
			
            foreach (var product in products)
            {
				if (product.ProductInfo.prodID == prodID)
				{
					products.RemoveAt(prodID - 1);					
					return products;					
				}   							
			}
			throw new ProductNotFoundExeption();
		}

		public override string ToString()
		{
			foreach (var product in products)
			{
				return product.ToString();
			}
			return ""; 
		}

		public void  ShowProducts()
		{
			foreach (var product in products)
			{
				product.ShowPoduct();
			}
			
		}

	}
}
