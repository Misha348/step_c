﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _10._09._19_Class_Creating
{
    public class Provider
    {
        private string name;
        public string Name
        {
            get
            {
                if (Char.IsLower(name[0]))
					return Char.ToUpper(name[0]) + name.Substring(1, name.Length - 1);					
				else
                    return name;
			}
			set
            {
                if (String.IsNullOrEmpty(value) || String.IsNullOrWhiteSpace(value))
                    name = "No name";
                else
                    name = value;
            }
        }
        
        public string country;
        public string Country
        {
            get
            {
                if (Char.IsLower(country[0]))
                    return Char.ToUpper(country[0]) + country.Substring(1, country.Length - 1);
                else
                return country; 
            }

            set
            {
				if (String.IsNullOrEmpty(value) || String.IsNullOrWhiteSpace(value))
					country = "no name";
				else country = value;

			}
        }
        private bool isPayVAT;
        public bool IsPayVAT
        {
            get
            {
                return isPayVAT;
            }

            private set
            {
                if (raiting <= 2)
                {
                    isPayVAT = false;
                }
                else
                    isPayVAT = true;
            }
        }

        private short raiting;
        public short Raiting
        {
            get
            {
                return raiting;
            }

            set
            {
				Random rnd = new Random();
				raiting = (short)rnd.Next(1, 5);

				if (raiting < 1 || raiting > 5)
                {
                    raiting = 0;                   
                }
                   
            }
        }

        public Provider()
        {
            SetInfo(out string name, out string country);
            Name = name;
            Country = country;
			Raiting = raiting;
			IsPayVAT = isPayVAT;
        }

        public void SetInfo(out string name, out string country)
        {
			Console.WriteLine($"\t\tProvider");
			Console.Write($"enter name: ");           
            name = Console.ReadLine();

            Console.Write($"enter country: ");
            country = Console.ReadLine();          
        }

		public override string ToString()
		{
			return  $"\nNAME: {Name}.\n" +
					$"VAT: {(isPayVAT ? "pay VAT." : "dont pay VAT.")} \nRAITING: {new string('*', raiting)}.\n" +
					$"country: {Country}.\n";
		}

		public void ShowProvider()
		{
			Console.WriteLine($"\nNAME: {Name}.\n" +
			   $"VAT: {(isPayVAT ? "pay VAT." : "dont pay VAT.")} \nRAITING: {new string('*', raiting)}.\n" +
			   $"country: {Country}.\n");
		}
	}
}


