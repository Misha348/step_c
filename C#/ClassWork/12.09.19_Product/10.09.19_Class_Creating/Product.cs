﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _10._09._19_Class_Creating
{
    public enum ProductType : byte
    { product = 1,
      notproduct,
      electronic,
      tabaco,
      clothes,
    }

    public class ProductInfo
    {
        public ProductType prType;
        public string name;
        public int prodID;
        public decimal price;
        readonly public int amount;
        readonly public DateTime produceDate;

       

        public ProductInfo(int amount, ProductType prType, string name, decimal price, int prodID)
        {
            this.name = name;
            this.amount = amount;
            this.prodID = prodID;
            this.price = price;
            this.prType = prType;
            produceDate = DateTime.Now;
        }
    }

    public class Product
    {
        public ProductInfo ProductInfo { get; private set; }
        public static int Amount { get; private set; }       

        static Product() => Amount = 0;

        public Product()
        {
            Amount++;
            SetData(out decimal prise, out string name, out ProductType prType, out int prodID);
            ProductInfo = new ProductInfo(Amount, prType, name, prise, prodID);
		}

        public void SetData(out decimal price, out string name, out ProductType prType, out int prodID)
        {
			Console.WriteLine("\t\tProduct");
            foreach (string item in Enum.GetNames(typeof(ProductType)))
            {
                Console.WriteLine($"{item}");
            }
            Console.WriteLine();

            Console.Write($"enter product type: ");
            while (!ProductType.TryParse(Console.ReadLine(), out prType))
            {
                Console.WriteLine("incorrect input, try again: ");
            }    
           
            Console.Write($"enter name: ");
            name = Console.ReadLine();            

            Console.Write($"enter ID: ");
            prodID = int.Parse(Console.ReadLine());           

            Console.Write($"enter price: ");
            while (!decimal.TryParse(Console.ReadLine(), out price))
            {                               
                Console.WriteLine("incorrect input, try again: ");
            }           
        }       

        public override string ToString()
        {
            return $"prod. type: {ProductInfo.prType}\n" +
                $"name: {ProductInfo.name}\n" +
                $"ID: {ProductInfo.prodID}\n" +
                $"price: {ProductInfo.price}\n" +
                $"amount: {ProductInfo.amount}\n " +
                $"date of produce: {ProductInfo.produceDate.ToShortDateString()}";
        }

		public void ShowPoduct()
		{
			 Console.WriteLine($"prod. type: {ProductInfo.prType}\n" +			
				$"name: {ProductInfo.name}\n" +
				$"ID: {ProductInfo.prodID}\n" +
				$"price: {ProductInfo.price}\n" +
				$"amount: {ProductInfo.amount}\n " +
				$"date of produce: {ProductInfo.produceDate.ToShortDateString()}");
			Console.WriteLine(new string('=', 30));
		}
	}
}
