﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _10._09._19_Class_Creating
{
    partial class Store
    {
        private List<Product>products;

        static int prodAmount;
        private decimal _totalPrice { get; set; }

        static Store()
        {
            prodAmount = 0;
        }

        public Store()
        {
           products = new List<Product>();
        }

        public int GetCountOfProd()
        {
            foreach (Product product in products)
            {
                prodAmount++;
            }
            return prodAmount;
        }

        public int GetCountOfProdBySomeType(ProductType prType)
        {
            var someTypeProd = 0;
            foreach (var product in products)
            {
                if (product.ProductInfo.prType == prType)
                someTypeProd++;                
            }
            return someTypeProd;           
        }

        public decimal GetTotalPrice()
        {           
            foreach (var product in products)
            {
                _totalPrice += product.ProductInfo.price;
            }
            return _totalPrice;
        }

        public decimal GetAverPrice()
        {
            decimal averPrice = 0;
            foreach (var product in products)
            {
                averPrice = _totalPrice / prodAmount;
            }
            return averPrice;
        }		
    }
}
