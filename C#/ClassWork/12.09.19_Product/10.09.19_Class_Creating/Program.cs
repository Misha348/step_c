﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace _10._09._19_Class_Creating
{
    class Program
    {
		static void ScreenCleaner(int time)
		{
			Thread.Sleep(time);
			Console.Clear();
		}

		static void Main(string[] args)
        {
			Store store = new Store();

			Provider provider = new Provider();
			provider.ShowProvider();

			Product[] products = new Product[3];
			for (int i = 0; i < products.Length; i++)
			{
				try
				{
					products[i] = new Product();
					store.AddProd(products[i]);
					Console.Clear();
				}
				catch (ArgumentNullException ex)
				{
					Console.WriteLine($"message: {ex.Message}"); 
				}				
			}			

			store.ShowProducts();
			ScreenCleaner(5000);

			try
			{
				store.GetProduct(2).ShowPoduct();
				ScreenCleaner(3000);

				Console.WriteLine($"quantity of all products: {store.GetCountOfProd()}");

				Console.Write("choose some prod type for counting: ");
				var choise = ProductType.TryParse(Console.ReadLine(), out ProductType type);
				Console.WriteLine($"products quantity of chosen product type: {store.GetCountOfProdBySomeType(type)}");

				Console.WriteLine($"total price of all products: {store.GetTotalPrice()}");

				Console.WriteLine($"average price of all products: {store.GetAverPrice()}");
				ScreenCleaner(5000);

				store.DelProduct(2);
				store.ShowProducts();
			}
			catch (StoreEmptyExeption ex)
			{
				ex.StoreEmptyExeptionMessage();
			}
			catch (ProductNotFoundExeption ex)
			{
				ex.ProductNotFoundExeptionMessage();
			}
		}
    }
}
