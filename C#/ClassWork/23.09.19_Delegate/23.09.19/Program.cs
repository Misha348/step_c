﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _23._09._19
{
    class Program
    {
        public class Calc
        {
            public double Plus(double a, double b)
            {
                return a + b;
            }

            public double Substruct(double a, double b)
            {
                return a - b;
            }

            public double Multiply(double a, double b)
            {
                return a * b;
            }

            public double Divide(double a, double b)
            {
                if (b == 0)
                    throw new DivideByZeroException();
                return a / b;
            }

            public double Sinus(double a)
            {
                return Math.Sin(a);
            }

            public double Cosinus(double a)
            {
                return Math.Cos(a);
            }

            public double Tangens(double a)
            {
                return Math.Tan(a);
            }


        }

        public delegate double MathOperation(double a, double b );
        public delegate double TrreegonometOperation(double a );


        static void Main(string[] args)
        {
            Calc calc = new Calc();

            Console.WriteLine("enter 1-st digit: ");
            double a = double.Parse(Console.ReadLine());

            Console.WriteLine("enter 2-nd digit: ");
            double b = double.Parse(Console.ReadLine());

            MathOperation[] operations = new MathOperation[4];
            operations[0] = new MathOperation(calc.Plus);
            operations[1] = new MathOperation(calc.Substruct);
            operations[2] = new MathOperation(calc.Multiply);
            operations[3] = new MathOperation(calc.Divide);


          
            TrreegonometOperation[] operat = new TrreegonometOperation[3];
            operat[0] = new TrreegonometOperation(calc.Sinus);
            operat[1] = new TrreegonometOperation(calc.Cosinus);
            operat[2] = new TrreegonometOperation(calc.Tangens);

            Console.WriteLine("[ 1 ] - math op.\t[ 2 ] - thregonom op.");
            var choise = int.Parse(Console.ReadLine());          
           
            switch (choise)
            {
                case 1:
                    try
                    {                       
                        Console.WriteLine("choose operation: [ 1 ] - plus. [ 2 ] - minus. [ 3 ] - multiply. [ 4 ] - divide");
                        var choise1 = int.Parse(Console.ReadLine());
                        Console.WriteLine(operations[choise1 - 1](a, b));                       
                    }
                    catch (DivideByZeroException ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                    break;
                case 2:
                    Console.WriteLine("choose operation: [ 1 ] - sin. [ 2 ] - cos. [ 3 ] - tan.");
                    var choise2 = int.Parse(Console.ReadLine());
                    Console.WriteLine(operat[choise2 - 1]( b ));                   
                    break;
                default:
                    break;
            }
        }
    }
}
