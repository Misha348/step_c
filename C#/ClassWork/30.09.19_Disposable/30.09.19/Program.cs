﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _30._09._19
{
    public class DrowColourFigure: IDisposable
    {
        public void DrowFigure()
        {
            for (int i = 0; i < 10; i++)
            {               
                Console.WriteLine("******************");
            }
        }

        public void Dispose()
        {
            Console.ResetColor();
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            DrowColourFigure colourFigure = new DrowColourFigure();
            Console.WriteLine("choose colour: ");
            string colourName = Console.ReadLine();

            Console.ForegroundColor = (ConsoleColor)Enum.Parse(typeof(ConsoleColor), colourName);
            
            //Type type = typeof(ConsoleColor);
            //foreach (var colourName in Enum.GetNames(type))
            //{
            //    Console.ForegroundColor = (ConsoleColor)Enum.Parse(type, colourName);
            //    Console.WriteLine(colourName);
            //}

            colourFigure.DrowFigure();
            colourFigure.Dispose();
            colourFigure.DrowFigure();
            Console.WriteLine(new string('=', 30));

            Console.WriteLine("choose colour: ");
            string colourName1 = Console.ReadLine();
            Console.ForegroundColor = (ConsoleColor)Enum.Parse(typeof(ConsoleColor), colourName1);
            DrowColourFigure colourFigure3 = new DrowColourFigure();
            try
            {
                colourFigure3.DrowFigure();
            }
            finally
            {
                colourFigure3.Dispose();
            }
            colourFigure.DrowFigure();
            Console.WriteLine(new string('=', 30));

            Console.WriteLine("choose colour: ");
            string colourName2 = Console.ReadLine();
            Console.ForegroundColor = (ConsoleColor)Enum.Parse(typeof(ConsoleColor), colourName2);
            using (var colourFigure2 = new DrowColourFigure())
            {
                colourFigure2.DrowFigure();
            }
            colourFigure.DrowFigure();

            Console.WriteLine(new string('=', 30));
            colourFigure.DrowFigure();

         

        }
    }
}
