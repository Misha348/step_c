﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _03._09._19_Reflector_Dotficsators
{
    class Program
    {
        static void Main(string[] args)
        {
           Random rnd = new Random();
           var a = rnd.Next(-100, 100);
            
            for (int i = 0; i < 5; i++)
            {
                Console.ForegroundColor = ConsoleColor.DarkGreen;
                Console.WriteLine($"{i+1} - {a}");
            }
            Console.ForegroundColor = ConsoleColor.DarkYellow;
            Console.WriteLine(new string('=', 40));

           int[] digits = new int[10];
            for (int i = 0; i < digits.Length; i++)
            {
                digits[i] = rnd.Next(-10, 10);
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.WriteLine($"{i+1} - {digits[i]}");
            }
            Console.ReadKey();
        }
      
    }
   
}
