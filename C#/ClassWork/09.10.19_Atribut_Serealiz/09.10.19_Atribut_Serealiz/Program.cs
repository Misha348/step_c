﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _09._10._19_Atribut_Serealiz
{
    [Serializable]
    public class User
    {
        [Required(ErrorMessage = "name must be inputed")]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "to short name")]
        [RegularExpression("^A-Z", ErrorMessage = "name must begin from Upper letter")]
        public string Name { get; set; }


        public string Login { get; set; }
        public string Password { get; set; }
        public string Confirm_Password { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public int Age { get; set; }
        public int? i = null;

        public User(string name, string login, string passwd, string confirm_passwd, string email, string phone, int age)
        {
            Name = name;
            Login = login;
            Password = passwd;
            Confirm_Password = confirm_passwd;
            Email = email;
            Phone = phone;
            Age = age;
        }

        public override string ToString()
        {
            return $"Name: {Name}.\nLogin: {Login}.\nPassword: {Password}.\nConfirm passwd: {Confirm_Password}.\n" +
                   $"Email: {Email}.\nPhone {Phone}.\nAge: {Age}";
        }
    }
    class Program
    {

        static void Main(string[] args)
        {
            Console.Write("enter name: ");
            var usName = Console.ReadLine();

            Console.Write("enter login: ");
            var usLogin = Console.ReadLine();

            Console.Write("enter password: ");
            var usPassword = Console.ReadLine();

            Console.Write("enter password confirmation: ");
            var usPasswdConfirmat = Console.ReadLine();

            Console.Write("enter email: ");
            var usEmail = Console.ReadLine();

            Console.Write("enter phone: ");
            var usPhone = Console.ReadLine();

            Console.Write("enter age: ");
            var usAge = int.Parse(Console.ReadLine());

            User user = new User(usName, usLogin, usPassword, usPasswdConfirmat, usEmail, usPhone, usAge);

            BinaryFormatter binFormater = new BinaryFormatter();
            using (Stream fstream = File.Create("test.bin"))
                binFormater.Serialize(fstream, user);
            Console.WriteLine("BinarySerialize OK!\n");

            User user2 = null;
            using (Stream fstream = File.OpenRead("test.bin"))
                user2 = (User)binFormater.Deserialize(fstream);
            Console.WriteLine(user2);

        }

      
    }
}
