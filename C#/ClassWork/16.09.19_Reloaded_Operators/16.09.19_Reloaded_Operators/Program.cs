﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

class RangeException: Exception
{
	public void RangeExceptionMessage()
	{
		Console.WriteLine("impossible create negative side");
	}
}

namespace _16._09._19_Reloaded_Operators
{
    class Rectangle
    {
        public int Side_A { get; set; }
		public int Side_B { get; set; }
		public int Squarer { get; set; }

        public Rectangle()
        {            
        }

        public int GetRectangleSquare()
        {
            Squarer = Side_A * Side_B;
            return Squarer;
        }

        public override string ToString()
        {
            return $"side A = {Side_A}.\nside B = {Side_B}";
        }
		public void ShowRectangle()
		{
			Console.WriteLine($"side A = {Side_A}.\nside B = {Side_B}"); 
		}

		public override bool Equals(object obj)
        {
            Rectangle rec = (Rectangle)obj;
            return (Side_A == rec?.Side_A && Side_B == rec?.Side_B);
        }

        public override int GetHashCode()
        {
            var hashCode = 1861411795;
            hashCode = hashCode * -1521134295 + Side_A.GetHashCode();
			hashCode = hashCode * -1521134295 + Side_B.GetHashCode();
			return hashCode;
        }

        public static Rectangle operator ++(Rectangle rec)
        {
            rec.Side_A++;
			rec.Side_B++;
			return rec;
        }

        public static Rectangle operator --(Rectangle rec)
        {
            rec.Side_A--;
			rec.Side_B--;
			return rec;
        }

        public static Rectangle operator +(Rectangle rec1, Rectangle rec2)
        {
			Rectangle rec3 = new Rectangle
			{
				Side_A = rec1.Side_A + rec2.Side_A,
				Side_B = rec1.Side_B + rec2.Side_B

			};
            return rec3;
        }

        public static Rectangle operator -(Rectangle rec1, Rectangle rec2)
        {
            Rectangle rec3 = new Rectangle
            {
                Side_A = rec1.Side_A - rec2.Side_A,
				Side_B = rec1.Side_B - rec2.Side_B
			};
			if (rec3.Side_A < 0 || rec3.Side_B < 0 || (rec3.Side_A < 0 && rec3.Side_B < 0) )
				throw new RangeException();            
            return rec3;
        }

		public static Rectangle operator *(Rectangle rec1, Rectangle rec2)
		{
			Rectangle rec3 = new Rectangle
			{
				Side_A = rec1.Side_A * rec2.Side_A,
				Side_B = rec1.Side_B * rec2.Side_B
			};
			return rec3;
		}
		public static Rectangle operator /(Rectangle rec1, Rectangle rec2)
		{
			Rectangle rec3 = new Rectangle
			{
				Side_A = rec1.Side_A / rec2.Side_A,
				Side_B = rec1.Side_B / rec2.Side_B
			};			
			return rec3;
		}

		public static bool operator ==(Rectangle rec1, Rectangle rec2)
        {
            return rec1.Equals(rec2);
        }
        public static bool operator !=(Rectangle rec1, Rectangle rec2)
        {
            return !(rec1 == rec2);
        }

        public static bool operator <(Rectangle rec1, Rectangle rec2)
        {
            return rec1.Side_A + rec1.Side_B < rec2.Side_A + rec2.Side_B;
        }
        public static bool operator >(Rectangle rec1, Rectangle rec2)
        {
			return rec1.Side_A + rec1.Side_B > rec2.Side_A + rec2.Side_B;
		}

		public static bool operator <=(Rectangle rec1, Rectangle rec2)
		{
			return rec1.Side_A + rec1.Side_B <= rec2.Side_A + rec2.Side_B;
		}
		public static bool operator >=(Rectangle rec1, Rectangle rec2)
		{
			return rec1.Side_A + rec1.Side_B >= rec2.Side_A + rec2.Side_B;
		}


		public static bool operator true(Rectangle rec)
        {
            return rec.Side_A != 0 || rec.Side_B != 0;
        }
        public static bool operator false(Rectangle rec)
        {
            return rec.Side_A == 0 || rec.Side_B == 0;
		}


        public static explicit operator int(Rectangle rec)
        {
            return ( (rec.Side_A + rec.Side_B) * 2 );
        }

        public static explicit operator Quadrat(Rectangle rec)
        {
			// return new Quadrat( (rec.Side_A + rec.Side_B)/2);
			return new Quadrat { Side_A = (rec.Side_A + rec.Side_B)/2 };
		}
    }

    class Quadrat
    {
        public int Side_A { get; set; }      
        public int Squarer { get; set; }

        public Quadrat()
        {                 
        }

        public int GetQatratSquare()
        {
            Squarer = Side_A * Side_A;
            return Squarer;
        }

        public override string ToString()
        {
            return $"side A = {Side_A}.";
        }
		public void ShowQuadrat()
		{
			Console.WriteLine($"side A = {Side_A}.");
		}

		//=======================================
		public override bool Equals(object obj)
		{
			Quadrat rec = (Quadrat)obj;
			return Side_A == rec?.Side_A;
		}

		public override int GetHashCode()
		{
			var hashCode = 1861411795;
			hashCode = hashCode * -1521134295 + Side_A.GetHashCode();			
			return hashCode;
		}

		public static Quadrat operator ++ (Quadrat quad )
		{
			quad.Side_A++;			
			return quad;
		}

		public static Quadrat operator -- (Quadrat quad)
		{
			quad.Side_A--;			
			return quad;
		}

		public static Quadrat operator + (Quadrat quad1, Quadrat quad2)
		{
			Quadrat quad3 = new Quadrat
			{
				Side_A = quad1.Side_A + quad2.Side_A,
			};
			return quad3;
		}

		public static Quadrat operator - (Quadrat quad1, Quadrat quad2)
		{
			Quadrat quad3 = new Quadrat
			{
				Side_A = quad1.Side_A - quad2.Side_A
				
			};
			if (quad3.Side_A < 0 )
				throw new RangeException();
			return quad3;
		}

		public static Quadrat operator *(Quadrat quad1, Quadrat quad2)
		{
			Quadrat quad3 = new Quadrat
			{
				Side_A = quad1.Side_A * quad2.Side_A				
			};
			return quad3;
		}
		public static Quadrat operator /(Quadrat quad1, Quadrat quad2)
		{
			Quadrat quad3 = new Quadrat
			{
				Side_A = quad1.Side_A / quad2.Side_A				
			};
			return quad3;
		}

		public static bool operator ==(Quadrat quad1, Quadrat quad2)
		{
			return quad1.Equals(quad2);
		}
		public static bool operator !=(Quadrat quad1, Quadrat quad2)
		{
			return !(quad1 == quad2);
		}


		public static bool operator <(Quadrat quad1, Quadrat quad2)
		{
			return quad1.Side_A  < quad2.Side_A ;
		}
		public static bool operator >(Quadrat quad1, Quadrat quad2)
		{
			return quad1.Side_A > quad2.Side_A ;
		}

		///
		public static bool operator <=(Quadrat quad1, Quadrat quad2)
		{
			return quad1.Side_A <= quad2.Side_A;
		}
		public static bool operator >=(Quadrat quad1, Quadrat quad2)
		{
			return quad1.Side_A >= quad2.Side_A;
		}


		public static bool operator true(Quadrat quad)
		{
			return quad.Side_A != 0;
		}
		public static bool operator false(Quadrat quad)
		{
			return quad.Side_A == 0;
		}


		public static implicit operator int(Quadrat quad)
		{
			return quad.Side_A * 4;
		}

		public static implicit operator Rectangle (Quadrat quad)
		{
			Rectangle rec = new Rectangle
			{
				Side_A = quad.Side_A,
				Side_B = quad.Side_A
			};
			return rec;
		}
	}
    

    class Program
    {
        static void Main(string[] args)
        {
			Rectangle rec = new Rectangle
			{
				Side_A = 5,
				Side_B = 8
			};
			Console.ForegroundColor = ConsoleColor.Green;
			Console.WriteLine("\tRECTANGLE");
			Console.ForegroundColor = ConsoleColor.White;
			rec.ShowRectangle();

			Console.WriteLine(new string('=', 11));

			Rectangle rec2 = rec++;
			Console.ForegroundColor = ConsoleColor.Cyan;
			Console.WriteLine("    ++");
			Console.ForegroundColor = ConsoleColor.White;
			rec2.ShowRectangle();

			Rectangle rec3 = rec--;
			Console.ForegroundColor = ConsoleColor.Cyan;
			Console.WriteLine("    --");
			Console.ForegroundColor = ConsoleColor.White;
			rec3.ShowRectangle();

			Rectangle rec4 = rec2 + rec;
			Console.ForegroundColor = ConsoleColor.Cyan;
			Console.WriteLine("    +");
			Console.ForegroundColor = ConsoleColor.White;
			rec4.ShowRectangle();

			Rectangle rec5 = rec4 - rec3;
			Console.ForegroundColor = ConsoleColor.Cyan;
			Console.WriteLine("    -");
			Console.ForegroundColor = ConsoleColor.White;
			rec5.ShowRectangle();

			Rectangle rec6 = rec4 * rec3;
			Console.ForegroundColor = ConsoleColor.Cyan;
			Console.WriteLine("    *");
			Console.ForegroundColor = ConsoleColor.White;
			rec6.ShowRectangle();

			Rectangle rec7 = rec3 / rec5;
			Console.ForegroundColor = ConsoleColor.Cyan;
			Console.WriteLine("    /");
			Console.ForegroundColor = ConsoleColor.White;
			rec7.ShowRectangle();

			Console.WriteLine(new string('=', 11));

			if (rec.Equals(rec4))
				Console.WriteLine("\nEquals");
			else
			   Console.WriteLine("\nNot Equals");

			if (rec3 == rec5)
				Console.WriteLine("\nEquals");
			else
				Console.WriteLine("\nNot Equals");

			if (rec3 != rec5)
				Console.WriteLine("\n!= - yes");
			else
				Console.WriteLine("\n!= - no");

			if(rec < rec4)
				Console.WriteLine("\nrec < rec4 (true)");
			else
				Console.WriteLine("\nrec < rec4 (false)");

			if (rec3 > rec5)
				Console.WriteLine("\nrec3 > rec5 (true)");
			else
				Console.WriteLine("\nrec3 > rec5 (false)");

			///
			if (rec <= rec4)
				Console.WriteLine("\nrec <= rec4 (true)");
			else
				Console.WriteLine("\nrec <= rec4 (false)");

			if (rec3 >= rec5)
				Console.WriteLine("\nrec3 >= rec5 (true)");
			else
				Console.WriteLine("\nrec3 >= rec5 (false)");

			if(rec)
				Console.WriteLine("\nrec != 0 (true)");
			else
				Console.WriteLine("\nrec != 0 (false)");
			Console.WriteLine(new string('=', 21));

			Console.WriteLine("expl: rec -> quad");
			Quadrat qvd = (Quadrat)rec;
			qvd.ShowQuadrat();
			
			int a = (int)rec;
			Console.WriteLine($"\nexpl: rec -> int ({a})");
			Console.WriteLine(new string('=', 21));

			/////////////////////////////////////////////////////////////////////////////

			Quadrat quad = new Quadrat
			{
				Side_A = 10
			};
			Console.ForegroundColor = ConsoleColor.Green;
			Console.WriteLine("\tQUADRAT");
			Console.ForegroundColor = ConsoleColor.White;
			quad.ShowQuadrat();

			Console.WriteLine(new string('=', 11));

			Quadrat quad2 = quad++;
			Console.ForegroundColor = ConsoleColor.Cyan;
			Console.WriteLine("    ++");
			Console.ForegroundColor = ConsoleColor.White;
			quad2.ShowQuadrat();

			Quadrat quad3 = quad--;
			Console.ForegroundColor = ConsoleColor.Cyan;
			Console.WriteLine("    --");
			Console.ForegroundColor = ConsoleColor.White;
			quad3.ShowQuadrat();

			Quadrat quad4 = quad2 + quad;
			Console.ForegroundColor = ConsoleColor.Cyan;
			Console.WriteLine("    +");
			Console.ForegroundColor = ConsoleColor.White;
			quad4.ShowQuadrat();

			Quadrat quad5 = quad4 - quad3;
			Console.ForegroundColor = ConsoleColor.Cyan;
			Console.WriteLine("    -");
			Console.ForegroundColor = ConsoleColor.White;
			quad5.ShowQuadrat();

			Quadrat quad6 = quad4 * quad3;
			Console.ForegroundColor = ConsoleColor.Cyan;
			Console.WriteLine("    *");
			Console.ForegroundColor = ConsoleColor.White;
			quad6.ShowQuadrat();

			Quadrat quad7 = quad3 / quad5;
			Console.ForegroundColor = ConsoleColor.Cyan;
			Console.WriteLine("    /");
			Console.ForegroundColor = ConsoleColor.White;
			quad7.ShowQuadrat();
			Console.WriteLine(new string('=', 11));

			if (quad.Equals(quad4))
				Console.WriteLine("\nEquals");
			else
				Console.WriteLine("\nNot Equals");

			if (quad3 == quad5)
				Console.WriteLine("\nEquals");
			else
				Console.WriteLine("\nNot Equals");

			if (quad3 != quad5)
				Console.WriteLine("\n!= - yes");
			else
				Console.WriteLine("\n!= - no");

			if (quad < quad4)
				Console.WriteLine("\nquad < quad4 (true)");
			else
				Console.WriteLine("\nquad < quad4 (false)");

			if (quad3 > quad5)
				Console.WriteLine("\nquad3 > quad5 (true)");
			else
				Console.WriteLine("\nquad3 > quad5 (false)");

			///
			if (quad <= quad4)
				Console.WriteLine("\nquad <= quad4 (true)");
			else
				Console.WriteLine("\nquad <= quad4 (false)");

			if (quad3 >= quad5)
				Console.WriteLine("\nquad3 >= quad5 (true)");
			else
				Console.WriteLine("\nquad3 >= quad5 (false)");

			if (quad)
				Console.WriteLine("\nquad != 0 (true)");
			else
				Console.WriteLine("\nquad != 0 (false)");
			Console.WriteLine(new string('=', 21));

			Console.WriteLine("impl: quad -> rec");
			Rectangle rtgl = quad;
			rtgl.ShowRectangle();

			int b = quad;
			Console.WriteLine($"\nimpl: quad -> int ({b})");
			Console.WriteLine(new string('=', 21));

			///////////////////////////////////////

		}
	}
}
