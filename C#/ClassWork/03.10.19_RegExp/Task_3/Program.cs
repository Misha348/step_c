﻿using System;
using System.IO;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_3
{
    class Program
    {
        static void Main(string[] args)
        {
            string readPath = @"test.txt";
            string text = "";
            using (StreamReader sr = new StreamReader(readPath, Encoding.Default))
                text = sr.ReadToEnd();
            string[] str = text.Split(' ');
            string result = string.Join(" ", str);            

            List<int> digits = new List<int>();

            string pattern = @"\d+";
            var regex = new Regex(pattern);
            for (int i = 0; i < str.Length; i++)
            {
                Match match = Regex.Match(str[i], pattern);
                if (match.Success)
                    digits.Add(Convert.ToInt32(match.Value));
            }

            foreach ( var digit in digits)
            {
                Console.WriteLine(digit);
            }
        }
    }
}
