﻿using System;
using System.IO;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_4
{
    class Program
    {
        static void Main(string[] args)
        {
            string readPath = @"test.txt";
            string text = "";
            using (StreamReader sr = new StreamReader(readPath, Encoding.Default))
                text = sr.ReadToEnd();
            string[] str = text.Split(' ');
            string result = string.Join(" ", str);

            string pattern = @"\s\d{10}\s";
            List<string> strings = new List<string>();
            List<string> numbers = new List<string>();

            //var regex = new Regex(pattern);
            for (int i = 0; i < str.Length; i++)
            {
                Match match = Regex.Match(str[i], pattern);
                if (match.Success)
                    strings.Add(match.Value);
            }

            for (int i = 0; i < strings.Count; i++)
            {
                string mobNumb = strings[i].Insert(0, "+");
                mobNumb = mobNumb.Insert(2, " (");
                mobNumb = mobNumb.Insert(5, ") ");
                mobNumb = mobNumb.Insert(8, "-");
                mobNumb = mobNumb.Insert(10, "-");
                numbers.Add(mobNumb);
            }

            foreach (var numb in numbers)
            {
                Console.WriteLine(numb);
            }
            //(^|\s)\d{4}(\s|$)
        }
    }
}
