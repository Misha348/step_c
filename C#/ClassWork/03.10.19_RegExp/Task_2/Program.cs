﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_2
{
    class Program
    {
        static void Main(string[] args)
        {
            string readPath = @"test.txt";
            string text = "";
            using (StreamReader sr = new StreamReader(readPath, Encoding.Default))
                text = sr.ReadToEnd();
            string[] str = text.Split(' ');

            for (int i = 0; i < str.Length - 1; i++)
            {
                char[] word = str[i].ToCharArray();
                for (int j = 0; j < word.Length; j++)
                {
                    if (char.IsLetter(word[0]))
                        word[0] = char.ToUpper(word[0]);
                    str[i] = (string.Join("", word)) + '.';                    
                }               
            }          

            FileStream file = File.Create(@"finish.txt");
            var writer = new StreamWriter(file);
            for (int i = 0; i < str.Length; i++)
            {
                writer.WriteLine($"{str[i]}\n");
            }
         
            writer.Close();
            file.Close();
        }
    }
}
