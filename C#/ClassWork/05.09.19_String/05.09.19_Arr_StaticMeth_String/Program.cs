﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _05._09._19_Arr_StaticMeth_String
{
    class Program
    {
        static void Main(string[] args)
        {
            Random rnd = new Random();
            int[] a = new int[10];
            int[] b = new int[7];
          

            for (int i = 0; i < a.Length; i++)
            {
                a[i] = rnd.Next(1, 10);
            }
            for (int i = 0; i < a.Length; i++)
            {
                Console.WriteLine($"el. - {i} = {a[i]}");
            }
            Console.WriteLine();


            for (int i = 0; i < b.Length; i++)
            {
                b[i] = rnd.Next(1, 10);
            }
            for (int i = 0; i < b.Length; i++)
            {
                Console.WriteLine($"el. - {i} = {b[i]}");
            }
            Console.WriteLine("===============");

            int c = 0;
            for (int i = 0; i < a.Length; i++)
            {
                for (int j = 0; j < b.Length; j++)
                {
                    if (a[i] == b[j])
                    {
                        c++;
                    }
                }
            }
            int[] com = new int[c];
            for (int i = 0; i < a.Length; i++)
            {
                for (int j = 0; j < b.Length; j++)
                {
                    if (a[i] == b[j])
                    {
                        com[i] = a[i];
                    }
                }
            }
            com.Except(a);

            for (int i = 0; i < com.Length; i++)
            {
                Console.WriteLine($"el. - {i} = {com[i]}");
            }

            Console.ReadKey();
        }
        
    }
}
