﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace strings_basics
{
    class Program
    {
        static void Main(string[] args)
        {
            //1. By assigning a string literal to a String variable
            //2. By using a String class constructor
            //3. By using the string concatenation operator (+)
            //4. By retrieving a property or calling a method that returns a string
            //5. By calling a formatting method to convert a value or an object to its string representation

            //from string literal and string concatenation
            string fname, lname;
            fname = "Rowan";
            lname = "Atkinson";

            string fullname = fname + " bla bla bla " + lname;
            Console.WriteLine($"Full Name: {fullname}");

            //by using string constructor
            char[] letters = { 'H', 'e', 'l', 'l', 'o' };
            string greetings = new string(letters);
            Console.WriteLine("Greetings: {0}", greetings);

            //methods returning string
            string[] sarray = { "Hello", "From", "Tutorials", "Point" };
            string message = String.Join(" - ", sarray);
            Console.WriteLine("Message: {0}", message);

            //formatting method to convert a value
            DateTime waiting = new DateTime(2012, 10, 10, 17, 58, 1);
            string chat = String.Format("Message sent at {0:t} on {0:D}", waiting);
            Console.WriteLine($"Message: {chat}!");
        }
    }
}

