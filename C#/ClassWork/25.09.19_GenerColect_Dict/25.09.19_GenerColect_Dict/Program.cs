﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _25._09._19_GenerColect_Dict
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;

            //string s = "some. bla. bla blu.";
            string s = "Ось будинок, який побудував Джек. " +
                        "А це пшениця, яка в темній комірці зберігається у будинку, який побудував Джек. " +
                        "А це весела птиця-синиця, яка часто краде пшеницю, " +
                        "яка в темній комірці зберігається у будинку, який побудував Джек.";
			Console.WriteLine(s);

			var s1 = new StringBuilder();
			foreach (char c in s)
			{
				if (!char.IsPunctuation(c))
					s1.Append(c);
				if (c == '-')
					s1.Append(" ");
			}
			s = s1.ToString();			
			Console.WriteLine(new string('=', 50));
			Console.WriteLine();


			string[] arr = s.Split(' ');
			for (int i = 0; i < arr.Length; i++)
			{
				Console.Write($"{arr[i]} ");
			}
			Console.WriteLine();
			Console.WriteLine(new string('=', 50));

			Dictionary<int, string> words = new Dictionary<int, string>();
			for (int i = 0; i < arr.Length; i++)
			{
				words.Add(i+1, arr[i]);
			}

			foreach (KeyValuePair<int, string> keyVal in words)
			{
				Console.WriteLine($"{keyVal.Key} - {keyVal.Value}");
			}

			//List<Dictionary<int, string>> list = new List<Dictionary<int, string>>();
			Dictionary<string, int> listOfWords = new Dictionary<string, int>();
			for (int j = 0; j < words.Count; j++)
			{
				var k = 0;
				for (int i = 0; i < words.Count; i++)
				{		
					if(words[i+1] == arr[j])
					//if (words[i].Contains(arr[j]))					
						k++;					
				}
				//if(!list.Contains(words.ContainsKey(j))
				if (listOfWords.Count == 0)
					listOfWords.Add(arr[j], k);
				else
				{
					foreach (KeyValuePair<string, int> keyVal in listOfWords)
					{				
						if (!listOfWords.ContainsKey(arr[j]))
						{
							listOfWords.Add(arr[j], k);
							break;
						}
						else
							continue;						

					}
				}				
				//Console.WriteLine($"{words[j+1]} - {k}");
			}
			Console.WriteLine();
			foreach (KeyValuePair<string, int> keyVal in listOfWords)
			{
				Console.WriteLine(keyVal.Key + " - " + keyVal.Value);
			}
				//int wordCount = 0;
				//for (int i = 0; i < s.Length; i++)
				//{
				//    if (s[i] == ' ')
				//        wordCount++;
				//}
				//Console.WriteLine(wordCount+1);
		}
	}
}
