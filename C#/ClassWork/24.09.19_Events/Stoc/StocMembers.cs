﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stoc
{
    public class Seller
    {
        public string Name { get; set; }
        public Seller(string name) { Name = name; }

        public void Sell() => Console.WriteLine("sellers sel currency.");        
    }

    public class Buyer
    {
        public string Name { get; set; }
        public Buyer(string name) { Name = name; }

        public void Buy() => Console.WriteLine(Name + " - biyers buy currency.");
    }
}
