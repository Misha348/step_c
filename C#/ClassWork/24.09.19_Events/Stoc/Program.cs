﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stoc
{
    class PriceCurrencyImplementationException: Exception
    {
        public void PriceCurrencyImplementationExceptionMessage()
        {
            Console.WriteLine("crash in price implementation");
        }
    }

    public delegate void SellingDelegate();
    public delegate void BuyingDelegate();

    public class StocExchange
    {
        public event SellingDelegate maxPriceEvent;   // event
        public void InvokeSellingEvent()
        {
            maxPriceEvent.Invoke();
        }

        public event BuyingDelegate minPriceEvent;     // event
        public void InvokeBuyingEvent()
        {
            minPriceEvent.Invoke();
        }

		private int _currentCurrencyPrice;

		public int CurrentCurrencyPrice
		{
			get
			{ return _currentCurrencyPrice; }

			set
			{
				_currentCurrencyPrice = value;
				if (value < 0)
					_currentCurrencyPrice = 0;
				if(value > 100)
					_currentCurrencyPrice = 100;
			}

		}
        public int MaxPrice { get; set; }    
        public int MinPrice { get; set; }
      

        public StocExchange(int pr) { CurrentCurrencyPrice = pr; }
            
        //public void PriceGotToMaxBoundary()
        //{
        //    if (CurrentCurrencyPrice >= 75 && CurrentCurrencyPrice <= 100)
        //        MaxPrice = CurrentCurrencyPrice;
        //}

        public bool IsGotToMinBoundary()
        {
            return (CurrentCurrencyPrice <= 25 && CurrentCurrencyPrice >= 0);
                 
        }
        public bool IsPriceGotToMaxBoundary()
        {
            return (CurrentCurrencyPrice >= 75 && CurrentCurrencyPrice <= 100) ;
               
        }

        //public void PriceGotToMinBoundary()
        //{
        //    if (CurrentCurrencyPrice <= 25 && CurrentCurrencyPrice >= 0)
        //        MinPrice = CurrentCurrencyPrice;
        //}

        public void MoveCurrencyPrise()
        {
            Random rnd = new Random();
            int curPriceDirrection = rnd.Next(-1, 2);
            switch (curPriceDirrection)
            {
                case -1:
                    CurrentCurrencyPrice -= rnd.Next(10, 15); break;
				
                case 0:
                    CurrentCurrencyPrice += 0; break;
                case 1:
                    CurrentCurrencyPrice += rnd.Next(10, 15); break;
                default:
                    throw new PriceCurrencyImplementationException();                  
            }
			if (IsGotToMinBoundary())
				minPriceEvent?.Invoke();
			else if (IsPriceGotToMaxBoundary())
				maxPriceEvent?.Invoke();

            Console.WriteLine($"Price - {CurrentCurrencyPrice}");
        }       
    }


    class Program
    {
        static void Main(string[] args)
        {
            Seller seller = new Seller("John");
            Buyer buyer = new Buyer("Mark");

            StocExchange stoc = new StocExchange(50);          

            stoc.minPriceEvent += buyer.Buy;
            stoc.maxPriceEvent += seller.Sell;
			
			bool flag = false;
			while (!flag)
			{
				stoc.MoveCurrencyPrise();
				string input = Console.ReadLine();
				if (input == "e")
					continue;
				else
					break;
			}        
        }
    }
}
