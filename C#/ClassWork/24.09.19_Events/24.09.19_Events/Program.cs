﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _24._09._19_Events
{
    public delegate void ExamDelegate();
    class Student
    {
        public string Name { get; set; }
        public void PassExam()
        {
            Console.WriteLine($"Student {Name} pass exam!");
        }
        public void BreakAnArms()
        {
            Console.WriteLine($"Student {Name} broken arms!");
        }
    }
    class Teacher
    {
        public string Name { get; set; }
        private ExamDelegate examDelegate;
        public event ExamDelegate ExamEvent
        {
            add { examDelegate += value; }
            remove { examDelegate -= value; }
        }
        public void StartExam()
        {
            foreach (var item in examDelegate.GetInvocationList())
            {
                Console.WriteLine(item.Method.Name);
            } 
            // event                     
            // examEvent();
            // delegate
            // examDelegate?.Invoke();
            // examDelegate();
            examDelegate.Invoke();
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Student[] students = new Student[]
            {
                new Student() {Name = "Vasia"},
                new Student() {Name = "Bob"},
                new Student() {Name = "Maria"}
            };
            Teacher t = new Teacher();
            foreach (var st in students)
            {
                //t.ExamEvent += st.PassExam;      
                t.ExamEvent += students[0].PassExam;
                //t.examDelegate += st.PassExam;
            }
            // t.examDelegate = null; // error
            t.StartExam();
            Console.WriteLine(new string('-', 20));
            t.ExamEvent -= students[0].PassExam;
            //t.ExamEvent += students[0].BreakAnArms;
            t.StartExam();
            Console.ReadKey();

        }
    }
}
