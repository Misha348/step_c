﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04._09._19_Arrays
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] a = new int[5];
            for (int i = 0; i < a.Length; i++)
            {
                Console.Write("Enter: " + i + " el = ");
                a[i] = int.Parse(Console.ReadLine());
                //Console.WriteLine($"el. - {a[i]} = ");               
            }
            Console.WriteLine();
            for (int i = 0; i < a.Length; i++)
            {
                Console.WriteLine($"el. - {i} = {a[i]}");
            }
            int maxEl = 0;
            int minEl = 0;
            int sum = 0;
            int sumCoupleEl = 0;
            for (int i = 0; i < a.Length; i++)
            {

                maxEl = a.Max();
                minEl = a.Min();
                sum += a[i];
                if (a[i] % 2 == 0)
                {
                    sumCoupleEl += a[i];
                }
            }
            Console.WriteLine($"min el = {minEl}");
            Console.WriteLine($"max el = {maxEl}");
            Console.WriteLine($"sum = {sum}");
            Console.WriteLine($"sumCoupleEl = {sumCoupleEl}");



            Console.WriteLine("===========");


            Random rnd = new Random();
            int maxEl1 = 0;
            int minEl1 = 0;
            int sum1 = 0;
            int sumNotCoupleCols = 0;

            int[,] b = new int[3, 4];
            for (int i = 0; i < b.GetUpperBound(0) + 1; i++)
            {
                for (int j = 0; j < b.GetUpperBound(1) + 1; j++)
                {
                    b[i, j] = rnd.Next(-100, 100);
                }
            }
            for (int i = 0; i < b.GetUpperBound(0) + 1; i++)
            {
                for (int j = 0; j < b.GetUpperBound(1) + 1; j++)
                {
                    maxEl1 = b.Cast<int>().Min();
                    minEl1 = b.Cast<int>().Max();
                    sum1 += b.Cast<int>().Sum();
                }
            }
            b.Cast<int>().Sum();
            
            for (int i = 0; i < b.GetUpperBound(0) + 1; i++)
            {
                for (int j = 0; j < b.GetUpperBound(1) + 1; j++)
                {
                    Console.Write($"{b[i, j]}  ");
                }
                Console.WriteLine();
            }

            for (int i = 0; i < b.GetUpperBound(0) + 1; i++)
            {
                for (int j = 0; j < b.GetUpperBound(1) + 1; j+=2)
                {
                    sumNotCoupleCols += b[i, j];
                }
                Console.WriteLine();
            }


            Console.WriteLine($"min = {minEl1}");
            Console.WriteLine($"max = {maxEl1}");
            Console.WriteLine($"sumNotCoupleCols = {sumNotCoupleCols}");

            Console.WriteLine("===========");


            Console.ReadKey();
        }
    }
}
