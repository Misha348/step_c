﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generic
{
    public class Point<T> where T: struct
    {
        public T X { get; set; }
        public T Y { get; set; }

        public Point(T x, T y)
        {
            X = x;
            Y = y;
        }

        public override string ToString()
        {
            return $"[x: {X}, y{Y}]";
        }
    }

    public class Line<T> where T : struct
    {
        public Point<T> Point_A { get; set; }
        public Point<T> Point_B { get; set; }

        public Line(Point<T> point1, Point<T> point2)
        {
            Point_A = point1;
            Point_B = point2;
        }

        public Line(T x1, T y1, T x2, T y2)
        {

        }

        public override string ToString()
        {
            return $"A: {Point_A}, B: {Point_B}";
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Point<int> p1 = new Point<int>(3, 5);
            Point<int> p2 = new Point<int>(6, 8);

            Line<int> l = new Line<int>(p1, p2);
            Console.WriteLine(l);

        }
    }
}
