﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2D_arrays
{
    class Program
    {
        static void Main(string[] args)
        {
            //1
            int[,] array = new int[3, 3];

            array[0, 0] = 1;
            array[0, 1] = 2;
            array[0, 2] = 3;

            array[1, 0] = 4;
            array[1, 1] = 5;
            array[1, 2] = 6;

            array[2, 0] = 7;
            array[2, 1] = 8;
            array[2, 2] = 9;

            Console.Write(array[0, 0]);
            Console.Write(array[0, 1]);
            Console.Write(array[0, 2]);
            Console.Write("\n");
            Console.Write(array[1, 0]);
            Console.Write(array[1, 1]);
            Console.Write(array[1, 2]);
            Console.Write("\n");
            Console.Write(array[2, 0]);
            Console.Write(array[2, 1]);
            Console.Write(array[2, 2]);

            //2            
            int[,] array2 = new int[3, 3];


            for (int i = 0; i < array2.GetUpperBound(0) + 1; i++)
            {
                for (int j = 0; j < array2.GetUpperBound(1) + 1; j++)
                {
                    array2[i, j] = i * j + 1;
                }
            }

            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    Console.Write("{0} ", array2[i, j]);
                    //Console.Write($"{array2[i, j]}"); // інтерполяція
                }
                Console.Write("\n");
                //Console.WriteLine();
            }

            //3
            int[,] array3 =
                            {
                             { 1, 2, 3 },
                             { 4, 5, 6 },
                             { 7, 8, 9 }
                           };

            Console.WriteLine(array3.Length);

            Console.WriteLine(array3);
            Console.WriteLine();

            int a = 3, b = 3; // c = 3;
            for (int i = 0; i < a; i++)
            {
                for (int j = 0; j < b; j++)
                {
                    Console.Write($"{array3[i, j]}");

                }
                Console.Write("\n");
            }

            //4
            int[] vector = new int[1];
            vector[0] = 100;
            Console.WriteLine(vector[0]);


            int[,] matrix = new int[1, 1];
            matrix[0, 0] = 200;
            Console.WriteLine(matrix[0, 0]);

            int? someValue = null;
            if (null == someValue)
            {
                Console.WriteLine(" it's not good!");
            }
            else
            {
                Console.WriteLine("something real wrong!");
            }
            Console.WriteLine();




        }
    }
}
