﻿using System;

namespace _1D_array
{
    class Program
    {
        static void Main(string[] args)
        {
            //1           
            int[] array = new int[5];

            array[0] = 10;
            array[1] = 20;
            array[2] = 30;
            array[3] = 40;
            array[4] = 50;

            Console.WriteLine(array[0]);
            Console.WriteLine(array[1]);
            Console.WriteLine(array[2]);
            Console.WriteLine(array[3]);
            Console.WriteLine(array[4]);

            Console.ReadKey();

            //2
            int[] array2 = new int[5];

            for (int i = 0; i < array2.Length; i++)
            {
                array2[i] = i * 2;
            }

            for (int i = 0; i < array2.Length; i++)
            {
                Console.Write(array2[i] + " ");
            }
            Console.WriteLine();
            //3
            int[] array3 = new int[5] { 1, 2, 3, 4, 5 };

            for (int i = 0; i < array3.Length; i++)
            {
                Console.Write(array3[i] + " ");
            }
            Console.WriteLine();
            //4
            int[] array4 = new int[] { 1, 2, 3, 4, 5 };

            for (int i = 0; i < array4.Length; i++)
            {
                Console.Write(array4[i] + " ");
            }
            Console.WriteLine();
            //5
            int[] array5 = new int[] { 1, 2, 3, 4 };


            for (int i = 0; i < array5.Length; i++)
            {
                Console.Write(array5[i] + " ");
            }
            Console.WriteLine();
            //6
            int[] array6 = new int[5];


            for (int i = 0; i < array6.Length; i++)
            {
                Console.Write(array6[i] + " ");
            }

            //int[] myArray = new int[100];
        }
    }
}
