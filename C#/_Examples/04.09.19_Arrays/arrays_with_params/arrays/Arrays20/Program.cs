﻿using System;

// Ключевое слово params, позволяет определить параметр метода, принимающий переменное количество аргументов.

namespace Arrays
{
    class Program
    {
        static void ShowArray(int a, params int[] array)
        {
            for (int i = 0; i < array.Length; i++)
            {
                Console.Write("{0} ", array[i]);
            }
        }

        static void Main()
        {
            ShowArray(10, 3, 4, 5, 6);

            // Delay.
            Console.ReadKey();
        }
    }
}
