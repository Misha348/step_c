﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4D_array
{
    class Program
    {
        static void Main(string[] args)
        {
            //  int[,,,] array = new int[2, 2, 2, 2];
            //
            //  array[0, 0, 0, 0] = 0x0;
            //  array[0, 0, 0, 1] = 0x1;
            //  array[0, 0, 1, 0] = 0x2;
            //  array[0, 0, 1, 1] = 0x3;
            //
            //  array[0, 1, 0, 0] = 0x4;
            //  array[0, 1, 0, 1] = 0x5;
            //  array[0, 1, 1, 0] = 0x6;
            //  array[0, 1, 1, 1] = 0x7;
            //
            //  array[1, 0, 0, 0] = 0x8;
            //  array[1, 0, 0, 1] = 0x9;
            //  array[1, 0, 1, 0] = 0xA;
            //  array[1, 0, 1, 1] = 0xB;
            //
            //  array[1, 1, 0, 0] = 0xC;
            //  array[1, 1, 0, 1] = 0xD;
            //  array[1, 1, 1, 0] = 0xE;
            //  array[1, 1, 1, 1] = 0xF;
            //
            //
            //  for (int i = 0; i < 2; i++)
            //  {
            //      for (int j = 0; j < 2; j++)
            //      {
            //          for (int k = 0; k < 2; k++)
            //          {
            //              for (int l = 0; l < 2; l++)
            //              {
            //                  Console.Write("{0:X} ", array[i, j, k, l]);
            //              }
            //              Console.Write('\n');
            //          }
            //          Console.WriteLine();
            //      }
            //      Console.Write("\n");
            //  }

            //2
            int[,,,] array2 =
             {
              {
                { { 0x0, 0x1 }, {0x2, 0x3 } },
                { { 0x4, 0x5 }, {0x6, 0x7 } }
              },
              {
                { { 0x8, 0x9 }, {0xA, 0xB } },
                { { 0xC, 0xD }, {0xE, 0xF } }
              }
            };


            for (int i = 0; i < 2; i++)
            {
                for (int j = 0; j < 2; j++)
                {
                    for (int k = 0; k < 2; k++)
                    {
                        for (int l = 0; l < 2; l++)
                        {
                            Console.Write("{0:X} ", array2[i, j, k, l]);
                        }
                        Console.Write("\n");
                    }
                    Console.Write("\n");
                }
                Console.Write("\n");
            }


        }
    }
}
