﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jagged_array
{
    class Program
    {
        static void Main(string[] args)
        {
            int[][] jagged = new int[3][];

            jagged[0] = new int[] { 1, 2 };
            jagged[1] = new int[] { 1, 2, 3, 4, 5 };
            jagged[2] = new int[] { 1, 2, 3 };

            Console.WriteLine(jagged.Length);

            for (int i = 0; i < jagged.Length; ++i)
            {
                for (int j = 0; j < jagged[i].Length; ++j)
                {
                    Console.Write($"{{{i};{j}}}={        jagged[i][j]       } ");
                }
                Console.Write("\n");
            }

            Console.WriteLine();

            foreach (int[] item in jagged)
            {
                foreach (int i in item)
                {
                    Console.Write(i + " - ");
                }
            }
        }
    }
}
