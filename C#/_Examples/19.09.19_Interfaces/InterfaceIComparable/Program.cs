﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfaceIComparable
{
    class Worker
    {
        public string Name { get; set; }
        public int Salary { get; set; }

        public override string ToString()
        {
            return "Name: " + Name + "\n" +
                "Salary: " + Salary + "\n";
        }
    }

    class Department
    {
        public List<Worker> workers { get; }

        public Department()
        {
            workers = new List<Worker>
            {
                new Worker() {Name = "Yura", Salary = 3677},
                new Worker() {Name = "Olga", Salary = 2000 },
                new Worker() {Name = "Vova", Salary = 3226 },
                new Worker() {Name = "Viktor", Salary = 361 },
                new Worker() {Name = "Marusia", Salary = 65827 },
            };
        }

        void SortDepartment()
        {
            workers.Sort();
        }
    }

    class Program
    {
        static void Main(string[] args)
        {

            Department dep = new Department()
        }
    }
}
