﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultyInterface
{
    interface IA
    {
        void MethodA();
    }
    interface IB
    {
        void MethodB();
    }
    interface IC : IA, IB
    {
        void MethodC();
    }

    class MultyClass : IC
    {
        public void MethodA()
        {
            Console.WriteLine("A");
        }

        public void MethodB()
        {
            throw new NotImplementedException();
        }

        public void MethodC()
        {
            throw new NotImplementedException();
        }
    }

    interface IDocument
    {
        void Print();
    }
    interface IShowable
    {
        void Print();
    }

    interface ILetter
    {
        void Print();
    }

    class SuperClass : IDocument, IShowable, ILetter
    {
        // private
        void IDocument.Print()
        {
            Console.WriteLine("Print document");
        }

        // private
        void IShowable.Print()
        {
            Console.WriteLine("Print to screen");
        }

        public void Print()
        {
            Console.WriteLine("Print Letter");
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            SuperClass c = new SuperClass();

            c.Print();

            IDocument doc = c;
            doc.Print();

            IShowable show = c;
            show.Print();

            ILetter let = c;
            let.Print();
        }
    }
}
