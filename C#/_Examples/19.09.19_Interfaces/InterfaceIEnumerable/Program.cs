﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfaceIEnumerable
{
    class Worker : IComparable
    {
        public string Name { get; set; }
        public int Salary { get; set; }

        public int CompareTo(object obj)
        {
            if (obj is Worker)
                return Name.CompareTo((obj as Worker).Name);
            throw new NotImplementedException();
        }

        public override string ToString()
        {
            return "Name: " + Name + "\n" +
                "Salary: " + Salary + "\n";
        }
    }

    class Department : IEnumerable
    {
        public List<Worker> workers { get; }

        public Department()
        {
            workers = new List<Worker>
            {
                new Worker() {Name = "Yura", Salary = 3677},
                new Worker() {Name = "Olga", Salary = 2000 },
                new Worker() {Name = "Vova", Salary = 3226 },
                new Worker() {Name = "Viktor", Salary = 361 },
                new Worker() {Name = "Marusia", Salary = 65827 },
            };

          
        }

        public IEnumerator GetEnumerator()
        {
            return workers.GetEnumerator();
        }

        public void SortDepartment()
        {
            workers.Sort();
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            int[] arr = new int[5] { 1, 2, 3, 4, 5 };

            ///Worker w = new Worker();
            ///w.CompareTo(w2);

            // foreach(int item in arr)
            // {
            //     Console.WriteLine(item);
            // }

            Department dep = new Department();

            Console.WriteLine("Before sorting...");
            foreach (var item in dep)
            {
                Console.WriteLine(item);
            }

            dep.SortDepartment();

            Console.WriteLine("After sorting...");
            foreach (var item in dep)
            {
                Console.WriteLine(item);
            }
        }
    }
}
