﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfaceICloneable
{
    class Pasport
    {
        public int SerialNumber { get; set; }
        public DateTime Date { get; set; }
    }

    class Worker : IComparable, ICloneable
    {
        public string Name { get; set; }
        public int Salary { get; set; }
        public Pasport Passport { get; set; }


        public object Clone()
        {
            Worker clone = (Worker)this.MemberwiseClone();

            clone.Passport = new Pasport()
            {
                SerialNumber = this.Passport.SerialNumber,
                Date = this.Passport.Date
            };
            return clone;
        }

        public int CompareTo(object obj)
        {
            if (obj is Worker)
                return Name.CompareTo((obj as Worker).Name);
            throw new NotImplementedException();
        }

        public override string ToString()
        {
            return "Name: " + Name + "\n" +
                "Salary: " + Salary + "\n";
        }
    }

    class Program
    {
        static void Main(string[] args)
        {

            Worker w1 = new Worker()
            {
                Name = "Vasia",
                Salary = 4634,
                Passport = new Pasport()
                {
                    Date = DateTime.Now,
                    SerialNumber = 2352427
                }
            };

            Worker w2 = (Worker)w1.Clone();

            Console.WriteLine(w1);
            w2.Salary = 0;
            Console.WriteLine(w1);
        }
    }
}
