﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfaceBase
{
    public interface IWorker
    {
        // public, protected, private - error
        // події
        //event EventHandler bla;
        // властивості
        bool IsWorking { get; set; } // {get;} {set;}
        // методи                                     
        void Work();
    }

    public interface IManager
    {        
        // властивості
        string Department { get; }
        // методи                                     
        void Control();
    }

    abstract class Human
    {
        protected string _name;
        protected string _surname;
        
        public Human() { }
        public Human(string name, string surname)
        {            
            _name = name;
            _surname = surname;
        }
        
        public override string ToString()
        {
            return $"Name: {_name}\n" +
                $"Surname: {_surname}";
        }
    }

    class Employee : Human, IWorker
    {       
        private int salary;
        private bool isWorking;

        public int Salary { get { return salary; } }

        public bool IsWorking { get; set; }
        //public bool IsWorking { get => isWorking; set { isWorking = value; } }

        public Employee() { }
        public Employee(int salary, string name, string surname) : base(name, surname)
        {
            this.salary = salary;
        }

        public void Work()
        {
            Console.WriteLine("Working like employee!");
        }

        public override string ToString()
        {
            return "I`m employee";
        }
    }

    class Seller : IWorker
    {
        public bool IsWorking { get; set; }

        public void Work()
        {
            Console.WriteLine("Sell products.");
        }

        public override string ToString()
        {
            return "I`m seller\n";
        }
    }

    class Manager : Human, IManager
    {
        public string Department { get; }

        public Manager(string name, string surname, string department) : base(name, surname)
        {
            this.Department = department;
        }
        public void Control()
        {
            Console.WriteLine("Controling department " + Department);
        }

        public override string ToString()
        {
            return "I`m manager";
        }

        void Work()
        {

        }
    }


    class Program
    {
        static void Main(string[] args)
        {
            Employee e = new Employee(1100, "Kolya", "Voz");
            Manager m = new Manager("Tania", "Lubasina", "Design");
            
            Human h = e;
			Console.ForegroundColor = ConsoleColor.Blue;
			Console.WriteLine(h.ToString());
			Console.ForegroundColor = ConsoleColor.White;

			IWorker worker = new Employee(1100, "Kolya", "Voz");
            worker.Work();

            worker = new Seller();
            worker.Work();

            if(worker is Employee)
            {
                Employee temp = worker as Employee;
                Console.WriteLine(temp.ToString());
            }
            else if (worker is Seller)
            {
                Seller temp = worker as Seller;
                Console.WriteLine(temp.ToString());
            }

            Console.ReadKey();
        }
    }
}
