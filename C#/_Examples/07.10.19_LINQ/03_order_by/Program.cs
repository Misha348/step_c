﻿using System.Collections.Generic;
using System.Linq;
using static System.Console;

namespace _03_order_by
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arrayInt = { 5, 34, 67, 12, 94, 42 };

            IEnumerable<int> query = from i in arrayInt
                                     where i % 2 == 0
                                     orderby i  // ascending - default
                                     select i;

            // Метод розширення
            var result = arrayInt.Where(i => i % 2 == 0).OrderByDescending(i => i);

            WriteLine("Even elements descending:");
            foreach (int item in result)
            {
                Write($"{item}\t");
            }
            WriteLine();           
        }
    }
}
