﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _09_sets
{
    class Program
    {
		static void Main(string[] args)
		{
			string[] soft = { "Blue", "Grey", "Yellow", "Cyan", "Grey", "Yellow" };
			string[] hard = { "Yellow", "Magenta", "White", "Blue" };
			IEnumerable<string> result;

			// Except -----------------------        
			// разность множеств
			result = soft.Except(hard);
			foreach (string s in result)
				Console.WriteLine(s);
			Console.WriteLine(new string('=', 15));

			// Intersect ---------------------------
			// пересечение множеств
			result = soft.Intersect(hard);
			foreach (string s in result)
				Console.WriteLine(s);
			Console.WriteLine(new string('=', 15));

			// Union ---------------------------
			// объединение множеств
			result = soft.Union(hard);
			foreach (string s in result)
				Console.WriteLine(s);
			Console.WriteLine(new string('=', 15));

			// Concat -------------
			// объединение двух
			result = soft.Concat(hard);
			foreach (string s in result)
				Console.WriteLine(s);
			Console.WriteLine(new string('=', 15));

			// Distinct ----------------
			// удаления дублей
			result = soft.Distinct();
			foreach (string s in result)
				Console.WriteLine(s);
			Console.WriteLine(new string('=', 15));

		}
    }
}
