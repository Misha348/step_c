﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exampl
{
	class Point
	{
		public int X { get; set; }
		public int Y { get; set; }

		// Constructors
		public Point() : this(0, 0) { }
		public Point(int x, int y)
		{
			X = x;
			Y = y;
		}
		
		public override string ToString()
		{
			return $"Point: x: {X}, y: {Y}";
		}

		// Override Equals and GetHashCode        
		public override bool Equals(object obj)
		{
			Point point = (Point)obj;
			return X == point?.X && Y == point?.Y;
			//return point != null &&
			//       X == point.X &&
			//       Y == point.Y;
		}

		public override int GetHashCode()
		{
			var hashCode = 1861411795;
			hashCode = hashCode * -1521134295 + X.GetHashCode();
			hashCode = hashCode * -1521134295 + Y.GetHashCode();
			return hashCode;
		}

		
       // Unar operators
        public static Point operator -(Point p)
		{
			Point p1 = new Point
			{
				X = p.X * -1,
				Y = p.Y * -1,
			};
			return p1;
		}
		public static Point operator ++(Point p)
		{
			p.Y++;
			p.X++;
			return p;
		}
		public static Point operator --(Point p)
		{
			p.Y--;
			p.X--;
			return p;
		}

		public static Point operator +(Point p1, Point p2)
		{
			Point p3 = new Point
			{
				X = p1.X + p2.X,
				Y = p1.Y + p2.Y
			};
			return p3;
		}

		// Binar operators
		public static Point operator -(Point p1, Point p2)
		{
			Point p3 = new Point
			{
				X = p1.X - p2.X,
				Y = p1.Y - p2.Y
			};
			return p3;
		}

		public static Point operator *(Point p1, Point p2)
		{
			Point p3 = new Point
			{
				X = p1.X * p2.X,
				Y = p1.Y * p2.Y
			};
			return p3;
		}

		public static Point operator /(Point p1, Point p2)
		{
			Point p3 = new Point
			{
				X = p1.X / p2.X,
				Y = p1.Y / p2.Y
			};
			return p3;
		}

		// Logical operators
		public static bool operator ==(Point p1, Point p2)
		{
			return p1.Equals(p2);
		}
		public static bool operator !=(Point p1, Point p2)
		{
			return !(p1 == p2);
		}

		public static bool operator >(Point p1, Point p2)
		{
			return p1.X + p1.Y > p2.X + p2.Y;
		}
		public static bool operator <(Point p1, Point p2)
		{
			return !(p1 > p2);
		}

		public static bool operator >=(Point p1, Point p2)
		{
			return p1.X + p1.Y >= p2.X + p2.Y;
		}
		public static bool operator <=(Point p1, Point p2)
		{
			return !(p1 >= p2);
		}

		// true/false op

		public static bool operator true(Point p)
		{
			return p.X != 0 || p.Y != 0;
		}
		public static bool operator false(Point p)
		{
			return p.X == 0 && p.Y == 0;
		}

		// upCast op
		public static explicit operator int(Point p)
		{
			return p.X + p.Y;
		}

		class Program
		{
			static void Main(string[] args)
			{
				//Point p1 = new Point()
				//{
				//    X = 3,
				//    Y = 2
				//};

				//Console.WriteLine("Point 1: " + p1);
				//Console.WriteLine("Point 1(++p): " + ++p1);
				//Console.WriteLine("Point 1(-p): " + -p1);

				//Point p2 = -p1;

				 //Console.WriteLine("Point 1: " + p1);
				 //Console.WriteLine("Point 2: " + p2);
				 //Console.WriteLine("Point 1(p--): " + p1--);

				 //Point p3 = p1 - p2;
				 //Console.WriteLine("Point 3(p1 + p2): " + p3);

				 //int a = 5;
				 //int b = 10;
				 //a = b;

				 //Point p1 = new Point(2, 5);
				 //Point p2 = new Point(2, 5);
				 //p1 = p2;

				 //Console.WriteLine(p1);
				 //p2++;
				  //Console.WriteLine(p1);

				//// Reference Equals
				  //if (Object.ReferenceEquals(p1, p2))
				  //    Console.WriteLine("Reference Equals");
				 //else
				 //    Console.WriteLine("Reference Not Equals");

				 //// Equals
				 //if (p1.Equals(p2))
				  //    Console.WriteLine("Equals");
				  //else
				 //    Console.WriteLine("Not Equals");
​
				  //Point p1 = new Point(2, 5);
				 //Point p2 = null;

				 //// Equals
				 //if (p1.Equals(p2))
				 //    Console.WriteLine("Equals");
				 //else
				 //    Console.WriteLine("Not Equals");

				 //// Equals
				 //if (p1 == p2)
			 	 //    Console.WriteLine("==");
				 //else
				 //    Console.WriteLine("!=");

				//Point p1 = new Point(0, 0);
				 //Point p2 = null;
​
				 //if (p1)
				 //    Console.WriteLine("TRUE");
				 //else
				 //    Console.WriteLine("FALSE");
​
				 //// error
				 ////if (p2)
				 ////    Console.WriteLine("TRUE");
				 ////else
				 ////    Console.WriteLine("FALSE");

				 //p1++;
				 //if (p1)
				 //    Console.WriteLine("TRUE");
				 //else
				 //    Console.WriteLine("FALSE");
​
				 int a = 5;
				double d = 6.7;
​
				 d = a;
				a = (int)d;
​
				 Point p1 = new Point(1, 1);
				a = (int)p1;
				d = (double)p1;
​
				 Console.WriteLine(a);

				


				
				

				
​
				​
				 
​
				
​
				
		​
				

​



			}

		}


	}

}