﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinaryReader
{
    class Program
    {
        static void Main(string[] args)
        {

            FileStream file = File.Open($@"test.txt", FileMode.Open);

            var reader = new System.IO.BinaryReader(file);

            //long number = 100;
            //var bytes = new byte[] { 10, 20, 50, 100 };
            //string s = "hunger";

            
            long number = reader.ReadInt64();
            byte[] bytes = reader.ReadBytes(4);
            string s = reader.ReadString();
            //reader.Re

            reader.Close();

            Console.WriteLine(number);
            foreach (byte b in bytes)
            {
                Console.Write("[{0}]", b);
            }

            Console.WriteLine();
            Console.WriteLine(s);


            Console.ReadKey();
        }
    }
}
