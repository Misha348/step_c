﻿using System;
using System.IO;
using System.Text;

namespace StreamWriterDemo
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("Enter string to put in file:");
            string text = Console.ReadLine();

            using (FileStream fstream = new FileStream(@"note.txt", FileMode.OpenOrCreate))
            {
                byte[] array = Encoding.Default.GetBytes(text);
                fstream.Write(array, 0, array.Length);
                Console.WriteLine("Text is written to file");
            }


            using (FileStream fstream = File.OpenRead(@"note.txt"))
            {
                byte[] array = new byte[fstream.Length];
                fstream.Read(array, 0, array.Length);
                string textFromFile = Encoding.Default.GetString(array);
                Console.WriteLine($"String from file: {textFromFile}");
            }


            FileStream file = File.Create(@"test.txt");

            // 1.
            var writer = new StreamWriter(file);
            writer.WriteLine("Hello");
            writer.Close();
            file.Close();

            // 2.
            writer = File.CreateText(@"test.txt");
            writer.WriteLine("Hello");
            writer.Close();
            file.Close();

            // 3.
            File.WriteAllText(@"test.txt", "Hello");
            file.Close();

            // 4.
            file = null;
            file = File.Open(@"test.txt", FileMode.Open, FileAccess.Write, FileShare.Write);
            file?.Close();


            // 5.
            file = File.OpenWrite(@"test.txt");
            file.Close();

            file = File.Open(@"test.txt", FileMode.OpenOrCreate, FileAccess.Write, FileShare.Write);
            file.Close();

        }
    }
}
