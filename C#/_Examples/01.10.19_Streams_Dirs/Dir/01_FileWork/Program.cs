﻿using System;
using System.IO;


namespace _01_FileWork
{
    class Program
    {
        static void Main(string[] args)
        {
			
            DirectoryInfo dir = new DirectoryInfo(@"E:\");

            Console.WriteLine("Directory: {0}", dir.FullName);
            foreach (var file in dir.GetDirectories())
            {
                Console.WriteLine($"File: {file.Name}  time:  {file.CreationTime} atr: {file.Attributes}" );
            }

            Console.ReadKey();
        }
    }
}
