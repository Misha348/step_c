﻿using System;
using System.Diagnostics;
using System.IO;

// Отслеживание изменений в системе.

namespace FileSystemWatcherDemo
{
    class Program
    {
        static void Main()
        {

            var watcher = new FileSystemWatcher { Path = @"." };
            watcher.Created += new FileSystemEventHandler(WatcherChanged);
            //  watcher.Deleted += WatcherChanged;
            watcher.EnableRaisingEvents = true;

            var change = watcher.WaitForChanged(WatcherChangeTypes.All);
            Console.WriteLine(change.ChangeType);
        }

        static void WatcherChanged(object sender, FileSystemEventArgs e)
        {
            Console.WriteLine("Directory changed({0}): {1}", e.ChangeType, e.FullPath);
        }
    }
}
