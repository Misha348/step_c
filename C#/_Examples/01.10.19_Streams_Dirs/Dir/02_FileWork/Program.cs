﻿using System;
using System.IO;

namespace _02_FileWork
{
    class Program
    {
        static void Main(string[] args)
        { 

            DriveInfo[] drives = DriveInfo.GetDrives();
            foreach (DriveInfo drive in drives)
            {
                Console.WriteLine("Drive: {0} Type: {1}", drive.Name, drive.DriveType);
            }
            Console.ReadKey();
        }
    }
}
