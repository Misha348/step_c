﻿using System;
using System.IO;

namespace FileInfo_Copy
{
    class Program
    {
        static void Main()
        {
            string param = $@"E:\test";

            var file = new DirectoryInfo(param);

            try
            {
				file.Delete();
                Console.WriteLine("File deleted!");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            // Delay.
            Console.ReadKey();
        }
    }
}
