﻿using System;
using System.IO;

namespace FileInfoDemo
{
    class Program
    {
        static void Main()
        {

            FileInfo file = new FileInfo(@"E:\CLEANING_WIND_OS.txt");

            if (file.Exists)
            {
                Console.WriteLine("FileName : {0}", file.Name);
                Console.WriteLine("Path     : {0}", file.FullName);
                Console.WriteLine("Dir      : {0}", file.Directory);

            }
            else
            {
                Console.WriteLine("File not existed.");
            }


            Console.ReadKey();
        }
    }
}

