﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication12
{
    class Program
    {
        class MyClass
        {
            public string field;
            public void Method()
            {
                Console.WriteLine(field);
            }
        }
        class MyClass1
        {
            private string field = null;

            public void SetField(string value) //  mutator  (setter)
            {
                field = value;
            }

            public string GetField()           // accessor  (getter)
            {
                return field;
            }
        }
        class MyClass2
        {
            private string field = null;

            //property = field + method
            public string Field
            {
                set
                {

                    field = value;
                }

                get
                {
                    return field;
                }
            }

        }
        class MyClass3
        {
            private string field = null;

            public int MyProperty { get; set; }

            private int number;
            public int PropFull
            {
                get { return number; }
                set { number = value; }
            }


            public string Field
            {
                set
                {
                    if (value == "Exit")
                    {
                        Console.WriteLine("Unexpected exit!");
                    }
                    else
                    {
                        field = value;
                    }
                }

                get
                {
                    if (field == null)
                    {
                        return "field field is empty";
                    }
                    else if (field == "hello world")
                    {
                        return field.ToUpper() + "!";
                    }
                    else
                    {
                        return field;
                    }

                }
            }

            public void SetField(string value)
            {
                if (value == "Exit")
                {
                    Console.WriteLine("Unexpected exit!");
                }
                else
                {
                    field = value;
                }
            }

            public string GetField()
            {
                if (field == null)
                {
                    return "field field is empty";
                }
                else if (field == "hello world")
                {
                    return field.ToUpper() + "!";
                }
                else
                {
                    return field;
                }
            }
        }
        class Constants
        {
            private double pi = 3.14d;
            private double e = 2.71D;

            // WriteOnly Property
            public double Pi
            {
                set { pi = value; }
            }

            // ReadOnly Property
            public double E
            {
                get { return e; }
            }
        }
        class Point
        {
            // fields
            private int x, y;

            private readonly int width;

            // properties
            public int X
            {
                get { return x; }
            }

            public int Y
            {
                get { return y; }
            }

            public Point()
            {
                Console.WriteLine("Def constructor");
            }

            public Point(int x, int y)
            {
                Console.WriteLine("Custom constructor");
                this.x = x;
                this.y = y;
            }
        }
        class Point2
        {
            private int x, y;

            public int Y
            {
                get { return y; }
            }

            public int X
            {
                get { return x; }
            }

            // Def con
            //public Point2()
            //{
            //    Console.WriteLine("Def con!");
            //}

            public Point2(int x, int y)
            {
                Console.WriteLine("Custom constructor");
                this.x = x;
                this.y = y;
            }
        }
        class Point3
        {
            private int x, y;
            private string name;

            public int X
            {
                get { return x; }
            }

            public int Y
            {
                get { return y; }
            }

            public string Name
            {
                get { return name; }
            }

            //Constructors
            public Point3(int x, int y)
            {
                Console.WriteLine("Constructor with 2 params");
                this.x = x;
                this.y = y;
            }

            // call Point3(int x, int y) with parameters
            public Point3(string name) : this(300, 200)
            {
                Console.WriteLine("Constructor with one parameter");
                this.name = name;
            }
        }
        public class Author
        {
            static public int counter;

            static Author()
            {
                counter = 0;
            }

            //Auto-implemented properties
            public string Name { get; set; }
            public string Book { get; set; }
        }
        class MyClass4
        {
            public string Book { get; set; }
            public void Method()
            {
                Console.WriteLine("Hello world!");
            }
        }
        class MyClass5
        {
            public void CallMethod(MyClass4 my)
            {
                my.Book = "ertrtrttertrrter";
                my.Method();
            }
        }

        static void Main(string[] args)
        {
            //var v = new MyClass3();

            //v.SetField("Hello");
            //v.Field = "Exit";

            //Console.WriteLine(v.GetField());
            //Console.WriteLine(v.Field);


            //Author myAuthor = new Author
            //{
            //    Name = "Oleg",
            //    Book = "GoF"

            //};

            //var p1 = new Author
            //{
            //    Name = " asdsd",
            //    Book = "sdfsdfs"
            //};

            //var p2 = new Author { Name = "34343", Book = "8987876" };
            //p1 = p2;
            //Console.WriteLine(p1.Name);

            //var d1 = new MyClass4
            //{
            //    Book = "2334233423",                
            //};
            //Console.WriteLine(d1.Book);            

            //var d2 = new MyClass5();
            //d2.CallMethod(d1);
            //Console.WriteLine(d1.Book);
            ////Console.WriteLine();


            var cv = new MyClassBlaBla()
            {
                Name = "Olga",
                Surname = "Suprun"
            };


            //double mark = 0;
            //string name = null;

            cv.GetAll(out double mark, out string name, out string surname);

            Console.WriteLine("Name: " + name + " - Mark: " + mark);

            Console.WriteLine(cv.Name + " - " + cv.Surname);
           
            //weak link
            new MyClass4().Method();

            Console.ReadKey();
        }
    }
}
