﻿using System;

// Делегаты.

namespace Delegates
{
    static class MyClass
    {

        public static void Method()
        {
            Console.WriteLine("My de,mn..nm,.legate ");
        }
    }

    public delegate void MyDelegate();  

    class Program
    {
        static void Main()
        {
            MyDelegate myDelegate = new MyDelegate(MyClass.Method); 

            myDelegate.Invoke();
            myDelegate();        

            Console.ReadKey();
        }
    }
}
