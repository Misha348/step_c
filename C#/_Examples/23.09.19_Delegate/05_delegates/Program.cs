﻿using System;

namespace Delegates
{

    public delegate void MyDelegate();

    class Program
    {
        static void Main()
        {

            MyDelegate myDelegate = delegate {
                                                for (int i = 0; i < 10; i++)
                                                {
                                                    Console.WriteLine($"{i} Hello world!");
                                                }
              
                                             };
            myDelegate();

            Console.ReadKey();
        }
    }
}