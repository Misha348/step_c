﻿using System;

namespace Delegates
{

    class MyClass
    {
  
        public void Method()
        {
            Console.WriteLine("My klllllllldelegate ");
        }
    }


    public delegate void MyDelegate(); 

    class Program
    {
        static void Main()
        {
            MyClass instance = new MyClass();
            MyDelegate myDelegate = new MyDelegate(instance.Method); 

            myDelegate.Invoke();   
            myDelegate();  
                   
            Console.ReadKey();
        }
    }
}
