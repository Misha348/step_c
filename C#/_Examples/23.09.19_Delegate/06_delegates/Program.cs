﻿using System;

// Анонимные (лямбда) методы.

namespace Delegates
{
    // Создаем класс делегата.
    public delegate int MyDelegate(int kx, int ky);

    class Program
    {
        static void Main()
        {
            int summand1 = 1, summand2 = 2, sum = 0;

            MyDelegate myDelegate = delegate (int c, int d) 
                                            {
                                                return c + d;
                                            };
            sum = myDelegate(ky:summand2, kx:summand1);
            Console.WriteLine($"{summand1} + {summand2} = {sum}");
            Console.ReadKey();
        }
    }
}
