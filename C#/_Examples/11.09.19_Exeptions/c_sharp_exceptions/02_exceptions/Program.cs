﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02_exceptions
{
    class Program
    {
        static void Main()
        {
            Exception ex = new Exception("My Exception jkkljkljlll");

            try
            {
                throw ex;
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception catch!");
                Console.WriteLine(e.Message);
            }

            // Delay.
            Console.ReadKey();
        }
    }
}
