﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04_exceptions
{
    class MyClass
    {
        public void MyMethod()
        {
            Exception exception = new Exception("MyException Message");

            exception.HelpLink = "https://google.com";
            exception.Data.Add("Exception  reason: ", "Test exception");
            exception.Data.Add("Time invokation: ", DateTime.Now);
           
            throw exception;
        }
    }

    class Program
    {
        static void Main()
        {
            try
            {
                MyClass instance = new MyClass();
                instance.MyMethod();
            }
            catch (Exception e)
            {
                Console.WriteLine("Member name:             {0}", e.TargetSite);
                Console.WriteLine("Member class:            {0}", e.TargetSite.DeclaringType);
                Console.WriteLine("Member type:             {0}", e.TargetSite.MemberType);
                Console.WriteLine("Message:                 {0}", e.Message);
                Console.WriteLine("Source:                  {0}", e.Source);
                Console.WriteLine("Help Link:               {0}", e.HelpLink);
                Console.WriteLine("Stack:                   {0}", e.StackTrace);

                foreach (DictionaryEntry de in e.Data)
                    Console.WriteLine("{0} : {1}", de.Key, de.Value);
            }

            // Delay.
            Console.ReadKey();
        }
    }
}
