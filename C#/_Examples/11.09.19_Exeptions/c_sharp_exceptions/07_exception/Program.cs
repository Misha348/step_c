﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _07_exception
{
    class Program
    {
        static void Main()
        {
            int a = 1, n = 2;

            try
            {
                // connect to DB

                // read data
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("Zero division.");
                Console.WriteLine("a / (2 - n) = {0}", a / (2 - n));
                
               
            }
            catch (DivideByZeroException e)
            {
                Console.ForegroundColor = ConsoleColor.White;
                Console.BackgroundColor = ConsoleColor.Cyan;
                Console.WriteLine("Exception catch!");
                Console.WriteLine(e.Message);
            }
            catch (Exception e)
            {
                Console.ForegroundColor = ConsoleColor.White;
                Console.BackgroundColor = ConsoleColor.Red;
                Console.WriteLine("Def Exception catch!");
                Console.WriteLine(e.Message);
            }
            // використовується для звільнення ресурсів
            finally
            {
               //Console.ResetColor();
               Console.ForegroundColor  = ConsoleColor.Gray;
               Console.BackgroundColor = ConsoleColor.Black;

                // close file stream
                // close connect
            }

            Console.WriteLine("Press any key...");

            // Delay.
            Console.ReadKey();
        }
    }
}
