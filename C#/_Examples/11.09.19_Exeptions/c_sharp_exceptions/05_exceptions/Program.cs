﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _05_exceptions
{
    class UserException : Exception
    {      
        public void Method() 
        {
            Console.WriteLine("My exception!");
        }
    }

    class Program
    {
        static void Main()
        {
            try
            {
                throw new UserException();
            }
            catch (UserException e)
            {
                Console.WriteLine("Exception catch!");
                e.Method();
            }

            // Delay.
            Console.ReadKey();
        }
    }
}
