﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _06_indexes
{
    class MyClass
    {
        private int[] arrayWeight = new int[5];
        private string[] arrayNames = new string[5];

        public MyClass()
        {
            arrayNames[0] = "Bob";  arrayWeight[0] = 78;
            arrayNames[1] = "Vova"; arrayWeight[1] = 90;
            arrayNames[2] = "Olga"; arrayWeight[2] = 150;
            arrayNames[3] = "Arthur"; arrayWeight[3] = 120;
            arrayNames[4] = "Viktor"; arrayWeight[4] = 45;
        }

        // indexer
        //public int this[int index]
        //{
        //    get
        //    {
        //        return arrayNumbers[index];
        //    }
        //    set
        //    {
        //        arrayNumbers[index] = value;
        //    }
        //}

        public int this[string name]
        {
            get
            {
                for (int i = 0; i < arrayNames.Length; i++)
                {
                    if (arrayNames[i] == name)
                        return arrayWeight[i];
                }
                throw new Exception();
            }
            set
            {
                for (int i = 0; i < arrayNames.Length; i++)
                {
                    if (arrayNames[i] == name)
                        arrayWeight[i] = value;
                }
            }
        }
    }

    class Program
    {
        static void Main()
        {

            MyClass c = new MyClass();

            //c[0] = 30;
            //c[1] = 50;
            //c[2] = 60;
            //c[3] = 20;
            //c[4] = 90;
            //Console.WriteLine(c[0]);
            //Console.WriteLine(c[1]);
            //Console.WriteLine(c[2]);
            //Console.WriteLine(c[3]);
            //Console.WriteLine(c[4]);

            Console.WriteLine(c["Olga"]);
            c["Olga"] = 151;
            c["Olga"] -= 2;
            Console.WriteLine(c["Olga"]);


            // Delay.
            Console.ReadKey();
        }
    }
}

