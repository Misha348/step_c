﻿using System;
using System.Threading;
using System.Text;
using static System.Console;

namespace SimpleProject
{    
    class FinalizeExample : IDisposable
    {
        public FinalizeExample()
        {
            Console.WriteLine("Constructor Invoke!");
        }
        ~FinalizeExample()
        {
            Console.WriteLine("Finalize Invoke!");
            Console.WriteLine("Clear resources");
        }

        public void Dispose()
        {
            Console.WriteLine("Dispose Invoke!");
            Console.WriteLine("Clear resources");

            GC.SuppressFinalize(this);
        }
    }
    class DisposeExample : IDisposable
    {
        //используется для того, чтобы выяснить, вызывался
        //ли метод Dispose()
        private bool isDisposed = false;
        private void Cleaning(bool disposing)
        //вспомогательный метод
        {
            //убедиться, что ресурсы ещё не освобождены
            if (!isDisposed) //очищать только один раз
            {
                //если true, то освобождаем все
                //управляемые ресурсы
                if (disposing)
                {
                    WriteLine("Освобождение управляемых ресурсов");
                }
                WriteLine("Освобождение неуправляемых ресурсов");
            }
            isDisposed = true;
        }
        public void Dispose()
        {
            Console.WriteLine("Dispose Invoke!");
            //вызов вспомогательного метода
            //true - очистка инициирована пользователем объекта
            Cleaning(true);
            //запретить сборщику мусора осуществлять
            //финализацию
            GC.SuppressFinalize(this);
        }
        ~DisposeExample()
        {
            Console.WriteLine("Finalize Invoke!");
            //false указывает на то, что очистку
            //инициировал сборщик мусора
            Cleaning(false);
        }
        public void DoSomething()
        {
            WriteLine("Выполнение определенных операций");
            //throw new Exception();
        }
    }
    class Program
    {
        static void CreateObject()
        {
            FinalizeExample f = new FinalizeExample();
            f.Dispose();
        }
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;
            // CreateObject();
            // // GC.Collect();
            // 
            // Thread.Sleep(1000);
            // Console.WriteLine("Continue....");

            // var dis = new DisposeExample();
            // try
            // {
            //     dis.DoSomething(); // throw                
            // }           
            // finally
            // {
            //     dis.Dispose();
            // }

            using (var dis2 = new DisposeExample())
            {
                dis2.DoSomething();
            }
        }
    }
}