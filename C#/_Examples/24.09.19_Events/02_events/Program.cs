﻿using System;

namespace Events
{
    public delegate void EventDelegate();

    public class MyClass
    {
        EventDelegate myEvent = null;

        private bool flag1 = false;
        private bool flag2 = true;

        public event EventDelegate MyEvent
        {
            add { if (flag1)
                    {
                        myEvent += value;
                    }
                }  //def hendler can contain custom code
            remove { myEvent -= value; }
        }

        public void InvokeEvent()
        {
            myEvent?.Invoke();
        }
    }

    class Program
    {
        static private void Handler1()
        {
            Console.WriteLine("Event processor 1");
        }

        static private void Handler2()
        {
            Console.WriteLine("Event processor 2");
        }

        static void Main()
        {
            MyClass instance = new MyClass();

            instance.MyEvent += Handler1;//new EventDelegate(Handler1);
            instance.MyEvent += Handler2;// new EventDelegate(Handler2);
            instance.InvokeEvent();

            Console.WriteLine(new string('-', 20));

            instance.MyEvent -= Handler2;//new EventDelegate(Handler2);
            instance.InvokeEvent();

            Console.ReadKey();
        }
    }
}
