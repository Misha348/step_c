﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _07_events
{
    public delegate void ExamDelegate();

    class Student
    {
        int num;
        public Student(int num)
        {
            this.num = num;
        }

        public string Name { get; set; }
        public void PassExam()
        {
            Console.WriteLine($"Student {Name} pass exam! {num}");
        }

        public void BreakAnArms()
        {
            Console.WriteLine($"Student {Name} broken arms! {num}");
        }
    }

    class Teacher
    {

        public string Name { get; set; }

        private ExamDelegate examDelegate;

        public event ExamDelegate ExamEvent
        {
            add { examDelegate += value; }
            remove { examDelegate -= value; }
        }

        public void StartExam()
        {
            foreach (var item in examDelegate.GetInvocationList())
            {
                Console.WriteLine(item.Method.Name);
            } 

            // event                     
            // examEvent();

            // delegate
            // examDelegate?.Invoke();
            // examDelegate();
            examDelegate.Invoke();
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Student[] students = new Student[]
            {
                new Student(1) {Name = "Vasia"},
                new Student(2) {Name = "Bob"},
                new Student(3) {Name = "Maria"}
            };


            Teacher t = new Teacher();

            foreach (var st in students)
            {
                //t.ExamEvent += st.PassExam;      
                t.ExamEvent += students[0].PassExam;
                //t.examDelegate += st.PassExam;
            }

            // t.examDelegate = null; // error

            t.StartExam();
            Console.WriteLine(new string('-', 20));

            t.ExamEvent -= students[0].PassExam;
            //t.ExamEvent += students[0].BreakAnArms;

            t.StartExam();

            Console.ReadKey();
        }        
    }
}
