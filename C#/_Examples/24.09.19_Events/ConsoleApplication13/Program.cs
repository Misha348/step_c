﻿using System;

namespace Events
{
    public delegate void EventDelegate();

    public class MyClass
    {
        public event EventDelegate myEvent = null;

        public void InvokeEvent()
        {
            myEvent.Invoke();
        }
    }

    class Program
    {
        static private void Handler1()
        {
            Console.WriteLine("Event handler 1");
        }



        static private void Handler2()
        {
            Console.WriteLine("Event handler 2");
        }

        static void Main()
        {
            MyClass instance = new MyClass();

            // subsribing to event
            instance.myEvent += new EventDelegate(Handler1);
            instance.myEvent += Handler2;

            instance.InvokeEvent(); //call event

            Console.WriteLine(new string('-', 20));

            instance.myEvent -= new EventDelegate(Handler2);

            instance.InvokeEvent();

            Console.ReadKey();
        }
    }
}
   




