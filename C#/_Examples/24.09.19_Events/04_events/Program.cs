﻿using System;

// События (abstract and virtual).

namespace Events
{
    public delegate void EventDelegate();

    interface IInterface
    {
        event EventDelegate MyEvent; // abstract event
    }

    public class BaseClass : IInterface
    {
        EventDelegate myEvent = null;

        public virtual event EventDelegate MyEvent //virtual event
        {
            add { myEvent += value; }
            remove { myEvent -= value; }
        }

        public void InvokeEvent()
        {
            myEvent.Invoke();
        }
    }

    public class DerivedClass : BaseClass
    {
        public override event EventDelegate MyEvent //overriding events
        {
            add
            {
                base.MyEvent += value;
                Console.WriteLine($"To base event added - {value.Method.Name}");
            }
            remove
            {
                base.MyEvent -= value;
                Console.WriteLine($"From base event removed - {value.Method.Name}");
            }
        }
    }

    class Program
    {
        static private void Handler1()
        {
            Console.WriteLine("Event handler 1");
        }

        static private void Handler2()
        {
            Console.WriteLine("Event handler 2");
        }

        static void Main()
        {
            DerivedClass instance = new DerivedClass();

            instance.MyEvent += new EventDelegate(Handler1);
            instance.MyEvent += new EventDelegate(Handler2);

            instance.InvokeEvent();

            Console.WriteLine(new string('-', 20));

            instance.MyEvent -= new EventDelegate(Handler2);
            instance.InvokeEvent();

            Console.ReadKey();
        }
    }
}
