﻿using System;

namespace Events
{
    public delegate void EventDelegate(int k);

    public class MyClass
    {
        public event EventDelegate MyEvent = null;

        public void PrintClassName() => Console.WriteLine("MyClass");


        public void InvokeEvent()
        {
            MyEvent?.Invoke(100);
        }
    }

    class Program
    {
        static private void Handler1(int a)
        {
            Console.WriteLine("Event handler 1");
        }

        static private void Handler2(int a)
        {
            Console.WriteLine("Event handler 2");
        }

        static void Main()
        {
            MyClass instance = new MyClass();

            // delegate (params) { body }
            // () => { body }

            // adding handlers
            instance.MyEvent += new EventDelegate(Handler1);
            instance.MyEvent += Handler2;
            instance.MyEvent += delegate 
            {
                Console.WriteLine("anonymous method 1.");
            };

            instance.MyEvent += Handler2;
            instance.MyEvent += delegate (int number) { Console.WriteLine("bla-bla - " + number); };
            instance.MyEvent += (number) => Console.WriteLine("bla-bla - " + number);

            // instance.MyEvent -= delegate (int number) { Console.WriteLine("bla-bla - " + number); };
            instance.MyEvent -= Handler2;

            instance.InvokeEvent();

            Console.WriteLine(new string('-', 20));
            instance.MyEvent -= new EventDelegate(Handler2);

            // dont remove anonymous methods
            //instance.MyEvent -= delegate { Console.WriteLine("Anonimous method 1."); };

            instance.InvokeEvent();
            Console.ReadKey();
        }
    }
}
