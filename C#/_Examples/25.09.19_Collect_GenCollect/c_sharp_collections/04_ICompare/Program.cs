﻿using System;
using System.Collections;

namespace ArrayListSort
{
    class Program
    {
        static void Main()
        {
            var list = new ArrayList { 2, 3, 1 };

            list.Sort(new DescendingComparer(1));

            foreach (int item in list)
            {
                Console.WriteLine(item);
            }
            Console.ReadKey();
        }
    }

    public class DescendingComparer : IComparer
    {
        // ignore UpperCase and LowerCase
        readonly CaseInsensitiveComparer comparer = new CaseInsensitiveComparer();

        readonly int SortingDirection;

        public DescendingComparer()
        {
        }
        public DescendingComparer(int x)
        {
            SortingDirection = x;
        }

         public int Compare(object x, object y)
         {
             int result = SortingDirection * comparer.Compare(x, y);
        
             //int result = comparer.Compare(y, x);
             return result;
         }
    }
}