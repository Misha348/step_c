﻿using System.Collections;
using static System.Console;
using System;
namespace ArrayListDemo
{
    class Program
    {
        static void Main()
        {
            var list = new ArrayList(20);
            WriteLine(list.Capacity);
            list.TrimToSize();
            WriteLine(list.Count);
            list.Add(50);
            WriteLine(list.Count);
            WriteLine(list.Capacity);

            // Добавление в набор одиночных элементов используя метод Add.
            string s = "Hello";
            list.Add(s);
            list.Add("hi");
            list.Add(50);
            list.Add(new object());
            list.Add(new int());

            // Добавление в набор групп элементов используя метод AddRange.
            string[] anArray = new[] { "more", "or", "less" };
            list.AddRange(anArray);

            object[] anotherArray = new[] { new object(), new ArrayList() };
            list.AddRange(anotherArray);

            list.Insert(3, "Hey All");
           
            var moreString = new[] { "goodnight", "see ya" };
            list.InsertRange(4, moreString);

            list[3] = "Hey All 2";


            list.Add("Hello");
            list.Remove("Hello");
            foreach (var item in list)
            {
                System.Console.WriteLine(item);
            }
            list.RemoveAt(0);

            var listFixed = ArrayList.FixedSize(list);
            System.Console.WriteLine(list.IsFixedSize);
            System.Console.WriteLine(listFixed.IsFixedSize);
            //listFixed.Add(12);
            //listFixed.Add(10);

            list.RemoveRange(0, 4);

            string myString = "My String";

            if (list.Contains(myString))
            {
                int index = list.IndexOf(myString);
                list.RemoveAt(index);
            }
            else
            {
                list.Clear();
            }
        }
    }
}
