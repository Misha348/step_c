﻿using System;
using System.Collections;

namespace QueueDemo
{
    class Program
    {
        static void Main()
        {
            var queue = new Queue();
            queue.Enqueue("First");
            queue.Enqueue("Second");
            queue.Enqueue("Third");
            queue.Enqueue("Fourth");

            //get First 
            object element = queue.Peek();
            Console.WriteLine(element as string); //First

            Console.WriteLine(new string('-', 10));

            if (element is string)
            {
                Console.WriteLine(queue.Dequeue());  
            }

            while (queue.Count > 0)
            {
                Console.WriteLine(queue.Dequeue()); // Second, Third, Fourth.
            }

            element = queue.Peek();
            Console.WriteLine(element as string);

            Console.ReadKey();
        }
    }
}
