﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace ArrayListDemo
{
    class MyClass
    {

        private int secondValue;

        public int GetSecondValue() { return secondValue; }

        public void SetSecondValue(int myValue) { secondValue = myValue; }

        private int myValue;
        public int MyProperty
        {
            get { return myValue; }
            set { myValue = value; }
        }

        public virtual void ShowText(string text)
        {
            Console.WriteLine(text);
        }


    }

    class SecondClass : MyClass
    {


        public int ThirdValue { get; set; }
        public void ShowText(int k, bool c, string text = "ssdl;k")
        {
            base.ShowText(text + text + text);
            Console.WriteLine($" {text} {k} {c}");
        }
    }
    class Program
    {
        static void Main()
        {
            var genList = new List<MyClass>();

            genList.Add(new MyClass { MyProperty = 67 });
            genList.Add(new MyClass { MyProperty = 74 });
            genList.Add(new MyClass { MyProperty = 8980 });

            foreach (var item in genList)
            {
                Console.WriteLine(item.MyProperty);
                item.MyProperty = 300;
                item.SetSecondValue(34);
                Console.WriteLine(item.GetSecondValue());
            }



            var list = new ArrayList();

            list.Add("Hello");
            list.Add("Goodbye");
            list.Add(12);


            for (int i = 0; i < list.Count; i++)
            {
                Console.WriteLine(list[i]);
            }


            IEnumerator enumerator = list.GetEnumerator();
            while (enumerator.MoveNext())
            {
                Console.WriteLine(enumerator.Current);
            }


            foreach (var item in list)
            {
                //dont modify in foreach
                //list.Remove(item);
                Console.WriteLine(item);
            }
        }
    }
}
