﻿using System;
using System.Collections;

namespace StackDemo2
{
    class Program
    {
        static void Main()
        {
            var stack = new Stack();

            stack.Push("First");
            stack.Push("Second");
            stack.Push("Third");
            stack.Push("Fourth");

            // Peek() - get first
            if (stack.Peek() is string)
            {
                Console.WriteLine(stack.Pop());
            }


            while (stack.Count > 0)
            {
                Console.WriteLine(stack.Pop());
            }

            Console.WriteLine(stack.Pop());
            Console.WriteLine(stack.Pop());
            Console.WriteLine(stack.Pop());

            Console.ReadKey();
        }
    }
}

