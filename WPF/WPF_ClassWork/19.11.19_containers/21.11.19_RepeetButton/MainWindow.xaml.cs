﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace _21._11._19_RepeetButton
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        string op;
        public int CurrentCounter { get; set; }
        public int AddIndex { get; set; }

        public MainWindow()
        {
            InitializeComponent();
            CurrentCounter = 0;
            AddIndex = 0;
        }

        private void RepeatButton_Click(object sender, RoutedEventArgs e)
        {
            CurrentCounter++;
            Digit.Content = CurrentCounter.ToString();
        }

        private void Button_Plus_Click(object sender, RoutedEventArgs e)
        {
            CurrentCounter--;
            Digit.Content = CurrentCounter.ToString();

        }

        private void ButtonChecker()
        {
            foreach (var item in Table.Children)
            {
            //    if (item is RadioButton)
            //    {
            //        RadioButton tmp = item as RadioButton;
            //        tmp.IsChecked
            //            tmp.Content
            //        tmp.CheckedChanged += RadioButton3_Deserts_CheckedChanged;
            //    }
            }
        }

        private void Button_Add_Click(object sender, RoutedEventArgs e)
        {
            Random rnd = new Random();
            int a = rnd.Next(100, 200);
            int b = rnd.Next(100, 200);
            int c = rnd.Next(100, 200);


            Button nb = new Button();

            nb.Height = 20;
            nb.Content = CurrentCounter.ToString();

            nb.Background = new SolidColorBrush(Color.FromRgb((byte)a, (byte)b, (byte)c));

            nb.HorizontalContentAlignment = HorizontalAlignment.Center;
            nb.FontSize = 12;
            nb.Tag = true;
            
            if ((Elements.Children[0] as Button).Tag.ToString() == (true).ToString())
            {
                Button nb2 = new Button();

                nb2.Height = 20;
                nb2.Content = op;

                nb2.Background = new SolidColorBrush(Color.FromRgb((byte)a, (byte)b, (byte)c));

                nb2.HorizontalContentAlignment = HorizontalAlignment.Center;
                nb2.FontSize = 12;
                nb2.Tag = false;

                Elements.Children.Insert(AddIndex, nb2);
            }

            Elements.Children.Insert(AddIndex, nb);
        }

        private void RadioButton_Checked(object sender, RoutedEventArgs e)
        {
            op = (sender as RadioButton).Content.ToString();
        }
    }
}
