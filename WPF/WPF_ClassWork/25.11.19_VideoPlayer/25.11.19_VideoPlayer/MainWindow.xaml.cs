﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace _25._11._19_VideoPlayer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
		
        public MainWindow()
        {
			InitializeComponent();
			//timer = new DispatcherTimer();
			//timer.Interval = TimeSpan.FromMilliseconds(500);
			//timer.Tick += new EventHandler()
        }

		private void ButtonPause_Click(object sender, RoutedEventArgs e)
		{
			player.Pause();
		}		


		//private void Test()
		//{
		//	TimeSpan ts = player.NaturalDuration.TimeSpan;
		//	Time_Possition.Maximum = ts.TotalSeconds;
		//	timer.Start();
		//}

		//private void Time_Possition_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
		//{
		//	player.Position = TimeSpan.FromSeconds(Time_Possition.Value);
		//	((Slider)sender).SelectionEnd = e.NewValue;
		//}

		private void Button_Play_Click(object sender, RoutedEventArgs e)
		{
			player.Play();
		}

		private void Button_5_sec_Forward_Click(object sender, RoutedEventArgs e)
		{
			Video_Time_Controller.Value = player.Position.TotalSeconds + 5;
			player.Position = TimeSpan.FromSeconds(Video_Time_Controller.Value);		
			Video_Time_Controller.SelectionEnd = Video_Time_Controller.Value;
		}

		private void Button_5_sec_Backward_Click(object sender, RoutedEventArgs e)
		{
			Video_Time_Controller.Value = player.Position.TotalSeconds - 5;
			player.Position = TimeSpan.FromSeconds(Video_Time_Controller.Value);
			Video_Time_Controller.SelectionEnd = Video_Time_Controller.Value;
		}



		private void Choose_File_Click(object sender, RoutedEventArgs e)
		{
			OpenFileDialog ofd;
			ofd = new OpenFileDialog();
			ofd.AddExtension = true;
			ofd.DefaultExt = "*.*";
			ofd.Filter = "Media Files (*.*)|*.*";
			ofd.ShowDialog();

			player.Source = new Uri(ofd.FileName);

			DispatcherTimer dispatcherTimer = new DispatcherTimer();
			dispatcherTimer.Tick += new EventHandler(Timer_Tick);
			dispatcherTimer.Interval = TimeSpan.FromSeconds(1);
			dispatcherTimer.Start();

			//new TimeSpan(0, 0, 1);
			

			player.Play();
			//if (player.NaturalDuration.HasTimeSpan)
			Video_Time_Controller.Maximum = 1000;
			//player.NaturalDuration.TimeSpan.Seconds
		}

		private void Timer_Tick(object sender, EventArgs e)
		{
			Video_Time_Controller.Value = player.Position.TotalSeconds;

			CurrTime.Content = String.Format("{0}", player.Position.ToString(@"hh\:mm\:ss") );
			TotalTime.Content = String.Format("{0}", player.NaturalDuration.TimeSpan.ToString(@"hh\:mm\:ss"));
		}

		//private void Video_Volume_Controller_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
		//{
		//	player.Volume = (double)Video_Volume_Controller.Value;
		//}

		private void Video_Time_Controller_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
		{
			player.Position = TimeSpan.FromSeconds(Video_Time_Controller.Value);
			Video_Time_Controller.SelectionEnd = Video_Time_Controller.Value;
		}

		private void Full_Screen_Menu_Item_Click(object sender, RoutedEventArgs e)
		{
			WindowState = WindowState.Maximized;
			Full_Screen_Menu_Item.IsChecked = true;
			Normal_Screen_Menu_Item.IsChecked = false;
		}

		private void Normal_Screen_Menu_Item_Click(object sender, RoutedEventArgs e)
		{
			WindowState = WindowState.Normal;
			Normal_Screen_Menu_Item.IsChecked = true;
			Full_Screen_Menu_Item.IsChecked = false;
		}

		private void Mute_Menu_Item_Click(object sender, RoutedEventArgs e)
		{
				if (Mute_Menu_Item.IsChecked != true)
				Volume.Value = 0.4;
		}

		private void Mute_Menu_Item_Checked(object sender, RoutedEventArgs e)
		{
			if (Mute_Menu_Item.IsChecked == true)
				Volume.Value = 0;
		}

		private void Player_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
		{
			if (e.ClickCount == 2)
			{
				if (WindowState == WindowState.Normal)
				{
					WindowState = WindowState.Maximized;
					Full_Screen_Menu_Item.IsChecked = true;
					Normal_Screen_Menu_Item.IsChecked = false;
				}
				else if (WindowState == WindowState.Maximized)
				{
					WindowState = WindowState.Normal;
					Normal_Screen_Menu_Item.IsChecked = true;
					Full_Screen_Menu_Item.IsChecked = false;
				}
			}
		}

		private void Volume_MouseWheel(object sender, MouseWheelEventArgs e)
		{
			player.Volume += (e.Delta > 0) ? 0.1 : -0.1;
		}		

		private double SetProgressBarValue(double MousePosition)
		{
			double ratio = MousePosition / Volume.ActualWidth;
			double ProgressBarValue = ratio * Volume.Maximum;
			return ProgressBarValue;
		}

		private void Volume_MouseDoubleClick(object sender, MouseButtonEventArgs e)
		{
			double mousePosition = e.GetPosition(Volume).X;
			Volume.Value = SetProgressBarValue(mousePosition);
		}	

		private void Fill_Click(object sender, RoutedEventArgs e)
		{
			player.Stretch = Stretch.Fill;

			Fill.IsChecked = true;
			None.IsChecked = false;
			Uniform.IsChecked = false;
			UniformFill.IsChecked = false;
		}

		private void None_Click(object sender, RoutedEventArgs e)
		{
			player.Stretch = Stretch.None;

			Fill.IsChecked = false;
			None.IsChecked = true;
			Uniform.IsChecked = false;
			UniformFill.IsChecked = false;
		}

		private void Uniform_Click(object sender, RoutedEventArgs e)
		{
			player.Stretch = Stretch.Uniform;

			Fill.IsChecked = false;
			None.IsChecked = false;
			Uniform.IsChecked = true;
			UniformFill.IsChecked = false;
		}

		private void UniformFill_Click(object sender, RoutedEventArgs e)
		{
			player.Stretch = Stretch.UniformToFill;

			Fill.IsChecked = false;
			None.IsChecked = false;
			Uniform.IsChecked = false;
			UniformFill.IsChecked = true;
		}		

		private void Half_Checked(object sender, RoutedEventArgs e)
		{
			player.SpeedRatio = 0.5;
		}

		private void Normal_Checked(object sender, RoutedEventArgs e)
		{
			player.SpeedRatio = 1;
		}

		private void OneandHalf_Checked(object sender, RoutedEventArgs e)
		{
			player.SpeedRatio = 1.5;
		}

		private void Twice_Checked(object sender, RoutedEventArgs e)
		{
			player.SpeedRatio = 2.0;
		}		
	}
}
