﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Figures
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Point StartPoint { get; set; }
        private Rectangle Rect { get; set; }

        public MainWindow()
        {
            InitializeComponent();
        }

        private void Rectangle_Click(object sender, RoutedEventArgs e)
        {
            Butt_Rectangle.IsEnabled = true;
        }

        private void Circle_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Line_Click(object sender, RoutedEventArgs e)
        {

        }

        private void BrockLine_Click(object sender, RoutedEventArgs e)
        {

        }






        private void Field_MouseDown(object sender, MouseButtonEventArgs e)
        {
            StartPoint = e.GetPosition(Field);
        }

        private void Field_MouseUp(object sender, MouseButtonEventArgs e)
        {
           var  endPoint = e.GetPosition(Field);
            Rectangle rect = new Rectangle()
            {
                Height = endPoint.Y - StartPoint.Y,
                Width = endPoint.X - StartPoint.X,
                Fill = new SolidColorBrush(Colors.Red)
            };

            Canvas.SetLeft(rect, StartPoint.X);
            Canvas.SetTop(rect, StartPoint.Y);
            Field.Children.Add(rect);



        }
    }
}
