﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace _11._27._19_KeyBoard
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		public DispatcherTimer Timer { get; private set; }
		public int TimeCounter { get; set; }
		public int MinuteCounter { get; set; }
		public int SecondsCounter { get; set; }		

		public int CurrentSymbol { get; set; }
		public int SelectionLength { get; set; }

		public int MatchesNumber { get; set; }
		public int ErrorsNumber { get; set; }

		public int J { get; set; }
		public int CurrSpeed { get; set; }
		public SolidColorBrush ColorBefore { get; set; }
		public bool PressEnable { get; set; }
		public bool IscapitalOn { get; set; }
		public bool IsEnglish { get; set; }

		public MainWindow()
        {			
			InitializeComponent();		
			SelectionLength = 1;
			MatchesNumber = 0;
			ErrorsNumber = 0;
			CurrSpeed = 0;
			MinuteCounter = 0;
			SecondsCounter = 0;
			ColorBefore = new SolidColorBrush();
			PressEnable = false;
			IscapitalOn = false;
			IsEnglish = true;

			TimeCounter = 0;			
			J = 0;			

			Timer = new DispatcherTimer
			{
				Interval = TimeSpan.FromSeconds(1)
			};
			Timer.Tick += Timer_Tick;
		}

		private void Timer_Tick(object sender, EventArgs e)
		{
			Time.Content = (++TimeCounter).ToString();
			SecondsCounter++;

			if (TimeCounter == 60)
			{
				Time1.Content = (++MinuteCounter).ToString();
				TimeCounter = 0;
				Time.Content = (++TimeCounter).ToString();
			}

			CurrSpeed = 60 * (SelectionLength - 1) / SecondsCounter;
			Spd.Content = CurrSpeed.ToString();
		}

		private void Button_Start_Click(object sender, RoutedEventArgs e)
		{
			Timer.Start();
			ButtonStart.IsEnabled = false;
			ButtonRestart.IsEnabled = true;
			ButtonStop.IsEnabled = true;
			ComBox.IsEnabled = false;
			PressEnable = true;

			MyTextBox.Focus();
			MyTextBox.SelectionStart = 0;
			MyTextBox.SelectionLength = 0;

			ComBox.IsEnabled = false;
		}

		private string StringGenerator(string source, int length)
		{
			Random random = new Random();
			string str = new string(Enumerable.Repeat(source, length).Select(s => s[random.Next(s.Length)]).ToArray());
			return str;
		}

		private void CheckBox_Checked(object sender, RoutedEventArgs e)
		{
			if (ChBox.IsChecked == true)
				ChBox.Background = Brushes.DarkBlue;
		}

		private void ChBox_Unchecked(object sender, RoutedEventArgs e)
		{
			if (ChBox.IsChecked == false)
				ChBox.Background = Brushes.LightBlue;
		}

		private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			if (ComBox.SelectedIndex == 1)
			{
				string source = "abcdefghijklmnopqrstuvwxyz          "; //
				MyTextBox.Text = StringGenerator(source, 200);
				ButtonStart.IsEnabled = true;
			}
			else if (ComBox.SelectedIndex == 2)
			{
				string source1 = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ          ";
				MyTextBox.Text = StringGenerator(source1, 250);
				ButtonStart.IsEnabled = true;
			}
			else if (ComBox.SelectedIndex == 3)
			{
				string source2 = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789                    ";
				MyTextBox.Text = StringGenerator(source2, 300);
				ButtonStart.IsEnabled = true;
			}
			else if (ComBox.SelectedIndex == 4)
			{
				string source3 = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()-_=+[]{}\\|;:'\",<.>/?          ";
				MyTextBox.Text = StringGenerator(source3, 350);
				ButtonStart.IsEnabled = true;
			}
			else if (ComBox.SelectedIndex == 5)
			{
				string source = "йцукенгшщзфівапролдячсмить ЙЦУКЕНГШЩЗФІВАПРОЛДЯЧСМИТЬ          "; //
				MyTextBox.Text = StringGenerator(source, 200);
				ButtonStart.IsEnabled = true;
			}
		}		

		#region GET_CHAR_FROM_KEY ( MSDN )
		public enum MapType : uint
		{
			MAPVK_VK_TO_VSC = 0x0,
			MAPVK_VSC_TO_VK = 0x1,
			MAPVK_VK_TO_CHAR = 0x2,
			MAPVK_VSC_TO_VK_EX = 0x3,
		}

		[DllImport("user32.dll")]
		public static extern int ToUnicode(
			uint wVirtKey,
			uint wScanCode,
			byte[] lpKeyState,
			[Out, MarshalAs(UnmanagedType.LPWStr, SizeParamIndex = 4)]
			StringBuilder pwszBuff,
			int cchBuff,
			uint wFlags);

		[DllImport("user32.dll")]
		public static extern bool GetKeyboardState(byte[] lpKeyState);

		[DllImport("user32.dll")]
		public static extern uint MapVirtualKey(uint uCode, MapType uMapType);

		public static char GetCharFromKey(Key key)
		{
			char ch = ' ';

			int virtualKey = KeyInterop.VirtualKeyFromKey(key);
			byte[] keyboardState = new byte[256];
			GetKeyboardState(keyboardState);

			uint scanCode = MapVirtualKey((uint)virtualKey, MapType.MAPVK_VK_TO_VSC);
			StringBuilder stringBuilder = new StringBuilder(8);

			int result = ToUnicode((uint)virtualKey, scanCode, keyboardState, stringBuilder, stringBuilder.Capacity, 0);
			switch (result)
			{
				case -1:
					break;
				case 0:
					break;
				case 1:					
					ch = stringBuilder[0];
					break;					
				default:					
					ch = stringBuilder[0];
					break;					
			}
			return ch;
		}
		#endregion

		private char LanguageTransformer(char ch)
		{
			switch (ch)
			{
				case 'q': ch = 'й'; break;
				case 'w': ch = 'ц'; break;
				case 'e': ch = 'у'; break;
				case 'r': ch = 'к'; break;
				case 't': ch = 'е'; break;
				case 'y': ch = 'н'; break;
				case 'u': ch = 'г'; break;
				case 'i': ch = 'ш'; break;
				case 'o': ch = 'щ'; break;
				case 'p': ch = 'з'; break;



				case 'a': ch = 'ф'; break;
				case 's': ch = 'і'; break;
				case 'd': ch = 'в'; break;
				case 'f': ch = 'а'; break;
				case 'g': ch = 'п'; break;
				case 'h': ch = 'р'; break;
				case 'j': ch = 'о'; break;
				case 'k': ch = 'л'; break;
				case 'l': ch = 'д'; break;


				case 'z': ch = 'я'; break;
				case 'x': ch = 'ч'; break;
				case 'c': ch = 'с'; break;
				case 'v': ch = 'м'; break;
				case 'b': ch = 'и'; break;
				case 'n': ch = 'т'; break;
				case 'm': ch = 'ь'; break;


			}

			return ch;
		}

		public void GetCharFromKeyProxy(Key key, ModifierKeys modifier)
		{
			if (modifier == ModifierKeys.None && IscapitalOn == false )
			{
				if (IsEnglish == true)
				{
					char ch = GetCharFromKey(key);
					SymbolMatcher(ch);
					char c;
					switch (ch)
					{
						case '[':
							c = 'х'; SymbolMatcher(c);
							break;
						case ']':
							c = 'ї'; SymbolMatcher(c);
							break;
						case ';':
							c = 'ж'; SymbolMatcher(c);
							break;
						case '\'':
							c = 'є'; SymbolMatcher(c);
							break;
						case ',':
							c = 'б'; SymbolMatcher(c);
							break;
						case '.':
							c = 'ю'; SymbolMatcher(c);
							break;
						default:
							SymbolMatcher(ch);
							break;
					}
				}
				else if (IsEnglish == false)
				{
					char ch = GetCharFromKey(key);
					ch = LanguageTransformer(ch);
					SymbolMatcher(ch);
				}
				
			}
			else if (modifier == ModifierKeys.None && IscapitalOn == true )
			{
				char ch = GetCharFromKey(key);
				ch = char.ToUpper(ch);
				SymbolMatcher(ch);
			}
			else if (modifier == ModifierKeys.Shift && IscapitalOn == true) 
			{
				char ch = GetCharFromKey(key);  
				char c;
				switch (ch)
				{
					case '1':
						c = '!'; SymbolMatcher(c);
						break;
					case '2':
						c = '@'; SymbolMatcher(c);
						break;
					case '3':
						c = '#'; SymbolMatcher(c);
						break;
					case '4':
						c = '$'; SymbolMatcher(c);
						break;
					case '5':
						c = '%'; SymbolMatcher(c);
						break;
					case '6':
						c = '^'; SymbolMatcher(c);
						break;
					case '7':
						c = '&'; SymbolMatcher(c);
						break;
					case '8':
						c = '*'; SymbolMatcher(c);
						break;
					case '9':
						c = '('; SymbolMatcher(c);
						break;
					case '0':
						c = ')'; SymbolMatcher(c);
						break;
					case '-':
						c = '_'; SymbolMatcher(c);
						break;
					case '=':
						c = '+'; SymbolMatcher(c);
						break;
					case '[':
						c = '{'; SymbolMatcher(c);
						break;
					case ']':
						c = '}'; SymbolMatcher(c);
						break;
					case '\\':
						c = '|'; SymbolMatcher(c);
						break;
					case ';':
						c = ':'; SymbolMatcher(c);
						break;
					case '\'':
						c = '\"'; SymbolMatcher(c);
						break;
					case ',':
						c = '<'; SymbolMatcher(c);
						break;
					case '.':
						c = '>'; SymbolMatcher(c);
						break;
					case '/':
						c = '?'; SymbolMatcher(c);
						break;
					default:
						SymbolMatcher(ch);
						break;
				}
			}

			else if (modifier == ModifierKeys.Shift && IscapitalOn == false )
			{
				if (IsEnglish == true)
				{
					char ch = GetCharFromKey(key);
					ch = char.ToUpper(ch);
					char c;
					switch (ch)
					{
						case '1':
							c = '!'; SymbolMatcher(c);
							break;
						case '2':
							c = '@'; SymbolMatcher(c);
							break;
						case '3':
							c = '#'; SymbolMatcher(c);
							break;
						case '4':
							c = '$'; SymbolMatcher(c);
							break;
						case '5':
							c = '%'; SymbolMatcher(c);
							break;
						case '6':
							c = '^'; SymbolMatcher(c);
							break;
						case '7':
							c = '&'; SymbolMatcher(c);
							break;
						case '8':
							c = '*'; SymbolMatcher(c);
							break;
						case '9':
							c = '('; SymbolMatcher(c);
							break;
						case '0':
							c = ')'; SymbolMatcher(c);
							break;
						case '-':
							c = '_'; SymbolMatcher(c);
							break;
						case '=':
							c = '+'; SymbolMatcher(c);
							break;
						case '[':
							c = '{'; SymbolMatcher(c);
							break;
						case ']':
							c = '}'; SymbolMatcher(c);
							break;
						case '\\':
							c = '|'; SymbolMatcher(c);
							break;
						case ';':
							c = ':'; SymbolMatcher(c);
							break;
						case '\'':
							c = '\"'; SymbolMatcher(c);
							break;
						case ',':
							c = '<'; SymbolMatcher(c);
							break;
						case '.':
							c = '>'; SymbolMatcher(c);
							break;
						case '/':
							c = '?'; SymbolMatcher(c);
							break;
						default:
							SymbolMatcher(ch);
							break;
					}
				}
				else if (IsEnglish == false)
				{
					char ch = GetCharFromKey(key);
					ch = LanguageTransformer(ch);
					ch = char.ToUpper(ch);
					SymbolMatcher(ch);

				}
				

			}
		}

		private void SymbolMatcher(char c)
		{
			for (int i = J; i < J + 1; i++)
			{
				if (MyTextBox.Text[J] == c)
				{
					Matchesnumber.Content = (++MatchesNumber).ToString();
					SelectionLengthChange();
					J++;
					break;
				}
				else if (MyTextBox.Text[i] != c)
				{
					Errorsnumber.Content = (++ErrorsNumber).ToString();
					break;
				}
			}			
		}		// йцукенгшщзхї\ ЙЦУКЕНГШЩЗХЇ/	  фівапролджє ФІВАПРОЛДЖЄ	  ячсмитьбю. ЯЧСМИТЬБЮ,

		private void KeyChecker(object sender, KeyEventArgs e, SolidColorBrush color)
		{
            if(e.KeyboardDevice.Modifiers == (ModifierKeys.Shift | ModifierKeys.Alt))           
            {
				if (Lang.Content.ToString() == "ENG")
				{
					Lang.Content = "UKR";
					IsEnglish = false;
				}
					
				else if (Lang.Content.ToString() == "UKR")
				{
					Lang.Content = "ENG";
					IsEnglish = true;
				}
				
			}

            char c;
			foreach (var brd in keyboard.Children)
			{
				if (brd is Border)
				{					
					TextBlock txtBl = ((brd as Border).Child as TextBlock);
					ColorBefore = ((brd as Border).Background) as SolidColorBrush;					

					if ((string)txtBl.Tag == e.Key.ToString() && e.KeyboardDevice.Modifiers == ModifierKeys.None)
					{
						if (e.Key.ToString() == "Space")
						{
							c = ' ';
							SymbolMatcher(c);
							(brd as Border).Background = color; break;
						}

						else if (e.Key.ToString() == "Tab")
						{ (brd as Border).Background = color; break; }

						else if (e.Key.ToString() == "Enter")
						{ (brd as Border).Background = color; break; }

						else if (e.Key.ToString() == "Back")
						{ (brd as Border).Background = color; break; }

						else if (e.Key.ToString() == "Capital")
						{				
							if (IscapitalOn == true)
								IscapitalOn = false;
							else if (IscapitalOn == false)
								IscapitalOn = true;
							
							(brd as Border).Background = color;
							break;
						}						
						else
						{
							GetCharFromKeyProxy(e.Key, e.KeyboardDevice.Modifiers);
							(brd as Border).Background = color; break;
						}											
					}

					if ((string)txtBl.Tag == e.Key.ToString() && e.KeyboardDevice.Modifiers == ModifierKeys.Shift)
					{
						if (e.Key.ToString() == "LeftShift")
						{ (brd as Border).Background = color; break; }

						else if (e.Key.ToString() == "RightShift")
						{ (brd as Border).Background = color; break; }
						
						else
						{
							GetCharFromKeyProxy(e.Key, e.KeyboardDevice.Modifiers);
							(brd as Border).Background = color; break;
						}
					}
					if ((string)txtBl.Tag == e.Key.ToString() && e.KeyboardDevice.Modifiers == ModifierKeys.Control)
					{
						if (e.Key.ToString() == "LeftCtrl")
						{ (brd as Border).Background = color; break; }

						else if (e.Key.ToString() == "RightCtrl")
						{ (brd as Border).Background = color; break; }
					}					
				}
			}			
		}

		private void KeyChecker1(object sender, KeyEventArgs e, SolidColorBrush color)
		{
			foreach (var brd in keyboard.Children)
			{
				if (brd is Border)
				{
					TextBlock txtBl = ((brd as Border).Child as TextBlock);

					if ((string)txtBl.Tag == e.Key.ToString())
					{
						if (e.Key.ToString() == "Capital")
						{
							if (IscapitalOn == false)
							{
								(brd as Border).Background = Brushes.DarkTurquoise; break;
							}
								
							if (IscapitalOn == true)
								(brd as Border).Background = Brushes.LightBlue; break;
						}
						else
							(brd as Border).Background = color; break;						
					}
				}
			}
		}
		
		private void KeyEventChecker(object sender, KeyEventArgs e, string Eventname)
		{
			SolidColorBrush color1 = Brushes.Yellow;
			SolidColorBrush color2 = ColorBefore;

			if (Eventname == "Window_KeyDown")			
				KeyChecker(sender, e, color1);
			else if (Eventname == "Window_KeyUp")			
				KeyChecker1(sender, e, color2);		
		}		

		private void SelectionLengthChange()
		{
			MyTextBox.Focus();
			MyTextBox.SelectionLength = SelectionLength;			
			SelectionLength++;	
		}

		private void Window_KeyDown(object sender, KeyEventArgs e)
		{
			if (PressEnable == true)
			{
				char c;
				if (e.Key == Key.Enter)
					enter.Background = Brushes.Yellow;
				else if (e.SystemKey == Key.LeftAlt)
				{
					if (e.SystemKey == Key.LeftAlt)
						LeftAlt.Background = Brushes.Yellow;
					if (e.SystemKey == Key.RightAlt)
						RightAlt.Background = Brushes.Yellow;
				}
				else if (e.Key == Key.Oem4 && e.KeyboardDevice.Modifiers == ModifierKeys.None)
				{
					c = '[';
					SymbolMatcher(c);
					spcBr.Background = Brushes.Yellow;
				}
				else if (e.Key == Key.Oem4 && e.KeyboardDevice.Modifiers == ModifierKeys.Shift)
				{
					c = '{';
					SymbolMatcher(c);
					spcBr.Background = Brushes.Yellow;
				}
				else if (e.Key == Key.Oem2 && e.KeyboardDevice.Modifiers == ModifierKeys.None)
				{
					c = '/';
					SymbolMatcher(c);
					Quest.Background = Brushes.Yellow;
				}
				else if (e.Key == Key.Oem2 && e.KeyboardDevice.Modifiers == ModifierKeys.Shift)
				{
					c = '?';
					SymbolMatcher(c);
					Quest.Background = Brushes.Yellow;
				}
				else if (e.Key == Key.Oem7 && e.KeyboardDevice.Modifiers == ModifierKeys.None)
				{
					c = '\'';
					SymbolMatcher(c);
					Quots.Background = Brushes.Yellow;
				}
				else if (e.Key == Key.Oem7 && e.KeyboardDevice.Modifiers == ModifierKeys.Shift)
				{
					c = '"';
					SymbolMatcher(c);
					Quots.Background = Brushes.Yellow;
				}
				else
					KeyEventChecker(sender, e, nameof(Window_KeyDown));
			}
			if (!PressEnable)
			{
			}
		}

		private void Window_KeyUp(object sender, KeyEventArgs e)
        {
			if (PressEnable == true)
			{
				char c;
				if (e.Key == Key.Enter)
					enter.Background = Brushes.DarkTurquoise;
				else if (e.SystemKey == Key.LeftAlt)
				{
					if (e.SystemKey == Key.LeftAlt)
						LeftAlt.Background = Brushes.DarkTurquoise;
					if (e.SystemKey == Key.RightAlt)
						RightAlt.Background = Brushes.DarkTurquoise;
				}
				else if (e.Key == Key.Oem4 && e.KeyboardDevice.Modifiers == ModifierKeys.None)
				{
					c = '[';
					SymbolMatcher(c);
					spcBr.Background = Brushes.DarkTurquoise;
				}
				else if (e.Key == Key.Oem4 && e.KeyboardDevice.Modifiers == ModifierKeys.Shift)
				{
					c = '{';
					SymbolMatcher(c);
					spcBr.Background = Brushes.DarkTurquoise;
				}
				else if (e.Key == Key.Oem2 && e.KeyboardDevice.Modifiers == ModifierKeys.None)
				{
					c = '/';
					SymbolMatcher(c);
					Quest.Background = Brushes.DarkTurquoise;
				}
				else if (e.Key == Key.Oem2 && e.KeyboardDevice.Modifiers == ModifierKeys.Shift)
				{
					c = '?';
					SymbolMatcher(c);
					Quest.Background = Brushes.DarkTurquoise;
				}
				else if (e.Key == Key.Oem7 && e.KeyboardDevice.Modifiers == ModifierKeys.None)
				{
					c = '\'';
					SymbolMatcher(c);
					Quots.Background = Brushes.DarkTurquoise;
				}
				else if (e.Key == Key.Oem7 && e.KeyboardDevice.Modifiers == ModifierKeys.Shift)
				{
					c = '"';
					SymbolMatcher(c);
					Quots.Background = Brushes.DarkTurquoise;
				}
				else
					KeyEventChecker(sender, e, nameof(Window_KeyUp));
			}
			if (!PressEnable)
			{
			}
		}

		private void ButtonRestart_Click(object sender, RoutedEventArgs e)
		{
			TimeCounter = 0;
			J = 0;		
			MatchesNumber = 0;
			ErrorsNumber = 0;
			Matchesnumber.Content = MatchesNumber.ToString();
			Errorsnumber.Content = ErrorsNumber.ToString();
			MyTextBox.Focus();
			SelectionLength = 1;
			MyTextBox.SelectionStart = 0;
			MyTextBox.SelectionLength = 0;			
		}

		private void ButtonStop_Click(object sender, RoutedEventArgs e)
		{
			Timer.Stop();
			TimeCounter = 0;
			Time1.Content = "0";
			Time.Content = "0";
			MatchesNumber = 0;
			ErrorsNumber = 0;
			Spd.Content = "0";
			Matchesnumber.Content = MatchesNumber.ToString();
			Errorsnumber.Content = ErrorsNumber.ToString();
			J = 0;
			
			SelectionLength = 1;
			MyTextBox.SelectionStart = 0;
			MyTextBox.SelectionLength = 0;

			ButtonStart.IsEnabled = false;
			ButtonRestart.IsEnabled = false;
			ButtonStop.IsEnabled = false;
			MyTextBox.Text = "";
			ComBox.IsEnabled = true;
		}		
	}
}
