﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace _09._12._12_Login
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            //Thread.CurrentThread.CurrentUICulture = new CultureInfo("en");
            InitializeComponent();
        }

        private void SignMenuChanges()
        {
            if(In.IsChecked == true)
                Group.Visibility = Visibility.Visible;
            else if (Up.IsChecked == true)
                Group.Visibility = Visibility.Collapsed;
        }


        private void In_Checked(object sender, RoutedEventArgs e)
        {
            Group.Visibility = Visibility.Visible;
        }

        private void In_Unchecked(object sender, RoutedEventArgs e)
        {
           
        }

        private void Up_Unchecked(object sender, RoutedEventArgs e)
        {
           
        }

        private void Up_Checked(object sender, RoutedEventArgs e)
        {
            Group.Visibility = Visibility.Collapsed;
        }
    }
}
