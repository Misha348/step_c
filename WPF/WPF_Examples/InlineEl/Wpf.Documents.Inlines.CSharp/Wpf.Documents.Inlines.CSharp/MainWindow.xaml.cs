﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;

namespace Wpf.Documents.Inlines.CSharp
{
    internal sealed partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            CreateWindowContent();
        }

        private void CreateWindowContent()
        {
            var italic = new Italic();
            italic.Inlines.Add(new Run("Italic"));

            var bold = new Bold();
            bold.Inlines.Add(new Run("Bold"));

            var underline = new Underline();
            underline.Inlines.Add(new Run("Underline"));

            var hyperlink = new Hyperlink();
            hyperlink.Inlines.Add(new Run("Hyperlink"));

            var menuItem21 = new MenuItem { Header = italic };
            var menuItem22 = new MenuItem { Header = bold };
            var menuItem23 = new MenuItem { Header = underline };
            var menuItem24 = new MenuItem { Header = hyperlink };

            var menuItem11 = new MenuItem { Header = "Inlines" };
            menuItem11.Items.Add(menuItem21);
            menuItem11.Items.Add(menuItem22);
            menuItem11.Items.Add(menuItem23);
            menuItem11.Items.Add(menuItem24);

            var menu = new Menu { IsMainMenu = true };
            menu.Items.Add(menuItem11);

            var part1 = new Run("Even a ");
            var part2 = new Italic(new Bold(new Run("simple")));
            var part3 = new Run(" ");
            var part4 = new Span(new Run("Button"))
            {
                FontFamily = new FontFamily("Consolas"),
                Foreground = Brushes.Blue
            };
            var part5 = new Run(" can have some ");
            var part6 = new Span(new Underline(new Run("advanced")))
            {
                Foreground = new LinearGradientBrush(
                    startColor: Colors.Red,
                    endColor: Colors.Green,
                    startPoint: new Point(0.0, 0.0),
                    endPoint: new Point(1.0, 1.0)
                )
            };
            var part7 = new Run(" text effects");

            var span = new Span();
            span.Inlines.Add(part1);
            span.Inlines.Add(part2);
            span.Inlines.Add(part3);
            span.Inlines.Add(part4);
            span.Inlines.Add(part5);
            span.Inlines.Add(part6);
            span.Inlines.Add(part7);

            var textBlock = new TextBlock(span) { TextWrapping = TextWrapping.Wrap };
            var button = new Button { Content = textBlock, FontSize = 35.0, Margin = new Thickness(5.0) };

            var dockPanel = new DockPanel();
            dockPanel.Children.Add(menu);
            dockPanel.Children.Add(button);

            DockPanel.SetDock(menu, Dock.Top);

            Content = dockPanel;
        }
    }
}