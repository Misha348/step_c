﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Wpf.Documents.Blocks.Table.CSharp
{
    using System.Windows.Documents;

    internal sealed partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            CreateWindowContent();
        }

        private void CreateWindowContent()
        {
            var cell11 = new TableCell(new Paragraph(new Run("Logical Operators"))) { ColumnSpan = 5, FontSize = 20.0 };
            var cell21 = new TableCell(new Paragraph(new Run("A")));
            var cell22 = new TableCell(new Paragraph(new Run("B")));
            var cell23 = new TableCell(new Paragraph(new Run("A && B")));
            var cell24 = new TableCell(new Paragraph(new Run("A || B")));
            var cell25 = new TableCell(new Paragraph(new Run("!A")));
            var cell31 = new TableCell(new Paragraph(new Run("false")));
            var cell32 = new TableCell(new Paragraph(new Run("false")));
            var cell33 = new TableCell(new Paragraph(new Run("false")));
            var cell34 = new TableCell(new Paragraph(new Run("false")));
            var cell35 = new TableCell(new Paragraph(new Run("true")));
            var cell41 = new TableCell(new Paragraph(new Run("false")));
            var cell42 = new TableCell(new Paragraph(new Run("true")));
            var cell43 = new TableCell(new Paragraph(new Run("false")));
            var cell44 = new TableCell(new Paragraph(new Run("true")));
            var cell45 = new TableCell(new Paragraph(new Run("true")));
            var cell51 = new TableCell(new Paragraph(new Run("true")));
            var cell52 = new TableCell(new Paragraph(new Run("false")));
            var cell53 = new TableCell(new Paragraph(new Run("false")));
            var cell54 = new TableCell(new Paragraph(new Run("true")));
            var cell55 = new TableCell(new Paragraph(new Run("false")));
            var cell61 = new TableCell(new Paragraph(new Run("true")));
            var cell62 = new TableCell(new Paragraph(new Run("true")));
            var cell63 = new TableCell(new Paragraph(new Run("true")));
            var cell64 = new TableCell(new Paragraph(new Run("true")));
            var cell65 = new TableCell(new Paragraph(new Run("false")));

            var row1 = new TableRow();
            row1.Cells.Add(cell11);

            var row2 = new TableRow();
            row2.Cells.Add(cell21);
            row2.Cells.Add(cell22);
            row2.Cells.Add(cell23);
            row2.Cells.Add(cell24);
            row2.Cells.Add(cell25);

            var row3 = new TableRow();
            row3.Cells.Add(cell31);
            row3.Cells.Add(cell32);
            row3.Cells.Add(cell33);
            row3.Cells.Add(cell34);
            row3.Cells.Add(cell35);

            var row4 = new TableRow();
            row4.Cells.Add(cell41);
            row4.Cells.Add(cell42);
            row4.Cells.Add(cell43);
            row4.Cells.Add(cell44);
            row4.Cells.Add(cell45);

            var row5 = new TableRow();
            row5.Cells.Add(cell51);
            row5.Cells.Add(cell52);
            row5.Cells.Add(cell53);
            row5.Cells.Add(cell54);
            row5.Cells.Add(cell55);

            var row6 = new TableRow();
            row6.Cells.Add(cell61);
            row6.Cells.Add(cell62);
            row6.Cells.Add(cell63);
            row6.Cells.Add(cell64);
            row6.Cells.Add(cell65);

            var rowGroup1 = new TableRowGroup { Background = Brushes.Wheat, FontWeight = FontWeights.Bold };
            rowGroup1.Rows.Add(row1);
            rowGroup1.Rows.Add(row2);

            var rowGroup2 = new TableRowGroup
            {
                Background = Brushes.AliceBlue,
                FontFamily = new FontFamily("Consolas"),
                Foreground = Brushes.Blue
            };
            rowGroup2.Rows.Add(row3);
            rowGroup2.Rows.Add(row4);
            rowGroup2.Rows.Add(row5);
            rowGroup2.Rows.Add(row6);

            var table = new Table { TextAlignment = TextAlignment.Center };
            table.RowGroups.Add(rowGroup1);
            table.RowGroups.Add(rowGroup2);

            var document = new FlowDocument();
            document.Blocks.Add(table);

            var documentReader = new FlowDocumentReader { Document = document };

            Content = document;
        }
    }
}