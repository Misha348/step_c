﻿using System.Windows;
using System.Windows.Controls;

namespace Wpf.Documents.Blocks.Paragraph.CSharp
{
    using System.Windows.Documents;

    internal sealed partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            CreateWindowContent();
        }

        private void CreateWindowContent()
        {
            var run1 = new Run("Rules of Optimization");
            var run2 = new Run("Rule 1: Don't do it");
            var run3 = new Run("Rule 2 (for experts only): Don't do it yet");
            var run4 = new Run("Michael A. Jackson");

            var paragraph1 = new Paragraph { FontSize = 22.0 };
            paragraph1.Inlines.Add(new Bold(run1));

            var paragraph2 = new Paragraph();
            paragraph2.Inlines.Add(run2);
            paragraph2.Inlines.Add(new LineBreak());
            paragraph2.Inlines.Add(run3);

            var paragraph3 = new Paragraph { TextAlignment = TextAlignment.Right };
            paragraph3.Inlines.Add(run4);

            var document = new FlowDocument();
            document.Blocks.Add(paragraph1);
            document.Blocks.Add(paragraph2);
            document.Blocks.Add(paragraph3);

            var documentReader = new FlowDocumentReader { Document = document };

            Content = documentReader;
        }
    }
}