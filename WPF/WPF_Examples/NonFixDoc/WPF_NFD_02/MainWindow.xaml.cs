﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WPF_NFD_02
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            //First part
            Run runFirst = new Run();
            runFirst.Text = "Hello world ";
            
            //Text with bold font
            Bold bold = new Bold();
            Run runBold = new Run();
            runBold.Text = "dynamicaly generated";
            bold.Inlines.Add(runBold);

            // Second part
            Run runLast = new Run();
            runLast.Text = " document";

            // Add to paragraph
            Paragraph paragraph = new Paragraph();
            paragraph.Inlines.Add(runFirst);
            paragraph.Inlines.Add(bold);
            paragraph.Inlines.Add(runLast);

            // Create doc and add paragraph
            FlowDocument document = new FlowDocument();


            document.Blocks.Add(paragraph);

            Paragraph p = new Paragraph(new Run("Hello, world!"));
            p.FontSize = 36;
            document.Blocks.Add(p);


            p = new Paragraph(new Run("The ultimate programming greeting!"));
            p.FontSize = 14;
            p.FontStyle = FontStyles.Italic;
            p.TextAlignment = TextAlignment.Left;
            p.Foreground = Brushes.Gray;
            document.Blocks.Add(p);


            // View doc
            docViewer.Document = document;
        }
    }
}
