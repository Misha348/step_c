﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Binding_08_toCollection
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
       // List<string> list = new List<string>();
        List<string> list = new List<string>();
        public MainWindow()
        {
            InitializeComponent();

            string[] arr = { "First", "Second", "Third", "Fourth" };
            foreach (string item in arr)
            {
                list.Add(item);
            }

            listBox1.ItemsSource = list;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            // INotifyCollectionChanged  - > ObservableCollection<T>
            list.Add(DateTime.Now.ToLongTimeString());
        }
    }
}
