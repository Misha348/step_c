﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Binding_10_INotifyPropChange
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Student myStudent = new Student();
        public MainWindow()
        {
            InitializeComponent();
            this.DataContext = myStudent;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            myStudent.Name = DateTime.Now.ToString();
        }
    }

    public class Student : INotifyPropertyChanged
    {
        private string name = String.Empty;
        public string Name
        {
            get
            {
                return this.name;
            }
            set
            {
                if (value != this.Name)
                {
                    this.name = value;
                    OnPropertyChange(nameof(Name));
                }
            }
        }

        private int myVar;

        public int MyProperty
        {
            get { return myVar; }
            set {
                myVar = value;
                OnPropertyChange(nameof(MyProperty));
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;

        void OnPropertyChange(string propName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));
        }
    }
}
