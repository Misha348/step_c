﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Runtime.CompilerServices;
using System.Collections.ObjectModel;

namespace _12._02._19_Binding
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    /// 
 
    public partial class MainWindow : Window
    {
        ViewModel viewModel = new ViewModel();
		//List<MyColor> Colors = new List<MyColor>();
		

        public MainWindow()
        {
            InitializeComponent();		
			this.DataContext = viewModel;
			ColorListBox.ItemsSource = viewModel.ColorList;
		}

		private void ColorMathcing()
		{
			//oreach (var item in ColorList)
			//
			//	if (item.ToString() == MyColor.ColorName)
			//		Button_AddColor.IsEnabled = false;			
			//
            //MyColor.ToString()
        }

        // create bool prop if current color exist
        // use OnPropChange
        // buttons prop IsEnable = binding to bool prop

        private void Button_AddColor_Click(object sender, RoutedEventArgs e)
		{			
			viewModel.ColorList.Add(viewModel.ToString());
            viewModel.OnPropertyChanged(nameof(viewModel.IsntExistColor));
			ColorMathcing();
		}

		private void Button_DeleteColor_Click(object sender, RoutedEventArgs e)
		{
            viewModel.ColorList.Remove(((Button)sender).Tag.ToString());
		}

		private void First_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
			// Button_AddColor.IsEnabled = true;
			// ColorMathcing();
			// myColor.R = (int)First.Value;
			// 
			// Color color = Color.FromArgb( (byte)myColor.A, (byte)myColor.R, (byte)myColor.G, (byte)myColor.B );
			// CastomColor.Background = new SolidColorBrush(color);            
		}

        private void Second_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
			// Button_AddColor.IsEnabled = true;
			// ColorMathcing();
			// myColor.G = (int)Second.Value;
			// 
			// Color color = Color.FromArgb( (byte)myColor.A, (byte)myColor.R, (byte)myColor.G, (byte)myColor.B );
			// CastomColor.Background = new SolidColorBrush(color);
		}

        private void Third_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
			// Button_AddColor.IsEnabled = true;
			// ColorMathcing();
			// myColor.B = (int)Third.Value;
			// 
			// Color color = Color.FromArgb( (byte)myColor.A, (byte)myColor.R, (byte)myColor.G, (byte)myColor.B );
			// CastomColor.Background = new SolidColorBrush(color);
		}

        private void Fourth_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
			// Button_AddColor.IsEnabled = true;
			// ColorMathcing();
			// myColor.A = (int)Fourth.Value;
			// 
			// Color color = Color.FromArgb( (byte)myColor.A, (byte)myColor.R, (byte)myColor.G, (byte)myColor.B );
			// CastomColor.Background = new SolidColorBrush(color);
		}


	}

    class ViewModel : INotifyPropertyChanged
    {
        public ObservableCollection<string> ColorList = new ObservableCollection<string>();


        private int r;
        public int R
        {
            get
            {
                return this.r;
            }
            set
            {
                this.r = value;
                OnPropertyChanged(nameof(R));
                OnPropertyChanged(nameof(ColorName));
                OnPropertyChanged(nameof(IsntExistColor));
            }
        }

        private int g;
        public int G
        {
            get
            {
                return this.g;
            }
            set
            {
                this.g = value;
                OnPropertyChanged(nameof(G));
                OnPropertyChanged(nameof(ColorName));
                OnPropertyChanged(nameof(IsntExistColor));
            }
        }

        private int b;
        public int B
        {
            get
            {
                return this.b;
            }
            set
            {
                this.b = value;
                OnPropertyChanged(nameof(B));
                OnPropertyChanged(nameof(ColorName));
                OnPropertyChanged(nameof(IsntExistColor));

            }
        }

        private int a;
        public int A
        {
            get
            {
                return this.a;
            }
            set
            {
                this.a = value;
                OnPropertyChanged(nameof(A));
                OnPropertyChanged(nameof(ColorName));
                OnPropertyChanged(nameof(IsntExistColor));

            }
        }

        public string ColorName
        {
            get
            {
                return Color.FromArgb((byte)A, (byte)R, (byte)G, (byte)B).ToString();
            }
        }

        public bool IsntExistColor
        {
            get
            {
                return ColorList.FirstOrDefault(e => e == ColorName) == null;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string propName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));
        }

        public override string ToString()
        {
            return Color.FromArgb((byte)A, (byte)R, (byte)G, (byte)B).ToString();
        }

    }
}
