﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Collections.ObjectModel;

namespace WpfApp_Bindings
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        class ColorForBinding : INotifyPropertyChanged
        {
           private byte a;
           public byte A
            {
                get
                {
                    return a;
                }
                set
                {
                    a = value;
                    OnPropertyChange(nameof(A));
                    OnPropertyChange(nameof(ColorProperty));
                }
            }
            private byte r;
            public byte R
            {
                get
                {
                    return r;
                }
                set
                {
                    r = value;
                    OnPropertyChange(nameof(R));
                    OnPropertyChange(nameof(ColorProperty));
                }
            }
            private byte g;
            public byte G
            {
                get
                {
                    return g;
                }
                set
                {
                    g = value;
                    OnPropertyChange(nameof(G));
                    OnPropertyChange(nameof(ColorProperty));
                }
            }
            private byte b;
            public byte B
            {
                get
                {
                    return b;
                }
                set
                {
                    b = value;
                    OnPropertyChange(nameof(B));
                    OnPropertyChange(nameof(ColorProperty));
                }
            }

            public event PropertyChangedEventHandler PropertyChanged;

            public ColorForBinding(byte a, byte r, byte g, byte b)
            {
                A = a;
                R = r;
                G = g;
                B = b;
            }
           public string ColorProperty
           {
                get
                {
                    return Color.FromArgb(A, R, G, B).ToString(); ;
                }
           }
            void OnPropertyChange(string propName)
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));
            }
            public override string ToString()
            {
                return Color.FromArgb(A, R, G, B).ToString();
            }
        }

        ColorForBinding myColor = new ColorForBinding(255,100,100,100);
        ObservableCollection<string> colorsCollection = new ObservableCollection<string>();
        public MainWindow()
        {
            InitializeComponent();
            this.DataContext = myColor;
            ListBox_ColorList.ItemsSource = colorsCollection;
        }

        private void Button_AddColor_Click(object sender, RoutedEventArgs e)
        {
            colorsCollection.Add(myColor.ToString());
        }

        private void Button_DeleteColor_Click(object sender, RoutedEventArgs e)
        {
            colorsCollection.Remove(((Button)sender).Tag.ToString());
        }
    }
}
