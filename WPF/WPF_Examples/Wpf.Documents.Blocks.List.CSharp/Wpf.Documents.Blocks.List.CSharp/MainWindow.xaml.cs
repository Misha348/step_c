﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Wpf.Documents.Blocks.List.CSharp
{
    using System.Windows.Documents;

    internal sealed partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            CreateWindowContent();
        }

        private void CreateWindowContent()
        {
            var fontFamily = new FontFamily("Consolas");

            var paragraph1 = new Paragraph();
            paragraph1.Inlines.Add(new Bold(new Run("Checklist for good programming")));

            var paragraph2 = new Paragraph();
            paragraph2.Inlines.Add(new Run("Identifiers: Make sure all your identifiers are meaningful."));

            var paragraph3 = new Paragraph();
            paragraph3.Inlines.Add(new Run("One-letter identifiers are almost never meaningful."));

            var paragraph4 = new Paragraph();
            paragraph4.Inlines.Add(new Run("Names like "));
            paragraph4.Inlines.Add(new Span(new Run("flag")) { FontFamily = fontFamily, Foreground = Brushes.Blue });
            paragraph4.Inlines.Add(new Run(" and "));
            paragraph4.Inlines.Add(new Span(new Run("temp")) { FontFamily = fontFamily, Foreground = Brushes.Blue });
            paragraph4.Inlines.Add(new Run(" are seldom meaningful. Instead of "));
            paragraph4.Inlines.Add(new Span(new Run("flag")) { FontFamily = fontFamily, Foreground = Brushes.Blue });
            paragraph4.Inlines.Add(new Run(", consider naming the Boolean condition it checks for, such as "));
            paragraph4.Inlines.Add(new Span(new Run("valueFound")) { FontFamily = fontFamily, Foreground = Brushes.Blue });
            paragraph4.Inlines.Add(new Run("."));

            var paragraph5 = new Paragraph();
            paragraph5.Inlines.Add(new Run("Consider multi-word identifiers, like "));
            paragraph5.Inlines.Add(new Span(new Run("nameIndex")) { FontFamily = fontFamily, Foreground = Brushes.Blue });
            paragraph5.Inlines.Add(new Run(". Long identifiers (within reason) tend to be very readable."));

            var paragraph6 = new Paragraph();
            paragraph6.Inlines.Add(new Run("Bare literals: Avoid numbers other than "));
            paragraph6.Inlines.Add(new Span(new Run("0")) { FontFamily = fontFamily });
            paragraph6.Inlines.Add(new Run(" and "));
            paragraph6.Inlines.Add(new Span(new Run("1")) { FontFamily = fontFamily });
            paragraph6.Inlines.Add(new Run(" and strings other than "));
            paragraph6.Inlines.Add(new Span(new Run("\"\"")) { FontFamily = fontFamily });
            paragraph6.Inlines.Add(new Run(" in your program except when you define constants."));

            var paragraph7 = new Paragraph();
            paragraph7.Inlines.Add(new Run("Don't use a literal integer as an array bound."));

            var paragraph8 = new Paragraph();
            paragraph8.Inlines.Add(new Run("Don't use a literal integer to measure the size of a string or some data; use "));
            paragraph8.Inlines.Add(new Span(new Run("sizeof()")) { FontFamily = fontFamily, Foreground = Brushes.Blue });
            paragraph8.Inlines.Add(new Run(" and "));
            paragraph8.Inlines.Add(new Span(new Run("strlen()")) { FontFamily = fontFamily, Foreground = Brushes.Blue });
            paragraph8.Inlines.Add(new Run(" in C and C++ and "));
            paragraph8.Inlines.Add(new Span(new Run(".Length")) { FontFamily = fontFamily, Foreground = Brushes.Blue });
            paragraph8.Inlines.Add(new Run(" in C#."));

            var listItem1 = new ListItem(paragraph3);
            var listItem2 = new ListItem(paragraph4);
            var listItem3 = new ListItem(paragraph5);

            var list1 = new List { MarkerStyle = TextMarkerStyle.Decimal };
            list1.ListItems.Add(listItem1);
            list1.ListItems.Add(listItem2);
            list1.ListItems.Add(listItem3);

            var listItem4 = new ListItem(paragraph7);
            var listItem5 = new ListItem(paragraph8);

            var list2 = new List { MarkerStyle = TextMarkerStyle.Decimal };
            list2.ListItems.Add(listItem4);
            list2.ListItems.Add(listItem5);

            var listItem6 = new ListItem();
            listItem6.Blocks.Add(paragraph2);
            listItem6.Blocks.Add(list1);
            var listItem7 = new ListItem();
            listItem7.Blocks.Add(paragraph6);
            listItem7.Blocks.Add(list2);

            var list3 = new List { MarkerStyle = TextMarkerStyle.Disc };
            list3.ListItems.Add(listItem6);
            list3.ListItems.Add(listItem7);

            var document = new FlowDocument();
            document.Blocks.Add(paragraph1);
            document.Blocks.Add(list3);

            var documentReader = new FlowDocumentReader { Document = document };

            Content = documentReader;
        }
    }
}