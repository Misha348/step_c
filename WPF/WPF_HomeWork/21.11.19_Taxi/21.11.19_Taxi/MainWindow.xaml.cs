﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace _21._11._19_Taxi
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		public int CurrentCounter { get; private set; }
		public static int MaxPas { get; private set; }
		public string TaxiClassType { get; set; }
			   
		public MainWindow()
		{
			InitializeComponent();
			CurrentCounter = 0;
			Book.IsEnabled = false;
			Refuse.IsEnabled = false;
		}

		static MainWindow()
		{
			MaxPas = 8;
		}

		private void Add_Click(object sender, RoutedEventArgs e)
		{
			if (CurrentCounter >= MaxPas)
			{
				CurrentCounter = 0;
				Pasengers.Content = CurrentCounter.ToString();
			}				
			else if (CurrentCounter < MaxPas)
			{
				CurrentCounter++;
				Pasengers.Content = CurrentCounter.ToString();
			}			
		}

		private void Remove_Click(object sender, RoutedEventArgs e)
		{
			if (CurrentCounter <= 0)
			{
				CurrentCounter = 0;
				Pasengers.Content = CurrentCounter.ToString();
			}
			else if (CurrentCounter > 0)
			{
				CurrentCounter--;
				Pasengers.Content = CurrentCounter.ToString();
			}			
		}

		

		private void Agreement_Checked(object sender, RoutedEventArgs e)
		{
			Book.IsEnabled = true;
			Refuse.IsEnabled = true;
		}

		private void Agreement_Unchecked(object sender, RoutedEventArgs e)
		{
			Book.IsEnabled = false;
			Refuse.IsEnabled = false;
		}

		private void Book_Click(object sender, RoutedEventArgs e)
		{
			MessageBox.Show($"\tYOUR ORDER\n\nName: {Name.Text}\nSurname: {Surname.Text}\nAddress from: {Addrress_From.Text}\n" +
							$"Passengers: {CurrentCounter}\nClass: {TaxiClassType}");
		}

		private void RadioButton_Checked(object sender, RoutedEventArgs e)
		{
			TaxiClassType = (sender as RadioButton).Content.ToString();
		}

		private void Refuse_Click(object sender, RoutedEventArgs e)
		{
			Name.Text = "";
			Surname.Text = "";
			Addrress_From.Text = "";
			Pasengers.Content = "0";
			CurrentCounter = 0;
			TaxiClassType = "";

			foreach (var item in classType.Children)
			{
				if (item is RadioButton)
				{
					RadioButton tmp = item as RadioButton;
					tmp.IsChecked = false;
				}					
			}

												//foreach (var item in classType.Children)		LOOP THROUGH LAYOUT ELEMENT
												//{
												//	if (item is RadioButton)
												//	{
												//		RadioButton tmp = item as RadioButton;
												//		if (tmp.IsChecked == true)
												//		{
												//			TaxiClassType = tmp.Content.ToString();
												//		}
												//	}
												//}
		}
	}
}
