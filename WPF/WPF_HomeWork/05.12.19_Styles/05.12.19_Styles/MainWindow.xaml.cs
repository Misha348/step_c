﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace _05._12._19_Styles
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public class MyColor : INotifyPropertyChanged
	{
		private int r;
		public int R
		{
			get
			{
				return this.r;
			}
			set
			{
				this.r = value;
				OnPropertyChanged(nameof(R));
				OnPropertyChanged(nameof(ColorName));
			}

		}

		private int g;
		public int G
		{
			get
			{
				return this.g;
			}
			set
			{
				this.g = value;
				OnPropertyChanged(nameof(G));
				OnPropertyChanged(nameof(ColorName));

			}
		}

		private int b;
		public int B
		{
			get
			{
				return this.b;
			}
			set
			{
				this.b = value;
				OnPropertyChanged(nameof(B));
				OnPropertyChanged(nameof(ColorName));

			}
		}

		private int a;
		public int A
		{
			get
			{
				return this.a;
			}
			set
			{
				this.a = value;
				OnPropertyChanged(nameof(A));
				OnPropertyChanged(nameof(ColorName));

			}
		}

		public string ColorName
		{
			get
			{
				return Color.FromArgb((byte)A, (byte)R, (byte)G, (byte)B).ToString();
			}
		}

		public event PropertyChangedEventHandler PropertyChanged;

		void OnPropertyChanged(string propName)
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));
		}

		public override string ToString()
		{
			return Color.FromArgb((byte)A, (byte)R, (byte)G, (byte)B).ToString();
		}
	}

	public partial class MainWindow : Window
	{
		MyColor MyColor = new MyColor();
		List<MyColor> _Colors = new List<MyColor>();
		ObservableCollection<string> ColorList = new ObservableCollection<string>();

		public MainWindow()
		{
			InitializeComponent();
			this.DataContext = MyColor;
			ColorListBox.ItemsSource = ColorList;
		}

		private void ColorMathcing()
		{
			foreach (var item in ColorList)
			{
				if (item.ToString() == MyColor.ToString())
					Button_AddColor.IsEnabled = false;
			}
		}

		private void Button_AddColor_Click(object sender, RoutedEventArgs e)
		{
			ColorList.Add(MyColor.ToString());
			ColorMathcing();
		}

		private void Button_DeleteColor_Click(object sender, RoutedEventArgs e)
		{
			ColorList.Remove(((Button)sender).Tag.ToString());
		}

		private void First_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
		{
			Button_AddColor.IsEnabled = true;
			ColorMathcing();
			// myColor.R = (int)First.Value;
			// 
			// Color color = Color.FromArgb( (byte)myColor.A, (byte)myColor.R, (byte)myColor.G, (byte)myColor.B );
			// CastomColor.Background = new SolidColorBrush(color);            
		}

		private void Second_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
		{
			Button_AddColor.IsEnabled = true;
			ColorMathcing();
			// myColor.G = (int)Second.Value;
			// 
			// Color color = Color.FromArgb( (byte)myColor.A, (byte)myColor.R, (byte)myColor.G, (byte)myColor.B );
			// CastomColor.Background = new SolidColorBrush(color);
		}

		private void Third_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
		{
			Button_AddColor.IsEnabled = true;
			ColorMathcing();
			// myColor.B = (int)Third.Value;
			// 
			// Color color = Color.FromArgb( (byte)myColor.A, (byte)myColor.R, (byte)myColor.G, (byte)myColor.B );
			// CastomColor.Background = new SolidColorBrush(color);
		}

		private void Fourth_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
		{
			Button_AddColor.IsEnabled = true;
			ColorMathcing();
			// myColor.A = (int)Fourth.Value;
			// 
			// Color color = Color.FromArgb( (byte)myColor.A, (byte)myColor.R, (byte)myColor.G, (byte)myColor.B );
			// CastomColor.Background = new SolidColorBrush(color);
		}

		private void Element_MouseEnter(object sender, MouseEventArgs e)
		{
			
			if (sender is Label)
			{			
				(sender as Label).Foreground = new SolidColorBrush(Colors.Aqua);				
				(sender as Label).FontSize = 25;				
				(sender as Label).VerticalContentAlignment = VerticalAlignment.Top;
			}
			if (sender is Slider)
			{
				(sender as Slider).Background = new SolidColorBrush(Colors.Lime);
				(sender as Slider).Foreground = new SolidColorBrush(Colors.Black);
			}
			if (sender is Button)
			{
				(sender as Button).BorderThickness = new Thickness(4, 4, 4, 4);
				(sender as Button).BorderBrush = new SolidColorBrush(Colors.Blue);
				(sender as Button).Width = 80;
				(sender as Button).Height = 40;
				(sender as Button).Background = new SolidColorBrush(Colors.Orange);
				(sender as Button).Foreground = new SolidColorBrush(Colors.Blue);
				(sender as Button).FontWeight = FontWeights.Bold;
			}
		}

		private void Element_MouseLeave(object sender, MouseEventArgs e)
		{
			if (sender is Label)
			{
				(sender as Label).Background = null;
				(sender as Label).FontWeight = FontWeights.Bold;
				(sender as Label).Foreground = this.Foreground;
				//(sender as Label).Height = 35;
				(sender as Label).FontSize = 16;
				//(sender as Label).VerticalAlignment = VerticalAlignment.Top;

			}
			if (sender is Slider)
			{
				(sender as Slider).Background = new SolidColorBrush(Colors.DarkViolet);
                (sender as Slider).Foreground = new SolidColorBrush(Colors.Aqua);
			}
			if (sender is Button)
			{
				(sender as Button).BorderThickness = new Thickness(0);
				(sender as Button).BorderBrush = new SolidColorBrush();
				(sender as Button).Width = 70;
				(sender as Button).Height = 30;
				(sender as Button).Background = this.Background;
				(sender as Button).Foreground = this.Foreground;
				(sender as Button).FontWeight = FontWeights.Regular;
			}
		}

	}
}


