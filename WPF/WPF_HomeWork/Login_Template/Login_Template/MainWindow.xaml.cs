﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Login_Template
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		public MainWindow()
		{
			InitializeComponent();
		}
		private void SignMenuChanges()
		{
			if (In.IsChecked == true)
				Group.Visibility = Visibility.Visible;
			else if (Up.IsChecked == true)
				Group.Visibility = Visibility.Collapsed;
		}


		private void In_Checked(object sender, RoutedEventArgs e)
		{
			Group.Visibility = Visibility.Visible;
		}

		private void In_Unchecked(object sender, RoutedEventArgs e)
		{

		}

		private void Up_Unchecked(object sender, RoutedEventArgs e)
		{

		}

		private void Up_Checked(object sender, RoutedEventArgs e)
		{
			Group.Visibility = Visibility.Collapsed;
		}

		private void Button_Click(object sender, RoutedEventArgs e)
		{

		}

		private void EventTrigger_MouseDown(object sender, MouseButtonEventArgs e)
		{

		}
	}
}
