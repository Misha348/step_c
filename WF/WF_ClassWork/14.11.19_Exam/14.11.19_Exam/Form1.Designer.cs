﻿namespace _14._11._19_Exam
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.components = new System.ComponentModel.Container();
			this.richTextBox1 = new System.Windows.Forms.RichTextBox();
			this.richTextBox2 = new System.Windows.Forms.RichTextBox();
			this.button_Go = new System.Windows.Forms.Button();
			this.statusStrip1 = new System.Windows.Forms.StatusStrip();
			this.toolStripStatusLabel_Time = new System.Windows.Forms.ToolStripStatusLabel();
			this.toolStripStatusLabel_TimeCounter = new System.Windows.Forms.ToolStripStatusLabel();
			this.toolStripStatusLabel_Mistackes = new System.Windows.Forms.ToolStripStatusLabel();
			this.toolStripStatusLabel_MistackesCounter = new System.Windows.Forms.ToolStripStatusLabel();
			this.toolStripStatusLabel_Space = new System.Windows.Forms.ToolStripStatusLabel();
			this.toolStripProgressBar1 = new System.Windows.Forms.ToolStripProgressBar();
			this.timer1 = new System.Windows.Forms.Timer(this.components);
			this.statusStrip1.SuspendLayout();
			this.SuspendLayout();
			// 
			// richTextBox1
			// 
			this.richTextBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.richTextBox1.Location = new System.Drawing.Point(12, 25);
			this.richTextBox1.Name = "richTextBox1";
			this.richTextBox1.Size = new System.Drawing.Size(475, 64);
			this.richTextBox1.TabIndex = 0;
			this.richTextBox1.Text = "";
			this.richTextBox1.TextChanged += new System.EventHandler(this.richTextBox1_TextChanged);
			// 
			// richTextBox2
			// 
			this.richTextBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.richTextBox2.Location = new System.Drawing.Point(12, 147);
			this.richTextBox2.Name = "richTextBox2";
			this.richTextBox2.Size = new System.Drawing.Size(475, 62);
			this.richTextBox2.TabIndex = 1;
			this.richTextBox2.Text = "";
			this.richTextBox2.TextChanged += new System.EventHandler(this.richTextBox2_TextChanged);
			// 
			// button_Go
			// 
			this.button_Go.Location = new System.Drawing.Point(214, 106);
			this.button_Go.Name = "button_Go";
			this.button_Go.Size = new System.Drawing.Size(75, 23);
			this.button_Go.TabIndex = 2;
			this.button_Go.Text = "GO";
			this.button_Go.UseVisualStyleBackColor = true;
			this.button_Go.Click += new System.EventHandler(this.button_Go_Click);
			// 
			// statusStrip1
			// 
			this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel_Time,
            this.toolStripStatusLabel_TimeCounter,
            this.toolStripStatusLabel_Mistackes,
            this.toolStripStatusLabel_MistackesCounter,
            this.toolStripStatusLabel_Space,
            this.toolStripProgressBar1});
			this.statusStrip1.Location = new System.Drawing.Point(0, 223);
			this.statusStrip1.Name = "statusStrip1";
			this.statusStrip1.Size = new System.Drawing.Size(499, 22);
			this.statusStrip1.TabIndex = 3;
			this.statusStrip1.Text = "statusStrip1";
			// 
			// toolStripStatusLabel_Time
			// 
			this.toolStripStatusLabel_Time.Name = "toolStripStatusLabel_Time";
			this.toolStripStatusLabel_Time.Size = new System.Drawing.Size(37, 17);
			this.toolStripStatusLabel_Time.Text = "Time:";
			// 
			// toolStripStatusLabel_TimeCounter
			// 
			this.toolStripStatusLabel_TimeCounter.Name = "toolStripStatusLabel_TimeCounter";
			this.toolStripStatusLabel_TimeCounter.Size = new System.Drawing.Size(13, 17);
			this.toolStripStatusLabel_TimeCounter.Text = "0";
			// 
			// toolStripStatusLabel_Mistackes
			// 
			this.toolStripStatusLabel_Mistackes.Name = "toolStripStatusLabel_Mistackes";
			this.toolStripStatusLabel_Mistackes.Size = new System.Drawing.Size(71, 17);
			this.toolStripStatusLabel_Mistackes.Text = "   Mistackes:";
			// 
			// toolStripStatusLabel_MistackesCounter
			// 
			this.toolStripStatusLabel_MistackesCounter.Name = "toolStripStatusLabel_MistackesCounter";
			this.toolStripStatusLabel_MistackesCounter.Size = new System.Drawing.Size(13, 17);
			this.toolStripStatusLabel_MistackesCounter.Text = "0";
			// 
			// toolStripStatusLabel_Space
			// 
			this.toolStripStatusLabel_Space.Name = "toolStripStatusLabel_Space";
			this.toolStripStatusLabel_Space.Size = new System.Drawing.Size(52, 17);
			this.toolStripStatusLabel_Space.Text = "               ";
			// 
			// toolStripProgressBar1
			// 
			this.toolStripProgressBar1.Name = "toolStripProgressBar1";
			this.toolStripProgressBar1.Size = new System.Drawing.Size(100, 16);
			// 
			// timer1
			// 
			this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(499, 245);
			this.Controls.Add(this.statusStrip1);
			this.Controls.Add(this.button_Go);
			this.Controls.Add(this.richTextBox2);
			this.Controls.Add(this.richTextBox1);
			this.Name = "Form1";
			this.Text = "Form1";
			this.statusStrip1.ResumeLayout(false);
			this.statusStrip1.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.RichTextBox richTextBox2;
        private System.Windows.Forms.Button button_Go;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel_Time;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel_TimeCounter;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel_Mistackes;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel_MistackesCounter;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel_Space;
        private System.Windows.Forms.ToolStripProgressBar toolStripProgressBar1;
        private System.Windows.Forms.Timer timer1;
    }
}

