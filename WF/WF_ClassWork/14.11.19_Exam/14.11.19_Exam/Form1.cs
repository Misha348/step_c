﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _14._11._19_Exam
{
    public partial class Form1 : Form
    {
        public int TimeCounter { get; set; }
        public char[] Arr { get; set; } 
        public int EntiredSymbolCounter { get; private set; }
		public int MistackesCounter { get; set; }


		public Form1()
        {
            InitializeComponent();
            //timer1.Tick += timer1_Tick;
            timer1.Interval = 1000;
            richTextBox1.AllowDrop = true;
            richTextBox1.DragDrop += RichTextBox_DragDrop;

            toolStripProgressBar1.Minimum = 0;
          
            toolStripProgressBar1.Value = 0;
            toolStripProgressBar1.Step = 1;
			MistackesCounter = 0;

			EntiredSymbolCounter = 0;
			button_Go.Enabled = false;
			richTextBox2.Enabled = false;
        }


        private void button_Go_Click(object sender, EventArgs e)
        {
            TimeCounter = 0;
            timer1.Start();
			richTextBox2.Enabled = true;
		}

        private void RichTextBox_DragDrop(object sender, DragEventArgs e)
        {
            var data = e.Data.GetData(DataFormats.FileDrop);
            if (data != null)
            {
               var fileNames = data as string[];
               if (fileNames.Length > 0)
                   richTextBox1.LoadFile(fileNames[0], RichTextBoxStreamType.PlainText);

				

				button_Go.Enabled = true;

			}
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            toolStripStatusLabel_TimeCounter.Text = (++TimeCounter).ToString();
        }

        private void SymbolChecker(string text)
        {           
            char[] arr = text.ToCharArray();			

			for (int i = 0; i < Arr.Length; i++)
            {
				if (arr[EntiredSymbolCounter] == Arr[EntiredSymbolCounter])
				{
					richTextBox1.SelectionStart = EntiredSymbolCounter;
					richTextBox1.SelectionLength = 1;
					richTextBox1.SelectionColor = Color.Red;
					richTextBox1.SelectionBackColor = Color.Yellow;

					toolStripProgressBar1.Value++;
					EntiredSymbolCounter++;
				
					break;
				}
				else if (arr[EntiredSymbolCounter] != Arr[EntiredSymbolCounter])
				{
					MessageBox.Show($"wrong symbol !!!", "MISTACKE");
					toolStripStatusLabel_MistackesCounter.Text = (++MistackesCounter).ToString();

					string tmp = (richTextBox2.Text).Remove((richTextBox2.Text).Length-1);
					richTextBox2.Text = tmp;
					richTextBox2.SelectionStart = EntiredSymbolCounter;
					break;
				}
			}
          
        }

        private void TextInfoChanges()
        {
            string text = richTextBox2.Text;		

            SymbolChecker(text);
			if (text == richTextBox1.Text)
			{				
				timer1.Stop();
				MessageBox.Show($"you have finished!\n   for {TimeCounter.ToString()} sec");
				richTextBox1.Text = "";
				richTextBox2.Text = "";
				button_Go.Enabled = false;
				toolStripStatusLabel_TimeCounter.Text = "0";
				EntiredSymbolCounter = 0;
				toolStripStatusLabel_MistackesCounter.Text = "0";
				MistackesCounter = 0;
				richTextBox2.Enabled = false;
			}
		}


        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {           
            Arr = (richTextBox1.Text).ToCharArray();
			toolStripProgressBar1.Maximum = richTextBox1.Text.Length;

			if(richTextBox1.Text.Length >= 1)
			button_Go.Enabled = true;
		}

        private void richTextBox2_TextChanged(object sender, EventArgs e)
        {
            TextInfoChanges();
        }
    }
}
