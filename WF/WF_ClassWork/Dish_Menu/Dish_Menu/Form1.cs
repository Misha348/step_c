﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dish_Menu
{
    public partial class Form1 : Form
    {
        private Order NewOrder { get; set; }
        protected bool IsCheckingFirstDish { get; set; }
		protected bool IsCheckingDrinks { get; set; }
		protected bool IsCheckingDeserts { get; set; }

		protected string FirstDishName { get; set; }
		protected int FirstDishPrice { get; set; }
		protected string DrinkName { get; set; }
		protected int DrinkPrice { get; set; }
		protected string DesertName { get; set; }
		protected int DesertPrice { get; set; }
		protected List<Order> Orders { get; set; }
		protected BinaryFormatter formatter;


		private  void SetDishPrices()
		{
			radioButton1_Borshch.Tag = 30;
			radioButton2_Steak.Tag = 90;
			radioButton3_Salat.Tag = 35;
			radioButton4_Pasta.Tag = 45;
			radioButton5_Hot_Pan.Tag = 75;
			radioButton6_Tea.Tag = 12;
			radioButton_Coffee.Tag = 20;
			radioButton8_Capuchino.Tag = 23;
			radioButton9_min_water.Tag = 18;
			radioButton10_pepsi.Tag = 22;
			radioButton11_tiramisu.Tag = 25;
			radioButton12_cheesCacke.Tag = 22;
			radioButton13_cacke.Tag = 22;
			radioButton14_fruit_salat.Tag = 24;
			radioButton15_ice_cream.Tag = 20;
		}

		public Form1()
        {
            InitializeComponent();
			SetDishPrices();
			button1_calc.Enabled = false;

			foreach (var item in groupBox1_first_dish.Controls)
			{
				if (item is RadioButton)
				{
					RadioButton tmp = item as RadioButton;
					tmp.CheckedChanged += RadioButton1_FirstDish_CheckedChanged;					
				}
			}

            foreach (var item in groupBox2_Drinks.Controls)
            {
                if (item is RadioButton)
                {
                    RadioButton tmp = item as RadioButton;
                    tmp.CheckedChanged += RadioButton2_Drinks_CheckedChanged;
                }
            }

            foreach (var item in groupBox3_Deserts.Controls)
            {
                if (item is RadioButton)
                {
                    RadioButton tmp = item as RadioButton;
                    tmp.CheckedChanged += RadioButton3_Deserts_CheckedChanged;
                }
            }

			Orders = new List<Order>();
		}

		private void RadioButton1_FirstDish_CheckedChanged(object sender, EventArgs e)
		{
            FirstDishName = (sender as RadioButton).Text;
            FirstDishPrice = (int)(sender as RadioButton).Tag;

            IsCheckingFirstDish = true;           
            DishCheckerVerificator();
        }

        private void RadioButton2_Drinks_CheckedChanged(object sender, EventArgs e)
        {
			DrinkName = (sender as RadioButton).Text;
			DrinkPrice = (int)(sender as RadioButton).Tag;

			IsCheckingDrinks = true;          
            DishCheckerVerificator();
        }

        private void RadioButton3_Deserts_CheckedChanged(object sender, EventArgs e)
        {
			DesertName = (sender as RadioButton).Text;
			DesertPrice = (int)(sender as RadioButton).Tag;

			IsCheckingDeserts = true;          
            DishCheckerVerificator();
        }

        public void DishCheckerVerificator()
        {
            if (IsCheckingFirstDish == true &&
                (IsCheckingDrinks == true || IsCheckingDeserts == true))
            {
                button1_calc.Enabled = true;
            }
        }

        public void CreateBill()
        {
			Random rnd = new Random();
			NewOrder = null;
			NewOrder = new Order
			{
				DishName = FirstDishName,
				DichPrice = FirstDishPrice,
				DrinkName = DrinkName,
				DrinkPrice = DrinkPrice,
				DesertName = DesertName,
				DesertPrice = DesertPrice,
				OrederTime = DateTime.Now,
				OrderPrice = FirstDishPrice + DrinkPrice + DesertPrice,
				OrderNo = rnd.Next(0, 10000)
			};
			//Orders = new List<Order>();
			Orders.Add(NewOrder);
		}

		public void TurnOffRadioButtons(GroupBox groupBoxName)
		{
			foreach (var item in groupBoxName.Controls)
			{
				if (item is RadioButton)
				{
					RadioButton tmp = item as RadioButton;
					tmp.Checked = false;
				}
			}
		}

        private void button1_calc_Click(object sender, EventArgs e)
        {			
			CreateBill();
			MessageBox.Show($"TOTAL PRICE: {NewOrder.OrderPrice} uah ", $"No: {NewOrder.OrderNo}.  {NewOrder.OrederTime.ToUniversalTime()}");

			//if (!this.listBox1.Items.Contains(this.Order.OrderNo))
			//{
			//	this.listBox1.Items.Add(this.Order.DispayShortOrderData);
			//}	
			listBox1.DataSource = null;
			listBox1.DataSource = Orders;
			listBox1.DisplayMember = nameof(NewOrder.DispayShortOrderData);			

			TurnOffRadioButtons(groupBox1_first_dish);
			TurnOffRadioButtons(groupBox2_Drinks);
			TurnOffRadioButtons(groupBox3_Deserts);			

			button1_calc.Enabled = false;
		}

		private void button1_Click(object sender, EventArgs e)
		{
			if (listBox1.SelectedIndex != -1)
			{
				Order ord = (Order)listBox1.SelectedItem;
				MessageBox.Show(ord.ToString());
			}			
		}

		private void button3_Save_Click(object sender, EventArgs e)
		{
			formatter = new BinaryFormatter();
			using (FileStream fs = new FileStream("dish.dat", FileMode.OpenOrCreate))
			{				
				formatter.Serialize(fs, Orders);
				MessageBox.Show("orders have saved");				
			}
		}		

		private void button2_Load_Click(object sender, EventArgs e)
		{
			formatter = new BinaryFormatter();
			Orders = null;
			using (Stream fs = File.OpenRead("dish.dat"))
			{
				Orders = (List<Order>)formatter.Deserialize(fs);
			}

			string res = "";
			foreach (var ord in Orders)
			{
				res += ord.ToString();
				res += "\n";			
			}
			MessageBox.Show(res);
		}

		private void button4_DelSelect_Click(object sender, EventArgs e)
		{
			Order ord = (Order)listBox1.SelectedItem;
			Orders.Remove(ord);

			listBox1.DataSource = null;
			listBox1.DataSource = Orders;
			listBox1.DisplayMember = nameof(NewOrder.DispayShortOrderData);
		}
	}
}
