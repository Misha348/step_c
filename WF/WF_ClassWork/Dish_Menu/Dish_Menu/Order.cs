﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dish_Menu
{
	[Serializable]
	public class Order
	{
		public string DishName { get; set; }
		public int DichPrice { get; set; }
		public string DrinkName { get; set; }
		public int DrinkPrice { get; set; }

		public string DesertName { get; set; }
		public int DesertPrice { get; set; }
		public int OrderPrice {get; set;}
		public DateTime OrederTime { get; set; }
		public int OrderNo { get; set; }
	    public string DispayShortOrderData
		{
			get
			{
				return $"No: {OrderNo.ToString()}. Time: {OrederTime.ToLocalTime()}.";
			}
		}

		public override string ToString()
		{
			return  $"Dish: {DishName} - {DichPrice} uah.\n" +
					$"Drink: {DrinkName} - {DrinkPrice} uah.\n" +
					$"Desert: {DesertName} - {DesertPrice} uah.\n" +
					$"------\n" +
					$"Sum - {OrderPrice}\n" +
					$"ord. No - {OrderNo}\n" +
					$"time: {OrederTime.ToLocalTime()}\n\n";
		}
	}
}
