﻿namespace Dish_Menu
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.components = new System.ComponentModel.Container();
			this.groupBox1_first_dish = new System.Windows.Forms.GroupBox();
			this.label5_price_HotPan = new System.Windows.Forms.Label();
			this.label4_price_Pasta = new System.Windows.Forms.Label();
			this.label3_price_Salat = new System.Windows.Forms.Label();
			this.label2_priceSteak = new System.Windows.Forms.Label();
			this.label1_priceBorshch = new System.Windows.Forms.Label();
			this.radioButton5_Hot_Pan = new System.Windows.Forms.RadioButton();
			this.radioButton4_Pasta = new System.Windows.Forms.RadioButton();
			this.radioButton3_Salat = new System.Windows.Forms.RadioButton();
			this.radioButton2_Steak = new System.Windows.Forms.RadioButton();
			this.radioButton1_Borshch = new System.Windows.Forms.RadioButton();
			this.groupBox2_Drinks = new System.Windows.Forms.GroupBox();
			this.label10_price_pepsi = new System.Windows.Forms.Label();
			this.label9_price_minwater = new System.Windows.Forms.Label();
			this.label8_price_capuchino = new System.Windows.Forms.Label();
			this.label7_price_coffee = new System.Windows.Forms.Label();
			this.label6_price_tea = new System.Windows.Forms.Label();
			this.radioButton10_pepsi = new System.Windows.Forms.RadioButton();
			this.radioButton9_min_water = new System.Windows.Forms.RadioButton();
			this.radioButton8_Capuchino = new System.Windows.Forms.RadioButton();
			this.radioButton_Coffee = new System.Windows.Forms.RadioButton();
			this.radioButton6_Tea = new System.Windows.Forms.RadioButton();
			this.groupBox3_Deserts = new System.Windows.Forms.GroupBox();
			this.label15_price_ice = new System.Windows.Forms.Label();
			this.label14_price_frSalad = new System.Windows.Forms.Label();
			this.label13_price_cacke = new System.Windows.Forms.Label();
			this.label12_price_chCacke = new System.Windows.Forms.Label();
			this.label11_price_tiramisu = new System.Windows.Forms.Label();
			this.radioButton15_ice_cream = new System.Windows.Forms.RadioButton();
			this.radioButton14_fruit_salat = new System.Windows.Forms.RadioButton();
			this.radioButton13_cacke = new System.Windows.Forms.RadioButton();
			this.radioButton12_cheesCacke = new System.Windows.Forms.RadioButton();
			this.radioButton11_tiramisu = new System.Windows.Forms.RadioButton();
			this.button1_calc = new System.Windows.Forms.Button();
			this.label1_item = new System.Windows.Forms.Label();
			this.listBox1 = new System.Windows.Forms.ListBox();
			this.label1 = new System.Windows.Forms.Label();
			this.button1 = new System.Windows.Forms.Button();
			this.button2_Load = new System.Windows.Forms.Button();
			this.button3_Save = new System.Windows.Forms.Button();
			this.button4_DelSelect = new System.Windows.Forms.Button();
			this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
			this.toolTip2 = new System.Windows.Forms.ToolTip(this.components);
			this.toolTip3 = new System.Windows.Forms.ToolTip(this.components);
			this.groupBox1_first_dish.SuspendLayout();
			this.groupBox2_Drinks.SuspendLayout();
			this.groupBox3_Deserts.SuspendLayout();
			this.SuspendLayout();
			// 
			// groupBox1_first_dish
			// 
			this.groupBox1_first_dish.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
			this.groupBox1_first_dish.Controls.Add(this.label5_price_HotPan);
			this.groupBox1_first_dish.Controls.Add(this.label4_price_Pasta);
			this.groupBox1_first_dish.Controls.Add(this.label3_price_Salat);
			this.groupBox1_first_dish.Controls.Add(this.label2_priceSteak);
			this.groupBox1_first_dish.Controls.Add(this.label1_priceBorshch);
			this.groupBox1_first_dish.Controls.Add(this.radioButton5_Hot_Pan);
			this.groupBox1_first_dish.Controls.Add(this.radioButton4_Pasta);
			this.groupBox1_first_dish.Controls.Add(this.radioButton3_Salat);
			this.groupBox1_first_dish.Controls.Add(this.radioButton2_Steak);
			this.groupBox1_first_dish.Controls.Add(this.radioButton1_Borshch);
			this.groupBox1_first_dish.Location = new System.Drawing.Point(33, 71);
			this.groupBox1_first_dish.Name = "groupBox1_first_dish";
			this.groupBox1_first_dish.Size = new System.Drawing.Size(177, 173);
			this.groupBox1_first_dish.TabIndex = 0;
			this.groupBox1_first_dish.TabStop = false;
			this.groupBox1_first_dish.Text = "FIRST DISH";
			// 
			// label5_price_HotPan
			// 
			this.label5_price_HotPan.AutoSize = true;
			this.label5_price_HotPan.Location = new System.Drawing.Point(120, 116);
			this.label5_price_HotPan.Name = "label5_price_HotPan";
			this.label5_price_HotPan.Size = new System.Drawing.Size(40, 13);
			this.label5_price_HotPan.TabIndex = 9;
			this.label5_price_HotPan.Tag = "75";
			this.label5_price_HotPan.Text = "75 uah";
			// 
			// label4_price_Pasta
			// 
			this.label4_price_Pasta.AutoSize = true;
			this.label4_price_Pasta.Location = new System.Drawing.Point(120, 92);
			this.label4_price_Pasta.Name = "label4_price_Pasta";
			this.label4_price_Pasta.Size = new System.Drawing.Size(40, 13);
			this.label4_price_Pasta.TabIndex = 8;
			this.label4_price_Pasta.Tag = "45";
			this.label4_price_Pasta.Text = "45 uah";
			// 
			// label3_price_Salat
			// 
			this.label3_price_Salat.AutoSize = true;
			this.label3_price_Salat.Location = new System.Drawing.Point(120, 68);
			this.label3_price_Salat.Name = "label3_price_Salat";
			this.label3_price_Salat.Size = new System.Drawing.Size(40, 13);
			this.label3_price_Salat.TabIndex = 7;
			this.label3_price_Salat.Tag = "35";
			this.label3_price_Salat.Text = "35 uah";
			// 
			// label2_priceSteak
			// 
			this.label2_priceSteak.AutoSize = true;
			this.label2_priceSteak.Location = new System.Drawing.Point(120, 44);
			this.label2_priceSteak.Name = "label2_priceSteak";
			this.label2_priceSteak.Size = new System.Drawing.Size(40, 13);
			this.label2_priceSteak.TabIndex = 6;
			this.label2_priceSteak.Tag = "90";
			this.label2_priceSteak.Text = "90 uah";
			// 
			// label1_priceBorshch
			// 
			this.label1_priceBorshch.AutoSize = true;
			this.label1_priceBorshch.Location = new System.Drawing.Point(120, 22);
			this.label1_priceBorshch.Name = "label1_priceBorshch";
			this.label1_priceBorshch.Size = new System.Drawing.Size(40, 13);
			this.label1_priceBorshch.TabIndex = 5;
			this.label1_priceBorshch.Tag = "30";
			this.label1_priceBorshch.Text = "30 uah";
			// 
			// radioButton5_Hot_Pan
			// 
			this.radioButton5_Hot_Pan.AutoSize = true;
			this.radioButton5_Hot_Pan.Location = new System.Drawing.Point(16, 116);
			this.radioButton5_Hot_Pan.Name = "radioButton5_Hot_Pan";
			this.radioButton5_Hot_Pan.Size = new System.Drawing.Size(63, 17);
			this.radioButton5_Hot_Pan.TabIndex = 4;
			this.radioButton5_Hot_Pan.TabStop = true;
			this.radioButton5_Hot_Pan.Text = "Hot pan";
			this.radioButton5_Hot_Pan.UseVisualStyleBackColor = true;
			// 
			// radioButton4_Pasta
			// 
			this.radioButton4_Pasta.AutoSize = true;
			this.radioButton4_Pasta.Location = new System.Drawing.Point(16, 92);
			this.radioButton4_Pasta.Name = "radioButton4_Pasta";
			this.radioButton4_Pasta.Size = new System.Drawing.Size(52, 17);
			this.radioButton4_Pasta.TabIndex = 3;
			this.radioButton4_Pasta.TabStop = true;
			this.radioButton4_Pasta.Text = "Pasta";
			this.radioButton4_Pasta.UseVisualStyleBackColor = true;
			// 
			// radioButton3_Salat
			// 
			this.radioButton3_Salat.AutoSize = true;
			this.radioButton3_Salat.Location = new System.Drawing.Point(16, 68);
			this.radioButton3_Salat.Name = "radioButton3_Salat";
			this.radioButton3_Salat.Size = new System.Drawing.Size(49, 17);
			this.radioButton3_Salat.TabIndex = 2;
			this.radioButton3_Salat.TabStop = true;
			this.radioButton3_Salat.Text = "Salat";
			this.toolTip3.SetToolTip(this.radioButton3_Salat, "batch: 200 gr\r\ningradients:\r\n - tomatous\r\n - feta\r\n - olives\r\n - olives oil");
			this.radioButton3_Salat.UseVisualStyleBackColor = true;
			// 
			// radioButton2_Steak
			// 
			this.radioButton2_Steak.AutoSize = true;
			this.radioButton2_Steak.Location = new System.Drawing.Point(16, 44);
			this.radioButton2_Steak.Name = "radioButton2_Steak";
			this.radioButton2_Steak.Size = new System.Drawing.Size(53, 17);
			this.radioButton2_Steak.TabIndex = 1;
			this.radioButton2_Steak.TabStop = true;
			this.radioButton2_Steak.Text = "Steak";
			this.toolTip2.SetToolTip(this.radioButton2_Steak, "batch: 250 gr.\r\ningradients;\r\n - pork\r\n - solt\r\n - paper\r\n -souce\r\n - spices");
			this.radioButton2_Steak.UseVisualStyleBackColor = true;
			// 
			// radioButton1_Borshch
			// 
			this.radioButton1_Borshch.AutoSize = true;
			this.radioButton1_Borshch.Location = new System.Drawing.Point(16, 20);
			this.radioButton1_Borshch.Name = "radioButton1_Borshch";
			this.radioButton1_Borshch.Size = new System.Drawing.Size(64, 17);
			this.radioButton1_Borshch.TabIndex = 0;
			this.radioButton1_Borshch.TabStop = true;
			this.radioButton1_Borshch.Text = "Borshch";
			this.toolTip1.SetToolTip(this.radioButton1_Borshch, "batch: 300 gr.\r\ningradients:\r\n - potatous\r\n - meat\r\n - beet\r\n - onion\r\n - bean\r\n " +
        "- sour cream");
			this.radioButton1_Borshch.UseVisualStyleBackColor = true;
			// 
			// groupBox2_Drinks
			// 
			this.groupBox2_Drinks.Controls.Add(this.label10_price_pepsi);
			this.groupBox2_Drinks.Controls.Add(this.label9_price_minwater);
			this.groupBox2_Drinks.Controls.Add(this.label8_price_capuchino);
			this.groupBox2_Drinks.Controls.Add(this.label7_price_coffee);
			this.groupBox2_Drinks.Controls.Add(this.label6_price_tea);
			this.groupBox2_Drinks.Controls.Add(this.radioButton10_pepsi);
			this.groupBox2_Drinks.Controls.Add(this.radioButton9_min_water);
			this.groupBox2_Drinks.Controls.Add(this.radioButton8_Capuchino);
			this.groupBox2_Drinks.Controls.Add(this.radioButton_Coffee);
			this.groupBox2_Drinks.Controls.Add(this.radioButton6_Tea);
			this.groupBox2_Drinks.Location = new System.Drawing.Point(265, 71);
			this.groupBox2_Drinks.Name = "groupBox2_Drinks";
			this.groupBox2_Drinks.Size = new System.Drawing.Size(180, 173);
			this.groupBox2_Drinks.TabIndex = 1;
			this.groupBox2_Drinks.TabStop = false;
			this.groupBox2_Drinks.Text = "DRINKS";
			// 
			// label10_price_pepsi
			// 
			this.label10_price_pepsi.AutoSize = true;
			this.label10_price_pepsi.Location = new System.Drawing.Point(115, 116);
			this.label10_price_pepsi.Name = "label10_price_pepsi";
			this.label10_price_pepsi.Size = new System.Drawing.Size(40, 13);
			this.label10_price_pepsi.TabIndex = 9;
			this.label10_price_pepsi.Text = "22 uah";
			// 
			// label9_price_minwater
			// 
			this.label9_price_minwater.AutoSize = true;
			this.label9_price_minwater.Location = new System.Drawing.Point(115, 92);
			this.label9_price_minwater.Name = "label9_price_minwater";
			this.label9_price_minwater.Size = new System.Drawing.Size(40, 13);
			this.label9_price_minwater.TabIndex = 8;
			this.label9_price_minwater.Text = "18 uah";
			// 
			// label8_price_capuchino
			// 
			this.label8_price_capuchino.AutoSize = true;
			this.label8_price_capuchino.Location = new System.Drawing.Point(115, 68);
			this.label8_price_capuchino.Name = "label8_price_capuchino";
			this.label8_price_capuchino.Size = new System.Drawing.Size(40, 13);
			this.label8_price_capuchino.TabIndex = 7;
			this.label8_price_capuchino.Text = "23 uah";
			// 
			// label7_price_coffee
			// 
			this.label7_price_coffee.AutoSize = true;
			this.label7_price_coffee.Location = new System.Drawing.Point(115, 44);
			this.label7_price_coffee.Name = "label7_price_coffee";
			this.label7_price_coffee.Size = new System.Drawing.Size(40, 13);
			this.label7_price_coffee.TabIndex = 6;
			this.label7_price_coffee.Text = "20 uah";
			// 
			// label6_price_tea
			// 
			this.label6_price_tea.AutoSize = true;
			this.label6_price_tea.Location = new System.Drawing.Point(115, 22);
			this.label6_price_tea.Name = "label6_price_tea";
			this.label6_price_tea.Size = new System.Drawing.Size(40, 13);
			this.label6_price_tea.TabIndex = 5;
			this.label6_price_tea.Text = "12 uah";
			// 
			// radioButton10_pepsi
			// 
			this.radioButton10_pepsi.AutoSize = true;
			this.radioButton10_pepsi.Location = new System.Drawing.Point(7, 116);
			this.radioButton10_pepsi.Name = "radioButton10_pepsi";
			this.radioButton10_pepsi.Size = new System.Drawing.Size(51, 17);
			this.radioButton10_pepsi.TabIndex = 4;
			this.radioButton10_pepsi.TabStop = true;
			this.radioButton10_pepsi.Text = "Pepsi";
			this.radioButton10_pepsi.UseVisualStyleBackColor = true;
			// 
			// radioButton9_min_water
			// 
			this.radioButton9_min_water.AutoSize = true;
			this.radioButton9_min_water.Location = new System.Drawing.Point(7, 92);
			this.radioButton9_min_water.Name = "radioButton9_min_water";
			this.radioButton9_min_water.Size = new System.Drawing.Size(71, 17);
			this.radioButton9_min_water.TabIndex = 3;
			this.radioButton9_min_water.TabStop = true;
			this.radioButton9_min_water.Text = "Min.water";
			this.radioButton9_min_water.UseVisualStyleBackColor = true;
			// 
			// radioButton8_Capuchino
			// 
			this.radioButton8_Capuchino.AutoSize = true;
			this.radioButton8_Capuchino.Location = new System.Drawing.Point(7, 68);
			this.radioButton8_Capuchino.Name = "radioButton8_Capuchino";
			this.radioButton8_Capuchino.Size = new System.Drawing.Size(76, 17);
			this.radioButton8_Capuchino.TabIndex = 2;
			this.radioButton8_Capuchino.TabStop = true;
			this.radioButton8_Capuchino.Text = "Capuchino";
			this.radioButton8_Capuchino.UseVisualStyleBackColor = true;
			// 
			// radioButton_Coffee
			// 
			this.radioButton_Coffee.AutoSize = true;
			this.radioButton_Coffee.Location = new System.Drawing.Point(7, 44);
			this.radioButton_Coffee.Name = "radioButton_Coffee";
			this.radioButton_Coffee.Size = new System.Drawing.Size(56, 17);
			this.radioButton_Coffee.TabIndex = 1;
			this.radioButton_Coffee.TabStop = true;
			this.radioButton_Coffee.Text = "Coffee";
			this.radioButton_Coffee.UseVisualStyleBackColor = true;
			// 
			// radioButton6_Tea
			// 
			this.radioButton6_Tea.AutoSize = true;
			this.radioButton6_Tea.Location = new System.Drawing.Point(7, 20);
			this.radioButton6_Tea.Name = "radioButton6_Tea";
			this.radioButton6_Tea.Size = new System.Drawing.Size(44, 17);
			this.radioButton6_Tea.TabIndex = 0;
			this.radioButton6_Tea.TabStop = true;
			this.radioButton6_Tea.Text = "Tea";
			this.radioButton6_Tea.UseVisualStyleBackColor = true;
			// 
			// groupBox3_Deserts
			// 
			this.groupBox3_Deserts.Controls.Add(this.label15_price_ice);
			this.groupBox3_Deserts.Controls.Add(this.label14_price_frSalad);
			this.groupBox3_Deserts.Controls.Add(this.label13_price_cacke);
			this.groupBox3_Deserts.Controls.Add(this.label12_price_chCacke);
			this.groupBox3_Deserts.Controls.Add(this.label11_price_tiramisu);
			this.groupBox3_Deserts.Controls.Add(this.radioButton15_ice_cream);
			this.groupBox3_Deserts.Controls.Add(this.radioButton14_fruit_salat);
			this.groupBox3_Deserts.Controls.Add(this.radioButton13_cacke);
			this.groupBox3_Deserts.Controls.Add(this.radioButton12_cheesCacke);
			this.groupBox3_Deserts.Controls.Add(this.radioButton11_tiramisu);
			this.groupBox3_Deserts.Location = new System.Drawing.Point(480, 71);
			this.groupBox3_Deserts.Name = "groupBox3_Deserts";
			this.groupBox3_Deserts.Size = new System.Drawing.Size(209, 173);
			this.groupBox3_Deserts.TabIndex = 2;
			this.groupBox3_Deserts.TabStop = false;
			this.groupBox3_Deserts.Text = "DESERTS";
			// 
			// label15_price_ice
			// 
			this.label15_price_ice.AutoSize = true;
			this.label15_price_ice.Location = new System.Drawing.Point(139, 118);
			this.label15_price_ice.Name = "label15_price_ice";
			this.label15_price_ice.Size = new System.Drawing.Size(40, 13);
			this.label15_price_ice.TabIndex = 9;
			this.label15_price_ice.Text = "20 uah";
			// 
			// label14_price_frSalad
			// 
			this.label14_price_frSalad.AutoSize = true;
			this.label14_price_frSalad.Location = new System.Drawing.Point(139, 94);
			this.label14_price_frSalad.Name = "label14_price_frSalad";
			this.label14_price_frSalad.Size = new System.Drawing.Size(40, 13);
			this.label14_price_frSalad.TabIndex = 8;
			this.label14_price_frSalad.Text = "24 uah";
			// 
			// label13_price_cacke
			// 
			this.label13_price_cacke.AutoSize = true;
			this.label13_price_cacke.Location = new System.Drawing.Point(139, 68);
			this.label13_price_cacke.Name = "label13_price_cacke";
			this.label13_price_cacke.Size = new System.Drawing.Size(40, 13);
			this.label13_price_cacke.TabIndex = 7;
			this.label13_price_cacke.Text = "22 uah";
			// 
			// label12_price_chCacke
			// 
			this.label12_price_chCacke.AutoSize = true;
			this.label12_price_chCacke.Location = new System.Drawing.Point(139, 44);
			this.label12_price_chCacke.Name = "label12_price_chCacke";
			this.label12_price_chCacke.Size = new System.Drawing.Size(40, 13);
			this.label12_price_chCacke.TabIndex = 6;
			this.label12_price_chCacke.Text = "22 uah";
			// 
			// label11_price_tiramisu
			// 
			this.label11_price_tiramisu.AutoSize = true;
			this.label11_price_tiramisu.Location = new System.Drawing.Point(139, 22);
			this.label11_price_tiramisu.Name = "label11_price_tiramisu";
			this.label11_price_tiramisu.Size = new System.Drawing.Size(40, 13);
			this.label11_price_tiramisu.TabIndex = 5;
			this.label11_price_tiramisu.Text = "25 uah";
			// 
			// radioButton15_ice_cream
			// 
			this.radioButton15_ice_cream.AutoSize = true;
			this.radioButton15_ice_cream.Location = new System.Drawing.Point(7, 116);
			this.radioButton15_ice_cream.Name = "radioButton15_ice_cream";
			this.radioButton15_ice_cream.Size = new System.Drawing.Size(72, 17);
			this.radioButton15_ice_cream.TabIndex = 4;
			this.radioButton15_ice_cream.TabStop = true;
			this.radioButton15_ice_cream.Text = "Ice cream";
			this.radioButton15_ice_cream.UseVisualStyleBackColor = true;
			// 
			// radioButton14_fruit_salat
			// 
			this.radioButton14_fruit_salat.AutoSize = true;
			this.radioButton14_fruit_salat.Location = new System.Drawing.Point(7, 92);
			this.radioButton14_fruit_salat.Name = "radioButton14_fruit_salat";
			this.radioButton14_fruit_salat.Size = new System.Drawing.Size(70, 17);
			this.radioButton14_fruit_salat.TabIndex = 3;
			this.radioButton14_fruit_salat.TabStop = true;
			this.radioButton14_fruit_salat.Text = "Fruit salat";
			this.radioButton14_fruit_salat.UseVisualStyleBackColor = true;
			// 
			// radioButton13_cacke
			// 
			this.radioButton13_cacke.AutoSize = true;
			this.radioButton13_cacke.Location = new System.Drawing.Point(7, 68);
			this.radioButton13_cacke.Name = "radioButton13_cacke";
			this.radioButton13_cacke.Size = new System.Drawing.Size(56, 17);
			this.radioButton13_cacke.TabIndex = 2;
			this.radioButton13_cacke.TabStop = true;
			this.radioButton13_cacke.Text = "Cacke";
			this.radioButton13_cacke.UseVisualStyleBackColor = true;
			// 
			// radioButton12_cheesCacke
			// 
			this.radioButton12_cheesCacke.AutoSize = true;
			this.radioButton12_cheesCacke.Location = new System.Drawing.Point(7, 44);
			this.radioButton12_cheesCacke.Name = "radioButton12_cheesCacke";
			this.radioButton12_cheesCacke.Size = new System.Drawing.Size(92, 17);
			this.radioButton12_cheesCacke.TabIndex = 1;
			this.radioButton12_cheesCacke.TabStop = true;
			this.radioButton12_cheesCacke.Text = "CheeseCacke";
			this.radioButton12_cheesCacke.UseVisualStyleBackColor = true;
			// 
			// radioButton11_tiramisu
			// 
			this.radioButton11_tiramisu.AutoSize = true;
			this.radioButton11_tiramisu.Location = new System.Drawing.Point(7, 20);
			this.radioButton11_tiramisu.Name = "radioButton11_tiramisu";
			this.radioButton11_tiramisu.Size = new System.Drawing.Size(64, 17);
			this.radioButton11_tiramisu.TabIndex = 0;
			this.radioButton11_tiramisu.TabStop = true;
			this.radioButton11_tiramisu.Text = "Tiramisu";
			this.radioButton11_tiramisu.UseVisualStyleBackColor = true;
			// 
			// button1_calc
			// 
			this.button1_calc.Location = new System.Drawing.Point(569, 285);
			this.button1_calc.Name = "button1_calc";
			this.button1_calc.Size = new System.Drawing.Size(120, 23);
			this.button1_calc.TabIndex = 3;
			this.button1_calc.Text = "CALCULATE";
			this.button1_calc.UseVisualStyleBackColor = true;
			this.button1_calc.Click += new System.EventHandler(this.button1_calc_Click);
			// 
			// label1_item
			// 
			this.label1_item.AutoSize = true;
			this.label1_item.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label1_item.Location = new System.Drawing.Point(43, 43);
			this.label1_item.Name = "label1_item";
			this.label1_item.Size = new System.Drawing.Size(133, 16);
			this.label1_item.TabIndex = 4;
			this.label1_item.Text = "COMPLEX LUNCH";
			// 
			// listBox1
			// 
			this.listBox1.FormattingEnabled = true;
			this.listBox1.Location = new System.Drawing.Point(33, 285);
			this.listBox1.MultiColumn = true;
			this.listBox1.Name = "listBox1";
			this.listBox1.Size = new System.Drawing.Size(445, 160);
			this.listBox1.TabIndex = 5;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label1.Location = new System.Drawing.Point(30, 266);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(72, 16);
			this.label1.TabIndex = 6;
			this.label1.Text = "ORDERS";
			// 
			// button1
			// 
			this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.button1.Location = new System.Drawing.Point(33, 460);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(104, 23);
			this.button1.TabIndex = 7;
			this.button1.Text = "ORD. DETAILS";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// button2_Load
			// 
			this.button2_Load.Location = new System.Drawing.Point(403, 460);
			this.button2_Load.Name = "button2_Load";
			this.button2_Load.Size = new System.Drawing.Size(75, 23);
			this.button2_Load.TabIndex = 8;
			this.button2_Load.Text = "LOAD";
			this.button2_Load.UseVisualStyleBackColor = true;
			this.button2_Load.Click += new System.EventHandler(this.button2_Load_Click);
			// 
			// button3_Save
			// 
			this.button3_Save.Location = new System.Drawing.Point(322, 460);
			this.button3_Save.Name = "button3_Save";
			this.button3_Save.Size = new System.Drawing.Size(75, 23);
			this.button3_Save.TabIndex = 9;
			this.button3_Save.Text = "SAVE";
			this.button3_Save.UseVisualStyleBackColor = true;
			this.button3_Save.Click += new System.EventHandler(this.button3_Save_Click);
			// 
			// button4_DelSelect
			// 
			this.button4_DelSelect.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.button4_DelSelect.Location = new System.Drawing.Point(143, 460);
			this.button4_DelSelect.Name = "button4_DelSelect";
			this.button4_DelSelect.Size = new System.Drawing.Size(102, 23);
			this.button4_DelSelect.TabIndex = 10;
			this.button4_DelSelect.Text = "DELETE";
			this.button4_DelSelect.UseVisualStyleBackColor = true;
			this.button4_DelSelect.Click += new System.EventHandler(this.button4_DelSelect_Click);
			// 
			// toolTip1
			// 
			this.toolTip1.AutoPopDelay = 7000;
			this.toolTip1.InitialDelay = 500;
			this.toolTip1.ReshowDelay = 100;
			this.toolTip1.ToolTipTitle = "  BORCHCH";
			// 
			// toolTip2
			// 
			this.toolTip2.AutoPopDelay = 7000;
			this.toolTip2.InitialDelay = 500;
			this.toolTip2.ReshowDelay = 100;
			this.toolTip2.ToolTipTitle = "  STEAK";
			// 
			// toolTip3
			// 
			this.toolTip3.AutoPopDelay = 7000;
			this.toolTip3.InitialDelay = 500;
			this.toolTip3.ReshowDelay = 100;
			this.toolTip3.ToolTipTitle = "  GREECK SALAD";
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
			this.ClientSize = new System.Drawing.Size(856, 676);
			this.Controls.Add(this.button4_DelSelect);
			this.Controls.Add(this.button3_Save);
			this.Controls.Add(this.button2_Load);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.listBox1);
			this.Controls.Add(this.label1_item);
			this.Controls.Add(this.button1_calc);
			this.Controls.Add(this.groupBox3_Deserts);
			this.Controls.Add(this.groupBox2_Drinks);
			this.Controls.Add(this.groupBox1_first_dish);
			this.Name = "Form1";
			this.Text = "Form1";
			this.groupBox1_first_dish.ResumeLayout(false);
			this.groupBox1_first_dish.PerformLayout();
			this.groupBox2_Drinks.ResumeLayout(false);
			this.groupBox2_Drinks.PerformLayout();
			this.groupBox3_Deserts.ResumeLayout(false);
			this.groupBox3_Deserts.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1_first_dish;
        private System.Windows.Forms.Label label2_priceSteak;
        private System.Windows.Forms.Label label1_priceBorshch;
        private System.Windows.Forms.RadioButton radioButton5_Hot_Pan;
        private System.Windows.Forms.RadioButton radioButton4_Pasta;
        private System.Windows.Forms.RadioButton radioButton3_Salat;
        private System.Windows.Forms.RadioButton radioButton2_Steak;
        private System.Windows.Forms.RadioButton radioButton1_Borshch;
        private System.Windows.Forms.GroupBox groupBox2_Drinks;
        private System.Windows.Forms.RadioButton radioButton10_pepsi;
        private System.Windows.Forms.RadioButton radioButton9_min_water;
        private System.Windows.Forms.RadioButton radioButton8_Capuchino;
        private System.Windows.Forms.RadioButton radioButton_Coffee;
        private System.Windows.Forms.RadioButton radioButton6_Tea;
        private System.Windows.Forms.GroupBox groupBox3_Deserts;
        private System.Windows.Forms.RadioButton radioButton15_ice_cream;
        private System.Windows.Forms.RadioButton radioButton14_fruit_salat;
        private System.Windows.Forms.RadioButton radioButton13_cacke;
        private System.Windows.Forms.RadioButton radioButton12_cheesCacke;
        private System.Windows.Forms.RadioButton radioButton11_tiramisu;
		private System.Windows.Forms.Label label5_price_HotPan;
		private System.Windows.Forms.Label label4_price_Pasta;
		private System.Windows.Forms.Label label3_price_Salat;
		private System.Windows.Forms.Label label10_price_pepsi;
		private System.Windows.Forms.Label label9_price_minwater;
		private System.Windows.Forms.Label label8_price_capuchino;
		private System.Windows.Forms.Label label7_price_coffee;
		private System.Windows.Forms.Label label6_price_tea;
		private System.Windows.Forms.Label label15_price_ice;
		private System.Windows.Forms.Label label14_price_frSalad;
		private System.Windows.Forms.Label label13_price_cacke;
		private System.Windows.Forms.Label label12_price_chCacke;
		private System.Windows.Forms.Label label11_price_tiramisu;
		private System.Windows.Forms.Button button1_calc;
		private System.Windows.Forms.Label label1_item;
        private System.Windows.Forms.ListBox listBox1;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Button button2_Load;
		private System.Windows.Forms.Button button3_Save;
		private System.Windows.Forms.Button button4_DelSelect;
		private System.Windows.Forms.ToolTip toolTip1;
		private System.Windows.Forms.ToolTip toolTip2;
		private System.Windows.Forms.ToolTip toolTip3;
	}
}

