﻿namespace GDI_Intro
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
			this.toolStrip1 = new System.Windows.Forms.ToolStrip();
			this.toolStripButton_Save = new System.Windows.Forms.ToolStripButton();
			this.toolStripButton_Clear = new System.Windows.Forms.ToolStripButton();
			this.FilledRecttoolStripButton = new System.Windows.Forms.ToolStripButton();
			this.RectFrame_toolStripButton = new System.Windows.Forms.ToolStripButton();
			this.Point_toolStripButton = new System.Windows.Forms.ToolStripButton();
			this.Elips_toolStripButton = new System.Windows.Forms.ToolStripButton();
			this.toolStripButton_Line = new System.Windows.Forms.ToolStripButton();
			this.toolStripButton_Image = new System.Windows.Forms.ToolStripButton();
			this.Color_toolStripLabel = new System.Windows.Forms.ToolStripLabel();
			this.toolStripComboBox1 = new System.Windows.Forms.ToolStripComboBox();
			this.Thickness_toolStripLabel = new System.Windows.Forms.ToolStripLabel();
			this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
			this.label1 = new System.Windows.Forms.Label();
			this.toolStrip1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
			this.SuspendLayout();
			// 
			// toolStrip1
			// 
			this.toolStrip1.BackColor = System.Drawing.Color.Silver;
			this.toolStrip1.Dock = System.Windows.Forms.DockStyle.Left;
			this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton_Save,
            this.toolStripButton_Clear,
            this.FilledRecttoolStripButton,
            this.RectFrame_toolStripButton,
            this.Point_toolStripButton,
            this.Elips_toolStripButton,
            this.toolStripButton_Line,
            this.toolStripButton_Image,
            this.Color_toolStripLabel,
            this.toolStripComboBox1,
            this.Thickness_toolStripLabel});
			this.toolStrip1.Location = new System.Drawing.Point(0, 0);
			this.toolStrip1.Name = "toolStrip1";
			this.toolStrip1.Size = new System.Drawing.Size(114, 450);
			this.toolStrip1.TabIndex = 0;
			this.toolStrip1.Text = "toolStrip1";
			// 
			// toolStripButton_Save
			// 
			this.toolStripButton_Save.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.toolStripButton_Save.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton_Save.Image")));
			this.toolStripButton_Save.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.toolStripButton_Save.Name = "toolStripButton_Save";
			this.toolStripButton_Save.Size = new System.Drawing.Size(111, 20);
			this.toolStripButton_Save.Text = "save";
			this.toolStripButton_Save.Click += new System.EventHandler(this.toolStripButton_Save_Click);
			// 
			// toolStripButton_Clear
			// 
			this.toolStripButton_Clear.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.toolStripButton_Clear.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton_Clear.Image")));
			this.toolStripButton_Clear.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.toolStripButton_Clear.Name = "toolStripButton_Clear";
			this.toolStripButton_Clear.Size = new System.Drawing.Size(111, 20);
			this.toolStripButton_Clear.Text = "clear";
			this.toolStripButton_Clear.Click += new System.EventHandler(this.toolStripButton_Clear_Click);
			// 
			// FilledRecttoolStripButton
			// 
			this.FilledRecttoolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.FilledRecttoolStripButton.ForeColor = System.Drawing.Color.Coral;
			this.FilledRecttoolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("FilledRecttoolStripButton.Image")));
			this.FilledRecttoolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.FilledRecttoolStripButton.Name = "FilledRecttoolStripButton";
			this.FilledRecttoolStripButton.Size = new System.Drawing.Size(111, 20);
			this.FilledRecttoolStripButton.Text = "filled rect.";
			this.FilledRecttoolStripButton.Click += new System.EventHandler(this.FilledRecttoolStripButton_Click);
			// 
			// RectFrame_toolStripButton
			// 
			this.RectFrame_toolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.RectFrame_toolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("RectFrame_toolStripButton.Image")));
			this.RectFrame_toolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.RectFrame_toolStripButton.Name = "RectFrame_toolStripButton";
			this.RectFrame_toolStripButton.Size = new System.Drawing.Size(111, 20);
			this.RectFrame_toolStripButton.Text = "Rect.Frame";
			this.RectFrame_toolStripButton.Click += new System.EventHandler(this.RectFrame_toolStripButton_Click_1);
			// 
			// Point_toolStripButton
			// 
			this.Point_toolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.Point_toolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("Point_toolStripButton.Image")));
			this.Point_toolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.Point_toolStripButton.Name = "Point_toolStripButton";
			this.Point_toolStripButton.Size = new System.Drawing.Size(111, 20);
			this.Point_toolStripButton.Text = "Point";
			this.Point_toolStripButton.Click += new System.EventHandler(this.Point_toolStripButton_Click);
			// 
			// Elips_toolStripButton
			// 
			this.Elips_toolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.Elips_toolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("Elips_toolStripButton.Image")));
			this.Elips_toolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.Elips_toolStripButton.Name = "Elips_toolStripButton";
			this.Elips_toolStripButton.Size = new System.Drawing.Size(111, 20);
			this.Elips_toolStripButton.Text = "Elipse";
			this.Elips_toolStripButton.Click += new System.EventHandler(this.Elips_toolStripButton_Click);
			// 
			// toolStripButton_Line
			// 
			this.toolStripButton_Line.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.toolStripButton_Line.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton_Line.Image")));
			this.toolStripButton_Line.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.toolStripButton_Line.Name = "toolStripButton_Line";
			this.toolStripButton_Line.Size = new System.Drawing.Size(111, 20);
			this.toolStripButton_Line.Text = "line";
			this.toolStripButton_Line.Click += new System.EventHandler(this.toolStripButton_Line_Click);
			// 
			// toolStripButton_Image
			// 
			this.toolStripButton_Image.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.toolStripButton_Image.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton_Image.Image")));
			this.toolStripButton_Image.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.toolStripButton_Image.Name = "toolStripButton_Image";
			this.toolStripButton_Image.Size = new System.Drawing.Size(111, 20);
			this.toolStripButton_Image.Text = "add img";
			this.toolStripButton_Image.Click += new System.EventHandler(this.toolStripButton_Image_Click);
			// 
			// Color_toolStripLabel
			// 
			this.Color_toolStripLabel.Name = "Color_toolStripLabel";
			this.Color_toolStripLabel.Size = new System.Drawing.Size(111, 15);
			this.Color_toolStripLabel.Text = "Color";
			// 
			// toolStripComboBox1
			// 
			this.toolStripComboBox1.BackColor = System.Drawing.Color.LightGray;
			this.toolStripComboBox1.Name = "toolStripComboBox1";
			this.toolStripComboBox1.Size = new System.Drawing.Size(109, 23);
			this.toolStripComboBox1.SelectedIndexChanged += new System.EventHandler(this.toolStripComboBox1_SelectedIndexChanged);
			// 
			// Thickness_toolStripLabel
			// 
			this.Thickness_toolStripLabel.Name = "Thickness_toolStripLabel";
			this.Thickness_toolStripLabel.Size = new System.Drawing.Size(111, 15);
			this.Thickness_toolStripLabel.Text = "Thickness";
			// 
			// numericUpDown1
			// 
			this.numericUpDown1.Location = new System.Drawing.Point(37, 281);
			this.numericUpDown1.Name = "numericUpDown1";
			this.numericUpDown1.Size = new System.Drawing.Size(50, 20);
			this.numericUpDown1.TabIndex = 2;
			this.numericUpDown1.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(118, 13);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(203, 13);
			this.label1.TabIndex = 3;
			this.label1.Text = "it didnt managet to create succesfull paint";
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(456, 450);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.numericUpDown1);
			this.Controls.Add(this.toolStrip1);
			this.Name = "Form1";
			this.Text = "Form1";
			this.Paint += new System.Windows.Forms.PaintEventHandler(this.Form1_Paint);
			this.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseClick);
			this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseDown);
			this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseUp);
			this.toolStrip1.ResumeLayout(false);
			this.toolStrip1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton Point_toolStripButton;
        private System.Windows.Forms.ToolStripButton RectFrame_toolStripButton;
        private System.Windows.Forms.ToolStripButton Elips_toolStripButton;
        private System.Windows.Forms.ToolStripLabel Color_toolStripLabel;
        private System.Windows.Forms.ToolStripComboBox toolStripComboBox1;
        private System.Windows.Forms.ToolStripLabel Thickness_toolStripLabel;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
		private System.Windows.Forms.ToolStripButton FilledRecttoolStripButton;
        private System.Windows.Forms.ToolStripButton toolStripButton_Line;
		private System.Windows.Forms.ToolStripButton toolStripButton_Image;
		private System.Windows.Forms.ToolStripButton toolStripButton_Clear;
        private System.Windows.Forms.ToolStripButton toolStripButton_Save;
		private System.Windows.Forms.Label label1;
	}
}

