﻿using System.Drawing;

namespace GDI_Intro
{
    public class MyLine
    {
        public int S_x { get; set; }
        public int F_x { get; set; }
        public int S_y { get; set; }
        public int F_y { get; set; }
		public Color LineColor { get; set; }
    }
}
