﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace GDI_Intro
{

    public partial class Form1 : Form
    {
        public SolidBrush Brush { get; private set; }

        public List<Point> Points = new List<Point>();
        public List<Rectangle> Rectangles = new List<Rectangle>();
		public List<Rectangle> FrameRectangles = new List<Rectangle>();
		public List<Rectangle> Circles = new List<Rectangle>();

		public MyLine Line { get; set; }
        public List<MyLine> Lines = new List<MyLine>();

	
	

        public int Down_X { get; set; }
        public int Down_Y { get; set; }

        public int Up_X { get; set; }
        public int Up_Y { get; set; }

		public int X_Tmp { get; set; }
		public int Y_Tmp { get; set; }

		public Pen Pen { get; set; }

		private void ColorSeter()
        {
            toolStripComboBox1.Items.AddRange(new string[]
            { "Black", "Red", "Yellow", "Green", "Blue" });
        }

        public Form1()
        {
            InitializeComponent();
         
            numericUpDown1.Value = 1;
            Graphics graphics = this.CreateGraphics();
            Brush = new SolidBrush(Color.Black);
			ColorSeter();
        }

		private void StateController(object sender, EventArgs e)
		{
			if ((sender as ToolStripButton).CheckState == CheckState.Unchecked)
				(sender as ToolStripButton).CheckState = CheckState.Checked;
			else (sender as ToolStripButton).CheckState = CheckState.Unchecked;
		}

        private void FilledRecttoolStripButton_Click(object sender, EventArgs e)
        {		
			StateController(sender, e);
        }	

        private void RectFrame_toolStripButton_Click(object sender, EventArgs e)
        {			
			StateController(sender, e);
		}

        private void Point_toolStripButton_Click(object sender, EventArgs e)
        {			
			StateController(sender, e);
		}

        private void toolStripButton_Line_Click(object sender, EventArgs e)
        {			
			StateController(sender, e);
		}

		private void RectFrame_toolStripButton_Click_1(object sender, EventArgs e)
		{
			StateController(sender, e);
		}

		private void Elips_toolStripButton_Click(object sender, EventArgs e)
		{
			StateController(sender, e);
		}

		private void toolStripComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (toolStripComboBox1.SelectedIndex)
            {
                case 0:
                    Brush.Color = Color.Black;
                    break;
                case 1:
                    Brush.Color = Color.Red;
                    break;
                case 2:
                    Brush.Color = Color.Yellow;
                    break;
                case 3:
                    Brush.Color = Color.Green;
                    break;
                case 4:
                    Brush.Color = Color.Blue;
                    break;
                default:
                    break;
            }
        }
		
        private void Form1_MouseClick(object sender, MouseEventArgs e)
        {
            if (Point_toolStripButton.CheckState == CheckState.Checked)
            {
                Points.Add(new Point(e.X, e.Y));
                PrintFigure(this.CreateGraphics());
            }
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            PrintFigure(e.Graphics);
        }

        private void Form1_MouseDown(object sender, MouseEventArgs e)
        {
			X_Tmp = e.X;
			Y_Tmp = e.Y;

			Down_X = e.X;
			Down_Y = e.Y;

			Line = new MyLine();
			Line.S_x = e.X;
            Line.S_y = e.Y;
        }

        private void Form1_MouseUp(object sender, MouseEventArgs e)
        {
			Point p = new Point();

			Up_X = e.X;
			Up_Y = e.Y;

			int d_x = Down_X;
			int d_y = Down_Y;

			int up_x = Up_X;
			int up_y = Up_Y;

			int hei = 0;
			int wid = 0;


			if (FilledRecttoolStripButton.Checked == true ||
				RectFrame_toolStripButton.Checked == true ||
				Elips_toolStripButton.Checked == true)
            {		
				if (d_x < up_x && d_y < up_y)
				{
					hei = up_y - d_y;
					wid = up_x - d_x;
					p = new Point(d_x, d_y);
				}
				else if (d_x < up_x && d_y > up_y)
				{
					hei = d_y - up_y;
					wid = up_x - d_x;

					X_Tmp = d_x;
					Y_Tmp = up_y;

					p = new Point(X_Tmp, Y_Tmp);
				}
				else if (d_x > up_x && d_y > up_y)
				{
					hei = d_y - up_y;
					wid = d_x - up_x;

					X_Tmp = up_x;
					Y_Tmp = up_y;

					p = new Point(X_Tmp, Y_Tmp);
				}
				else if (d_x > up_x && d_y < up_y)
				{
					hei = up_y - d_y;
					wid = d_x - up_x;

					X_Tmp = up_x;
					Y_Tmp = d_y;
					p = new Point(X_Tmp, Y_Tmp);
				}				

				Size sz = new Size(wid, hei);
				Rectangle r = new Rectangle(p, sz);

				if (FilledRecttoolStripButton.Checked == true)
				{
					this.CreateGraphics().FillRectangle(new SolidBrush(Brush.Color), r);
					Rectangles.Add(r);
				}
				else if (RectFrame_toolStripButton.Checked == true)
				{
					int width = (int)numericUpDown1.Value;
					Pen = new Pen(Brush.Color, (int)width);
					this.CreateGraphics().DrawRectangle(Pen, r);
					FrameRectangles.Add(r);
				}
				else if (Elips_toolStripButton.Checked == true)
				{
					int width = (int)numericUpDown1.Value;
					Pen = new Pen(Brush.Color, (int)width);
					this.CreateGraphics().DrawEllipse(Pen, r);
					Circles.Add(r);
				}
            }

			if (toolStripButton_Image.Checked == true)
			{
				hei = up_y - d_y;
				wid = up_x - d_x;
				p = new Point(d_x, d_y);
				Size sz = new Size(wid, hei);
				Rectangle r = new Rectangle(p, sz);

				OpenFileDialog open = new OpenFileDialog();
				open.Filter = "Image File (*.jpg; *.jpeg; *.png; *.ico)| *.jpg; *.jpeg; *.png; *.ico";
				if (open.ShowDialog() == DialogResult.OK)
				{
					Image img = new Bitmap(open.FileName);
					this.CreateGraphics().DrawImage(img, r);
				}				
			}           
         
            if (toolStripButton_Line.Checked == true)
            {
                int width = (int)numericUpDown1.Value;

                Line.F_x = e.X;
                Line.F_y = e.Y;

                Pen = new Pen(Brush.Color, (int)width);
                this.CreateGraphics().DrawLine(Pen, Line.S_x, Line.S_y, Line.F_x, Line.F_y);
				Line.LineColor = this.Brush.Color;

				Lines.Add(Line);
            }
        }

        private void PrintFigure(Graphics g)
        {          
			int width = (int)numericUpDown1.Value;
			int height = (int)numericUpDown1.Value;               
			foreach (Point point in Points)
			{
			    g.FillEllipse(this.Brush, point.X, point.Y, width, height);
			}  		
			
			foreach (Rectangle item in Rectangles)
			{
			    g.FillRectangle(this.Brush, item);					
			}

			foreach (Rectangle item in FrameRectangles)
			{
				g.DrawRectangle(Pen, item);
			}

			foreach (Rectangle item in Circles)
			{
				g.DrawEllipse(Pen, item);
			}

			foreach (MyLine line in Lines)
			{
				g.DrawLine(Pen, line.S_x, line.S_y, line.F_x, line.F_y);
			}           
        }

		private void toolStripButton_Clear_Click(object sender, EventArgs e)
		{
			Graphics g = this.CreateGraphics();
			g.Clear(this.BackColor);
		}

		private void toolStripButton_Image_Click(object sender, EventArgs e)
		{
			if ((sender as ToolStripButton).CheckState == CheckState.Unchecked)
			{
				(sender as ToolStripButton).CheckState = CheckState.Checked;
				MessageBox.Show("select space for image");
			}			
			else (sender as ToolStripButton).CheckState = CheckState.Unchecked;
		}

        private void toolStripButton_Save_Click(object sender, EventArgs e)
        {
            if ((sender as ToolStripButton).CheckState == CheckState.Unchecked)
            {
                (sender as ToolStripButton).CheckState = CheckState.Checked;
                // MessageBox.Show("select space for saving");
            }
            else (sender as ToolStripButton).CheckState = CheckState.Unchecked;

            SaveFileDialog save = new SaveFileDialog();
            save.DefaultExt = ".png";
            if (save.ShowDialog() == DialogResult.OK)
            {
                using (var bmpTemp = new Bitmap(this.ClientSize.Width, this.ClientSize.Height))
                {
                    this.DrawToBitmap(bmpTemp, this.ClientRectangle);
                    bmpTemp.Save(save.FileName, System.Drawing.Imaging.ImageFormat.Png);
                    //bmpTemp.Save(@"D:\test\form_screen.png", System.Drawing.Imaging.ImageFormat.Png);
                }
            }
        }
    }
}
