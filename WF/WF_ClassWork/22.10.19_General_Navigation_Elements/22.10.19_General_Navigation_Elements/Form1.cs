﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows.Forms;

namespace _22._10._19_General_Navigation_Elements
{
    public partial class Form1 : Form
    {
        public Student Student;

        public Form1()
        {
            InitializeComponent();
            Male.Checked = true;           
            dateTimePicker1.MaxDate = DateTime.Parse("01.01.2020");            
        }

		public void StudentCreating()
		{
			Student = new Student();

			Student.Name = textBox1.Text;
			Student.Surname = textBox2.Text;
			Student.Country = textBox3.Text;
			Student.City = textBox4.Text;
			Student.Gender = Male.Text;
			Student.BirthDate = dateTimePicker1.Value;
			Student.PrLang = new System.Collections.Generic.List<string>();

			foreach (var item in groupBox1.Controls)
			{
				if (((CheckBox)item).Checked)
					Student.PrLang.Add((item as CheckBox).Text);
			}			
		}

        private void button_Show_Click(object sender, MouseEventArgs e)
        {
			StudentCreating();
			MessageBox.Show($"{Student.Name}\n{Student.Surname}\n" +
				  $"{Student.Country}\n{Student.City}\n{Student.Gender}\n" +
				  $"{Student.BirthDate}\n");
			foreach (var item in Student.PrLang)
			{
				MessageBox.Show(item.ToString());
			}
		}

        private void button_SAVE_Click(object sender, EventArgs e)
        {
			StudentCreating();

			BinaryFormatter formatter = new BinaryFormatter();
            using (FileStream fs = new FileStream("student.dat", FileMode.OpenOrCreate))
            {
                formatter.Serialize(fs, Student);
                MessageBox.Show("stud serialised");
            }
        }

        private void button_LOAD_Click(object sender, EventArgs e)
        {
            BinaryFormatter formatter = new BinaryFormatter();
            Student = null;
            using (Stream fs = File.OpenRead("student.dat"))
            {
                Student = (Student)formatter.Deserialize(fs);
            }

			MessageBox.Show($"{Student.Name}\n{Student.Surname}\n" +
				  $"{Student.Country}\n{Student.City}\n{Student.Gender}\n" +
				  $"{Student.BirthDate}\n");
			foreach (var item in Student.PrLang)
			{
				MessageBox.Show(item.ToString());
			}
		}
    }
}
