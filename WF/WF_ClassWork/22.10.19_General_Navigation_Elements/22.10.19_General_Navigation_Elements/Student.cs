﻿using System;
using System.Collections.Generic;


namespace _22._10._19_General_Navigation_Elements
{
    [Serializable]
    public class Student
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string Gender { get; set; }
        public DateTime BirthDate { get; set; }

        // public string PrLang { get; set; }

        public List<string> PrLang { get; set; }


        public Student()
        {           
        }


    }
}
