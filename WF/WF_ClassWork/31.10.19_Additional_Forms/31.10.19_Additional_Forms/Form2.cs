﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _31._10._19_Additional_Forms
{
    public partial class Form2 : Form
    {
	    public string NewName { set { textBox1_Name.Text = value; } }
		public decimal NewPrice { set { numericUpDown1_Set_Price.Value = value; } }
		public int NewQuantity { set => numericUpDown2_Set_Quant.Value = value; }
		public string NewCountry { set => comboBox1.Text = value; }
		public decimal NewDiscount { set => numericUpDown3_Set_Discount.Value = value; }

		public Product NewProduct { get; private set; }

		public Form2()
        {
            InitializeComponent();
			SetCountriesList();
		}

		private void SetCountriesList()
		{
			comboBox1.Items.AddRange(new string[] {"n/a", "China", "India", "United States",
			"Brazil", "Germany", "France", "Ukraine" });		
		}

		// if(dialogResult == OK) - for checking what was in puted in onother form

		private void SetDefaultControlValues()
		{
			textBox1_Name.Text = "";
			numericUpDown1_Set_Price.Value = 0;
			numericUpDown2_Set_Quant.Value = 0;
			comboBox1.SelectedIndex = 0;
			numericUpDown3_Set_Discount.Value = 0;
		}

        private Product CreateNewProduct()
        {
			Product newProd = new Product
			{
				ProdName = textBox1_Name.Text,
				Price = numericUpDown1_Set_Price.Value,
				Quantity = (int)numericUpDown2_Set_Quant.Value,
				Country = comboBox1.Items[comboBox1.SelectedIndex].ToString(),
				//newProd.Country = comboBox1.SelectedIndex.ToString();
				Discount = numericUpDown3_Set_Discount.Value
			};

			return newProd;
        }

        private void button1_Ok_Click(object sender, EventArgs e)
        {
            NewProduct = CreateNewProduct();
            this.DialogResult = DialogResult.OK;
			SetDefaultControlValues();
		}

		private void button1_Cancel_Click(object sender, EventArgs e)
		{
			this.Close();
			SetDefaultControlValues();
		}

	
	}
}
