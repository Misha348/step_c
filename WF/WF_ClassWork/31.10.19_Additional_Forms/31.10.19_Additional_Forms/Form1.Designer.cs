﻿namespace _31._10._19_Additional_Forms
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.label1_Title = new System.Windows.Forms.Label();
			this.listBox1 = new System.Windows.Forms.ListBox();
			this.button1_Edit = new System.Windows.Forms.Button();
			this.button1_Info = new System.Windows.Forms.Button();
			this.button1_Add_Prod = new System.Windows.Forms.Button();
			this.button1_Remove_Prod = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// label1_Title
			// 
			this.label1_Title.AutoSize = true;
			this.label1_Title.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label1_Title.Location = new System.Drawing.Point(104, 29);
			this.label1_Title.Name = "label1_Title";
			this.label1_Title.Size = new System.Drawing.Size(121, 24);
			this.label1_Title.TabIndex = 0;
			this.label1_Title.Text = "PRODUCTS";
			// 
			// listBox1
			// 
			this.listBox1.FormattingEnabled = true;
			this.listBox1.Location = new System.Drawing.Point(23, 66);
			this.listBox1.Name = "listBox1";
			this.listBox1.Size = new System.Drawing.Size(290, 251);
			this.listBox1.TabIndex = 1;
			// 
			// button1_Edit
			// 
			this.button1_Edit.Location = new System.Drawing.Point(23, 336);
			this.button1_Edit.Name = "button1_Edit";
			this.button1_Edit.Size = new System.Drawing.Size(54, 23);
			this.button1_Edit.TabIndex = 2;
			this.button1_Edit.Text = "EDIT";
			this.button1_Edit.UseVisualStyleBackColor = true;
			this.button1_Edit.Click += new System.EventHandler(this.button1_Edit_Click);
			// 
			// button1_Info
			// 
			this.button1_Info.Location = new System.Drawing.Point(83, 336);
			this.button1_Info.Name = "button1_Info";
			this.button1_Info.Size = new System.Drawing.Size(58, 23);
			this.button1_Info.TabIndex = 3;
			this.button1_Info.Text = "INFO";
			this.button1_Info.UseVisualStyleBackColor = true;
			this.button1_Info.Click += new System.EventHandler(this.button1_Info_Click);
			// 
			// button1_Add_Prod
			// 
			this.button1_Add_Prod.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.button1_Add_Prod.Location = new System.Drawing.Point(199, 337);
			this.button1_Add_Prod.Name = "button1_Add_Prod";
			this.button1_Add_Prod.Size = new System.Drawing.Size(54, 23);
			this.button1_Add_Prod.TabIndex = 4;
			this.button1_Add_Prod.Text = "+";
			this.button1_Add_Prod.UseVisualStyleBackColor = true;
			this.button1_Add_Prod.Click += new System.EventHandler(this.button1_Add_Prod_Click);
			// 
			// button1_Remove_Prod
			// 
			this.button1_Remove_Prod.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.button1_Remove_Prod.Location = new System.Drawing.Point(259, 337);
			this.button1_Remove_Prod.Name = "button1_Remove_Prod";
			this.button1_Remove_Prod.Size = new System.Drawing.Size(54, 23);
			this.button1_Remove_Prod.TabIndex = 5;
			this.button1_Remove_Prod.Text = "-";
			this.button1_Remove_Prod.UseVisualStyleBackColor = true;
			this.button1_Remove_Prod.Click += new System.EventHandler(this.button1_Remove_Prod_Click);
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(339, 438);
			this.Controls.Add(this.button1_Remove_Prod);
			this.Controls.Add(this.button1_Add_Prod);
			this.Controls.Add(this.button1_Info);
			this.Controls.Add(this.button1_Edit);
			this.Controls.Add(this.listBox1);
			this.Controls.Add(this.label1_Title);
			this.Name = "Form1";
			this.Text = "Form1";
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1_Title;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Button button1_Edit;
        private System.Windows.Forms.Button button1_Info;
        private System.Windows.Forms.Button button1_Add_Prod;
        private System.Windows.Forms.Button button1_Remove_Prod;
    }
}

