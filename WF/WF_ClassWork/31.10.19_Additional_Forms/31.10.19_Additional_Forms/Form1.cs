﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _31._10._19_Additional_Forms
{
    public partial class Form1 : Form
    {
		protected List<Product> Products { get; set; }
		Form2 Form_2;

		public Form1()
        {
            InitializeComponent();
			Form_2 = new Form2();
			Products = new List<Product>();
		}    

        private void button1_Add_Prod_Click(object sender, EventArgs e)
        {          
            if (Form_2.ShowDialog() == DialogResult.OK)            
				Products.Add(Form_2.NewProduct);		

			listBox1.DataSource = null;
			listBox1.DataSource = Products;
			listBox1.DisplayMember = nameof(Form_2.NewProduct.DisplayShortProductData);
        }

		private void button1_Remove_Prod_Click(object sender, EventArgs e)
		{
			Product prod = (Product)listBox1.SelectedItem;
			Products.Remove(prod);

			listBox1.DataSource = null;
			listBox1.DataSource = Products;
			listBox1.DisplayMember = nameof(Form_2.NewProduct.DisplayShortProductData);
		}

		private void button1_Info_Click(object sender, EventArgs e)
		{
			if (listBox1.Items.Count == 0)
				MessageBox.Show("no any product yet");
			else
			{
				Product prod = (Product)listBox1.SelectedItem;
				Form_2.NewName = prod.ProdName;
				Form_2.NewPrice = prod.Price;
				Form_2.NewQuantity = prod.Quantity;
				Form_2.NewCountry = prod.Country;
				Form_2.NewDiscount = prod.Discount;

				Form_2.ShowDialog();
			}
			
		}

		private void button1_Edit_Click(object sender, EventArgs e)
		{
			if (listBox1.Items.Count == 0)
				MessageBox.Show("no any product yet");
			else
			{
				Product prod = (Product)listBox1.SelectedItem;
				int index = Products.IndexOf(prod);

				Form_2.NewName = prod.ProdName;
				Form_2.NewPrice = prod.Price;
				Form_2.NewQuantity = prod.Quantity;
				Form_2.NewCountry = prod.Country;
				Form_2.NewDiscount = prod.Discount;				

				Products.Remove(prod);

				if (Form_2.ShowDialog() == DialogResult.OK)
					Products.Insert(index, Form_2.NewProduct);

				listBox1.DataSource = null;
				listBox1.DataSource = Products;
				listBox1.DisplayMember = nameof(Form_2.NewProduct.DisplayShortProductData);
			}
			
		}
	}
}
