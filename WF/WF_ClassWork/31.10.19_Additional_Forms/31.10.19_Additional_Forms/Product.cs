﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _31._10._19_Additional_Forms
{
    public class Product
    {
        public string ProdName { get; set; }
        public decimal Price { get; set; }
        public int Quantity { get; set; }
        public string Country { get; set; }
        public decimal Discount { get; set; }

        public Product() { }

		public string DisplayShortProductData
		{
			get
			{
				return $"name: {ProdName.ToString()}  price: {Price}  quant: {Quantity}";
			}
		}
    }
}
