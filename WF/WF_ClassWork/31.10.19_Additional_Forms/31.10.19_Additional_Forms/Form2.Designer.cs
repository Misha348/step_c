﻿namespace _31._10._19_Additional_Forms
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.label1_INFO = new System.Windows.Forms.Label();
			this.label1_Name = new System.Windows.Forms.Label();
			this.label1_Price = new System.Windows.Forms.Label();
			this.label1_Country = new System.Windows.Forms.Label();
			this.label1_Quantity = new System.Windows.Forms.Label();
			this.label1_Discount = new System.Windows.Forms.Label();
			this.button1_Cancel = new System.Windows.Forms.Button();
			this.button1_Ok = new System.Windows.Forms.Button();
			this.textBox1_Name = new System.Windows.Forms.TextBox();
			this.numericUpDown1_Set_Price = new System.Windows.Forms.NumericUpDown();
			this.numericUpDown2_Set_Quant = new System.Windows.Forms.NumericUpDown();
			this.comboBox1 = new System.Windows.Forms.ComboBox();
			this.numericUpDown3_Set_Discount = new System.Windows.Forms.NumericUpDown();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown1_Set_Price)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown2_Set_Quant)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown3_Set_Discount)).BeginInit();
			this.SuspendLayout();
			// 
			// label1_INFO
			// 
			this.label1_INFO.AutoSize = true;
			this.label1_INFO.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label1_INFO.Location = new System.Drawing.Point(136, 24);
			this.label1_INFO.Name = "label1_INFO";
			this.label1_INFO.Size = new System.Drawing.Size(51, 20);
			this.label1_INFO.TabIndex = 0;
			this.label1_INFO.Text = "INFO";
			// 
			// label1_Name
			// 
			this.label1_Name.AutoSize = true;
			this.label1_Name.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label1_Name.Location = new System.Drawing.Point(23, 68);
			this.label1_Name.Name = "label1_Name";
			this.label1_Name.Size = new System.Drawing.Size(55, 20);
			this.label1_Name.TabIndex = 1;
			this.label1_Name.Text = "Name:";
			// 
			// label1_Price
			// 
			this.label1_Price.AutoSize = true;
			this.label1_Price.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label1_Price.Location = new System.Drawing.Point(23, 107);
			this.label1_Price.Name = "label1_Price";
			this.label1_Price.Size = new System.Drawing.Size(48, 20);
			this.label1_Price.TabIndex = 2;
			this.label1_Price.Text = "Price:";
			// 
			// label1_Country
			// 
			this.label1_Country.AutoSize = true;
			this.label1_Country.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label1_Country.Location = new System.Drawing.Point(23, 189);
			this.label1_Country.Name = "label1_Country";
			this.label1_Country.Size = new System.Drawing.Size(68, 20);
			this.label1_Country.TabIndex = 3;
			this.label1_Country.Text = "Country:";
			// 
			// label1_Quantity
			// 
			this.label1_Quantity.AutoSize = true;
			this.label1_Quantity.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label1_Quantity.Location = new System.Drawing.Point(23, 147);
			this.label1_Quantity.Name = "label1_Quantity";
			this.label1_Quantity.Size = new System.Drawing.Size(72, 20);
			this.label1_Quantity.TabIndex = 4;
			this.label1_Quantity.Text = "Quantity:";
			// 
			// label1_Discount
			// 
			this.label1_Discount.AutoSize = true;
			this.label1_Discount.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label1_Discount.Location = new System.Drawing.Point(27, 237);
			this.label1_Discount.Name = "label1_Discount";
			this.label1_Discount.Size = new System.Drawing.Size(76, 20);
			this.label1_Discount.TabIndex = 5;
			this.label1_Discount.Text = "Discount:";
			// 
			// button1_Cancel
			// 
			this.button1_Cancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.button1_Cancel.Location = new System.Drawing.Point(31, 334);
			this.button1_Cancel.Name = "button1_Cancel";
			this.button1_Cancel.Size = new System.Drawing.Size(75, 23);
			this.button1_Cancel.TabIndex = 6;
			this.button1_Cancel.Text = "CANCEL";
			this.button1_Cancel.UseVisualStyleBackColor = true;
			this.button1_Cancel.Click += new System.EventHandler(this.button1_Cancel_Click);
			// 
			// button1_Ok
			// 
			this.button1_Ok.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.button1_Ok.Location = new System.Drawing.Point(212, 333);
			this.button1_Ok.Name = "button1_Ok";
			this.button1_Ok.Size = new System.Drawing.Size(75, 23);
			this.button1_Ok.TabIndex = 7;
			this.button1_Ok.Text = "OK";
			this.button1_Ok.UseVisualStyleBackColor = true;
			this.button1_Ok.Click += new System.EventHandler(this.button1_Ok_Click);
			// 
			// textBox1_Name
			// 
			this.textBox1_Name.Location = new System.Drawing.Point(110, 67);
			this.textBox1_Name.Name = "textBox1_Name";
			this.textBox1_Name.Size = new System.Drawing.Size(177, 20);
			this.textBox1_Name.TabIndex = 8;
			// 
			// numericUpDown1_Set_Price
			// 
			this.numericUpDown1_Set_Price.Location = new System.Drawing.Point(110, 106);
			this.numericUpDown1_Set_Price.Name = "numericUpDown1_Set_Price";
			this.numericUpDown1_Set_Price.Size = new System.Drawing.Size(177, 20);
			this.numericUpDown1_Set_Price.TabIndex = 9;
			// 
			// numericUpDown2_Set_Quant
			// 
			this.numericUpDown2_Set_Quant.Location = new System.Drawing.Point(110, 146);
			this.numericUpDown2_Set_Quant.Name = "numericUpDown2_Set_Quant";
			this.numericUpDown2_Set_Quant.Size = new System.Drawing.Size(177, 20);
			this.numericUpDown2_Set_Quant.TabIndex = 10;
			// 
			// comboBox1
			// 
			this.comboBox1.FormattingEnabled = true;
			this.comboBox1.Location = new System.Drawing.Point(110, 187);
			this.comboBox1.Name = "comboBox1";
			this.comboBox1.Size = new System.Drawing.Size(177, 21);
			this.comboBox1.TabIndex = 11;
			// 
			// numericUpDown3_Set_Discount
			// 
			this.numericUpDown3_Set_Discount.Location = new System.Drawing.Point(110, 236);
			this.numericUpDown3_Set_Discount.Name = "numericUpDown3_Set_Discount";
			this.numericUpDown3_Set_Discount.Size = new System.Drawing.Size(177, 20);
			this.numericUpDown3_Set_Discount.TabIndex = 12;
			// 
			// Form2
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(323, 436);
			this.Controls.Add(this.numericUpDown3_Set_Discount);
			this.Controls.Add(this.comboBox1);
			this.Controls.Add(this.numericUpDown2_Set_Quant);
			this.Controls.Add(this.numericUpDown1_Set_Price);
			this.Controls.Add(this.textBox1_Name);
			this.Controls.Add(this.button1_Ok);
			this.Controls.Add(this.button1_Cancel);
			this.Controls.Add(this.label1_Discount);
			this.Controls.Add(this.label1_Quantity);
			this.Controls.Add(this.label1_Country);
			this.Controls.Add(this.label1_Price);
			this.Controls.Add(this.label1_Name);
			this.Controls.Add(this.label1_INFO);
			this.Name = "Form2";
			this.Text = "Form2";
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown1_Set_Price)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown2_Set_Quant)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown3_Set_Discount)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1_INFO;
        private System.Windows.Forms.Label label1_Name;
        private System.Windows.Forms.Label label1_Price;
        private System.Windows.Forms.Label label1_Country;
        private System.Windows.Forms.Label label1_Quantity;
        private System.Windows.Forms.Label label1_Discount;
        private System.Windows.Forms.Button button1_Cancel;
        private System.Windows.Forms.Button button1_Ok;
        private System.Windows.Forms.TextBox textBox1_Name;
        private System.Windows.Forms.NumericUpDown numericUpDown1_Set_Price;
        private System.Windows.Forms.NumericUpDown numericUpDown2_Set_Quant;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.NumericUpDown numericUpDown3_Set_Discount;
    }
}