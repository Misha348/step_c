﻿namespace _23._10._19_Gen_Navigat_2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.radioButton1_Year = new System.Windows.Forms.RadioButton();
            this.radioButton2_Month = new System.Windows.Forms.RadioButton();
            this.radioButton3_days = new System.Windows.Forms.RadioButton();
            this.radioButton4_Hours = new System.Windows.Forms.RadioButton();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox_Time_scales = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox_Time_scales.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(27, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(66, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "choose date";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(115, 36);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(194, 20);
            this.dateTimePicker1.TabIndex = 1;
            // 
            // radioButton1_Year
            // 
            this.radioButton1_Year.AutoSize = true;
            this.radioButton1_Year.Location = new System.Drawing.Point(6, 19);
            this.radioButton1_Year.Name = "radioButton1_Year";
            this.radioButton1_Year.Size = new System.Drawing.Size(52, 17);
            this.radioButton1_Year.TabIndex = 2;
            this.radioButton1_Year.TabStop = true;
            this.radioButton1_Year.Text = "Years";
            this.radioButton1_Year.UseVisualStyleBackColor = true;
            this.radioButton1_Year.CheckedChanged += new System.EventHandler(this.radioButton1_Year_CheckedChanged);
            // 
            // radioButton2_Month
            // 
            this.radioButton2_Month.AutoSize = true;
            this.radioButton2_Month.Location = new System.Drawing.Point(64, 19);
            this.radioButton2_Month.Name = "radioButton2_Month";
            this.radioButton2_Month.Size = new System.Drawing.Size(55, 17);
            this.radioButton2_Month.TabIndex = 3;
            this.radioButton2_Month.TabStop = true;
            this.radioButton2_Month.Text = "Month";
            this.radioButton2_Month.UseVisualStyleBackColor = true;
            this.radioButton2_Month.CheckedChanged += new System.EventHandler(this.radioButton2_Month_CheckedChanged);
            // 
            // radioButton3_days
            // 
            this.radioButton3_days.AutoSize = true;
            this.radioButton3_days.Location = new System.Drawing.Point(125, 19);
            this.radioButton3_days.Name = "radioButton3_days";
            this.radioButton3_days.Size = new System.Drawing.Size(49, 17);
            this.radioButton3_days.TabIndex = 4;
            this.radioButton3_days.TabStop = true;
            this.radioButton3_days.Text = "Days";
            this.radioButton3_days.UseVisualStyleBackColor = true;
            this.radioButton3_days.CheckedChanged += new System.EventHandler(this.radioButton3_days_CheckedChanged);
            // 
            // radioButton4_Hours
            // 
            this.radioButton4_Hours.AutoSize = true;
            this.radioButton4_Hours.Location = new System.Drawing.Point(180, 19);
            this.radioButton4_Hours.Name = "radioButton4_Hours";
            this.radioButton4_Hours.Size = new System.Drawing.Size(53, 17);
            this.radioButton4_Hours.TabIndex = 5;
            this.radioButton4_Hours.TabStop = true;
            this.radioButton4_Hours.Text = "Hours";
            this.radioButton4_Hours.UseVisualStyleBackColor = true;
            this.radioButton4_Hours.CheckedChanged += new System.EventHandler(this.radioButton4_Hours_CheckedChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(144, 153);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(0, 13);
            this.label3.TabIndex = 7;
            // 
            // groupBox_Time_scales
            // 
            this.groupBox_Time_scales.Controls.Add(this.radioButton1_Year);
            this.groupBox_Time_scales.Controls.Add(this.radioButton2_Month);
            this.groupBox_Time_scales.Controls.Add(this.radioButton3_days);
            this.groupBox_Time_scales.Controls.Add(this.radioButton4_Hours);
            this.groupBox_Time_scales.Location = new System.Drawing.Point(33, 71);
            this.groupBox_Time_scales.Name = "groupBox_Time_scales";
            this.groupBox_Time_scales.Size = new System.Drawing.Size(276, 57);
            this.groupBox_Time_scales.TabIndex = 8;
            this.groupBox_Time_scales.TabStop = false;
            this.groupBox_Time_scales.Text = "Time scales";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(33, 143);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 9;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(347, 503);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.groupBox_Time_scales);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.groupBox_Time_scales.ResumeLayout(false);
            this.groupBox_Time_scales.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.RadioButton radioButton1_Year;
        private System.Windows.Forms.RadioButton radioButton2_Month;
        private System.Windows.Forms.RadioButton radioButton3_days;
        private System.Windows.Forms.RadioButton radioButton4_Hours;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox_Time_scales;
        private System.Windows.Forms.Button button1;
    }
}

