﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _23._10._19_Gen_Navigat_2
{
    public partial class Form1 : Form
    {
        public DateTime EnteredDate {get; set;}
        public TimeSpan ExpectationTime { get; set; }
        public Form1()
        {
            InitializeComponent();
            dateTimePicker1.MinDate = DateTime.Now;
            //EnteredDate = dateTimePicker1.Value;
        }

        //public void SetExpectationTime()
        //{
        //    foreach (var item in groupBox_Time_scales.Controls)
        //    {
        //        if (((RadioButton)item).Checked)
        //        {
        //        }
        //    }
        //}

        private void button1_Click(object sender, EventArgs e)
        {
            label3.Text = ExpectationTime.ToString();
        }

        private void radioButton1_Year_CheckedChanged(object sender, EventArgs e)
        {
            double t = 88.89687658;
            Math.Round(t, 3);
            ExpectationTime = EnteredDate.Subtract(DateTime.Now);
            //MessageBox.Show(radioButton1_Year.Text);
           
        }

        private void radioButton2_Month_CheckedChanged(object sender, EventArgs e)
        {
            ExpectationTime = EnteredDate.Subtract(DateTime.Now);
            //MessageBox.Show(ExpectationTime.);
        }

        private void radioButton3_days_CheckedChanged(object sender, EventArgs e)
        {
            EnteredDate = dateTimePicker1.Value;
            ExpectationTime = EnteredDate - DateTime.Now;
            MessageBox.Show(ExpectationTime.TotalDays.ToString());

        }

        private void radioButton4_Hours_CheckedChanged(object sender, EventArgs e)
        {
            EnteredDate = dateTimePicker1.Value;
            ExpectationTime = EnteredDate - DateTime.Now;
            MessageBox.Show(ExpectationTime.TotalHours.ToString());

        }
    }
}
