﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _07._11._19_ListView
{
    public partial class Form1 : Form
    {
        ListView table;
       

        public void ComboBoxItemsSeter()
        {
            toolStripComboBox1.Items.AddRange(new string[] {"Large", "Small", "Detail", "Tile", "List"});           
        }

        public Form1()
        {
            InitializeComponent();

            table = new ListView();
            table.SetBounds(15, 40, 450, 400);
            this.Controls.Add(table);
            table.View = View.Details;

            table.Columns.Add("NAME");
            table.Columns[0].Width = 125;
            table.Columns.Add("SIZE");
            table.Columns[1].Width = 125;
            table.Columns.Add("LAST EDIT");
            table.Columns[2].Width = 200;

            ComboBoxItemsSeter();
            toolStripComboBox1.SelectedIndex = 0;

        }

        private void toolStripComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (toolStripComboBox1.SelectedIndex)
            {
                case 0:
                    this.table.View = View.LargeIcon;
                    break;
                case 1:
                    this.table.View = View.SmallIcon;
                    break;
                case 2:
                    this.table.View = View.Details;
                    break;
                case 3:
                    this.table.View = View.Tile;
                    break;
                default:
                    break;
            }
        }



        private void Browse_toolStripButton1_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog();

            if (folderBrowserDialog.ShowDialog() == DialogResult.OK)
            {
                DirectoryInfo dir = new DirectoryInfo(folderBrowserDialog.SelectedPath);

                foreach (var file in dir.GetFiles())
                {
                    ListViewItem listItem = new ListViewItem();

                    listItem.Text = file.Name;
                    listItem.SubItems.Add(file.LastAccessTime.ToLongDateString());
                    listItem.SubItems.Add(file.Length.ToString());
                    table.Items.Add(listItem);
                }
            }           
        }
    }
}
