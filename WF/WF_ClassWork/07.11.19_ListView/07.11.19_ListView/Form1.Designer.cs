﻿namespace _07._11._19_ListView
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.Add_toolStripButton = new System.Windows.Forms.ToolStripButton();
            this.Remove_toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.Details_toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripComboBox1 = new System.Windows.Forms.ToolStripComboBox();
            this.Browse_toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.PathName_toolStripTextBox1 = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Add_toolStripButton,
            this.Remove_toolStripButton1,
            this.Details_toolStripButton1,
            this.toolStripComboBox1,
            this.Browse_toolStripButton1,
            this.PathName_toolStripTextBox1,
            this.toolStripButton2});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(620, 25);
            this.toolStrip1.TabIndex = 7;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // Add_toolStripButton
            // 
            this.Add_toolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.Add_toolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("Add_toolStripButton.Image")));
            this.Add_toolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Add_toolStripButton.Name = "Add_toolStripButton";
            this.Add_toolStripButton.Size = new System.Drawing.Size(23, 22);
            this.Add_toolStripButton.Text = "+";
            // 
            // Remove_toolStripButton1
            // 
            this.Remove_toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.Remove_toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("Remove_toolStripButton1.Image")));
            this.Remove_toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Remove_toolStripButton1.Name = "Remove_toolStripButton1";
            this.Remove_toolStripButton1.Size = new System.Drawing.Size(23, 22);
            this.Remove_toolStripButton1.Text = "Remove";
            // 
            // Details_toolStripButton1
            // 
            this.Details_toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.Details_toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("Details_toolStripButton1.Image")));
            this.Details_toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Details_toolStripButton1.Name = "Details_toolStripButton1";
            this.Details_toolStripButton1.Size = new System.Drawing.Size(23, 22);
            this.Details_toolStripButton1.Text = "Details";
            // 
            // toolStripComboBox1
            // 
            this.toolStripComboBox1.Name = "toolStripComboBox1";
            this.toolStripComboBox1.Size = new System.Drawing.Size(121, 25);
            this.toolStripComboBox1.SelectedIndexChanged += new System.EventHandler(this.toolStripComboBox1_SelectedIndexChanged);
            // 
            // Browse_toolStripButton1
            // 
            this.Browse_toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.Browse_toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("Browse_toolStripButton1.Image")));
            this.Browse_toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Browse_toolStripButton1.Name = "Browse_toolStripButton1";
            this.Browse_toolStripButton1.Size = new System.Drawing.Size(23, 22);
            this.Browse_toolStripButton1.Text = "Browse";
            this.Browse_toolStripButton1.Click += new System.EventHandler(this.Browse_toolStripButton1_Click);
            // 
            // PathName_toolStripTextBox1
            // 
            this.PathName_toolStripTextBox1.Name = "PathName_toolStripTextBox1";
            this.PathName_toolStripTextBox1.Size = new System.Drawing.Size(100, 25);
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton2.Image")));
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton2.Text = "Load_toolStripButton2";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(620, 450);
            this.Controls.Add(this.toolStrip1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton Add_toolStripButton;
        private System.Windows.Forms.ToolStripButton Remove_toolStripButton1;
        private System.Windows.Forms.ToolStripButton Details_toolStripButton1;
        private System.Windows.Forms.ToolStripComboBox toolStripComboBox1;
        private System.Windows.Forms.ToolStripButton Browse_toolStripButton1;
        private System.Windows.Forms.ToolStripTextBox PathName_toolStripTextBox1;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
    }
}

