﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _30._10._19_StatusStrip
{
    public partial class Form1 : Form
    {
        public int SymbCounter { get; set; }
        public int DigitCounter { get; set; }

        public int LetterCounter { get; set; }
        public int WordCounter { get; set; }
		public Dictionary<string, string> Pages { get; private set; }
		//public List<int> PageIndex { get; set; }


		private RichTextBox GetCurrentRichTextBox()
        {
            return (tabControl1.SelectedTab.Controls[0] as RichTextBox);
        }

        public Form1()
        {
            InitializeComponent();
            CreateNewTab("Page_1");		

            SymbCounter = 0;
            DigitCounter = 0;
            LetterCounter = 0;           

            numericUpDown_NumSymb.Minimum = 1;
            numericUpDown_NumSymb.Maximum = decimal.MaxValue;

            toolStripProgressBar1.Minimum = 0;
            toolStripProgressBar1.Value = 0;
            toolStripProgressBar1.Step = 1;
            GetCurrentRichTextBox().MaxLength = 1;
			Pages = new Dictionary<string, string>();	
		}

		private void CreateNewTab(string pageName)
        {
            TabPage newTabPage = new TabPage();
            RichTextBox newRichTextBox = new RichTextBox();

            this.tabControl1.Controls.Add(newTabPage);

            newTabPage.Controls.Add(newRichTextBox);
            newTabPage.Location = new System.Drawing.Point(4, 22);
            newTabPage.Name = "tabPage1";
            newTabPage.Padding = new System.Windows.Forms.Padding(3);
            newTabPage.Size = new System.Drawing.Size(556, 365);
            newTabPage.TabIndex = 0;
            newTabPage.Text = pageName;
            newTabPage.UseVisualStyleBackColor = true;

           newRichTextBox.Location = new System.Drawing.Point(0, 0);
           newRichTextBox.Multiline = true;
           newRichTextBox.Name = "textBox1";
           newRichTextBox.ScrollBars = RichTextBoxScrollBars.Both;
           newRichTextBox.Size = new System.Drawing.Size(556, 365);
           newRichTextBox.TabIndex = 0;
           newRichTextBox.TextChanged += new System.EventHandler(this.textBox1_TextChanged);

           newRichTextBox.ContextMenuStrip = contextMenuStrip1;


			newRichTextBox.AllowDrop = true;
			newRichTextBox.DragDrop += RichTextBox_DragDrop;

            //tabControl1.SelectTab(newTabPage);    !!!! [ Answer ]

			//GetCurrentRichTextBox().AllowDrop = true;
			//GetCurrentRichTextBox().DragDrop += RichTextBox_DragDrop;
		}
	
		private void numericUpDown_NumSymb_ValueChanged(object sender, EventArgs e)
        {
            toolStripStatusLabel3.Text = (numericUpDown_NumSymb.Value).ToString();         
            GetCurrentRichTextBox().MaxLength = (int)numericUpDown_NumSymb.Value;			
			toolStripProgressBar1.Maximum = (int)numericUpDown_NumSymb.Value;  
		}

        private void SymbolChecker(string str)
        {
			DigitCounter = 0;
			LetterCounter = 0;

			char[] arr = null;
			arr = str.ToCharArray();
			SymbCounter = arr.Length;
			for (int i = 0; i < arr.Length; i++)
			{
			    if (Char.IsDigit(arr[i]))
			    {					
					DigitCounter++;									
				}
			    if (Char.IsLetter(arr[i]))
			    {					
					LetterCounter++;									
				}
			}
		}

        private void TextInfoChanges()
        {
            string text = GetCurrentRichTextBox().Text;
            SymbolChecker(text);

            toolStripStatusLabel5.Text = DigitCounter.ToString();
            toolStripStatusLabel7.Text = LetterCounter.ToString();
            toolStripStatusLabel11.Text = SymbCounter.ToString();
            toolStripProgressBar1.Value = SymbCounter;

            char[] separator = { ' ', ',', '.', ':', '!', '(', ')', ';', '\n', '\t', '\r' };
            WordCounter = text.Split(separator, StringSplitOptions.RemoveEmptyEntries).Length;
            toolStripStatusLabel9.Text = WordCounter.ToString();
        }

		private void textBox1_TextChanged(object sender, EventArgs e)
        {
            TextInfoChanges();
        }      

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            TextInfoChanges();
        }

		#region//========TOOLSTRIP MENU=========//

		private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
			string currPageName = tabControl1.SelectedTab.Text;
			SaveFileDialog save = new SaveFileDialog();
			save.DefaultExt = ".rtf";

			if (save.ShowDialog() == DialogResult.OK)
			{
				StreamWriter writer = new StreamWriter(save.FileName);
				writer.Write(GetCurrentRichTextBox().Text);
				writer.Close();
			}
			Pages.Add(currPageName, save.FileName);
		}
		
        private void Add_ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string pageName = $"Page_{tabControl1.TabPages.Count + 1}";			
			CreateNewTab(pageName);
			
        }

        private void Remove_SelectedToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (tabControl1.TabPages.Count > 1)
            {
                TabPage tp = tabControl1.SelectedTab;
                tabControl1.TabPages.Remove(tp);
            }
        }

        private void Open_ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult result;
            result = MessageBox.Show($"rewrite this file?", "title", MessageBoxButtons.YesNo);
            switch (result)
            {
                case DialogResult.Yes:
                    OpenFileDialog open = new OpenFileDialog();
                    open.Filter = "All Files(*.*)|*.*|Text Files(*.txt)|*.txt||";
                    open.FilterIndex = 1;

                    if (open.ShowDialog() == DialogResult.OK)
                    {
                        StreamReader reader = File.OpenText(open.FileName);
                        string text = reader.ReadToEnd();
                        reader.Close();
                        
                        this.numericUpDown_NumSymb.Value = text.Length;
                        GetCurrentRichTextBox().Text = text;
                    }
                    break;
                case DialogResult.No:
					string pageName = $"Page_{tabControl1.TabPages.Count + 1}";
					CreateNewTab(pageName);
					//Form1 newForm = new Form1();
					//newForm.numericUpDown_NumSymb.Value = GetCurrentRichTextBox().MaxLength;
					//newForm.ShowDialog();
					break;
            }
        }

		// use dictionary (key - name of page, val - path to file)  +
		// use GetCurrentTextBox() tag for holding files path  -
		private void Save_ToolStripMenuItem_Click(object sender, EventArgs e)
        {
			bool resaved = false;
			SaveFileDialog save = new SaveFileDialog();
			save.DefaultExt = ".rtf";

			string currPageName = tabControl1.SelectedTab.Text;
			
			foreach (KeyValuePair<string, string> keyValue in Pages)
			{
				if (currPageName == keyValue.Key)
				{					
					save.FileName = keyValue.Value;
					StreamWriter writer = new StreamWriter(save.FileName);
					writer.Write(GetCurrentRichTextBox().Text);
					writer.Close();
					resaved = true;					
				}
			}
			if (resaved != true)
			{
				if (save.ShowDialog() == DialogResult.OK)
				{
					StreamWriter writer = new StreamWriter(save.FileName);
					writer.Write(GetCurrentRichTextBox().Text);
					writer.Close();
					Pages.Add(currPageName, save.FileName );
				}
			}		
        }

        private void FontToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FontDialog font = new FontDialog();
            if (font.ShowDialog() == DialogResult.OK)
            {
                GetCurrentRichTextBox().Font = font.Font;
            }
        }

        private void ColorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ColorDialog color = new ColorDialog();
            if (color.ShowDialog() == DialogResult.OK)
            {
                GetCurrentRichTextBox().ForeColor = color.Color;
            }
        }

        private void Copy_ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(GetCurrentRichTextBox().SelectedText);
        }

        private void Paste_ToolStripMenuItem_Click(object sender, EventArgs e)
        {           
            GetCurrentRichTextBox().Paste();
        }

        private void SelectAll_ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GetCurrentRichTextBox().SelectAll();
        }

        private void Delete_AllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GetCurrentRichTextBox().SelectedText = " ";
        }

        private void DeselectAll_ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GetCurrentRichTextBox().DeselectAll();            
        }

        private void Backward_ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GetCurrentRichTextBox().Undo();
        }
		#endregion
			   
		#region //========TOOLSTRIP BUTTON=======//
		private void Open_toolStripButton1_Click(object sender, EventArgs e)
        {
            DialogResult result;
            result = MessageBox.Show($"rewrite this file?", "title", MessageBoxButtons.YesNo);
            switch (result)
            {
                case DialogResult.Yes:
                    OpenFileDialog open = new OpenFileDialog();
                    open.Filter = "All Files(*.*)|*.*|Text Files(*.txt)|*.txt||";
                    open.FilterIndex = 1;

                    if (open.ShowDialog() == DialogResult.OK)
                    {
                        StreamReader reader = File.OpenText(open.FileName);
                        string text = reader.ReadToEnd();
                        reader.Close();

                        this.numericUpDown_NumSymb.Value = text.Length;
                        GetCurrentRichTextBox().Text = text;
                    }
                    break;
                case DialogResult.No:

                    Form1 newForm = new Form1();
                    newForm.numericUpDown_NumSymb.Value = GetCurrentRichTextBox().MaxLength;
                    newForm.ShowDialog();
                    break;
            }
        }

        private void Copy_toolStripButton3_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(GetCurrentRichTextBox().SelectedText);
        }

        // use dictionary (key - name of page, val - path to file)
        // use GetCurrentTextBox() tag for holding files path
        private void Save_toolStripButton2_Click(object sender, EventArgs e)
        {
            SaveFileDialog save = new SaveFileDialog();
            save.DefaultExt = ".txt";          

            if (save.ShowDialog() == DialogResult.OK)
            {
                StreamWriter writer = new StreamWriter(save.FileName);
                writer.Write(GetCurrentRichTextBox().Text);
                writer.Close();
            }           
        }

        private void toolStripButton1_LightMode_Click(object sender, EventArgs e)
        {
            if (toolStripButton1_LightMode.CheckState == CheckState.Unchecked)
            {    
                string path = Environment.CurrentDirectory;
                path = Path.Combine(path, @"..\..\imagines\lamp_on.png");               

                toolStripButton1_LightMode.Image = Image.FromFile(path);               

                toolStripButton1_LightMode.CheckState = CheckState.Checked;

				BackColor = Color.WhiteSmoke;
				GetCurrentRichTextBox().BackColor = Color.WhiteSmoke;
				label1_MaxSymb.ForeColor = Color.Black;
				numericUpDown_NumSymb.BackColor = Color.WhiteSmoke;
				statusStrip1.BackColor = Color.WhiteSmoke;				
			}
			else if (toolStripButton1_LightMode.CheckState == CheckState.Checked)
            {
               
                string path = Environment.CurrentDirectory;
                path = Path.Combine(path, @"..\..\imagines\lamp_off.png");

                toolStripButton1_LightMode.Image = Image.FromFile(path);
                toolStripButton1_LightMode.CheckState = CheckState.Unchecked;
             
				BackColor = Color.Black;
				GetCurrentRichTextBox().BackColor = Color.Black;
				label1_MaxSymb.ForeColor = Color.DarkGray;
				
				numericUpDown_NumSymb.BackColor = Color.DarkGray;
				statusStrip1.BackColor = Color.Black;

				#region //Menu Colors Changes
				toolStripStatusLabel2.BackColor = Color.Black;
				toolStripStatusLabel2.ForeColor = Color.DarkGray;

				toolStripStatusLabel11.BackColor = Color.Black;
				toolStripStatusLabel11.ForeColor = Color.DarkGray;

				toolStripStatusLabel10.BackColor = Color.Black;
				toolStripStatusLabel10.ForeColor = Color.DarkGray;

				toolStripStatusLabel3.BackColor = Color.Black;
				toolStripStatusLabel3.ForeColor = Color.DarkGray;

				toolStripStatusLabel4.BackColor = Color.Black;
				toolStripStatusLabel4.ForeColor = Color.DarkGray;

				toolStripStatusLabel5.BackColor = Color.Black;
				toolStripStatusLabel5.ForeColor = Color.DarkGray;

				toolStripStatusLabel6.BackColor = Color.Black;
				toolStripStatusLabel6.ForeColor = Color.DarkGray;

				toolStripStatusLabel7.BackColor = Color.Black;
				toolStripStatusLabel7.ForeColor = Color.DarkGray;

				toolStripStatusLabel8.BackColor = Color.Black;
				toolStripStatusLabel8.ForeColor = Color.DarkGray;

				toolStripStatusLabel9.BackColor = Color.Black;
				toolStripStatusLabel9.ForeColor = Color.DarkGray;

				toolStripProgressBar1.BackColor = Color.DimGray;
				toolStrip1.BackColor = Color.Black;

				menuStrip1.BackColor = Color.Black;
				menuStrip1.ForeColor = Color.DarkGray;
				#endregion

			}
		}
		#endregion

		#region //=======CONTEXT MENU STRIP======//

		private void copyAllToolStripMenuItem_Click(object sender, EventArgs e)
        {          
            Clipboard.SetText(GetCurrentRichTextBox().Text);
        }

        private void copySelectionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(GetCurrentRichTextBox().SelectedText);
        }

        private void pasteToolStripMenuItem1_Click(object sender, EventArgs e)
        {           
            GetCurrentRichTextBox().Paste();			
        }

        private void selectAllToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            GetCurrentRichTextBox().SelectAll();
        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GetCurrentRichTextBox().SelectedText = " ";
        }

        private void setFontToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FontDialog font = new FontDialog();
            if (font.ShowDialog() == DialogResult.OK)
            {
                GetCurrentRichTextBox().SelectionFont = font.Font;
            }
        }

        private void setColorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ColorDialog color = new ColorDialog();
            if (color.ShowDialog() == DialogResult.OK)
            {
                GetCurrentRichTextBox().SelectionColor = color.Color;
            }
        }

        private void showToolsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (showToolsToolStripMenuItem.CheckState == CheckState.Checked)
            {
                toolStrip1.Visible = false;
                showToolsToolStripMenuItem.CheckState = CheckState.Unchecked;
            }

            else if (showToolsToolStripMenuItem.CheckState == CheckState.Unchecked)
            {
                showToolsToolStripMenuItem.CheckState = CheckState.Checked;
                toolStrip1.Visible = true;
            }
        }

		private void cutToolStripMenuItem_Click(object sender, EventArgs e)
		{
			Clipboard.SetText(GetCurrentRichTextBox().SelectedText);
			GetCurrentRichTextBox().SelectedText = "";
		}

		private void saveToolStripMenuItem_Click(object sender, EventArgs e)
		{
			SaveFileDialog save = new SaveFileDialog();
			save.DefaultExt = ".rtf";
			
			if (save.ShowDialog() == DialogResult.OK)
			{
				GetCurrentRichTextBox().SaveFile(save.FileName);
			}
			//Pages.Add(currPageName, save.FileName);
			//string currPageName = tabControl1.SelectedTab.Text;
		}

		private void loadToolStripMenuItem_Click(object sender, EventArgs e)
		{
			DialogResult result;
			result = MessageBox.Show($"rewrite this file?", "title", MessageBoxButtons.YesNo);

			
			OpenFileDialog open = new OpenFileDialog();
			open.Filter = "All Files(*.*)|*.*|Text Files(*.rtf)|*.rtf||";
			open.FilterIndex = 1;
			switch (result)
			{
				case DialogResult.Yes:
					if (open.ShowDialog() == DialogResult.OK)
					{						
						GetCurrentRichTextBox().LoadFile(open.FileName);
						this.numericUpDown_NumSymb.Value = GetCurrentRichTextBox().Text.Length;						
					}
					break;
				case DialogResult.No:

					string pageName = $"Page_{tabControl1.TabPages.Count + 1}";
                    //string pageName = $"~ FileName(with help of \\ get name of file)";

                    CreateNewTab(pageName);
					if (open.ShowDialog() == DialogResult.OK)
					{
						RichTextBox currTextBox;
						foreach (var item in tabControl1.Controls)
						{
							if (item is TabPage)
							{
								TabPage tmp = item as TabPage;
								if (tmp.Text == pageName)
								{
									currTextBox = tmp.Controls[0] as RichTextBox;
                                    currTextBox.LoadFile(open.FileName);                                    ;
									this.numericUpDown_NumSymb.Value = currTextBox.Text.Length;
								}
							}
						}
					}
					break;								
			}
		}


		private void RichTextBox_DragDrop(object sender, DragEventArgs e)
		{
			var data = e.Data.GetData(DataFormats.FileDrop);
			if (data != null)
			{
				var fileNames = data as string[];
				if (fileNames.Length > 0)
					GetCurrentRichTextBox().LoadFile(fileNames[0], RichTextBoxStreamType.PlainText);
				this.numericUpDown_NumSymb.Value = GetCurrentRichTextBox().TextLength;
			}
		}
		#endregion

	}
}
