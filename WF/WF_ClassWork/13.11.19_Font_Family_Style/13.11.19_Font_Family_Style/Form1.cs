﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _13._11._19_Font_Family_Style
{
    public partial class Form1 : Form
    {
        Random rnd = new Random();      

        private  Color CreateColor()
        {
            int first = rnd.Next(0, 254);
            int second = rnd.Next(0, 254);
            int third = rnd.Next(0, 254);

            var newColor = Color.FromArgb(first, second, third);
            return newColor;
        }

        public Form1()
        {
            InitializeComponent();
        }      

        private void button_scatter_Click(object sender, EventArgs e)
        {           
            string[] str = textBox1.Text.Split(' ', ',', '.');

            for (int i = 0; i < str.Length; i++)
            {
                int max = FontFamily.Families.Count();
                int fonInd = rnd.Next(1, max);
               
                FontFamily fonfam = FontFamily.Families[fonInd];

                int size = str[i].Length;

                System.Drawing.Font f = new System.Drawing.Font(fonfam, size+10);
                DrawWord(f, str[i]);
            }
            //some new long entered line in new window, but actually this line should be longer

        }


        private void DrawWord(System.Drawing.Font font, String str)
        {
            Graphics g = this.CreateGraphics();

            int x = rnd.Next(25, 450);
            int y = rnd.Next(70, 400);
            int colInd = rnd.Next(0, 4);

            Color newColor = CreateColor();
            SolidBrush brush = new SolidBrush(newColor);          

            g.DrawString(str, font, brush, x, y);
        }

        private void button_Clear_Click(object sender, EventArgs e)
        {
            Graphics g = this.CreateGraphics();
            g.Clear(this.BackColor);
        }
    }
}
