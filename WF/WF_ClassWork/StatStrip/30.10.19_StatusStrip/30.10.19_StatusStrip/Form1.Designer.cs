﻿namespace _30._10._19_StatusStrip
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
			this.label1_MaxSymb = new System.Windows.Forms.Label();
			this.numericUpDown_NumSymb = new System.Windows.Forms.NumericUpDown();
			this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
			this.tabControl1 = new System.Windows.Forms.TabControl();
			this.menuStrip1 = new System.Windows.Forms.MenuStrip();
			this.someTextToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.Open_ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.Save_ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.saveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
			this.anotherTextToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.FontToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.ColorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripMenuItem6 = new System.Windows.Forms.ToolStripSeparator();
			this.showToolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.pageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.addToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.removeSelectedToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.copyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.pasteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
			this.selectAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.deleteAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.deselectAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripSeparator();
			this.backwardToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStrip1 = new System.Windows.Forms.ToolStrip();
			this.Open_toolStripButton1 = new System.Windows.Forms.ToolStripButton();
			this.Save_toolStripButton2 = new System.Windows.Forms.ToolStripButton();
			this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
			this.Copy_toolStripButton3 = new System.Windows.Forms.ToolStripButton();
			this.toolStripButton1_LightMode = new System.Windows.Forms.ToolStripButton();
			this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
			this.AddPage_toolStripButton1 = new System.Windows.Forms.ToolStripButton();
			this.Remove_toolStripButton1 = new System.Windows.Forms.ToolStripButton();
			this.toolStripButton_Paste = new System.Windows.Forms.ToolStripButton();
			this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
			this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
			this.toolStripStatusLabel11 = new System.Windows.Forms.ToolStripStatusLabel();
			this.toolStripStatusLabel10 = new System.Windows.Forms.ToolStripStatusLabel();
			this.toolStripStatusLabel3 = new System.Windows.Forms.ToolStripStatusLabel();
			this.toolStripProgressBar1 = new System.Windows.Forms.ToolStripProgressBar();
			this.toolStripStatusLabel4 = new System.Windows.Forms.ToolStripStatusLabel();
			this.toolStripStatusLabel5 = new System.Windows.Forms.ToolStripStatusLabel();
			this.toolStripStatusLabel6 = new System.Windows.Forms.ToolStripStatusLabel();
			this.toolStripStatusLabel7 = new System.Windows.Forms.ToolStripStatusLabel();
			this.toolStripStatusLabel8 = new System.Windows.Forms.ToolStripStatusLabel();
			this.toolStripStatusLabel9 = new System.Windows.Forms.ToolStripStatusLabel();
			this.statusStrip1 = new System.Windows.Forms.StatusStrip();
			this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.copyToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
			this.copyAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.copySelectionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.pasteToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripSeparator();
			this.selectAllToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
			this.deleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripMenuItem5 = new System.Windows.Forms.ToolStripSeparator();
			this.setFontToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.setColorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripMenuItem7 = new System.Windows.Forms.ToolStripSeparator();
			this.cutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripMenuItem8 = new System.Windows.Forms.ToolStripSeparator();
			this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.loadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown_NumSymb)).BeginInit();
			this.menuStrip1.SuspendLayout();
			this.toolStrip1.SuspendLayout();
			this.statusStrip1.SuspendLayout();
			this.contextMenuStrip1.SuspendLayout();
			this.SuspendLayout();
			// 
			// label1_MaxSymb
			// 
			this.label1_MaxSymb.AutoSize = true;
			this.label1_MaxSymb.Location = new System.Drawing.Point(12, 84);
			this.label1_MaxSymb.Name = "label1_MaxSymb";
			this.label1_MaxSymb.Size = new System.Drawing.Size(75, 13);
			this.label1_MaxSymb.TabIndex = 1;
			this.label1_MaxSymb.Text = "Max Symbols: ";
			// 
			// numericUpDown_NumSymb
			// 
			this.numericUpDown_NumSymb.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
			this.numericUpDown_NumSymb.Location = new System.Drawing.Point(93, 77);
			this.numericUpDown_NumSymb.Maximum = new decimal(new int[] {
            280,
            0,
            0,
            0});
			this.numericUpDown_NumSymb.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.numericUpDown_NumSymb.Name = "numericUpDown_NumSymb";
			this.numericUpDown_NumSymb.Size = new System.Drawing.Size(53, 20);
			this.numericUpDown_NumSymb.TabIndex = 2;
			this.numericUpDown_NumSymb.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.numericUpDown_NumSymb.ValueChanged += new System.EventHandler(this.numericUpDown_NumSymb_ValueChanged);
			// 
			// notifyIcon1
			// 
			this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
			this.notifyIcon1.Text = "notifyIcon1";
			this.notifyIcon1.Visible = true;
			// 
			// tabControl1
			// 
			this.tabControl1.Location = new System.Drawing.Point(12, 103);
			this.tabControl1.Name = "tabControl1";
			this.tabControl1.SelectedIndex = 0;
			this.tabControl1.Size = new System.Drawing.Size(445, 407);
			this.tabControl1.TabIndex = 8;
			this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
			// 
			// menuStrip1
			// 
			this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.someTextToolStripMenuItem,
            this.anotherTextToolStripMenuItem,
            this.pageToolStripMenuItem,
            this.editToolStripMenuItem});
			this.menuStrip1.Location = new System.Drawing.Point(0, 0);
			this.menuStrip1.Name = "menuStrip1";
			this.menuStrip1.Size = new System.Drawing.Size(564, 24);
			this.menuStrip1.TabIndex = 11;
			this.menuStrip1.Text = "menuStrip1";
			// 
			// someTextToolStripMenuItem
			// 
			this.someTextToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Open_ToolStripMenuItem,
            this.Save_ToolStripMenuItem,
            this.saveAsToolStripMenuItem,
            this.toolStripMenuItem1});
			this.someTextToolStripMenuItem.Name = "someTextToolStripMenuItem";
			this.someTextToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
			this.someTextToolStripMenuItem.Text = "File";
			// 
			// Open_ToolStripMenuItem
			// 
			this.Open_ToolStripMenuItem.Name = "Open_ToolStripMenuItem";
			this.Open_ToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
			this.Open_ToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
			this.Open_ToolStripMenuItem.Text = "Open";
			this.Open_ToolStripMenuItem.Click += new System.EventHandler(this.Open_ToolStripMenuItem_Click);
			// 
			// Save_ToolStripMenuItem
			// 
			this.Save_ToolStripMenuItem.Name = "Save_ToolStripMenuItem";
			this.Save_ToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
			this.Save_ToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
			this.Save_ToolStripMenuItem.Text = "Save";
			this.Save_ToolStripMenuItem.Click += new System.EventHandler(this.Save_ToolStripMenuItem_Click);
			// 
			// saveAsToolStripMenuItem
			// 
			this.saveAsToolStripMenuItem.Name = "saveAsToolStripMenuItem";
			this.saveAsToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.S)));
			this.saveAsToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
			this.saveAsToolStripMenuItem.Text = "Save As..";
			this.saveAsToolStripMenuItem.Click += new System.EventHandler(this.saveAsToolStripMenuItem_Click);
			// 
			// toolStripMenuItem1
			// 
			this.toolStripMenuItem1.Name = "toolStripMenuItem1";
			this.toolStripMenuItem1.Size = new System.Drawing.Size(153, 6);
			// 
			// anotherTextToolStripMenuItem
			// 
			this.anotherTextToolStripMenuItem.Checked = true;
			this.anotherTextToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
			this.anotherTextToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.FontToolStripMenuItem,
            this.ColorToolStripMenuItem,
            this.toolStripMenuItem6,
            this.showToolsToolStripMenuItem});
			this.anotherTextToolStripMenuItem.Name = "anotherTextToolStripMenuItem";
			this.anotherTextToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
			this.anotherTextToolStripMenuItem.Text = "View";
			// 
			// FontToolStripMenuItem
			// 
			this.FontToolStripMenuItem.Name = "FontToolStripMenuItem";
			this.FontToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F)));
			this.FontToolStripMenuItem.Size = new System.Drawing.Size(151, 22);
			this.FontToolStripMenuItem.Text = "Font";
			this.FontToolStripMenuItem.Click += new System.EventHandler(this.FontToolStripMenuItem_Click);
			// 
			// ColorToolStripMenuItem
			// 
			this.ColorToolStripMenuItem.Name = "ColorToolStripMenuItem";
			this.ColorToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.K)));
			this.ColorToolStripMenuItem.Size = new System.Drawing.Size(151, 22);
			this.ColorToolStripMenuItem.Text = "Colour";
			this.ColorToolStripMenuItem.Click += new System.EventHandler(this.ColorToolStripMenuItem_Click);
			// 
			// toolStripMenuItem6
			// 
			this.toolStripMenuItem6.Name = "toolStripMenuItem6";
			this.toolStripMenuItem6.Size = new System.Drawing.Size(148, 6);
			// 
			// showToolsToolStripMenuItem
			// 
			this.showToolsToolStripMenuItem.Checked = true;
			this.showToolsToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
			this.showToolsToolStripMenuItem.Name = "showToolsToolStripMenuItem";
			this.showToolsToolStripMenuItem.Size = new System.Drawing.Size(151, 22);
			this.showToolsToolStripMenuItem.Text = "Show Tools";
			this.showToolsToolStripMenuItem.Click += new System.EventHandler(this.showToolsToolStripMenuItem_Click);
			// 
			// pageToolStripMenuItem
			// 
			this.pageToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addToolStripMenuItem,
            this.removeSelectedToolStripMenuItem});
			this.pageToolStripMenuItem.Name = "pageToolStripMenuItem";
			this.pageToolStripMenuItem.Size = new System.Drawing.Size(45, 20);
			this.pageToolStripMenuItem.Text = "Page";
			// 
			// addToolStripMenuItem
			// 
			this.addToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("addToolStripMenuItem.Image")));
			this.addToolStripMenuItem.Name = "addToolStripMenuItem";
			this.addToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
			this.addToolStripMenuItem.Text = "Add";
			this.addToolStripMenuItem.Click += new System.EventHandler(this.Add_ToolStripMenuItem_Click);
			// 
			// removeSelectedToolStripMenuItem
			// 
			this.removeSelectedToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("removeSelectedToolStripMenuItem.Image")));
			this.removeSelectedToolStripMenuItem.Name = "removeSelectedToolStripMenuItem";
			this.removeSelectedToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
			this.removeSelectedToolStripMenuItem.Text = "Remove Selected";
			this.removeSelectedToolStripMenuItem.Click += new System.EventHandler(this.Remove_SelectedToolStripMenuItem_Click);
			// 
			// editToolStripMenuItem
			// 
			this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.copyToolStripMenuItem,
            this.pasteToolStripMenuItem,
            this.toolStripMenuItem2,
            this.selectAllToolStripMenuItem,
            this.deleteAllToolStripMenuItem,
            this.deselectAllToolStripMenuItem,
            this.toolStripMenuItem3,
            this.backwardToolStripMenuItem});
			this.editToolStripMenuItem.Name = "editToolStripMenuItem";
			this.editToolStripMenuItem.Size = new System.Drawing.Size(39, 20);
			this.editToolStripMenuItem.Text = "Edit";
			// 
			// copyToolStripMenuItem
			// 
			this.copyToolStripMenuItem.Name = "copyToolStripMenuItem";
			this.copyToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.C)));
			this.copyToolStripMenuItem.Size = new System.Drawing.Size(209, 22);
			this.copyToolStripMenuItem.Text = "Copy";
			this.copyToolStripMenuItem.Click += new System.EventHandler(this.Copy_ToolStripMenuItem_Click);
			// 
			// pasteToolStripMenuItem
			// 
			this.pasteToolStripMenuItem.Name = "pasteToolStripMenuItem";
			this.pasteToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.V)));
			this.pasteToolStripMenuItem.Size = new System.Drawing.Size(209, 22);
			this.pasteToolStripMenuItem.Text = "Paste";
			this.pasteToolStripMenuItem.Click += new System.EventHandler(this.Paste_ToolStripMenuItem_Click);
			// 
			// toolStripMenuItem2
			// 
			this.toolStripMenuItem2.Name = "toolStripMenuItem2";
			this.toolStripMenuItem2.Size = new System.Drawing.Size(206, 6);
			// 
			// selectAllToolStripMenuItem
			// 
			this.selectAllToolStripMenuItem.Name = "selectAllToolStripMenuItem";
			this.selectAllToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.A)));
			this.selectAllToolStripMenuItem.Size = new System.Drawing.Size(209, 22);
			this.selectAllToolStripMenuItem.Text = "Select All";
			this.selectAllToolStripMenuItem.Click += new System.EventHandler(this.SelectAll_ToolStripMenuItem_Click);
			// 
			// deleteAllToolStripMenuItem
			// 
			this.deleteAllToolStripMenuItem.Name = "deleteAllToolStripMenuItem";
			this.deleteAllToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.Delete;
			this.deleteAllToolStripMenuItem.Size = new System.Drawing.Size(209, 22);
			this.deleteAllToolStripMenuItem.Text = "Delete All";
			this.deleteAllToolStripMenuItem.Click += new System.EventHandler(this.Delete_AllToolStripMenuItem_Click);
			// 
			// deselectAllToolStripMenuItem
			// 
			this.deselectAllToolStripMenuItem.Name = "deselectAllToolStripMenuItem";
			this.deselectAllToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.A)));
			this.deselectAllToolStripMenuItem.Size = new System.Drawing.Size(209, 22);
			this.deselectAllToolStripMenuItem.Text = "Deselect All";
			this.deselectAllToolStripMenuItem.Click += new System.EventHandler(this.DeselectAll_ToolStripMenuItem_Click);
			// 
			// toolStripMenuItem3
			// 
			this.toolStripMenuItem3.Name = "toolStripMenuItem3";
			this.toolStripMenuItem3.Size = new System.Drawing.Size(206, 6);
			// 
			// backwardToolStripMenuItem
			// 
			this.backwardToolStripMenuItem.Name = "backwardToolStripMenuItem";
			this.backwardToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.R)));
			this.backwardToolStripMenuItem.Size = new System.Drawing.Size(209, 22);
			this.backwardToolStripMenuItem.Text = "Backward";
			this.backwardToolStripMenuItem.Click += new System.EventHandler(this.Backward_ToolStripMenuItem_Click);
			// 
			// toolStrip1
			// 
			this.toolStrip1.Dock = System.Windows.Forms.DockStyle.None;
			this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Open_toolStripButton1,
            this.Save_toolStripButton2,
            this.toolStripSeparator1,
            this.Copy_toolStripButton3,
            this.toolStripButton1_LightMode,
            this.toolStripSeparator2,
            this.AddPage_toolStripButton1,
            this.Remove_toolStripButton1,
            this.toolStripButton_Paste});
			this.toolStrip1.Location = new System.Drawing.Point(178, 72);
			this.toolStrip1.Name = "toolStrip1";
			this.toolStrip1.Size = new System.Drawing.Size(185, 25);
			this.toolStrip1.TabIndex = 12;
			this.toolStrip1.Text = "toolStrip1";
			// 
			// Open_toolStripButton1
			// 
			this.Open_toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.Open_toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("Open_toolStripButton1.Image")));
			this.Open_toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.Open_toolStripButton1.Name = "Open_toolStripButton1";
			this.Open_toolStripButton1.Size = new System.Drawing.Size(23, 22);
			this.Open_toolStripButton1.Text = "Open";
			this.Open_toolStripButton1.Click += new System.EventHandler(this.Open_toolStripButton1_Click);
			// 
			// Save_toolStripButton2
			// 
			this.Save_toolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.Save_toolStripButton2.Image = ((System.Drawing.Image)(resources.GetObject("Save_toolStripButton2.Image")));
			this.Save_toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.Save_toolStripButton2.Name = "Save_toolStripButton2";
			this.Save_toolStripButton2.Size = new System.Drawing.Size(23, 22);
			this.Save_toolStripButton2.Text = "Save";
			this.Save_toolStripButton2.Click += new System.EventHandler(this.Save_toolStripButton2_Click);
			// 
			// toolStripSeparator1
			// 
			this.toolStripSeparator1.Name = "toolStripSeparator1";
			this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
			// 
			// Copy_toolStripButton3
			// 
			this.Copy_toolStripButton3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.Copy_toolStripButton3.Image = ((System.Drawing.Image)(resources.GetObject("Copy_toolStripButton3.Image")));
			this.Copy_toolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.Copy_toolStripButton3.Name = "Copy_toolStripButton3";
			this.Copy_toolStripButton3.Size = new System.Drawing.Size(23, 22);
			this.Copy_toolStripButton3.Text = "Copy";
			this.Copy_toolStripButton3.Click += new System.EventHandler(this.Copy_toolStripButton3_Click);
			// 
			// toolStripButton1_LightMode
			// 
			this.toolStripButton1_LightMode.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.toolStripButton1_LightMode.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1_LightMode.Image")));
			this.toolStripButton1_LightMode.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.toolStripButton1_LightMode.Name = "toolStripButton1_LightMode";
			this.toolStripButton1_LightMode.Size = new System.Drawing.Size(23, 22);
			this.toolStripButton1_LightMode.Text = "Light";
			this.toolStripButton1_LightMode.Click += new System.EventHandler(this.toolStripButton1_LightMode_Click);
			// 
			// toolStripSeparator2
			// 
			this.toolStripSeparator2.Name = "toolStripSeparator2";
			this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
			// 
			// AddPage_toolStripButton1
			// 
			this.AddPage_toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.AddPage_toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("AddPage_toolStripButton1.Image")));
			this.AddPage_toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.AddPage_toolStripButton1.Name = "AddPage_toolStripButton1";
			this.AddPage_toolStripButton1.Size = new System.Drawing.Size(23, 22);
			this.AddPage_toolStripButton1.Text = "Add Page";
			// 
			// Remove_toolStripButton1
			// 
			this.Remove_toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.Remove_toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("Remove_toolStripButton1.Image")));
			this.Remove_toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.Remove_toolStripButton1.Name = "Remove_toolStripButton1";
			this.Remove_toolStripButton1.Size = new System.Drawing.Size(23, 22);
			this.Remove_toolStripButton1.Text = "Remove";
			// 
			// toolStripButton_Paste
			// 
			this.toolStripButton_Paste.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.toolStripButton_Paste.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton_Paste.Image")));
			this.toolStripButton_Paste.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.toolStripButton_Paste.Name = "toolStripButton_Paste";
			this.toolStripButton_Paste.Size = new System.Drawing.Size(23, 22);
			this.toolStripButton_Paste.Text = "toolStripButton1";
			// 
			// toolStripStatusLabel1
			// 
			this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
			this.toolStripStatusLabel1.Size = new System.Drawing.Size(0, 17);
			// 
			// toolStripStatusLabel2
			// 
			this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
			this.toolStripStatusLabel2.Size = new System.Drawing.Size(57, 17);
			this.toolStripStatusLabel2.Text = "symbols: ";
			// 
			// toolStripStatusLabel11
			// 
			this.toolStripStatusLabel11.Name = "toolStripStatusLabel11";
			this.toolStripStatusLabel11.Size = new System.Drawing.Size(13, 17);
			this.toolStripStatusLabel11.Text = "0";
			// 
			// toolStripStatusLabel10
			// 
			this.toolStripStatusLabel10.Name = "toolStripStatusLabel10";
			this.toolStripStatusLabel10.Size = new System.Drawing.Size(12, 17);
			this.toolStripStatusLabel10.Text = "/";
			// 
			// toolStripStatusLabel3
			// 
			this.toolStripStatusLabel3.Name = "toolStripStatusLabel3";
			this.toolStripStatusLabel3.Size = new System.Drawing.Size(13, 17);
			this.toolStripStatusLabel3.Text = "0";
			// 
			// toolStripProgressBar1
			// 
			this.toolStripProgressBar1.Name = "toolStripProgressBar1";
			this.toolStripProgressBar1.Size = new System.Drawing.Size(100, 16);
			// 
			// toolStripStatusLabel4
			// 
			this.toolStripStatusLabel4.Name = "toolStripStatusLabel4";
			this.toolStripStatusLabel4.Size = new System.Drawing.Size(48, 17);
			this.toolStripStatusLabel4.Text = "  digits: ";
			// 
			// toolStripStatusLabel5
			// 
			this.toolStripStatusLabel5.Name = "toolStripStatusLabel5";
			this.toolStripStatusLabel5.Size = new System.Drawing.Size(13, 17);
			this.toolStripStatusLabel5.Text = "0";
			// 
			// toolStripStatusLabel6
			// 
			this.toolStripStatusLabel6.Name = "toolStripStatusLabel6";
			this.toolStripStatusLabel6.Size = new System.Drawing.Size(51, 17);
			this.toolStripStatusLabel6.Text = "  letters: ";
			// 
			// toolStripStatusLabel7
			// 
			this.toolStripStatusLabel7.Name = "toolStripStatusLabel7";
			this.toolStripStatusLabel7.Size = new System.Drawing.Size(13, 17);
			this.toolStripStatusLabel7.Text = "0";
			// 
			// toolStripStatusLabel8
			// 
			this.toolStripStatusLabel8.Name = "toolStripStatusLabel8";
			this.toolStripStatusLabel8.Size = new System.Drawing.Size(48, 17);
			this.toolStripStatusLabel8.Text = "  words:";
			// 
			// toolStripStatusLabel9
			// 
			this.toolStripStatusLabel9.Name = "toolStripStatusLabel9";
			this.toolStripStatusLabel9.Size = new System.Drawing.Size(13, 17);
			this.toolStripStatusLabel9.Text = "0";
			// 
			// statusStrip1
			// 
			this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.toolStripStatusLabel2,
            this.toolStripStatusLabel11,
            this.toolStripStatusLabel10,
            this.toolStripStatusLabel3,
            this.toolStripProgressBar1,
            this.toolStripStatusLabel4,
            this.toolStripStatusLabel5,
            this.toolStripStatusLabel6,
            this.toolStripStatusLabel7,
            this.toolStripStatusLabel8,
            this.toolStripStatusLabel9});
			this.statusStrip1.Location = new System.Drawing.Point(0, 513);
			this.statusStrip1.Name = "statusStrip1";
			this.statusStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.ManagerRenderMode;
			this.statusStrip1.Size = new System.Drawing.Size(564, 22);
			this.statusStrip1.TabIndex = 3;
			this.statusStrip1.Text = "statusStrip1";
			// 
			// contextMenuStrip1
			// 
			this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.copyToolStripMenuItem1,
            this.pasteToolStripMenuItem1,
            this.toolStripMenuItem4,
            this.selectAllToolStripMenuItem1,
            this.deleteToolStripMenuItem,
            this.toolStripMenuItem5,
            this.setFontToolStripMenuItem,
            this.setColorToolStripMenuItem,
            this.toolStripMenuItem7,
            this.cutToolStripMenuItem,
            this.toolStripMenuItem8,
            this.saveToolStripMenuItem,
            this.loadToolStripMenuItem});
			this.contextMenuStrip1.Name = "contextMenuStrip1";
			this.contextMenuStrip1.Size = new System.Drawing.Size(132, 226);
			// 
			// copyToolStripMenuItem1
			// 
			this.copyToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.copyAllToolStripMenuItem,
            this.copySelectionToolStripMenuItem});
			this.copyToolStripMenuItem1.Name = "copyToolStripMenuItem1";
			this.copyToolStripMenuItem1.Size = new System.Drawing.Size(131, 22);
			this.copyToolStripMenuItem1.Text = "Copy";
			// 
			// copyAllToolStripMenuItem
			// 
			this.copyAllToolStripMenuItem.Name = "copyAllToolStripMenuItem";
			this.copyAllToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
			this.copyAllToolStripMenuItem.Text = "Copy All";
			this.copyAllToolStripMenuItem.Click += new System.EventHandler(this.copyAllToolStripMenuItem_Click);
			// 
			// copySelectionToolStripMenuItem
			// 
			this.copySelectionToolStripMenuItem.Name = "copySelectionToolStripMenuItem";
			this.copySelectionToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
			this.copySelectionToolStripMenuItem.Text = "Copy Selection";
			this.copySelectionToolStripMenuItem.Click += new System.EventHandler(this.copySelectionToolStripMenuItem_Click);
			// 
			// pasteToolStripMenuItem1
			// 
			this.pasteToolStripMenuItem1.Name = "pasteToolStripMenuItem1";
			this.pasteToolStripMenuItem1.Size = new System.Drawing.Size(131, 22);
			this.pasteToolStripMenuItem1.Text = "Paste";
			this.pasteToolStripMenuItem1.Click += new System.EventHandler(this.pasteToolStripMenuItem1_Click);
			// 
			// toolStripMenuItem4
			// 
			this.toolStripMenuItem4.Name = "toolStripMenuItem4";
			this.toolStripMenuItem4.Size = new System.Drawing.Size(128, 6);
			// 
			// selectAllToolStripMenuItem1
			// 
			this.selectAllToolStripMenuItem1.Name = "selectAllToolStripMenuItem1";
			this.selectAllToolStripMenuItem1.Size = new System.Drawing.Size(131, 22);
			this.selectAllToolStripMenuItem1.Text = "Select All";
			this.selectAllToolStripMenuItem1.Click += new System.EventHandler(this.selectAllToolStripMenuItem1_Click);
			// 
			// deleteToolStripMenuItem
			// 
			this.deleteToolStripMenuItem.Name = "deleteToolStripMenuItem";
			this.deleteToolStripMenuItem.Size = new System.Drawing.Size(131, 22);
			this.deleteToolStripMenuItem.Text = "Delete All";
			this.deleteToolStripMenuItem.Click += new System.EventHandler(this.deleteToolStripMenuItem_Click);
			// 
			// toolStripMenuItem5
			// 
			this.toolStripMenuItem5.Name = "toolStripMenuItem5";
			this.toolStripMenuItem5.Size = new System.Drawing.Size(128, 6);
			// 
			// setFontToolStripMenuItem
			// 
			this.setFontToolStripMenuItem.Name = "setFontToolStripMenuItem";
			this.setFontToolStripMenuItem.Size = new System.Drawing.Size(131, 22);
			this.setFontToolStripMenuItem.Text = "Set Font...";
			this.setFontToolStripMenuItem.Click += new System.EventHandler(this.setFontToolStripMenuItem_Click);
			// 
			// setColorToolStripMenuItem
			// 
			this.setColorToolStripMenuItem.Name = "setColorToolStripMenuItem";
			this.setColorToolStripMenuItem.Size = new System.Drawing.Size(131, 22);
			this.setColorToolStripMenuItem.Text = "Set Color...";
			this.setColorToolStripMenuItem.Click += new System.EventHandler(this.setColorToolStripMenuItem_Click);
			// 
			// toolStripMenuItem7
			// 
			this.toolStripMenuItem7.Name = "toolStripMenuItem7";
			this.toolStripMenuItem7.Size = new System.Drawing.Size(128, 6);
			// 
			// cutToolStripMenuItem
			// 
			this.cutToolStripMenuItem.Name = "cutToolStripMenuItem";
			this.cutToolStripMenuItem.Size = new System.Drawing.Size(131, 22);
			this.cutToolStripMenuItem.Text = "Cut";
			this.cutToolStripMenuItem.Click += new System.EventHandler(this.cutToolStripMenuItem_Click);
			// 
			// toolStripMenuItem8
			// 
			this.toolStripMenuItem8.Name = "toolStripMenuItem8";
			this.toolStripMenuItem8.Size = new System.Drawing.Size(128, 6);
			// 
			// saveToolStripMenuItem
			// 
			this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
			this.saveToolStripMenuItem.Size = new System.Drawing.Size(131, 22);
			this.saveToolStripMenuItem.Text = "Save";
			this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
			// 
			// loadToolStripMenuItem
			// 
			this.loadToolStripMenuItem.Name = "loadToolStripMenuItem";
			this.loadToolStripMenuItem.Size = new System.Drawing.Size(131, 22);
			this.loadToolStripMenuItem.Text = "Load";
			this.loadToolStripMenuItem.Click += new System.EventHandler(this.loadToolStripMenuItem_Click);
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(564, 535);
			this.Controls.Add(this.toolStrip1);
			this.Controls.Add(this.tabControl1);
			this.Controls.Add(this.statusStrip1);
			this.Controls.Add(this.menuStrip1);
			this.Controls.Add(this.numericUpDown_NumSymb);
			this.Controls.Add(this.label1_MaxSymb);
			this.MainMenuStrip = this.menuStrip1;
			this.Name = "Form1";
			this.Text = "Form1";
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown_NumSymb)).EndInit();
			this.menuStrip1.ResumeLayout(false);
			this.menuStrip1.PerformLayout();
			this.toolStrip1.ResumeLayout(false);
			this.toolStrip1.PerformLayout();
			this.statusStrip1.ResumeLayout(false);
			this.statusStrip1.PerformLayout();
			this.contextMenuStrip1.ResumeLayout(false);
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1_MaxSymb;
        private System.Windows.Forms.NumericUpDown numericUpDown_NumSymb;
        private System.Windows.Forms.NotifyIcon notifyIcon1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem someTextToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem Open_ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem Save_ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem anotherTextToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem FontToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ColorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveAsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem removeSelectedToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem copyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pasteToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem selectAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deselectAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem backwardToolStripMenuItem;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton Open_toolStripButton1;
        private System.Windows.Forms.ToolStripButton Copy_toolStripButton3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton Save_toolStripButton2;
        private System.Windows.Forms.ToolStripButton toolStripButton1_LightMode;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton AddPage_toolStripButton1;
        private System.Windows.Forms.ToolStripButton Remove_toolStripButton1;
		private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
		private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
		private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel11;
		private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel10;
		private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel3;
		private System.Windows.Forms.ToolStripProgressBar toolStripProgressBar1;
		private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel4;
		private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel5;
		private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel6;
		private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel7;
		private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel8;
		private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel9;
		private System.Windows.Forms.StatusStrip statusStrip1;
		private System.Windows.Forms.ToolStripButton toolStripButton_Paste;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem copyToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem copyAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem copySelectionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pasteToolStripMenuItem1;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem selectAllToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem deleteToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem setFontToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem setColorToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem6;
        private System.Windows.Forms.ToolStripMenuItem showToolsToolStripMenuItem;
		private System.Windows.Forms.ToolStripSeparator toolStripMenuItem7;
		private System.Windows.Forms.ToolStripMenuItem cutToolStripMenuItem;
		private System.Windows.Forms.ToolStripSeparator toolStripMenuItem8;
		private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem loadToolStripMenuItem;
	}
}

