﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _29._10._19_Control_Bars
{
    public partial class Form1 : Form
    {       
		public int[] Arr { get; set; }
		public static int Counter { get; set; }
		public int TimeCounter { get; set; }
		public int LastDigit { get; }
		public int MaxPossibleTime { get; set; }
		public bool IsFinished { get; set; }

		public Form1()
        {
            InitializeComponent();
            foreach (var item in groupBox1.Controls)
            {
                if (item is Label)
                {
                    Label tmp = item as Label;
                    tmp.Click += Lable_Click;                  
                }
            }
			LastDigit = groupBox1.Controls.Count - 1;
			MaxPossibleTime = 20;

			TimeCounter = 0;
			timer1.Tick += timer1_Tick;
			timer1.Interval = 1000;
			//progressBar1.PerformStep();
		}

		static Form1()
		{
			Counter = 0;
		}

		private void SetDefaultColor()
		{
			foreach (var item in groupBox1.Controls)
			{
				if (item is Label)
				{
					Label tmp = item as Label;
					tmp.ForeColor = Color.Black;
				}
			}
		}

		public void LablesInitializing()
        {
            Random rnd = new Random();
            //for (int i = 0; i < groupBox1.Controls.Count; i++)
            //{
            //    groupBox1.Controls[i].Text = (rnd.Next(i, 100)).ToString();
            //}

            Arr = Enumerable.Range(0, 15).OrderBy(c => rnd.Next()).ToArray();
            //for (int i = 0; i < arr.Length; i++)
            //    MessageBox.Show($"{arr[i]}");

            for (int i = 0; i < groupBox1.Controls.Count; i++)
            {
                groupBox1.Controls[i].Text = Arr[i].ToString();
            }
        }

        private void button1_Start_Click(object sender, EventArgs e)
        {

			SetDefaultColor();
			timer1.Stop();
			TimeCounter = 0;
			Counter = 0;
			IsFinished = false;
			button1_Start.Enabled = false;

			LablesInitializing();          
            progressBar1.Minimum = 0;
            progressBar1.Maximum = groupBox1.Controls.Count;
            progressBar1.Value = 0;
            progressBar1.Step = 1;
			timer1.Start();
        }

        private void Lable_Click(object sender, EventArgs e)
        {
            int digit = int.Parse((sender as Label).Text);           	

			if (digit == Counter)
			{
				progressBar1.Value++;
				(sender as Label).ForeColor = Color.Red;

				if (digit == LastDigit)
				{
					IsFinished = true;
				}
					Counter++;
			}
			else if (digit != Counter)
				MessageBox.Show("wrong digit !!!");

            //if (Digits.Count == 0)
            //{
            //    Digits.Add(digit);
            //    progressBar1.Value++;
            //    (sender as Label).ForeColor = Color.Red;
            //}

            //else if (digit >= Digits.Last())
            //{
            //    Digits.Add(digit);
            //    progressBar1.Value++;
            //    (sender as Label).ForeColor = Color.Red;
            //}
            //else if (digit < Digits.Last())
            //    MessageBox.Show("to small digit");
           
        }

		private void timer1_Tick(object sender, EventArgs e)
		{			
			label2_Time.Text = (++TimeCounter).ToString();
			
			if ((TimeCounter <= MaxPossibleTime) && IsFinished == true)
			{
				timer1.Stop();
				MessageBox.Show("Win !!!");
				button1_Start.Enabled = true;
			}

			else if (TimeCounter > MaxPossibleTime && IsFinished != true)
			{
				timer1.Stop();
				MessageBox.Show("Failed !!!");
				button1_Start.Enabled = true;
			}
            //label2_Time.Update();           
		}

		private void trackBar1_Scroll(object sender, EventArgs e)
		{
			if (trackBar1.Value == 0)			
				MaxPossibleTime = 20;
			else if (trackBar1.Value == 1)
				MaxPossibleTime = 15;
			else if (trackBar1.Value == 2)
				MaxPossibleTime = 10;
		}
	}

   
}
