﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Font
{
    public partial class Form1 : Form
    {
        Random random = new Random();
        public Form1()
        {
            InitializeComponent();            
        }

        private void DrawStr(System.Drawing.Font font, String str)
        {
            // create Graphic
            Graphics g = this.CreateGraphics();

            // random coord
            int x = random.Next(10, 200);
            int y = random.Next(10, 400);

            // create brush
            SolidBrush sldBrs = new SolidBrush(Color.Blue);

            // show string on form
            g.DrawString(str, font, sldBrs, x, y);
        }

        private void stmiFont_Click(object sender, EventArgs e) // show
        {
            FontDialog fDialog = new FontDialog();
            if (fDialog.ShowDialog() == DialogResult.OK)
            {
                System.Drawing.Font font = fDialog.Font;
                DrawStr(font, "Some string!!! Font: " + font.Name);
            }
        }

        private void tsmiClear_Click(object sender, EventArgs e) // clear
        {
            Graphics g = this.CreateGraphics();
            g.Clear(this.BackColor);
        }

        private void button3_Click(object sender, EventArgs e) // test
        {
            FontFamily fontFamily = new FontFamily("Times New Roman");

            System.Drawing.Font f = new System.Drawing.Font(fontFamily, 20, FontStyle.Underline);

            Graphics g = this.CreateGraphics();

            g.DrawString("Sone text!!!", f, Brushes.LightSeaGreen, 10, 10);
        }
    }
}
