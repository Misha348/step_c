﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Threading;
using System.Text;
using System.Windows.Forms;

namespace ProgressBarExample1
{
    public partial class Form1 : Form
    {        
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int max = (int)numericUpDown1.Value;            
            progressBar1.Value = 0;
            progressBar1.Minimum = 0;
            progressBar1.Maximum = max;
            progressBar1.Step = 1;
           
            //if (progressBar1.Value == 0)
            for (int i = 0; i <= max; i++)
            {
                progressBar1.PerformStep();

                label1.Text = "Value = " + progressBar1.Value.ToString();
                this.Update();
                Thread.Sleep(50);                
            }
            //else
            //    for (int i = 50; i >= 0; i--)
            //    {

            //        progressBar1.Value = i;

            //        label1.Text = "Value = " + progressBar1.Value.ToString();
            //        this.Update();
            //        Thread.Sleep(50);

            //    }
        }
    }
}
