﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ListViewExample
{
    public partial class Form1 : Form
    {
        ListView table;
        ListBox box;

        public Form1()
        {
            InitializeComponent();
            // створення екземпляра ListView
            table = new ListView();
            table.SetBounds(400, 10, 300, 200);
            this.Controls.Add(table);
            // дадаємо елементи
            table.Items.Add(new ListViewItem("Перший"));
            table.Items.Add(new ListViewItem("Другий"));
            table.Items.Add(new ListViewItem("Третій"));
            table.Items.Add(new ListViewItem("Четвертий"));
            table.Items.Add(new ListViewItem("Пятий"));
            table.View = View.Details;// у вигляді таблиця          

            // додаємо стовбці
            table.Columns.Add("Ім'я");
            table.Columns[0].Width = 100;
            table.Columns.Add("Інформація");
            table.Columns[1].Width = 100;

            // додаємо піделементи
            int k=1;
            foreach (ListViewItem i in table.Items)
            {
                i.SubItems.Add("телефон - " + String.Format("{0}",(k++)));
            }
            // додаємо обробник DoubleClick
            table.DoubleClick += new System.EventHandler(this.listView_DoubleClick);

            // додаємо listBox
            box = new ListBox();
            box.SetBounds(300, 300, 300, 200);
            this.Controls.Add(box);
            viewToBox();

        }

        private void listView_DoubleClick(object sender, EventArgs e)
        {
            ListView table = (ListView)sender;
            // MessageBox.Show(table.SelectedItems[0].Text);
            // видаляємо вибраний елемент
            table.Items.Remove(table.SelectedItems[0]);
        }
        // вивід даних ListView у вигляді ListBox
        private void viewToBox()
        {
            // перегдяд колекції елементів
            foreach (ListViewItem i in table.Items)
            {
                string text = i.Text;
                // перегляд піделементів в колекції елементів
                foreach (ListViewItem.ListViewSubItem si in i.SubItems)
                {
                    text += ":" + si.Text;
                }
                box.Items.Add(text);
            }
        }
    }
}