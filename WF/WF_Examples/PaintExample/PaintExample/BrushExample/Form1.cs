﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PaintExample
{
    public partial class Form1 : Form
    {    
        private int paintCount;        
        public Form1()
        {
            InitializeComponent();
            paintCount = 0;

            this.Show();
           
          
            Graphics g = this.CreateGraphics();                 // створення об'єкту для роблти з графікою
            Rectangle rect = new Rectangle(0, 0, 250, 140);     // створення екземпляру структури прямокутника
            SolidBrush redBrush = new SolidBrush(Color.Red);    // створення екземпляру структури кисточки

            g.FillRectangle(redBrush, rect);    // заповнення прямокутника
        }

        // перевизначення методу OnPaint - спрацьовує при перемальовуванні форми
        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);    // виклик базової реалізації

            Graphics g = e.Graphics;
            SolidBrush redBrush = new SolidBrush(Color.Red);
            Rectangle rect = new Rectangle(0, 0, 250, 140);
            g.FillRectangle(redBrush, rect);

            // оновлення кількості
            this.label1.Text = String.Format("paintCount: {0}", paintCount++);
        }

        // подія, яка спрацьовує при перемальовуванні форми
        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            //// зона відсікання
            //if (e.ClipRectangle.Top < 140 && e.ClipRectangle.Left < 250)
            {               
                Graphics g = e.Graphics;
                SolidBrush redBrush = new SolidBrush(Color.Red);
                Rectangle rect = new Rectangle(0, 0, 250, 140);
                g.FillRectangle(redBrush, rect);
                                
                this.label1.Text = String.Format("paintCount: {0}", paintCount++);
            }
        }
        
        private void button1_Click(object sender, EventArgs e)
        {
            Graphics g = this.CreateGraphics();
            Rectangle rect = new Rectangle(0, 0, 250, 140);
            SolidBrush redBrush = new SolidBrush(Color.Red);
            
            g.FillRectangle(redBrush, rect);
        }
    }
}
