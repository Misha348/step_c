﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows.Forms;

namespace General_Navigation_Profile
{
	public partial class PROFILE : Form
	{
		public Member Member { get; set; }
        public List<Member> Members { get; set; }
		public BinaryFormatter formatter;
		public PROFILE()
		{
			InitializeComponent();
			dateTimePicker1.MaxDate = DateTime.Parse("01.01.2020");
			radioButton_GenderMale.Checked = true;
            Members = new List<Member>();

            var technologiesList = new string[]
            {
                "C#",
                "C++",
                "Java",
                "WPF",
                "WCF",
                "ASP.MVC",
                "WinForms",
                "MySQL",
                "Angular",               
            };

            checked_ListBox1_technologies.Items.AddRange(technologiesList);
            //checked_ListBox1_technologies.CheckedItems
        }

		public void MemberCreating()
		{
            Member = new Member
            {
                Name = textBox1.Text,
                Surname = textBox2.Text,
                Region = comboBox1_Countries.SelectedItem.ToString(),
                City = textBox4.Text,
                Gender = radioButton_GenderMale.Text,
                BirthDate = dateTimePicker1.Value,
                ID = Convert.ToInt32(textBox3.Text),
                //Technolog = textBox5_technolog.Text,
                PrLang = new List<string>()               
            };

            foreach (var item in checked_ListBox1_technologies.CheckedItems)
            {
                Member.PrLang.Add(item.ToString());
            }

            //foreach (var item in groupBox_Technoligies.Controls)
            //{
            //	if ( ((CheckBox)item).Checked)
            //		Member.PrLang.Add((item as CheckBox).Text);
            //}
        }

       

		private void button_Save_Click(object sender, EventArgs e)
		{			
           //Members.Add(this.Member);

			formatter = new BinaryFormatter();
			using (FileStream fs = new FileStream("member.dat", FileMode.OpenOrCreate))
			{
				formatter.Serialize(fs, Members);
				MessageBox.Show("member have saved");
			}
		}

		private void button_Download_Click(object sender, EventArgs e)
		{
			
			formatter = new BinaryFormatter();
			Members = null;
			using (Stream fs = File.OpenRead("member.dat"))
			{
				Members = (List<Member>)formatter.Deserialize(fs);
			}
			

            //foreach (var item in groupBox_Technoligies.Controls)
            //{
            //	if (item is CheckBox)
            //	{
            //		CheckBox tmp = item as CheckBox;
            //		if (tmp.Checked)
            //			result += tmp.Text + "\n";
            //	}				
            //}
            //MessageBox.Show(result);

           

            foreach (var memb in Members)
            {
                string result = "";
                result += $"name: {memb.Name}\nsurname: {memb.Surname}\n";
                result += $"gender: {memb.Gender}\ncity: {memb.City}\nregion: {memb.Region}\n";
                result += $"birth: {memb.BirthDate.ToShortDateString()}\n";
                result += "   Technologies:\n";

                foreach (var item in checked_ListBox1_technologies.CheckedItems)
                {
                    result += item.ToString() + "\n";
                }
                MessageBox.Show(result);
            }
        }

        private void label2_Click(object sender, EventArgs e)
        {        }      

        private void button1_Add_Stud_Click(object sender, EventArgs e)
        {
            MemberCreating();
            //MessageBox.Show(Member.ID.ToString());
            Members.Add(Member);
            //if (!this.listBox1.Items.Contains(this.Member.ID))
            //    this.listBox1.Items.Add(this.Member);

            listBox1.DataSource = null;
            //listBox1.ClearSelected();
            listBox1.DataSource = Members;
            listBox1.DisplayMember = "ShowMembData";
            //listBox1.ValueMember = "ID";

        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            
            listBox1.ClearSelected();
            textBox3.Text = ((listBox1.SelectedItem as Member).ID).ToString();
            textBox1.Text = (listBox1.SelectedItem as Member).Name;
            textBox2.Text = (listBox1.SelectedItem as Member).Surname;
            comboBox1_Countries.Text = (listBox1.SelectedItem as Member).Region;
            textBox4.Text = (listBox1.SelectedItem as Member).City;
            dateTimePicker1.Value = (listBox1.SelectedItem as Member).BirthDate;

            foreach (var item in (listBox1.SelectedItem as Member).PrLang)
            {
                //checked_ListBox1_technologies.Item
            }

            checked_ListBox1_technologies.Items.AddRange((listBox1.SelectedItem as Member).PrLang.ToArray());
        }


		//private void Button1_Selected_Click(object sender, EventArgs e)
		//{
		//    this.listBox1.Items.Clear();
		//    if (this.checked_ListBox1_technologies.CheckedItems.Count != 0)
		//    {
		//        for (int i = 0; i < this.checked_ListBox1_technologies.CheckedItems.Count; i++)
		//            this.listBox1.Items.Add(this.checked_ListBox1_technologies.CheckedItems[i]);
		//    }
		//    //else
		//}

		//private void Button1_Technol_List_Click(object sender, EventArgs e)
		//{
		//    if (!String.IsNullOrEmpty(this.textBox5_technolog.Text))
		//    {
		//        if (this.checkedListBox1_technologies.Items.Contains(this.textBox5_technolog.Text))
		//            this.checkedListBox1_technologies.Items.Add(this.textBox5_technolog.Text);
		//        else
		//            MessageBox.Show("CheckedListBox already contains this item");
		//    }
		//    else
		//        MessageBox.Show("Empty string");
		//}


	}
}
