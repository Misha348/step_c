﻿using System;
using System.Collections.Generic;


namespace General_Navigation_Profile
{
	[Serializable]
	public class Member
	{
        public int ID { get; set; }
		public string Name { get; set; }
		public string Surname { get; set; }
		public string Region { get; set; }
		public string City { get; set; }
		public string Gender { get; set; }
		public DateTime BirthDate { get; set; }
        //public string Technolog { get; set; }
		public List<string> PrLang { get; set; }
        public string ShowMembData
        {
            get
            {
                return $"{ID.ToString()}  {Name}  {Surname}";
            }
        }
       


		public Member()
		{
		}
	}
}
