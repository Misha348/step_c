﻿namespace General_Navigation_Profile
{
	partial class PROFILE
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.label_Name = new System.Windows.Forms.Label();
			this.label_Surname = new System.Windows.Forms.Label();
			this.label_City = new System.Windows.Forms.Label();
			this.label_Region = new System.Windows.Forms.Label();
			this.label_BirthDate = new System.Windows.Forms.Label();
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.textBox2 = new System.Windows.Forms.TextBox();
			this.textBox4 = new System.Windows.Forms.TextBox();
			this.radioButton_GenderMale = new System.Windows.Forms.RadioButton();
			this.radioButton_Female = new System.Windows.Forms.RadioButton();
			this.button_Save = new System.Windows.Forms.Button();
			this.button_Download = new System.Windows.Forms.Button();
			this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
			this.checked_ListBox1_technologies = new System.Windows.Forms.CheckedListBox();
			this.button1_Add_Stud = new System.Windows.Forms.Button();
			this.comboBox1_Countries = new System.Windows.Forms.ComboBox();
			this.button1 = new System.Windows.Forms.Button();
			this.listBox1 = new System.Windows.Forms.ListBox();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.textBox3 = new System.Windows.Forms.TextBox();
			this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
			this.SuspendLayout();
			// 
			// label_Name
			// 
			this.label_Name.AutoSize = true;
			this.label_Name.Location = new System.Drawing.Point(27, 107);
			this.label_Name.Name = "label_Name";
			this.label_Name.Size = new System.Drawing.Size(35, 13);
			this.label_Name.TabIndex = 0;
			this.label_Name.Text = "Name";
			// 
			// label_Surname
			// 
			this.label_Surname.AutoSize = true;
			this.label_Surname.Location = new System.Drawing.Point(27, 140);
			this.label_Surname.Name = "label_Surname";
			this.label_Surname.Size = new System.Drawing.Size(49, 13);
			this.label_Surname.TabIndex = 1;
			this.label_Surname.Text = "Surname";
			// 
			// label_City
			// 
			this.label_City.AutoSize = true;
			this.label_City.Location = new System.Drawing.Point(27, 193);
			this.label_City.Name = "label_City";
			this.label_City.Size = new System.Drawing.Size(24, 13);
			this.label_City.TabIndex = 2;
			this.label_City.Text = "City";
			// 
			// label_Region
			// 
			this.label_Region.AutoSize = true;
			this.label_Region.Location = new System.Drawing.Point(27, 167);
			this.label_Region.Name = "label_Region";
			this.label_Region.Size = new System.Drawing.Size(41, 13);
			this.label_Region.TabIndex = 3;
			this.label_Region.Text = "Region";
			// 
			// label_BirthDate
			// 
			this.label_BirthDate.AutoSize = true;
			this.label_BirthDate.Location = new System.Drawing.Point(27, 219);
			this.label_BirthDate.Name = "label_BirthDate";
			this.label_BirthDate.Size = new System.Drawing.Size(51, 13);
			this.label_BirthDate.TabIndex = 4;
			this.label_BirthDate.Text = "BirthDate";
			// 
			// textBox1
			// 
			this.textBox1.Location = new System.Drawing.Point(85, 100);
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new System.Drawing.Size(186, 20);
			this.textBox1.TabIndex = 5;
			// 
			// textBox2
			// 
			this.textBox2.Location = new System.Drawing.Point(85, 133);
			this.textBox2.Name = "textBox2";
			this.textBox2.Size = new System.Drawing.Size(186, 20);
			this.textBox2.TabIndex = 6;
			// 
			// textBox4
			// 
			this.textBox4.Location = new System.Drawing.Point(85, 186);
			this.textBox4.Name = "textBox4";
			this.textBox4.Size = new System.Drawing.Size(186, 20);
			this.textBox4.TabIndex = 8;
			// 
			// radioButton_GenderMale
			// 
			this.radioButton_GenderMale.AutoSize = true;
			this.radioButton_GenderMale.Location = new System.Drawing.Point(87, 238);
			this.radioButton_GenderMale.Name = "radioButton_GenderMale";
			this.radioButton_GenderMale.Size = new System.Drawing.Size(48, 17);
			this.radioButton_GenderMale.TabIndex = 10;
			this.radioButton_GenderMale.TabStop = true;
			this.radioButton_GenderMale.Text = "Male";
			this.radioButton_GenderMale.UseVisualStyleBackColor = true;
			// 
			// radioButton_Female
			// 
			this.radioButton_Female.AutoSize = true;
			this.radioButton_Female.Location = new System.Drawing.Point(187, 238);
			this.radioButton_Female.Name = "radioButton_Female";
			this.radioButton_Female.Size = new System.Drawing.Size(59, 17);
			this.radioButton_Female.TabIndex = 11;
			this.radioButton_Female.TabStop = true;
			this.radioButton_Female.Text = "Female";
			this.radioButton_Female.UseVisualStyleBackColor = true;
			// 
			// button_Save
			// 
			this.button_Save.Location = new System.Drawing.Point(466, 405);
			this.button_Save.Name = "button_Save";
			this.button_Save.Size = new System.Drawing.Size(86, 23);
			this.button_Save.TabIndex = 20;
			this.button_Save.Text = "SAVE";
			this.button_Save.UseVisualStyleBackColor = true;
			this.button_Save.Click += new System.EventHandler(this.button_Save_Click);
			// 
			// button_Download
			// 
			this.button_Download.Location = new System.Drawing.Point(318, 405);
			this.button_Download.Name = "button_Download";
			this.button_Download.Size = new System.Drawing.Size(86, 23);
			this.button_Download.TabIndex = 21;
			this.button_Download.Text = "LOAD";
			this.button_Download.UseVisualStyleBackColor = true;
			this.button_Download.Click += new System.EventHandler(this.button_Download_Click);
			// 
			// dateTimePicker1
			// 
			this.dateTimePicker1.Location = new System.Drawing.Point(86, 212);
			this.dateTimePicker1.Name = "dateTimePicker1";
			this.dateTimePicker1.Size = new System.Drawing.Size(185, 20);
			this.dateTimePicker1.TabIndex = 23;
			// 
			// checked_ListBox1_technologies
			// 
			this.checked_ListBox1_technologies.FormattingEnabled = true;
			this.checked_ListBox1_technologies.Location = new System.Drawing.Point(86, 290);
			this.checked_ListBox1_technologies.Name = "checked_ListBox1_technologies";
			this.checked_ListBox1_technologies.Size = new System.Drawing.Size(190, 109);
			this.checked_ListBox1_technologies.TabIndex = 24;
			// 
			// button1_Add_Stud
			// 
			this.button1_Add_Stud.Location = new System.Drawing.Point(86, 405);
			this.button1_Add_Stud.Name = "button1_Add_Stud";
			this.button1_Add_Stud.Size = new System.Drawing.Size(80, 23);
			this.button1_Add_Stud.TabIndex = 27;
			this.button1_Add_Stud.Text = "Add stud.";
			this.toolTip1.SetToolTip(this.button1_Add_Stud, "adding new created member");
			this.button1_Add_Stud.UseVisualStyleBackColor = true;
			this.button1_Add_Stud.Click += new System.EventHandler(this.button1_Add_Stud_Click);
			// 
			// comboBox1_Countries
			// 
			this.comboBox1_Countries.FormattingEnabled = true;
			this.comboBox1_Countries.Items.AddRange(new object[] {
            "Poland",
            "Ukraine",
            "Hungary",
            "Germany",
            "Chehia"});
			this.comboBox1_Countries.Location = new System.Drawing.Point(85, 159);
			this.comboBox1_Countries.Name = "comboBox1_Countries";
			this.comboBox1_Countries.Size = new System.Drawing.Size(186, 21);
			this.comboBox1_Countries.TabIndex = 30;
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(190, 405);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(86, 23);
			this.button1.TabIndex = 31;
			this.button1.Text = "Edit";
			this.button1.UseVisualStyleBackColor = true;
			// 
			// listBox1
			// 
			this.listBox1.FormattingEnabled = true;
			this.listBox1.Location = new System.Drawing.Point(318, 70);
			this.listBox1.Name = "listBox1";
			this.listBox1.Size = new System.Drawing.Size(234, 329);
			this.listBox1.TabIndex = 32;
			this.listBox1.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label1.Location = new System.Drawing.Point(83, 269);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(109, 18);
			this.label1.TabIndex = 33;
			this.label1.Text = "Technologies";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label2.Location = new System.Drawing.Point(376, 30);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(105, 18);
			this.label2.TabIndex = 34;
			this.label2.Text = "Student\'s list";
			this.label2.Click += new System.EventHandler(this.label2_Click);
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(27, 76);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(18, 13);
			this.label3.TabIndex = 35;
			this.label3.Text = "ID";
			// 
			// textBox3
			// 
			this.textBox3.Location = new System.Drawing.Point(85, 69);
			this.textBox3.Name = "textBox3";
			this.textBox3.Size = new System.Drawing.Size(186, 20);
			this.textBox3.TabIndex = 36;
			// 
			// toolTip1
			// 
			this.toolTip1.AutomaticDelay = 300;
			this.toolTip1.IsBalloon = true;
			// 
			// PROFILE
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
			this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.ClientSize = new System.Drawing.Size(600, 597);
			this.Controls.Add(this.textBox3);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.listBox1);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.comboBox1_Countries);
			this.Controls.Add(this.button1_Add_Stud);
			this.Controls.Add(this.checked_ListBox1_technologies);
			this.Controls.Add(this.dateTimePicker1);
			this.Controls.Add(this.button_Download);
			this.Controls.Add(this.button_Save);
			this.Controls.Add(this.radioButton_Female);
			this.Controls.Add(this.radioButton_GenderMale);
			this.Controls.Add(this.textBox4);
			this.Controls.Add(this.textBox2);
			this.Controls.Add(this.textBox1);
			this.Controls.Add(this.label_BirthDate);
			this.Controls.Add(this.label_Region);
			this.Controls.Add(this.label_City);
			this.Controls.Add(this.label_Surname);
			this.Controls.Add(this.label_Name);
			this.ForeColor = System.Drawing.Color.Black;
			this.Name = "PROFILE";
			this.Text = "PROFILE";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label label_Name;
		private System.Windows.Forms.Label label_Surname;
		private System.Windows.Forms.Label label_City;
		private System.Windows.Forms.Label label_Region;
		private System.Windows.Forms.Label label_BirthDate;
		private System.Windows.Forms.TextBox textBox1;
		private System.Windows.Forms.TextBox textBox2;
		private System.Windows.Forms.TextBox textBox4;
		private System.Windows.Forms.RadioButton radioButton_GenderMale;
		private System.Windows.Forms.RadioButton radioButton_Female;
		private System.Windows.Forms.Button button_Save;
		private System.Windows.Forms.Button button_Download;
		private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.CheckedListBox checked_ListBox1_technologies;
        private System.Windows.Forms.Button button1_Add_Stud;
        private System.Windows.Forms.ComboBox comboBox1_Countries;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}

