﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _21._10._19_RunAwayButton
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
		}

		private void Button1_Click(object sender, EventArgs e)
		{
			MessageBox.Show("We know it !!!");
		}

		private void Form1_MouseMove(object sender, MouseEventArgs e)
		{
			label1.Text = $"X: {e.X.ToString()}  Y: {e.Y.ToString()}";
		}

		private void button2_MouseMove(object sender, MouseEventArgs e)
		{
			Point current = this.PointToClient(Cursor.Position);
			Point location = button2.Location;
			int new_X = location.X;
			int new_Y = location.Y;

			// moovement right
			if (current.X < location.X + 10)
			{ new_X += 10; }

			// moovement left
			if (current.X + 10 > location.X + button2.Width)
			{ new_X += -10; }

			if (new_X < 5)
			{ new_X = 5; }

			if (new_X > this.Width - button2.Width - 25)
			{ new_X = this.Width - button2.Width - 25; }

			// moovement down
			if (current.Y < location.Y + 10)
			{ new_Y += 10; }

			// moovement up
			if (current.Y + 10 > location.Y + button2.Height)
			{ new_Y += -10; }

			if (new_Y < 10)
			{ new_Y = 10; }

			if (new_Y > this.Height - button2.Height - 45)
			{ new_Y = this.Height - button2.Height - 45; }

			if (new_Y > this.Height - button2.Height - 50)
			{ new_Y = this.Height - button2.Height - 80; }

			if (new_X > this.Width - button2.Width - 26)
			{ new_X += -100; }

			if (new_Y < 11)
			{ new_Y += 50; }

			if (new_X < 6)
			{ new_X += 100; }

			button2.Location = new Point(new_X, new_Y);
		}

		private void button2_Click(object sender, EventArgs e)
		{
			MessageBox.Show("We know it !!!");
		}
	}
}
