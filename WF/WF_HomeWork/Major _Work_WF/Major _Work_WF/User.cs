﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Major__Work_WF
{
	public class User
	{
		public string UserName { get; set; }
		public string UserPasswd { get; set; }
		public string UserMail { get; set; }

		public User(string name, string passwd, string mail)
		{
			UserName = name;
			UserPasswd = passwd;
			UserMail = mail;
		}

	}
}
