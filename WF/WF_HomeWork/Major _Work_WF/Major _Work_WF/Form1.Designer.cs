﻿namespace Major__Work_WF
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
			this.pictureBox_user = new System.Windows.Forms.PictureBox();
			this.textBox_UserName = new System.Windows.Forms.TextBox();
			this.textBox_Password = new System.Windows.Forms.TextBox();
			this.pictureBox_Password = new System.Windows.Forms.PictureBox();
			this.textBox_Email = new System.Windows.Forms.TextBox();
			this.pictureBox_email = new System.Windows.Forms.PictureBox();
			this.button1 = new System.Windows.Forms.Button();
			this.panel1 = new System.Windows.Forms.Panel();
			this.panel2 = new System.Windows.Forms.Panel();
			this.panel3 = new System.Windows.Forms.Panel();
			this.pictureBox_Effort = new System.Windows.Forms.PictureBox();
			this.button2 = new System.Windows.Forms.Button();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox_user)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox_Password)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox_email)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox_Effort)).BeginInit();
			this.SuspendLayout();
			// 
			// pictureBox_user
			// 
			this.pictureBox_user.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(36)))), ((int)(((byte)(49)))));
			this.pictureBox_user.Enabled = false;
			this.pictureBox_user.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox_user.Image")));
			this.pictureBox_user.Location = new System.Drawing.Point(29, 178);
			this.pictureBox_user.Name = "pictureBox_user";
			this.pictureBox_user.Size = new System.Drawing.Size(33, 29);
			this.pictureBox_user.TabIndex = 1;
			this.pictureBox_user.TabStop = false;
			// 
			// textBox_UserName
			// 
			this.textBox_UserName.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(36)))), ((int)(((byte)(49)))));
			this.textBox_UserName.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.textBox_UserName.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.textBox_UserName.ForeColor = System.Drawing.Color.DimGray;
			this.textBox_UserName.Location = new System.Drawing.Point(68, 185);
			this.textBox_UserName.Multiline = true;
			this.textBox_UserName.Name = "textBox_UserName";
			this.textBox_UserName.Size = new System.Drawing.Size(218, 29);
			this.textBox_UserName.TabIndex = 2;
			this.textBox_UserName.Text = "username";
			this.textBox_UserName.Click += new System.EventHandler(this.textBox_UserName_Click);
			// 
			// textBox_Password
			// 
			this.textBox_Password.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(36)))), ((int)(((byte)(49)))));
			this.textBox_Password.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.textBox_Password.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.textBox_Password.ForeColor = System.Drawing.SystemColors.GrayText;
			this.textBox_Password.Location = new System.Drawing.Point(68, 235);
			this.textBox_Password.Multiline = true;
			this.textBox_Password.Name = "textBox_Password";
			this.textBox_Password.Size = new System.Drawing.Size(218, 29);
			this.textBox_Password.TabIndex = 4;
			this.textBox_Password.Text = "Password";
			this.textBox_Password.Click += new System.EventHandler(this.textBox_Password_Click);
			// 
			// pictureBox_Password
			// 
			this.pictureBox_Password.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(36)))), ((int)(((byte)(49)))));
			this.pictureBox_Password.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox_Password.Image")));
			this.pictureBox_Password.Location = new System.Drawing.Point(29, 228);
			this.pictureBox_Password.Name = "pictureBox_Password";
			this.pictureBox_Password.Size = new System.Drawing.Size(33, 29);
			this.pictureBox_Password.TabIndex = 3;
			this.pictureBox_Password.TabStop = false;
			// 
			// textBox_Email
			// 
			this.textBox_Email.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(36)))), ((int)(((byte)(49)))));
			this.textBox_Email.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.textBox_Email.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.textBox_Email.ForeColor = System.Drawing.SystemColors.GrayText;
			this.textBox_Email.Location = new System.Drawing.Point(68, 283);
			this.textBox_Email.Multiline = true;
			this.textBox_Email.Name = "textBox_Email";
			this.textBox_Email.Size = new System.Drawing.Size(218, 29);
			this.textBox_Email.TabIndex = 6;
			this.textBox_Email.Text = "Email";
			this.textBox_Email.Click += new System.EventHandler(this.textBox_Email_Click);
			// 
			// pictureBox_email
			// 
			this.pictureBox_email.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(36)))), ((int)(((byte)(49)))));
			this.pictureBox_email.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox_email.Image")));
			this.pictureBox_email.Location = new System.Drawing.Point(29, 276);
			this.pictureBox_email.Name = "pictureBox_email";
			this.pictureBox_email.Size = new System.Drawing.Size(33, 29);
			this.pictureBox_email.TabIndex = 5;
			this.pictureBox_email.TabStop = false;
			// 
			// button1
			// 
			this.button1.BackColor = System.Drawing.Color.DodgerBlue;
			this.button1.Cursor = System.Windows.Forms.Cursors.Default;
			this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.button1.Font = new System.Drawing.Font("MS Reference Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.button1.Location = new System.Drawing.Point(29, 350);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(275, 34);
			this.button1.TabIndex = 7;
			this.button1.Text = "Sign In";
			this.button1.UseVisualStyleBackColor = false;
			this.button1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.button1_MouseClick);
			// 
			// panel1
			// 
			this.panel1.BackColor = System.Drawing.Color.White;
			this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.panel1.Location = new System.Drawing.Point(29, 213);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(275, 1);
			this.panel1.TabIndex = 8;
			// 
			// panel2
			// 
			this.panel2.BackColor = System.Drawing.Color.White;
			this.panel2.Location = new System.Drawing.Point(29, 263);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(275, 1);
			this.panel2.TabIndex = 9;
			// 
			// panel3
			// 
			this.panel3.BackColor = System.Drawing.Color.White;
			this.panel3.Location = new System.Drawing.Point(29, 311);
			this.panel3.Name = "panel3";
			this.panel3.Size = new System.Drawing.Size(275, 1);
			this.panel3.TabIndex = 10;
			// 
			// pictureBox_Effort
			// 
			this.pictureBox_Effort.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(36)))), ((int)(((byte)(49)))));
			this.pictureBox_Effort.ErrorImage = null;
			this.pictureBox_Effort.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox_Effort.Image")));
			this.pictureBox_Effort.Location = new System.Drawing.Point(120, 36);
			this.pictureBox_Effort.Name = "pictureBox_Effort";
			this.pictureBox_Effort.Size = new System.Drawing.Size(102, 107);
			this.pictureBox_Effort.TabIndex = 13;
			this.pictureBox_Effort.TabStop = false;
			// 
			// button2
			// 
			this.button2.BackColor = System.Drawing.Color.DodgerBlue;
			this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.button2.Font = new System.Drawing.Font("MS Reference Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.button2.ForeColor = System.Drawing.SystemColors.ControlText;
			this.button2.Location = new System.Drawing.Point(29, 390);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(275, 36);
			this.button2.TabIndex = 14;
			this.button2.Text = "Registrate";
			this.button2.UseVisualStyleBackColor = false;
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(36)))), ((int)(((byte)(49)))));
			this.ClientSize = new System.Drawing.Size(327, 453);
			this.Controls.Add(this.button2);
			this.Controls.Add(this.pictureBox_Effort);
			this.Controls.Add(this.panel3);
			this.Controls.Add(this.panel2);
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.textBox_Email);
			this.Controls.Add(this.pictureBox_email);
			this.Controls.Add(this.textBox_Password);
			this.Controls.Add(this.pictureBox_Password);
			this.Controls.Add(this.textBox_UserName);
			this.Controls.Add(this.pictureBox_user);
			this.Name = "Form1";
			this.Text = "Form1";
			((System.ComponentModel.ISupportInitialize)(this.pictureBox_user)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox_Password)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox_email)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox_Effort)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion
		private System.Windows.Forms.PictureBox pictureBox_user;
		private System.Windows.Forms.TextBox textBox_UserName;
		private System.Windows.Forms.TextBox textBox_Password;
		private System.Windows.Forms.PictureBox pictureBox_Password;
		private System.Windows.Forms.TextBox textBox_Email;
		private System.Windows.Forms.PictureBox pictureBox_email;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.Panel panel3;
		private System.Windows.Forms.PictureBox pictureBox_Effort;
		private System.Windows.Forms.Button button2;
	}
}

