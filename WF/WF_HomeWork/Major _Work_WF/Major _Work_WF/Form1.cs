﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Major__Work_WF
{
	public partial class Form1 : Form
	{
		Form2 Form2;

		public User NewUser { get; set; }
		private string UsName { get; set; }
		private string UsPasswd { get; set; }
		private string UsMail { get; set; }
		

		public Form1()
		{
			InitializeComponent();
			Form2 = new Form2();

			textBox_UserName.Select(0, 0);
			textBox_Password.Select(0, 0);
			textBox_Email.Select(0, 0);

			pictureBox_user.Enabled = false;
			pictureBox_Password.Enabled = false;
			pictureBox_email.Enabled = false;
		}

		#region FieldStateController
		private void FieldStateController()
        {
			if (pictureBox_user.Enabled == true)
			{
				string path = Environment.CurrentDirectory;
				path = Path.Combine(path, @"..\..\images\userBlue.png");
				pictureBox_user.Image = Image.FromFile(path);

				string path1 = Environment.CurrentDirectory;
				path1 = Path.Combine(path1, @"..\..\images\locWhite.png");
				pictureBox_Password.Image = Image.FromFile(path1);

				string path2 = Environment.CurrentDirectory;
				path2 = Path.Combine(path2, @"..\..\images\mailWhite.png");
				pictureBox_Password.Image = Image.FromFile(path1);


			}
			else if (pictureBox_Password.Enabled == true)
			{
				string path = Environment.CurrentDirectory;
				path = Path.Combine(path, @"..\..\images\lockBlue.png");
				pictureBox_Password.Image = Image.FromFile(path);

				string path1 = Environment.CurrentDirectory;
				path1 = Path.Combine(path1, @"..\..\images\userWhite.png");
				pictureBox_user.Image = Image.FromFile(path1);

				string path2 = Environment.CurrentDirectory;
				path2 = Path.Combine(path2, @"..\..\images\mailWhite.png");
				pictureBox_email.Image = Image.FromFile(path2);
			}
			else if (pictureBox_email.Enabled == true)
			{
				string path = Environment.CurrentDirectory;
				path = Path.Combine(path, @"..\..\images\mailBlue.png");
				pictureBox_email.Image = Image.FromFile(path);

				string path1 = Environment.CurrentDirectory;
				path1 = Path.Combine(path1, @"..\..\images\userWhite.png");
				pictureBox_user.Image = Image.FromFile(path1);

				string path2 = Environment.CurrentDirectory;
				path2 = Path.Combine(path2, @"..\..\images\locWhite.png");
				pictureBox_Password.Image = Image.FromFile(path2);
			}
        }
		#endregion

		private void textBox_UserName_Click(object sender, EventArgs e)
		{
			
			pictureBox_user.Enabled = true;
			pictureBox_Password.Enabled = false;
			pictureBox_email.Enabled = false;
            FieldStateController();

			textBox_UserName.Clear();

			textBox_UserName.ForeColor = Color.White;
			
		}	

		private void textBox_Password_Click(object sender, EventArgs e)
		{
			pictureBox_Password.Enabled = true;
			pictureBox_user.Enabled = false;
			pictureBox_email.Enabled = false;

			FieldStateController();
			textBox_Password.Clear();

			textBox_Password.ForeColor = Color.White;
			
		}

        private void textBox_Email_Click(object sender, EventArgs e)
        {
			pictureBox_email.Enabled = true;
			pictureBox_Password.Enabled = false;
			pictureBox_user.Enabled = false;

			FieldStateController();
			textBox_Email.Clear();

			textBox_Email.ForeColor = Color.White;
			
		}

		private User NewUserCretor()
		{
			UsName = textBox_UserName.Text;
			UsPasswd = textBox_Password.Text;
			UsMail = textBox_Email.Text;

			if (UsName.Length > 0 && UsPasswd.Length > 0 && UsMail.Length > 0)
				NewUser = new User(UsName, UsPasswd, UsMail);
			else
			{
				
				
				
			}
			return NewUser;			
		}

		private void button1_MouseClick(object sender, MouseEventArgs e)
		{
			NewUserCretor();
			if (NewUser != null)
			{
				// check if such user exist (Dictionary)
				if (Form2.ShowDialog() == DialogResult.OK)
				{ }
			}
			else { MessageBox.Show("uncorrect filled data"); }
				

		}
	}
}
