using System;
using System.Collections.Generic;

namespace EF_CRUD_Users_17._02._20
{   
    public partial class UsersTable
    {
        public int Id { get; set; }
        public string UsLogin { get; set; }
        public string UsPasswdord { get; set; }
        public string UsAddress { get; set; }
        public string UsPhone { get; set; }
        public bool UsRole { get; set; }
    }
}
