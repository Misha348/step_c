﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EF_CRUD_Users_17._02._20
{
	public class Authorization
	{
		private UsersEntities ctx { get; set; }
		private bool _currUserRoleIsAdmin { get; set; }
		private bool _invalidLogin { get; set; }
		private bool _invalidPassword { get; set; }

	

		public Authorization()
		{
			ctx = new UsersEntities();
			_currUserRoleIsAdmin = false;
			_invalidLogin = true;
			_invalidPassword = true;
		}

		public bool GetUsersRole()
		{
			while (_invalidLogin)
			{
				Console.Write("Enter login: ");
				string login = Console.ReadLine();
				var currLog = ctx.UsersTables.FirstOrDefault(l => l.UsLogin == login);

				if (currLog != null)
				{
					while (_invalidPassword)
					{
						Console.Write("Enter password: ");
						string password = Console.ReadLine();
						var thisLogPasswd = currLog.UsPasswdord;
						if (thisLogPasswd == password)
						{
							_currUserRoleIsAdmin = currLog.UsRole;
							_invalidPassword = false;
							_invalidLogin = false;
						}
						else
							Console.WriteLine("wrong password");
					}
				}
				else
					Console.WriteLine("login not exist");
			}
			_invalidPassword = true;
			_invalidLogin = true;
			return _currUserRoleIsAdmin;
		}
	}
}
