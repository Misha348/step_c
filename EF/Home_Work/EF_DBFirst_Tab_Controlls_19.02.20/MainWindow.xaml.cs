﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace EF_DbFirst_TabControlls
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		UniversityEntities ctx = new UniversityEntities();
		public static DataGrid dataGrid;
		public static DataGrid dataGrid_1;
		public static DataGrid dataGrid_2;
		public MainWindow()
		{
			InitializeComponent();
			Load();
		}

		private void Load()
		{
			SpecializTable.ItemsSource = ctx.Specializations.ToList();
			dataGrid = SpecializTable;

			StudTable.ItemsSource = ctx.Students.ToList();
			dataGrid_1 = StudTable;

			SubjTable.ItemsSource = ctx.Subjects.ToList();
			dataGrid_2 = SubjTable;
		}

		#region subjects actions
		private void Subj_InsertBt_Click(object sender, RoutedEventArgs e)
		{
			INSERT_SUBJECT InsSubj = new INSERT_SUBJECT();
			InsSubj.ShowDialog();
		}
		private void Subj_UpdateBt_Click(object sender, RoutedEventArgs e)
		{
			int id = (dataGrid_2.SelectedItem as Subject).Id;
			UPDATA_SUBJECT upSubj = new UPDATA_SUBJECT(id);
			upSubj.ShowDialog();			
		}
		private void Subj_DeleteBt_Click(object sender, RoutedEventArgs e)
		{
			int id = (dataGrid_2.SelectedItem as Subject).Id;
			var delSubj = ctx.Subjects.Where(sb => sb.Id == id).Single();
			ctx.Subjects.Remove(delSubj);
			ctx.SaveChanges();
			dataGrid_2.ItemsSource = ctx.Subjects.ToList();
		}
		#endregion

		#region Stud actions
		private void St_InsertBt_Click(object sender, RoutedEventArgs e)
		{
			INSERT_STUDENT InsStud = new INSERT_STUDENT();
			InsStud.ShowDialog();
		}

		private void St_UpdateBt_Click(object sender, RoutedEventArgs e)
		{
			int id = (dataGrid_1.SelectedItem as Student).Id;
			UPDATE_STUDENT upStdnt = new UPDATE_STUDENT(id);
			upStdnt.ShowDialog();		
		}

		private void St_DeleteBt_Click(object sender, RoutedEventArgs e)
		{
			int id = (dataGrid_1.SelectedItem as Student).Id;
			var delStud = ctx.Students.Where(st => st.Id == id).Single();
			ctx.Students.Remove(delStud);
			ctx.SaveChanges();
			dataGrid_1.ItemsSource = ctx.Students.ToList();
		}
		#endregion

		#region Specializ action buttons
		private void InsertBt_Click(object sender, RoutedEventArgs e)
		{
			INSERT_SPECIALIZATION InsSpecializ = new INSERT_SPECIALIZATION();
			InsSpecializ.ShowDialog();
		}
		private void UpdateBt_Click(object sender, RoutedEventArgs e)
		{
			int id = (dataGrid.SelectedItem as Specialization).Id;
			UPDATE_SPECIALIZATION UpSpec = new UPDATE_SPECIALIZATION(id);			
			UpSpec.ShowDialog();			
		}
		private void DeleteBt_Click(object sender, RoutedEventArgs e)
		{
			int id = (dataGrid.SelectedItem as Specialization).Id;
			var delSpec = ctx.Specializations.Where(s => s.Id == id).Single();
			ctx.Specializations.Remove(delSpec);
			ctx.SaveChanges();
			dataGrid.ItemsSource = ctx.Specializations.ToList();
		}



		#endregion

	
	}
}
