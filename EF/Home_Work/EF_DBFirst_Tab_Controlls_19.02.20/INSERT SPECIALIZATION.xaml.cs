﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace EF_DbFirst_TabControlls
{
	/// <summary>
	/// Interaction logic for INSERT_SPECIALIZATION.xaml
	/// </summary>
	public partial class INSERT_SPECIALIZATION : Window
	{
		UniversityEntities ctx = new UniversityEntities();
		public INSERT_SPECIALIZATION()
		{
			InitializeComponent();
		}

		private void Button_Click(object sender, RoutedEventArgs e)
		{
			Specialization newSpec = new Specialization()
			{
				Name = specNameTxtBox.Text
			};
			ctx.Specializations.Add(newSpec);
			ctx.SaveChanges();
			MainWindow.dataGrid.ItemsSource = ctx.Specializations.ToList();
			this.Hide();
		}
	}
}
