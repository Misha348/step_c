﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace EF_DbFirst_TabControlls
{
	/// <summary>
	/// Interaction logic for INSERT_SUBJECT.xaml
	/// </summary>
	public partial class INSERT_SUBJECT : Window
	{
		UniversityEntities ctx = new UniversityEntities();
		public INSERT_SUBJECT()
		{
			InitializeComponent();
		}

		private void Subj_InsBt_Click(object sender, RoutedEventArgs e)
		{
			Subject newSbj = new Subject()
			{
				Name = subjNameTxtBox.Text,
				HoursCount = Convert.ToInt32(hoursTxtBox.Text),
				SpecializationId = Convert.ToInt32(subjSpecIdTxtBox.Text)
			};
			ctx.Subjects.Add(newSbj);
			ctx.SaveChanges();
			MainWindow.dataGrid_2.ItemsSource = ctx.Subjects.ToList();
			this.Hide();
		}
	}
}
