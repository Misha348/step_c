﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace EF_DbFirst_TabControlls
{
	/// <summary>
	/// Interaction logic for UPDATE_SPECIALIZATION.xaml
	/// </summary>
	public partial class UPDATE_SPECIALIZATION : Window
	{
		UniversityEntities ctx = new UniversityEntities();
		int Id = 0;
		public UPDATE_SPECIALIZATION(int specId)
		{
			InitializeComponent();
			Id = specId;
		}

		private void Button_Click(object sender, RoutedEventArgs e)
		{
			Specialization upSpecializat = (from s in ctx.Specializations
											where s.Id == Id
											select s).Single();
			upSpecializat.Name = specNameTxtBox.Text;
			ctx.SaveChanges();
			MainWindow.dataGrid.ItemsSource = ctx.Specializations.ToList();
			this.Hide();
		}
	}
}
