﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace EF_DbFirst_TabControlls
{
	/// <summary>
	/// Interaction logic for INSERT_STUDENT.xaml
	/// </summary>
	public partial class INSERT_STUDENT : Window
	{
		UniversityEntities ctx = new UniversityEntities();
		public INSERT_STUDENT()
		{
			InitializeComponent();
		}

		private void St_InsBt_Click(object sender, RoutedEventArgs e)
		{
			Student newStudent = new Student()
			{
				FirstName = studNameTxtBox.Text,
				LastSurname = studSurNameTxtBox.Text,
				Phone = studPhoneTxtBox.Text,
				Email = studEmailTxtBox.Text,
				SpecializationId = Convert.ToInt32(studSpecIdTxtBox.Text)
			};
			ctx.Students.Add(newStudent);
			ctx.SaveChanges();
			MainWindow.dataGrid_1.ItemsSource = ctx.Students.ToList();
			this.Hide();
		}

	
	}
}
