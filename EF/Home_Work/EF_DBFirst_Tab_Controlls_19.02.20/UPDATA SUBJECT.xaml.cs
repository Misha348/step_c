﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace EF_DbFirst_TabControlls
{
	/// <summary>
	/// Interaction logic for UPDATA_SUBJECT.xaml
	/// </summary>
	public partial class UPDATA_SUBJECT : Window
	{
		UniversityEntities ctx = new UniversityEntities();
		int Id = 0;
		public UPDATA_SUBJECT(int subjId)
		{
			InitializeComponent();
			Id = subjId;
		}

		private void Subj_Updt_Bt_Click(object sender, RoutedEventArgs e)
		{
			Subject upSubject = (from sb in ctx.Subjects
								 where sb.Id == Id
								 select sb).Single();
			upSubject.Name = subjNameTxtBox.Text;
			upSubject.HoursCount = Convert.ToInt32(hoursTxtBox.Text);
			upSubject.SpecializationId = Convert.ToInt32(subjSpecIdTxtBox.Text);
			ctx.SaveChanges();
			MainWindow.dataGrid.ItemsSource = ctx.Subjects.ToList();
			this.Hide();		
		}
	}
}
