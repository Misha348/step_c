﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace EF_DbFirst_TabControlls
{
	/// <summary>
	/// Interaction logic for UPDATE_STUDENT.xaml
	/// </summary>
	public partial class UPDATE_STUDENT : Window
	{
		UniversityEntities ctx = new UniversityEntities();
		int Id = 0;
		public UPDATE_STUDENT(int studId)
		{
			InitializeComponent();
			Id = studId;
		}

		private void St_Updt_Bt_Click(object sender, RoutedEventArgs e)
		{
			Student upStud = (from st in ctx.Students
							  where st.Id == Id
							  select st).Single();
			upStud.FirstName = studNameTxtBox.Text;
			upStud.LastSurname = studSurNameTxtBox.Text;
			upStud.Phone = studPhoneTxtBox.Text;
			upStud.Email = studEmailTxtBox.Text;
			upStud.SpecializationId = Convert.ToInt32(studSpecIdTxtBox.Text);
			ctx.SaveChanges();
			MainWindow.dataGrid_1.ItemsSource = ctx.Students.ToList();
			this.Hide();			
		}
	}
}
