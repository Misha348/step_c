using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
namespace CF_Barbers_20._02._20
{

	public class BarberShopContext : DbContext
	{
		
		public BarberShopContext(): base("name=BarberShopContext")
		{ }	

		public virtual DbSet<Possition> Possitions { get; set; }
		public virtual DbSet<Barber> Barbers { get; set; }
		public virtual DbSet<Schedule> Schedules { get; set; }
		public virtual DbSet<Service> Services { get; set; }
		public virtual DbSet<BarberService> BarberServices { get; set; }
		public virtual DbSet<EstimationType> EstimationTypes { get; set; }
		public virtual DbSet<Estimation> Estimations { get; set; }
		public virtual DbSet<Customer> Customers { get; set; }
		public virtual DbSet<Record> Records { get; set; }
		public virtual DbSet<FeedBack> FeedBacks { get; set; }
	}

	public class Possition
	{
        [Key]
        [Required(ErrorMessage = "Id is required")]
        public int Id { get; set; }

        [Required(ErrorMessage = "Name is required")]
        [StringLength(30, ErrorMessage = "Name must contains up to 30 characters")]
        public string Name { get; set; }


		public ICollection<Barber> Barbers { get; set; }
	}

	public class Barber
	{
        [Key]
        [Required(ErrorMessage = "Id is required")]
        public int Id { get; set; }

        [Required(ErrorMessage = "FirstName is required")]
        [StringLength(30, ErrorMessage = "FirstName must contains up to 30 characters")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "LastName is required")]
        [StringLength(30, ErrorMessage = "LastName must contains up to 30 characters")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Gender is required")]
        public string Gender { get; set; }

        [Required(ErrorMessage = "Phone is required")]
        public string Phone { get; set; }

        [Required(ErrorMessage = "Email is required")]
        public string Email { get; set; }
       
        public DateTime? BirthDate { get; set; }

        [Required(ErrorMessage = "Email is required")]
        public DateTime Emloymentdate { get; set; }

        [ForeignKey("Possition")]
        public int PossitionId { get; set; }

        [ForeignKey("PossitionId")]
        public virtual Possition Possition { get; set; }

		public ICollection<Schedule> Schedules { get; set; }
		public ICollection<BarberService> BarberServices { get; set; }
		public ICollection<Estimation> Estimations { get; set; }
		public ICollection<Record> Records { get; set; }
		public ICollection<FeedBack> FeedBacks { get; set; }
		public ICollection<VisitingArchive> VisitingArchives { get; set; }
	}

	public class Schedule
	{
        [Key]
        [Required(ErrorMessage = "Id is required")]
        public int Id { get; set; }

        [Required(ErrorMessage = "Day is required")]
        public DateTime Day { get; set; }

        [Required(ErrorMessage = "StartTime is required")]
        public DateTime StartTime { get; set; }

        [Required(ErrorMessage = "EndTime is required")]
        public DateTime EndTime { get; set; }

        [ForeignKey("Barber")]
        public int BarberId { get; set; }

        [ForeignKey("BarberId")]
        public virtual Barber Barber { get; set; }
	}

	public class Service
	{
        [Key]
        [Required(ErrorMessage = "Id is required")]
        public int Id { get; set; }

        [Required(ErrorMessage = "Name is required")]
        [StringLength(30, ErrorMessage = "Name must contains up to 30 characters")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Price is required")]
        public double Price { get; set; }


		public DateTime? Duration { get; set; }

		public ICollection<BarberService> BarberServices { get; set; }
		public ICollection<VisitingArchive> VisitingArchives { get; set; }
	}

	public class BarberService
	{
        [Key]
        [Required(ErrorMessage = "Id is required")]
        public int Id { get; set; }

        [ForeignKey("Service")]
        public int ServiceId { get; set; }

        [ForeignKey("ServiceId")]
        public virtual Service Service { get; set; }

        [ForeignKey("Barber")]
        public int BarberId { get; set; }

        [ForeignKey("BarberId")]
        public virtual Barber Barber { get; set; }
	}

	public class EstimationType
	{
		public int Id { get; set; }
		public string Type { get; set; }
		public ICollection<Estimation> Estimations { get; set; }
	}

	public class Estimation
	{
        [Key]
        [Required(ErrorMessage = "Id is required")]
        public int Id { get; set; }

        [ForeignKey("Barber")]
        public int BarberId { get; set; }

        [ForeignKey("BarberId")]
        public virtual Barber Barber{ get; set; }

        [ForeignKey("Customer")]
        public int CustomerId { get; set; }

        [ForeignKey("CustomerId")]
        public virtual Customer Customer { get; set; }

        [ForeignKey("EstimationType")]
        public int EstimationTypeId { get; set; }

        [ForeignKey("EstimationTypeId")]
        public virtual EstimationType EstimationType { get; set; }
		public ICollection<VisitingArchive> VisitingArchives { get; set; }

	}

	public class Customer
	{
        [Key]
        [Required(ErrorMessage = "Id is required")]
        public int Id { get; set; }

        [Required(ErrorMessage = "FirstName is required")]
        [StringLength(30, ErrorMessage = "FirstName must contains up to 30 characters")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "LastName is required")]
        [StringLength(30, ErrorMessage = "LastName must contains up to 30 characters")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Phone is required")]
        public string Phone { get; set; }

        [Required(ErrorMessage = "Email is required")]
        public string Email { get; set; }

		public ICollection<Estimation> Estimations { get; set; }
		public ICollection<Record> Record { get; set; }
		public ICollection<FeedBack> FeedBacks { get; set; }
		public ICollection<VisitingArchive> VisitingArchives { get; set; }
	}

	public class Record
	{
        [Key]
        [Required(ErrorMessage = "Id is required")]
        public int Id { get; set; }

        [ForeignKey("Customer")]
        public int CustomerId { get; set; }

        [ForeignKey("CustomerId")]
        public virtual Customer Customer { get; set; }

        [ForeignKey("Barber")]
        public int BarberId { get; set; }

        [ForeignKey("BarberId")]
        public virtual Barber Barber { get; set; }

        [Required(ErrorMessage = "Date is required")]
        public DateTime Date { get; set; }

        [Required(ErrorMessage = "Time is required")]
        public DateTime Time { get; set; }
	}

	public class FeedBack
	{
        [Key]
        [Required(ErrorMessage = "Id is required")]
        public int Id { get; set; }

        [Required(ErrorMessage = "Feedback message is required")]
        [StringLength(250, ErrorMessage = "Feedback message  must contains up to 250 characters")]
        public string Message { get; set; }

        [ForeignKey("Customer")]
        public int CustomerId { get; set; }

        [ForeignKey("CustomerId")]
        public virtual Customer Customer { get; set; }

        [ForeignKey("Barber")]
        public int BarberId { get; set; }

        [ForeignKey("Barber")]
        public virtual Barber Barber { get; set; }
		public ICollection<VisitingArchive> VisitingArchives { get; set; }
	}

	public class VisitingArchive
	{
        [Key]
        [Required(ErrorMessage = "Id is required")]
        public int Id { get; set; }

        [Required(ErrorMessage = "Date is required")]
        public DateTime Date { get; set; }

        [ForeignKey("Customer")]
        public int CustomerId { get; set; }

        [ForeignKey("CustomerId")]
        public virtual Customer Customer { get; set; }

        [ForeignKey("Barber")]
        public int BarberId { get; set; }

        [ForeignKey("BarberId")]
        public virtual Barber Barber { get; set; }

        [ForeignKey("Service")]
        public int ServiceId { get; set; }

        [ForeignKey("ServiceId")]
        public virtual Service Service { get; set; }

        [ForeignKey("Estimation")]
        public  int EstimationId { get; set; }

        [ForeignKey("EstimationId")]
        public virtual Estimation Estimation { get; set; }

        [ForeignKey("FeedBack")]
        public int FeedBackId { get; set; }

        [ForeignKey("FeedBackId")]
        public virtual FeedBack FeedBack { get; set; }
    }
}