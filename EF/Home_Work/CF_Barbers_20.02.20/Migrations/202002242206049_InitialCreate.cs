﻿namespace CF_Barbers_20._02._20.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Barbers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FirstName = c.String(),
                        LastName = c.String(),
                        Gender = c.String(),
                        Phone = c.String(),
                        Email = c.String(),
                        BirthDate = c.DateTime(nullable: false),
                        Emloymentdate = c.DateTime(nullable: false),
                        PossitionId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Possitions", t => t.PossitionId, cascadeDelete: true)
                .Index(t => t.PossitionId);
            
            CreateTable(
                "dbo.BarberServices",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ServiceId = c.Int(nullable: false),
                        BarberId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Barbers", t => t.BarberId, cascadeDelete: true)
                .ForeignKey("dbo.Services", t => t.ServiceId, cascadeDelete: true)
                .Index(t => t.ServiceId)
                .Index(t => t.BarberId);
            
            CreateTable(
                "dbo.Services",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Price = c.Double(nullable: false),
                        Duration = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Estimations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        BarberId = c.Int(nullable: false),
                        CustomerId = c.Int(nullable: false),
                        TypeId = c.Int(nullable: false),
                        EstimationType_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Barbers", t => t.BarberId, cascadeDelete: true)
                .ForeignKey("dbo.Customers", t => t.CustomerId, cascadeDelete: true)
                .ForeignKey("dbo.EstimationTypes", t => t.EstimationType_Id)
                .Index(t => t.BarberId)
                .Index(t => t.CustomerId)
                .Index(t => t.EstimationType_Id);
            
            CreateTable(
                "dbo.Customers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FirstName = c.String(),
                        LastName = c.String(),
                        Phone = c.String(),
                        Email = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.FeedBacks",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Message = c.String(),
                        CustomerId = c.Int(nullable: false),
                        BarberId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Barbers", t => t.BarberId, cascadeDelete: true)
                .ForeignKey("dbo.Customers", t => t.CustomerId, cascadeDelete: true)
                .Index(t => t.CustomerId)
                .Index(t => t.BarberId);
            
            CreateTable(
                "dbo.Records",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CustomerId = c.Int(nullable: false),
                        BarberId = c.Int(nullable: false),
                        Date = c.DateTime(nullable: false),
                        Time = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Barbers", t => t.BarberId, cascadeDelete: true)
                .ForeignKey("dbo.Customers", t => t.CustomerId, cascadeDelete: true)
                .Index(t => t.CustomerId)
                .Index(t => t.BarberId);
            
            CreateTable(
                "dbo.EstimationTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Type = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Possitions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Schedules",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Day = c.DateTime(nullable: false),
                        StartTime = c.DateTime(nullable: false),
                        EndTime = c.DateTime(nullable: false),
                        BarberId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Barbers", t => t.BarberId, cascadeDelete: true)
                .Index(t => t.BarberId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Schedules", "BarberId", "dbo.Barbers");
            DropForeignKey("dbo.Barbers", "PossitionId", "dbo.Possitions");
            DropForeignKey("dbo.Estimations", "EstimationType_Id", "dbo.EstimationTypes");
            DropForeignKey("dbo.Records", "CustomerId", "dbo.Customers");
            DropForeignKey("dbo.Records", "BarberId", "dbo.Barbers");
            DropForeignKey("dbo.FeedBacks", "CustomerId", "dbo.Customers");
            DropForeignKey("dbo.FeedBacks", "BarberId", "dbo.Barbers");
            DropForeignKey("dbo.Estimations", "CustomerId", "dbo.Customers");
            DropForeignKey("dbo.Estimations", "BarberId", "dbo.Barbers");
            DropForeignKey("dbo.BarberServices", "ServiceId", "dbo.Services");
            DropForeignKey("dbo.BarberServices", "BarberId", "dbo.Barbers");
            DropIndex("dbo.Schedules", new[] { "BarberId" });
            DropIndex("dbo.Records", new[] { "BarberId" });
            DropIndex("dbo.Records", new[] { "CustomerId" });
            DropIndex("dbo.FeedBacks", new[] { "BarberId" });
            DropIndex("dbo.FeedBacks", new[] { "CustomerId" });
            DropIndex("dbo.Estimations", new[] { "EstimationType_Id" });
            DropIndex("dbo.Estimations", new[] { "CustomerId" });
            DropIndex("dbo.Estimations", new[] { "BarberId" });
            DropIndex("dbo.BarberServices", new[] { "BarberId" });
            DropIndex("dbo.BarberServices", new[] { "ServiceId" });
            DropIndex("dbo.Barbers", new[] { "PossitionId" });
            DropTable("dbo.Schedules");
            DropTable("dbo.Possitions");
            DropTable("dbo.EstimationTypes");
            DropTable("dbo.Records");
            DropTable("dbo.FeedBacks");
            DropTable("dbo.Customers");
            DropTable("dbo.Estimations");
            DropTable("dbo.Services");
            DropTable("dbo.BarberServices");
            DropTable("dbo.Barbers");
        }
    }
}
