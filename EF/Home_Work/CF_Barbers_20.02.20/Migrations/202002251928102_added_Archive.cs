﻿namespace CF_Barbers_20._02._20.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class added_Archive : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.VisitingArchives",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Date = c.DateTime(nullable: false),
                        CustomerId = c.Int(nullable: false),
                        BarberId = c.Int(nullable: false),
                        ServiceId = c.Int(nullable: false),
                        EstimationId = c.Int(nullable: false),
                        FeedBackId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Barbers", t => t.BarberId, cascadeDelete: true)
                .ForeignKey("dbo.Estimations", t => t.EstimationId, cascadeDelete: true)
                .ForeignKey("dbo.FeedBacks", t => t.FeedBackId, cascadeDelete: true)
                .ForeignKey("dbo.Customers", t => t.CustomerId, cascadeDelete: true)
                .ForeignKey("dbo.Services", t => t.ServiceId, cascadeDelete: true)
                .Index(t => t.CustomerId)
                .Index(t => t.BarberId)
                .Index(t => t.ServiceId)
                .Index(t => t.EstimationId)
                .Index(t => t.FeedBackId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.VisitingArchives", "ServiceId", "dbo.Services");
            DropForeignKey("dbo.VisitingArchives", "CustomerId", "dbo.Customers");
            DropForeignKey("dbo.VisitingArchives", "FeedBackId", "dbo.FeedBacks");
            DropForeignKey("dbo.VisitingArchives", "EstimationId", "dbo.Estimations");
            DropForeignKey("dbo.VisitingArchives", "BarberId", "dbo.Barbers");
            DropIndex("dbo.VisitingArchives", new[] { "FeedBackId" });
            DropIndex("dbo.VisitingArchives", new[] { "EstimationId" });
            DropIndex("dbo.VisitingArchives", new[] { "ServiceId" });
            DropIndex("dbo.VisitingArchives", new[] { "BarberId" });
            DropIndex("dbo.VisitingArchives", new[] { "CustomerId" });
            DropTable("dbo.VisitingArchives");
        }
    }
}
