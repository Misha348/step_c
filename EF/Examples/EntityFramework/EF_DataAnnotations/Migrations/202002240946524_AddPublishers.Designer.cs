// <auto-generated />
namespace EF_DataAnnotations.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class AddPublishers : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AddPublishers));
        
        string IMigrationMetadata.Id
        {
            get { return "202002240946524_AddPublishers"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
