namespace EF_DataAnnotations.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeBioTableName : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.Bios", newName: "AuthorBio");
            AlterColumn("dbo.Authors", "BioId", c => c.Int());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Authors", "BioId", c => c.Int(nullable: false));
            RenameTable(name: "dbo.AuthorBio", newName: "Bios");
        }
    }
}
