namespace EF_DataAnnotations.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Authors",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FirstName = c.String(nullable: false, maxLength: 50),
                        LastName = c.String(nullable: false, maxLength: 50),
                        Country = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.BooksTable",
                c => new
                    {
                        Identity = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Pages = c.Int(nullable: false),
                        Price = c.Int(),
                        AuthorId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Identity)
                .ForeignKey("dbo.Authors", t => t.AuthorId, cascadeDelete: true)
                .Index(t => t.AuthorId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.BooksTable", "AuthorId", "dbo.Authors");
            DropIndex("dbo.BooksTable", new[] { "AuthorId" });
            DropTable("dbo.BooksTable");
            DropTable("dbo.Authors");
        }
    }
}
