namespace EF_DataAnnotations.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddAuthorBio : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Bios",
                c => new
                    {
                        AuthorId = c.Int(nullable: false),
                        Country = c.String(nullable: false),
                        BirthDate = c.DateTime(nullable: false),
                        DeathDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.AuthorId)
                .ForeignKey("dbo.Authors", t => t.AuthorId)
                .Index(t => t.AuthorId);
            
            AddColumn("dbo.Authors", "BioId", c => c.Int());
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Bios", "AuthorId", "dbo.Authors");
            DropIndex("dbo.Bios", new[] { "AuthorId" });
            DropColumn("dbo.Authors", "BioId");
            DropTable("dbo.Bios");
        }
    }
}
