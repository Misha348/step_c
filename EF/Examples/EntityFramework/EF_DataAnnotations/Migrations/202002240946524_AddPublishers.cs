namespace EF_DataAnnotations.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPublishers : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Publishers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.PublisherBooks",
                c => new
                    {
                        Publisher_Id = c.Int(nullable: false),
                        Book_Identity = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Publisher_Id, t.Book_Identity })
                .ForeignKey("dbo.Publishers", t => t.Publisher_Id, cascadeDelete: true)
                .ForeignKey("dbo.BooksTable", t => t.Book_Identity, cascadeDelete: true)
                .Index(t => t.Publisher_Id)
                .Index(t => t.Book_Identity);                        
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PublisherBooks", "Book_Identity", "dbo.BooksTable");
            DropForeignKey("dbo.PublisherBooks", "Publisher_Id", "dbo.Publishers");
            DropIndex("dbo.PublisherBooks", new[] { "Book_Identity" });
            DropIndex("dbo.PublisherBooks", new[] { "Publisher_Id" });
            AlterColumn("dbo.Authors", "BioId", c => c.Int());
            DropTable("dbo.PublisherBooks");
            DropTable("dbo.Publishers");
        }
    }
}
