﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EF_DataAnnotations
{
    class Program
    {
        static void Main(string[] args)
        {
            using (LibraryContext ctx = new LibraryContext())
            {
                ctx.Authors.ToArray();
                ctx.SaveChanges();
            }
        }
    }
}
