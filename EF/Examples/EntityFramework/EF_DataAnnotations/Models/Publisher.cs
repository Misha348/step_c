﻿using System.Collections.Generic;

namespace EF_DataAnnotations
{
    public partial class Publisher
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public virtual ICollection<Book> Books { get; set; }
    }
}