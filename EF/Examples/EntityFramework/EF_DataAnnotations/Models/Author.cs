﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace EF_DataAnnotations
{
    public partial class Author
    {
        [Key]                                                   // primary key
        [Required(ErrorMessage = "Id is required")]             
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]   // identity
        public int Id { get; set; }
        [Required(ErrorMessage = "First Name is required")]
        [StringLength(50, ErrorMessage = "Name must contains up to 50 characters")]
        public string FirstName { get; set; }
        [Required(ErrorMessage = "Last name is required")]
        [StringLength(50, ErrorMessage = "Name must contains up to 50 characters")]        
        public string LastName { get; set; }
        public string Country { get; set; }
        [ForeignKey("Bio")]
        public int? BioId { get; set; }

        [NotMapped] // only on model
        public string FullName { get { return FirstName + " " + LastName; } }

        // Navigation Props
        public virtual ICollection<Book> Books { get; set; }
        [ForeignKey("BioId")]
        public virtual Bio Bio { get; set; }
    }
}