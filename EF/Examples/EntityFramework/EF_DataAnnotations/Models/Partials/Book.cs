﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EF_DataAnnotations
{
    public partial class Book
    {
        public override string ToString()
        {
            return $"Book: {Name} Pages: {PageCount} {(Price == null ? "Price" : Price.ToString())}";
        }
    }

    public partial class Author
    {
        public override string ToString()
        {
            return $"Author: {FirstName} {LastName} {Country ?? "no country"}";
        }
    }
}
