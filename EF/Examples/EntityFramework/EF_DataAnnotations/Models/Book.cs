﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EF_DataAnnotations
{
    [Table("BooksTable")]
    public partial class Book
    {
        // Id BookId
        [Key, Required(ErrorMessage = "Id is required"), DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Identity { get; set; }
        [Required(ErrorMessage = "Name is required")]
        public string Name { get; set; }
        [Column("Pages")]
        public int PageCount { get; set; }
        public int? Price { get; set; }
        public int AuthorId { get; set; }        

        // Navigation Props
        [ForeignKey("AuthorId")]
        public virtual Author Author { get; set; }
        public virtual ICollection<Publisher> Publishers { get; set; }
    }
}