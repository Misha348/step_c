﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace EF_DataAnnotations
{
    [Table("AuthorBio")]
    public partial class Bio
    {
        [Key, ForeignKey("Author")]
        public int AuthorId { get; set; }

        //public int Id { get; set; }
        [Required(ErrorMessage = "Country is required")]
        [DefaultValue("No country")]
        public string Country { get; set; }       
        public DateTime BirthDate { get; set; }
        public DateTime? DeathDate { get; set; }       

        // Navigation Props        
        [ForeignKey("AuthorId")]
        public virtual Author Author { get; set; }
    }
}