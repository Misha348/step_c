using System;
using System.Data.Entity;
using System.Linq;

namespace EF_DataAnnotations
{
    public class LibraryContext : DbContext
    {        
        public LibraryContext()
            : base("name=LibraryContext")
        {
            Database.SetInitializer<LibraryContext>(new CreateDatabaseIfNotExists<LibraryContext>());
        }
        
        public virtual DbSet<Book> Books { get; set; }
        public virtual DbSet<Author> Authors { get; set; }
        public virtual DbSet<Bio> Bios { get; set; }
        public virtual DbSet<Publisher> Publishers { get; set; }
    }

    public class LibraryInitializer : CreateDatabaseIfNotExists<LibraryContext>
    {
        protected override void Seed(LibraryContext context)
        {
            base.Seed(context);

            context.Authors.Add(new Author(){ FirstName = "Gear", LastName = "arga", Country = "Ukr"});
            context.SaveChanges();
        }
    }
}