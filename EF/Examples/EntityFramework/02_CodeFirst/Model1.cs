namespace _02_CodeFirst
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;

    public class Model1 : DbContext
    {
        // Your context has been configured to use a 'Model1' connection string from your application's 
        // configuration file (App.config or Web.config). By default, this connection string targets the 
        // '_02_CodeFirst.Model1' database on your LocalDb instance. 
        // 
        // If you wish to target a different database and/or database provider, modify the 'Model1' 
        // connection string in the application configuration file.
        public Model1()
            : base("name=Model1")
        {
            Database.SetInitializer<Model1>(new CreateDatabaseIfNotExists<Model1>()); // (default)
            // DropCreateDatabaseIfModelChanges<>
            // DropCreateDatabaseAlways<>
        }

        // Add a DbSet for each entity type that you want to include in your model. For more information 
        // on configuring and using a Code First model, see http://go.microsoft.com/fwlink/?LinkId=390109.

        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<Country> Countries { get; set; }
    }

    public class User
    {
        // Primary Key
        // Id id ID
        // UserId USERID userid
        public int Id { get; set; }
        public string Name { get; set; }
        // Foreign Key
        // EntityName + Id
        public int CountryId { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        //public string Login { get; set; }

        // Navigation Properties
        public virtual Country Country { get; set; }
    }

    public class Country
    {
        public int Id { get; set; }
        public string Name { get; set; }

        // Navigation Properties
        public ICollection<User> Users { get; set; }
    }
}