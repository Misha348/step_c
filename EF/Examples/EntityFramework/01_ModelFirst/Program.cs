﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01_ModelFirst
{
    class Program
    {
        static void Main(string[] args)
        {
            using (Model1Container ctx = new Model1Container())
            {
                ctx.Countries.Add(new Country() { Name = "Ukraine" });
                ctx.SaveChanges();

                ctx.Users.Add(new User() { Name = "Bob", CountryId = 1 });
                ctx.SaveChanges();

                var res = ctx.Users;
                foreach (var item in res)
                {
                    Console.WriteLine(item.Id + " " + item.Name + " " + item.Country.Name);
                }
            }
        }
    }
}
