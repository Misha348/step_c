namespace FluentAPI
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;

    public class LibDB : DbContext
    {        
        public LibDB()
            : base("name=LibDB")
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            // Fluent API Code

            ////////////////////// Book
            modelBuilder.Entity<Book>().ToTable("BooksTable").HasKey(b => b.Identity)
                .Property(b => b.Identity)
                    .IsRequired()
                    .HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);

            modelBuilder.Entity<Book>().Property(b => b.Name).HasMaxLength(50).IsRequired();    // non-nullable type
            modelBuilder.Entity<Book>().Property(b => b.PageCount).HasColumnName("Pages");
            modelBuilder.Entity<Book>().Property(b => b.Price).IsOptional();                    // nullable type

            //////////////////////// Author
            modelBuilder.Entity<Author>().Ignore(a => a.FullName); // only in model

            //////////////////////// Bio
            modelBuilder.Entity<Bio>().HasKey(b => b.AuthorId);
            modelBuilder.Entity<Bio>().Property(b => b.BirthDate).IsRequired();
            modelBuilder.Entity<Bio>().Property(b => b.DeathDate).IsOptional();

            //////////////////////// Relations
            //// One to Many
            modelBuilder.Entity<Book>().HasRequired(b => b.Author).WithMany(a => a.Books).HasForeignKey(b => b.AuthorId)
                .WillCascadeOnDelete(false);
            //// Many to Many
            modelBuilder.Entity<Publisher>().HasMany(p => p.Books).WithMany(b => b.Publishers);
            //// One to Zero-One
            //modelBuilder.Entity<Author>().HasOptional(a => a.Bio).WithRequired(b => b.Author);
            modelBuilder.Entity<Bio>().HasRequired(b => b.Author).WithOptional(a => a.Bio).Map(x => x.MapKey("BioId"));
         }

        public virtual DbSet<Book> Books { get; set; }
        public virtual DbSet<Author> Authors { get; set; }
        public virtual DbSet<Bio> Bios { get; set; }
        public virtual DbSet<Publisher> Publishers { get; set; }
    }

    public class Book
    {
        public int Identity { get; set; }
        public string Name { get; set; }
        public int PageCount { get; set; }
        public int? Price { get; set; }        
        public int AuthorId { get; set; }

        // Navigation Props
        public virtual Author Author { get; set; }
        public virtual ICollection<Publisher> Publishers { get; set; }
    }

    public class Author
    {       
        public int Id { get; set; }       
        public string FirstName { get; set; }       
        public string LastName { get; set; }
        public string Country { get; set; }
        public int? BioId { get; set; }

        public string FullName { get { return FirstName + " " + LastName; } }

        // Navigation Props
        public virtual ICollection<Book> Books { get; set; }
        public virtual Bio Bio { get; set; }

    }
    public class Publisher
    {
        public int Id { get; set; }
        public string Name { get; set; }

        // Navigation Props             
        public virtual ICollection<Book> Books { get; set; }
    }

    public class Bio
    {
        public int AuthorId { get; set; }        
        public string Country { get; set; }
        public DateTime BirthDate { get; set; }
        public DateTime? DeathDate { get; set; }

        // Navigation Props               
        public virtual Author Author { get; set; }
    }
}