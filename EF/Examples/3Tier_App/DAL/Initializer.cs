﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    class Initializer : DropCreateDatabaseIfModelChanges<LibDB>
    {
        protected override void Seed(LibDB context)
        {
            base.Seed(context);

            List<Author> authors = new List<Author>()
            {
                new Author() {FirstName = "Tom", LastName = "Dethon", Country = "USA"},
                new Author() {FirstName = "Taras", LastName = "Shevchenko", Country = "Ukraine"},
                new Author() {FirstName = "Lesia", LastName = "Ukrainka", Country = "Ukraine"},
            };

            context.Authors.AddRange(authors);
            context.SaveChanges();
        }
    }
}
