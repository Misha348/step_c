﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace DAL
{
    class BookConfig : EntityTypeConfiguration<Book>
    {
        public BookConfig()
        {
            ////////////////////// Book
            this.ToTable("BooksTable").HasKey(b => b.Identity)
                .Property(b => b.Identity)
                    .IsRequired()
                    .HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);

            this.Property(b => b.Name).HasMaxLength(50).IsRequired();    // non-nullable type
            this.Property(b => b.PageCount).HasColumnName("Pages");
            this.Property(b => b.Price).IsOptional();                    // nullable type

            //// One to Many
            this.HasRequired(b => b.Author).WithMany(a => a.Books).HasForeignKey(b => b.AuthorId)
                .WillCascadeOnDelete(false);
        }
    }
}
