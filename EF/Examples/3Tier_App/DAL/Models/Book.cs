﻿using System.Collections.Generic;

namespace DAL
{
    public class Book
    {
        // Primary key
        public int Identity { get; set; }
        public string Name { get; set; }
        public int PageCount { get; set; }
        public int? Price { get; set; }

        // Foreign key
        public int AuthorId { get; set; }

        // Navigation properties
        public virtual Author Author { get; set; }
        public virtual ICollection<Publisher> Publishers { get; set; }
    }
}