﻿using System;

namespace DAL
{
    public class Bio
    {
        // Primary key and Foreign key
        public int AuthorId { get; set; }
        public string Country { get; set; }
        public DateTime BirthDate { get; set; }
        public DateTime? DeathDate { get; set; }

        // Navigation properties
        public virtual Author Author { get; set; }
    }
}