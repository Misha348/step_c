﻿using System.Collections.Generic;

namespace DAL
{
    public class Author
    {
        // Primary key
        public int Id { get; set; }       
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Country { get; set; }

        // Not mapped with database
        public string FullName { get { return FirstName + " " + LastName; } }

        // Navigation properties
        public virtual ICollection<Book> Books { get; set; }
        public virtual Bio Bio { get; set; }

    }
}