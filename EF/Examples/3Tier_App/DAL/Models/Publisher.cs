﻿using System.Collections.Generic;

namespace DAL
{
    public class Publisher
    {
        // Primary key
        public int Id { get; set; }
        public string Name { get; set; }

        // Navigation properties
        public virtual ICollection<Book> Books { get; set; }
    }
}