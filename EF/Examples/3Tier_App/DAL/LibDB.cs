using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
using System.Linq;

namespace DAL
{
    public class LibDB : DbContext
    {        
        public LibDB()
            : base("name=LibDB")
        {
            Database.SetInitializer(new Initializer());
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            // Fluent API Code

            modelBuilder.Configurations.Add(new BookConfig());
            modelBuilder.Configurations.Add(new BioConfig());

            //////////////////////// Author
            modelBuilder.Entity<Author>().Ignore(a => a.FullName); // only in model

            //////////////////////// Relations
            //// Many to Many
            modelBuilder.Entity<Publisher>().HasMany(p => p.Books).WithMany(b => b.Publishers).Map(
                x => x
                    .MapLeftKey("PubId")
                    .MapRightKey("BookId")
                    .ToTable("PublisherBooks")
            );            
        }

        public virtual DbSet<Book> Books { get; set; }
        public virtual DbSet<Author> Authors { get; set; }
        public virtual DbSet<Bio> Bios { get; set; }
        public virtual DbSet<Publisher> Publishers { get; set; }
    }   
}