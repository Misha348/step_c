﻿using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    // DTO (Data Transfer Object), old name - POCO (Plain Old Class Object) = wrapper class
    public class AuthorDTO
    {       
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Country { get; set; }
        
        public string FullName { get { return FirstName + " " + LastName; } }
    }

    public class BookDTO
    {        
        public int Id { get; set; }
        public string Name { get; set; }
        public int PageCount { get; set; }
        public int? Price { get; set; }

        // Foreign key
        public int AuthorId { get; set; }
        public override string ToString()
        {
            return $"Book: {Name} with {PageCount} pages";
        }
    }

    public class BLLClass
    {
        private DALClass _dal;
        public BLLClass()
        {
            _dal = new DALClass();
        }

        public IEnumerable<AuthorDTO> GetAllAuthors()
        {            
            var result = _dal.GetAllAuthors().Select(a => new AuthorDTO()
            {
                Id = a.Id,
                FirstName = a.FirstName,
                LastName = a.LastName,
                Country = a.Country
            });

            foreach (var a in result)
            {
                yield return new AuthorDTO()
                {
                    Id = a.Id,
                    FirstName = a.FirstName,
                    LastName = a.LastName,
                    Country = a.Country
                };
            }
        }

        public IEnumerable<BookDTO> GetBooksByAuthorId(int authorId)
        {
            return _dal.GetBooksByAuthorId(authorId).Select(a => new BookDTO()
            {
                Id = a.Identity,
                Name = a.Name,
                PageCount = a.PageCount,
                Price = a.Price,
                AuthorId = a.AuthorId
            });
        }
    }
}
