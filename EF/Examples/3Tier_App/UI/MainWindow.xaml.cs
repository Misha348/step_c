﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BLL;
namespace UI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private BLL.BLLClass _bll;
        public MainWindow()
        {
            InitializeComponent();
            _bll = new BLL.BLLClass();

            authorBox.ItemsSource = _bll.GetAllAuthors();
            authorBox.DisplayMemberPath = nameof(BLL.AuthorDTO.FullName);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            AuthorDTO author = authorBox.SelectedItem as BLL.AuthorDTO;

            bookList.ItemsSource = _bll.GetBooksByAuthorId(author.Id);
        }
    }
}
