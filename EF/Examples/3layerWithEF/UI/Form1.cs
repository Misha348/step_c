﻿using BLL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UI
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            var bll = new BLLclass();
            bll.AddNewUser(new UserDTO()
            {
                Id = 1,
                Name = "NameFirst",
                Surname = "SurnameFirst",
                Age = 30,
                TimeStamp = DateTime.Now
            });
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            var bll = new BLLclass();
            var userList = bll.GetAllUserFromDAL().ToList();

            comboBox1.DataSource = userList;
            comboBox1.DisplayMember = "Name";

        }
    }
}
