﻿using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class UserDTO   //Data-Transfer-Object class or old name POCO = wrapper classe
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public DateTime TimeStamp { get; set; }      
        public int? Age { get; set; }
     
    }

    //public class 
    public class BLLclass
    {

        private DALclass _dal = new DALclass();

        public IEnumerable<UserDTO> GetAllUserFromDAL()
        {
            var users = _dal.GetUserTable().Select(s => s);

            foreach (var u in users)
            {
                yield return new UserDTO()
                {
                    Id = u.Id,
                    Name = u.Name,
                    Surname = u.Surname,
                    Age = u.Age,
                    TimeStamp = u.TimeStamp
                };
            }
        }

        public void AddNewUser(UserDTO user)
        {
            _dal.AddUser(new User()
            {
                Id = user.Id,
                Name = user.Name,
                Surname = user.Surname,
                Age = user.Age,
                TimeStamp = user.TimeStamp
            });

        }
    }
}
