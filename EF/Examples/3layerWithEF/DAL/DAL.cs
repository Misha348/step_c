﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class DALclass
    {
        private UsersDBmodel _ctx = new UsersDBmodel();
        public IQueryable<User> GetUserTable()
        {
            IQueryable<User> userTableFromEntityFramework;
            userTableFromEntityFramework = _ctx.Users;
            return userTableFromEntityFramework;
        }

        public void AddUser(User user)
        {
            _ctx.Users.Add(user);
            _ctx.SaveChanges();


        }
    }
}
