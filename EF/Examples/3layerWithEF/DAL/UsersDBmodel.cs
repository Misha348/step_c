namespace DAL
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity;
    using System.Linq;

    public class UsersDBmodel : DbContext
    {
        // Your context has been configured to use a 'UsersDBmodel' connection string from your application's 
        // configuration file (App.config or Web.config). By default, this connection string targets the 
        // 'DAL.UsersDBmodel' database on your LocalDb instance. 
        // 
        // If you wish to target a different database and/or database provider, modify the 'UsersDBmodel' 
        // connection string in the application configuration file.
        public UsersDBmodel()
            : base("name=UsersDBmodel")
        {
            Database.SetInitializer<UsersDBmodel>(new DropCreateDatabaseIfModelChanges<UsersDBmodel>());
        }

        // Add a DbSet for each entity type that you want to include in your model. For more information 
        // on configuring and using a Code First model, see http://go.microsoft.com/fwlink/?LinkId=390109.

        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<Sender> Senders { get; set; }
        public virtual DbSet<Receiver> Receivers { get; set; }
 
    }

    public class User
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [MaxLength(100)]
        public string Name { get; set; }
        [Required]
        [MaxLength(100)]
        public string Surname { get; set; }
        public DateTime TimeStamp { get; set;}

        [NotMapped]
        public int? Age { get; set; }
    }

    public partial class Sender : User
    {
        public Sender()
        {
            this.Receivers = new HashSet<Receiver>();
        }

        public virtual ICollection<Receiver> Receivers { get; set; } 
    }

    public partial class Receiver : User
    {
        public Receiver()
        {
            this.Senders = new HashSet<Sender>();
        }

        public virtual ICollection<Sender> Senders { get; set; }

    }
}