﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FluentAPI
{
    class Program
    {
        static void Main(string[] args)
        {
            using (LibDB ctx = new LibDB())
            {
                IQueryable<Author> query = ctx.Authors;   // delayed
                List<Author> query2 = ctx.Authors.ToList();  // imediatelly

                ctx.Authors.Add(new Author()
                {
                    FirstName = "Grigorii",
                    LastName = "Shevchenko",
                    Country = "Ukr"
                });                
                ctx.SaveChanges();

                foreach (var item in query)
                {
                    Console.WriteLine(item.FullName);
                }
            }
        }
    }
}
