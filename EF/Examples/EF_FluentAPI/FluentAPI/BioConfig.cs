﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FluentAPI
{
    class BioConfig : EntityTypeConfiguration<Bio>
    {
        public BioConfig()
        {
            //////////////////////// Bio
            this.HasKey(b => b.AuthorId);
            this.Property(b => b.BirthDate).IsRequired();
            this.Property(b => b.DeathDate).IsOptional();

            //// Zero-One to One            
            this.HasRequired(b => b.Author).WithOptional(a => a.Bio);
            //// One to One
            //this.HasRequired(b => b.Author).WithRequiredDependent(a => a.Bio);
        }
    }
}
