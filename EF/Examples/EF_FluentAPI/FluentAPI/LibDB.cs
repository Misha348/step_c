using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
using System.Linq;

namespace FluentAPI
{
    public class LibDB : DbContext
    {        
        public LibDB()
            : base("name=LibDB")
        {
            Database.SetInitializer(new DropCreateDatabaseAlways<LibDB>());
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            // Fluent API Code

            modelBuilder.Configurations.Add(new BookConfig());
            modelBuilder.Configurations.Add(new BioConfig());

            //////////////////////// Author
            modelBuilder.Entity<Author>().Ignore(a => a.FullName); // only in model

            //////////////////////// Relations
            //// Many to Many
            modelBuilder.Entity<Publisher>().HasMany(p => p.Books).WithMany(b => b.Publishers).Map(
                x => x
                    .MapLeftKey("PubId")
                    .MapRightKey("BookId")
                    .ToTable("PublisherBooks")
            );            
        }

        public virtual DbSet<Book> Books { get; set; }
        public virtual DbSet<Author> Authors { get; set; }
        public virtual DbSet<Bio> Bios { get; set; }
        public virtual DbSet<Publisher> Publishers { get; set; }
    }   

    public class Book
    {
        // Primary key
        public int Identity { get; set; }
        public string Name { get; set; }
        public int PageCount { get; set; }
        public int? Price { get; set; }

        // Foreign key
        public int AuthorId { get; set; }

        // Navigation properties
        public virtual Author Author { get; set; }
        public virtual ICollection<Publisher> Publishers { get; set; }
    }

    public class Author
    {
        // Primary key
        public int Id { get; set; }       
        public string FirstName { get; set; }       
        public string LastName { get; set; }
        public string Country { get; set; }       

        // Not mapped with database
        public string FullName { get { return FirstName + " " + LastName; } }

        // Navigation properties
        public virtual ICollection<Book> Books { get; set; }
        public virtual Bio Bio { get; set; }

    }
    public class Publisher
    {
        // Primary key
        public int Id { get; set; }
        public string Name { get; set; }

        // Navigation properties
        public virtual ICollection<Book> Books { get; set; }
    }

    public class Bio
    {
        // Primary key and Foreign key
        public int AuthorId { get; set; }
        public string Country { get; set; }
        public DateTime BirthDate { get; set; }
        public DateTime? DeathDate { get; set; }

        // Navigation properties
        public virtual Author Author { get; set; }
    }
}