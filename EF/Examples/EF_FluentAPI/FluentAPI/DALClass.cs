﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FluentAPI
{
    public class DALClass
    {
        private LibDB _context;

        public DALClass()
        {
            _context = new LibDB();
        }

        // PUBLIC METHODS

        // CREATE
        public void AddBook(Book book) { }
        public void AddAuthor(Author author)
        {
            var existing = _context.Authors.FirstOrDefault(a => a.FullName == author.FullName);

            if (existing != null) return;

            _context.Authors.Add(author);
            _context.SaveChanges();
        }

        // READ
        public IQueryable<Book> GetAllBooks()
        {
            return _context.Books;
        }
        public Book GetBookById(int id)
        {
            return _context.Books.Find(id);
            //return _context.Books.FirstOrDefault(b => b.Identity == id);
        }

        // UPDATE
        public void UpdateBook(Book newBook)
        {
            var book = _context.Books.Find(newBook.Identity);

            if (book == null) return;

            book.Name = newBook.Name;
            book.PageCount = newBook.PageCount;
            book.Price = newBook.Price;
            book.AuthorId = newBook.AuthorId;

            _context.SaveChanges();
        }

        // DELETE
        public void DeleteBookById(int id)
        {
            var book = _context.Books.Find(id);

            if (book == null) return;

            _context.Books.Remove(book);
            _context.Entry(book).State = System.Data.Entity.EntityState.Deleted;
            _context.SaveChanges();
        }

    }
}
