using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Code_First
{
   

    public class Model1 : DbContext
    {
      
        public Model1()
            : base("name=Model1")
        {
        }
       
        public virtual DbSet<Specialization> Specializations { get; set; }
        public virtual DbSet<Subject> Subjects { get; set; }       
        public virtual DbSet<Student> Students { get; set; }

    }

    public class Specialization
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public ICollection<Subject> Subjects { get; set; }
        public ICollection<Student> Students { get; set; }
    }

    public class Subject
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public int SpecializationId { get; set; }
        public virtual Specialization Specialization { get; set; }
    }

    public class Student
    {
        public int Id { get; set; }
        public string FirstName { get; set; }

        public string SecondName { get; set; }

        public string Phone { get; set; }

        public int SpecializationId { get; set; }
        public virtual Specialization Specialization { get; set; }
    }


}

   
