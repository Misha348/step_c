﻿using CF_FluentAPI_Anotat_Shop_Network_25._02._20;
using System.Collections.Generic;
using System.Linq;

namespace BLL
{
	public partial class BLLClass
	{
        public IEnumerable<WorkerDTO> GetAllWorkers()
        {
            var result = _dal.GetAllWorkers().Select(w => new WorkerDTO()
            {
                Id = w.Id,
                FirstName = w.FirstName,
                LastName = w.LastName,
                Phone = w.Phone,
                ShopId = w.ShopId,
                WorkerTypeId = w.WorkerTypeId
            });

            foreach (var item in result)
            {
                yield return new WorkerDTO()
                {
                    Id = item.Id,
                    FirstName = item.FirstName,
                    LastName = item.LastName,
                    Phone = item.Phone,
                    ShopId = item.ShopId,
                    WorkerTypeId = item.WorkerTypeId
                };
            }
        }

        public void AddWorkerDataFromUI(WorkerDTO worker)
        {
            Worker newWorker = new Worker
            {
                FirstName = worker.FirstName,
                LastName = worker.LastName,
                Phone = worker.Phone,
                ShopId = worker.ShopId,
                WorkerTypeId = worker.WorkerTypeId
            };
            _dal.AddNewWorker(newWorker);
        }

        public void GetWorkersDataForEditing(int id, string FirstN, string SecondN,
                                    string Phone, int ShopId, int WorkTypeId)
        {
            var workerForEditing = _dal.GetWorkerById(id);

            workerForEditing.FirstName = FirstN;
            workerForEditing.LastName = SecondN;
            workerForEditing.Phone = Phone;
            workerForEditing.ShopId = ShopId;
            workerForEditing.WorkerTypeId = WorkTypeId;

            _dal.GetEditedWorker(workerForEditing);
        }
        public void GetWorkersIdForDeleting(int id)
        {
            _dal.DelWorkerById(id);
        }
    }
}
