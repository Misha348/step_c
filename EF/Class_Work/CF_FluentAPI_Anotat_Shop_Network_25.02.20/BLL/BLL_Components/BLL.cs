﻿using CF_FluentAPI_Anotat_Shop_Network_25._02._20;

namespace BLL
{
    public class WorkerDTO
    {       
        public int Id { get; set; }       
        public string FirstName { get; set; }     
        public string LastName { get; set; }
        public string Phone { get; set; }       
        public int ShopId { get; set; }
        public int WorkerTypeId { get; set; }
    }

    public class ShopDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public int? ParkingArea { get; set; }
        public int CityId { get; set; }
    }

    public class ProductDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int CategoryId { get; set; }
        public double Price { get; set; }
        public double? Discount { get; set; }
        public string IsInStook { get; set; }
    }

    public partial class BLLClass
    {
        private readonly DAL _dal;  
        public BLLClass()
        {
            _dal = new DAL();
        }      
    }
}
