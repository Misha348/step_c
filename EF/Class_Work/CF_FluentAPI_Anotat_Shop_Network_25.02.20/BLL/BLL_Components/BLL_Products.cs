﻿using CF_FluentAPI_Anotat_Shop_Network_25._02._20;
using System.Collections.Generic;
using System.Linq;

namespace BLL
{
	public partial class BLLClass
	{
        public IEnumerable<ProductDTO> GetAllProducts()
        {
            var result = _dal.GetAllProducts().Select(p => new ProductDTO()
            {
                Id = p.Id,
                Name = p.Name,
                CategoryId = p.CategoryId,
                Price = p.Price,
                Discount = p.Discount,
                IsInStook = p.IsInStook.ToString()
            }); 
            foreach (var item in result)
            {
                yield return new ProductDTO()
                {
                    Id = item.Id,
                    Name = item.Name,
                    CategoryId = item.CategoryId,
                    Price = item.Price,
                    Discount = item.Discount,
                    IsInStook = item.IsInStook == "True" ? "Yes" : "No"
                };
            }
        }

        public void AddProductDataFromUI(ProductDTO product)
        {
            Product newProduct = new Product
            {
                Name = product.Name,
                CategoryId = product.CategoryId,
                Price = product.Price,
                Discount = product.Discount,
                IsInStook = product.IsInStook == "Yes" ? true : false

            };
            _dal.AddNewProduct(newProduct);
        }

        public void GetProductsDataForEditing(int id, string name, int catId,
                                   double price, double discount, string presenceInStook)
        {
            var productForEditing = _dal.GetProductById(id);

            productForEditing.Name = name;
            productForEditing.CategoryId = catId;
            productForEditing.Price = price;
            productForEditing.Discount = discount;
            productForEditing.IsInStook = presenceInStook == "Yes" ? true : false;

            _dal.GetEditedProduct(productForEditing);
        }

        public void GetProductsIdForDeleting(int id)
        {
            _dal.DelProductById(id);
        }
    }
}
