﻿using CF_FluentAPI_Anotat_Shop_Network_25._02._20;
using System.Collections.Generic;
using System.Linq;

namespace BLL
{
	public partial class BLLClass
	{
        public IEnumerable<ShopDTO> GetAllShops()
        {
            var result = _dal.GetAllShops().Select(sh => new ShopDTO()
            {
                Id = sh.Id,
                Name = sh.Name,
                Address = sh.Address, 
                ParkingArea = sh.ParkingArea,
                CityId = sh.CityId,
               
            });
            foreach (var item in result)
            {
                yield return new ShopDTO()
                {
                    Id = item.Id,
                    Name = item.Name,
                    Address = item.Address,
                    ParkingArea = item.ParkingArea,
                    CityId = item.CityId,
                };
            }
        }

        public void AddShopDataFromUI(ShopDTO shop)
        {
            Shop newShop = new Shop
            {
                Name = shop.Name,
                Address = shop.Address,
                ParkingArea = shop.ParkingArea,
                CityId = shop.CityId,
               
            };
            _dal.AddNewShop(newShop);
        }

        public void GetShopsDataForEditing(int id, string name, string addr,
                                  int parkTer, int cityId )
        {
            var shopForEditing = _dal.GetShopById(id);

            shopForEditing.Name = name;
            shopForEditing.Address = addr;
            shopForEditing.ParkingArea = parkTer;
            shopForEditing.CityId = cityId;          

            _dal.GetEditedShop(shopForEditing);
        }

        public void GetShopsIdForDeleting(int id)
        {
            _dal.DelShopById(id);
        }
    }
}
