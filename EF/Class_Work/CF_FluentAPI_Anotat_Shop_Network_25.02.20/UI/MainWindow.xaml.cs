﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using BLL;

namespace UI
{
	public partial class MainWindow : Window
	{
		private readonly BLLClass _bLL;
		private readonly IFormatProvider formatter;

		public MainWindow()
		{
			InitializeComponent();
			_bLL = new BLLClass();		
			WorkersTable.ItemsSource = _bLL.GetAllWorkers().ToList();
			ShopsTable.ItemsSource = _bLL.GetAllShops().ToList();
			ProductTable.ItemsSource = _bLL.GetAllProducts().ToList();

			formatter = new NumberFormatInfo { NumberDecimalSeparator = "." };
		}


		private void C_1_MouseMove(object sender, MouseEventArgs e)
		{
			MyGrid.ColumnDefinitions[0].Width = new GridLength(850);
			MyGrid.RowDefinitions[0].Height = new GridLength(500);
		}

		private void C_2_MouseMove(object sender, MouseEventArgs e)
		{
			MyGrid.ColumnDefinitions[0].Width = new GridLength(200);
			MyGrid.RowDefinitions[0].Height = new GridLength(500);
		}		

		private void C_3_MouseMove(object sender, MouseEventArgs e)
		{
			MyGrid.ColumnDefinitions[0].Width = new GridLength(850);
			MyGrid.RowDefinitions[0].Height = new GridLength(100);
		}

		private void C_4_MouseMove(object sender, MouseEventArgs e)
		{
			MyGrid.ColumnDefinitions[0].Width = new GridLength(300);
			MyGrid.RowDefinitions[0].Height = new GridLength(100);
		}	

		private void WorkersTable_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			WorkerDTO w = WorkersTable.SelectedItem as WorkerDTO;
			if (w != null)
			{
				E_Tb_Fn.Text = w.FirstName;
				E_Tb_Sn.Text = w.LastName;
				E_Tb_Ph.Text = w.Phone;
				E_Tb_ShId.Text = w.ShopId.ToString();
				E_Tb_WtId.Text = w.WorkerTypeId.ToString();
			}			
		}

		private void ShopsTable_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			ShopDTO sh = ShopsTable.SelectedItem as ShopDTO;
			if (sh != null)
			{
				E_Sh_N.Text = sh.Name;
				E_Sh_Adr.Text = sh.Address;
				E_Sh_PA.Text = sh.ParkingArea.ToString();
				E_Sh_CiId.Text = sh.CityId.ToString();				
			}
		}

		private void ProductTable_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			ProductDTO p = ProductTable.SelectedItem as ProductDTO;
			if (p != null)
			{
				E_Pr_N.Text = p.Name;
				E_Pr_CatId.Text = p.CategoryId.ToString();
				E_Pr_Pric.Text = p.Price.ToString();
				E_Pr_Disc.Text = p.Discount.ToString();
				E_Pr_Pres.Text = p.IsInStook;
			}
		}








		//private void WorkersTable_SelectionChanged(object sender, SelectionChangedEventArgs e)
		//{
		//	DataGrid dg = (DataGrid)sender;

		//	DataRowView selectedRow = dg.SelectedItem as DataRowView;
		//	if (selectedRow != null)
		//	{
		//		E_Tb_Fn.Text = selectedRow["FirstName"].ToString();
		//		E_Tb_Sn.Text = selectedRow["LastName"].ToString();
		//		E_Tb_Ph.Text = selectedRow["Phone"].ToString();
		//		E_Tb_ShId.Text = selectedRow["ShopId"].ToString();
		//		E_Tb_WtId.Text = selectedRow["WorkerTypeId"].ToString();

		//	}
		//}
	}
}
