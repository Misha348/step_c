﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using BLL;

namespace UI
{
	public partial class MainWindow : Window
	{
		private ShopDTO shopsDTO;
		private List<string> shopsDataFields;

		private void Sh_ADD_Click(object sender, RoutedEventArgs e)
		{
			shopsDataFields = new List<string>();
			foreach (var item in Gr_2.Children)
			{
				if (item is TextBox)
				{
					TextBox tmp = item as TextBox;
					shopsDataFields.Add(tmp.Text);
				}
			}
			if (shopsDataFields.Any(f => f.Length == 0))
				MessageBox.Show("FILL ALL FIELDS.");
			else
			{
				shopsDTO = new ShopDTO
				{
					Name = Sh_N.Text,
					Address = Sh_Adr.Text,
					ParkingArea = Convert.ToInt32(Sh_PA.Text),
					CityId = Convert.ToInt32(Sh_CiId.Text),
					
				};
				_bLL.AddShopDataFromUI(shopsDTO);
				ShopsTable.ItemsSource = _bLL.GetAllShops().ToList();
				Sh_N.Clear();
				Sh_Adr.Clear();
				Sh_PA.Clear();
				Sh_CiId.Clear();				
			}
		}

		private void Sh_EDIT_Click(object sender, RoutedEventArgs e)
		{
			if ( (ShopsTable.SelectedItem as ShopDTO) != null )
			{
				int id = (ShopsTable.SelectedItem as ShopDTO).Id;
				_bLL.GetShopsDataForEditing(id, E_Sh_N.Text, E_Sh_Adr.Text, Convert.ToInt32(E_Sh_PA.Text),
												Convert.ToInt32(E_Sh_CiId.Text) );
				ShopsTable.ItemsSource = _bLL.GetAllShops().ToList();
				E_Sh_N.Clear();
				E_Sh_Adr.Clear();
				E_Sh_PA.Clear();
				E_Sh_CiId.Clear();				
			}
		}

		private void Sh_DEL_Click(object sender, RoutedEventArgs e)
		{
			if ((ShopsTable.SelectedItem as ShopDTO) != null)
			{
				int id = (ShopsTable.SelectedItem as ShopDTO).Id;
				_bLL.GetShopsIdForDeleting(id);
				ShopsTable.ItemsSource = _bLL.GetAllShops().ToList();
			}
		}
	}
}
