﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using BLL;

namespace UI
{
	public partial class MainWindow : Window
	{
		private ProductDTO productDTO;
		private List<string> productsdataFields;

		private void Pr_ADD_Click(object sender, RoutedEventArgs e)
		{
			productsdataFields = new List<string>();
			foreach (var item in Gr_3.Children)
			{
				if (item is TextBox)
				{
					TextBox tmp = item as TextBox;
					productsdataFields.Add(tmp.Text);
				}
			}
			if (productsdataFields.Any(f => f.Length == 0))
				MessageBox.Show("FILL ALL FIELDS.");
			else
			{
				productDTO = new ProductDTO
				{
					Name = Pr_N.Text,
					CategoryId = Convert.ToInt32(Pr_CatId.Text),
					Price = double.Parse(Pr_Pric.Text, CultureInfo.InvariantCulture),
					Discount = double.Parse(Pr_Disc.Text, CultureInfo.InvariantCulture),
					IsInStook = Pr_Stor.Text
				};

				_bLL.AddProductDataFromUI(productDTO);
				ProductTable.ItemsSource = _bLL.GetAllProducts().ToList();
				Pr_N.Clear();
				Pr_CatId.Clear();
				Pr_Pric.Clear();
				Pr_Disc.Clear();
				Pr_Stor.Clear();
			}
		}
		
		private void Pr_EDIT_Click(object sender, RoutedEventArgs e)
		{                                                               // double b = double.Parse("23.56", formatter);
			if ( (ProductTable.SelectedItem as ProductDTO) != null)     // double.Parse(E_Pr_Pric.Text, CultureInfo.InvariantCulture)
			{
				int id = (ProductTable.SelectedItem as ProductDTO).Id;
				//double a = double.Parse(E_Pr_Pric.Text, formatter);
				//double b = double.Parse(E_Pr_Disc.Text, formatter);
				//MessageBox.Show($"{a}\n{b}");
				_bLL.GetProductsDataForEditing( id, E_Pr_N.Text, Convert.ToInt32(E_Pr_CatId.Text), double.Parse(E_Pr_Pric.Text, formatter),
												 double.Parse(E_Pr_Disc.Text, formatter), E_Pr_Pres.Text );				
				ProductTable.ItemsSource = _bLL.GetAllProducts().ToList();
				E_Pr_N.Clear();
				E_Pr_CatId.Clear();
				E_Pr_Pric.Clear();
				E_Pr_Disc.Clear();
				E_Pr_Pres.Clear();
			}
		}

		private void Pr_DEL_Click(object sender, RoutedEventArgs e)
		{
			if( (ProductTable.SelectedItem as ProductDTO) != null)
			{
				int id = (ProductTable.SelectedItem as ProductDTO).Id;
				_bLL.GetProductsIdForDeleting(id);
				ProductTable.ItemsSource = _bLL.GetAllProducts().ToList();
			}
		}
	}
}
