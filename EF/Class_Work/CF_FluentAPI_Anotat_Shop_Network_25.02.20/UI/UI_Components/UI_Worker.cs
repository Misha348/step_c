﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using BLL;

namespace UI
{
	public partial class MainWindow : Window
	{
		private WorkerDTO workerDTO;
		private List<string> workersdataFields;

		private void Add_Click(object sender, RoutedEventArgs e)
		{
			workersdataFields = new List<string>();
			foreach (var item in Gr_1.Children)
			{
				if (item is TextBox)
				{
					TextBox tmp = item as TextBox;
					workersdataFields.Add(tmp.Text);
				}
			}
			if (workersdataFields.Any(f => f.Length == 0))
				MessageBox.Show("FILL ALL FIELDS.");
			else
			{
				workerDTO = new WorkerDTO
				{
					FirstName = Tb_Fn.Text,
					LastName = Tb_Ln.Text,
					Phone = Tb_Ph.Text,
					ShopId = Convert.ToInt32(Tb_ShId.Text),
					WorkerTypeId = Convert.ToInt32(Tb_WtId.Text)
				};

				_bLL.AddWorkerDataFromUI(workerDTO);
				WorkersTable.ItemsSource = _bLL.GetAllWorkers().ToList();
				Tb_Fn.Clear();
				Tb_Ln.Clear();
				Tb_Ph.Clear();
				Tb_ShId.Clear();
				Tb_WtId.Clear();
			}
		}

		private void EDIT_Click(object sender, RoutedEventArgs e)
		{
			if ((WorkersTable.SelectedItem as WorkerDTO) != null)
			{
				int id = (WorkersTable.SelectedItem as WorkerDTO).Id;
				_bLL.GetWorkersDataForEditing(id, E_Tb_Fn.Text, E_Tb_Sn.Text, E_Tb_Ph.Text,
												Convert.ToInt32(E_Tb_ShId.Text), Convert.ToInt32(E_Tb_WtId.Text));
				WorkersTable.ItemsSource = _bLL.GetAllWorkers().ToList();
				E_Tb_Fn.Clear();
				E_Tb_Sn.Clear();
				E_Tb_Ph.Clear();
				E_Tb_ShId.Clear();
				E_Tb_WtId.Clear();
			}
		}

		private void DEL_Click(object sender, RoutedEventArgs e)
		{
			if ((WorkersTable.SelectedItem as WorkerDTO) != null)
			{
				int id = (WorkersTable.SelectedItem as WorkerDTO).Id;
				_bLL.GetWorkersIdForDeleting(id);
				WorkersTable.ItemsSource = _bLL.GetAllWorkers().ToList();
			}
		}
	}
}
