﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace CF_FluentAPI_Anotat_Shop_Network_25._02._20
{
    public partial class Director
    {
        public override string ToString()
        {
            return $"Full director name: {FullName}";
        }
    }
}
