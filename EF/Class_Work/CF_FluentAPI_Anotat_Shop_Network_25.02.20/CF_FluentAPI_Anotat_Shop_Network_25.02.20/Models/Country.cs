﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace CF_FluentAPI_Anotat_Shop_Network_25._02._20
{
	public class Country
    {
        [Key]
        [Required(ErrorMessage = "Id is required")]
        public int Id { get; set; }
        [Required(ErrorMessage = "Name is required")]
        [StringLength(30, ErrorMessage = "Name must contains up to 30 characters")]
        public string Name { get; set; }
        public ICollection<City> Cities { get; set; }

     
    }
}