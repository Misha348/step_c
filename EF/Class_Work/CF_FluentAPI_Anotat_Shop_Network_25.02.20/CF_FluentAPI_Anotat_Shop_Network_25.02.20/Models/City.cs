﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace CF_FluentAPI_Anotat_Shop_Network_25._02._20
{
	public class City
    {
        [Key]
        [Required(ErrorMessage = "Id is required")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required(ErrorMessage = "First Name is required")]
        [StringLength(30, ErrorMessage = "Name must contains up to 30 characters")]
        public string Name { get; set; }

        [ForeignKey("Country")]
        public int CountryId { get; set; }

        [ForeignKey("CountryId")]
        public virtual Country Country { get; set; }

        public ICollection<Shop> Shops { get; set; }
    }
}