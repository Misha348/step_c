﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace CF_FluentAPI_Anotat_Shop_Network_25._02._20
{ 

	public partial class Director
    {
        [Key]
        [Required(ErrorMessage = "Id is required")]
        public int Id { get; set; }

        [Required(ErrorMessage = "FirstName is required")]
        [StringLength(30, ErrorMessage = "FirstName must contains up to 30 characters")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "LastName is required")]
        [StringLength(30, ErrorMessage = "LastName must contains up to 30 characters")]
        public string LastName { get; set; }

        [NotMapped]
        public string FullName { get; set; } //
        public string Phone { get; set; }
        public string Email { get; set; }
        public bool? Education { get; set; }

        //[ForeignKey("Shop")]
        //public int ShopId { get; set; }
        //[ForeignKey("ShopId")]
        public virtual Shop Shop { get; set; }
    }
}