﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace CF_FluentAPI_Anotat_Shop_Network_25._02._20
{
    public class Shop
    {
        [Key]
        [Required(ErrorMessage = "Id is required")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required(ErrorMessage = "Name is required")]
        [StringLength(30, ErrorMessage = "Name must contains up to 30 characters")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Address is required")]
        [StringLength(30, ErrorMessage = "Address must contains up to 50 characters")]
        public string Address { get; set; }

        public int? ParkingArea { get; set; }

        [ForeignKey("City")]
        public int CityId { get; set; }

        [ForeignKey("CityId")]
        public virtual City City { get; set; }

        //[ForeignKey("Director")]
        //public int DirectorId { get; set; }

        //[ForeignKey("DirectorId")]
        public virtual Director Director { get; set; }

        public ICollection<Worker> Workers { get; set; }
        public ICollection<Product> Products { get; set; }
    }
}