﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace CF_FluentAPI_Anotat_Shop_Network_25._02._20
{
	public class Product
    {
        [Key]
        [Required(ErrorMessage = "Id is required")]
        public int Id { get; set; }
        [Required(ErrorMessage = "Name is required")]
        [StringLength(30, ErrorMessage = "Name must contains up to 30 characters")]
        public string Name { get; set; }

        [ForeignKey("Category")]
        public int CategoryId { get; set; }
        [ForeignKey("CategoryId")]
        public virtual Category Category { get; set; }

        [Required(ErrorMessage = "Price is required")]
        public double Price { get; set; }
        public double? Discount { get; set; }
        public bool IsInStook { get; set; }
        public ICollection<Shop> Shops { get; set; }
    }
}