﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace CF_FluentAPI_Anotat_Shop_Network_25._02._20
{
	public class Worker
    {
        [Key]
        [Required(ErrorMessage = "Id is required")]
        public int Id { get; set; }

        [Required(ErrorMessage = "FirstName is required")]
        [StringLength(30, ErrorMessage = "FirstName must contains up to 30 characters")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "LastName is required")]
        [StringLength(30, ErrorMessage = "LastName must contains up to 30 characters")]
        public string LastName { get; set; }
        public string Phone { get; set; }

       // [ForeignKey("Shop")]
        public int ShopId { get; set; }

        //[ForeignKey("ShopId")]
        public virtual Shop Shop { get; set; }

       //[ForeignKey("WorkerType")]
        public int WorkerTypeId { get; set; }

       // [ForeignKey("WorkerTypeId")]
        public virtual WorkerType WorkerType { get; set; }
    }
}