﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

using System.Collections.Generic;

namespace CF_FluentAPI_Anotat_Shop_Network_25._02._20
{
	public class WorkerType
    {
        [Key]
        [Required(ErrorMessage = "Id is required")]
        public int Id { get; set; }

        [Required(ErrorMessage = "Name is required")]
        [StringLength(30, ErrorMessage = "Name must contains up to 30 characters")]
        public string Name { get; set; }

        public bool? IsStaticsalary { get; set; }
        public ICollection<Worker> Workers { get; set; }
    }
}