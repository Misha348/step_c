﻿using System.Linq;

namespace CF_FluentAPI_Anotat_Shop_Network_25._02._20
{
	public partial class DAL
	{
		private Product _productForEditing;
		private Product _productForDeleting;

		public IQueryable<Product> GetAllProducts()
		{
			var q = _shopCtx.Products.Where(p => p.Id == p.Id);
			return q;
		}

		public void AddNewProduct(Product receivedProductFromBLL)
		{
			Product newProduct = new Product();
			newProduct = receivedProductFromBLL;
			_shopCtx.Products.Add(newProduct);
			_shopCtx.SaveChanges();
		}

		public Product GetProductById(int id)
		{
			_productForEditing = new Product();
			_productForEditing = _shopCtx.Products.Where(p => p.Id == id).Single();
			return _productForEditing;
		}

		public void GetEditedProduct(Product editedProductFromBLL)
		{
			_productForEditing = editedProductFromBLL;
			_shopCtx.SaveChanges();
		}

		public void DelProductById(int id)
		{
			_productForDeleting = _shopCtx.Products.Where(p => p.Id == id).Single();
			_shopCtx.Products.Remove(_productForDeleting);
			_shopCtx.SaveChanges();
		}
	}
}
