﻿using System.Linq;

namespace CF_FluentAPI_Anotat_Shop_Network_25._02._20
{
	public partial class DAL
	{
		private Worker _workerForEditing;
		private Worker _workerForDeleting;
        public IQueryable<Worker> GetAllWorkers()
        {
            var q = _shopCtx.Workers.Where(w => w.Id == w.Id);
            return q;
        }
        public void AddNewWorker(Worker receivedWorkerFromBLL)
        {
            Worker newWorker = new Worker();
            newWorker = receivedWorkerFromBLL;
            _shopCtx.Workers.Add(newWorker);
            _shopCtx.SaveChanges();
        }

        public Worker GetWorkerById(int id)
        {
            _workerForEditing = new Worker();
            _workerForEditing = _shopCtx.Workers.Where(w => w.Id == id).Single();
            return _workerForEditing;
        }

        public void GetEditedWorker(Worker editedWorkerFromBLL)
        {
            _workerForEditing = editedWorkerFromBLL;
            _shopCtx.SaveChanges();
        }

        public void DelWorkerById(int id)
        {
            _workerForDeleting = _shopCtx.Workers.Where(w => w.Id == id).Single();
            _shopCtx.Workers.Remove(_workerForDeleting);
            _shopCtx.SaveChanges();
        }
    }
}
