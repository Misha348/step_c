﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CF_FluentAPI_Anotat_Shop_Network_25._02._20
{
	public partial class DAL
	{
		private Shop _shopForEditing;
		private Shop _shopForDeleting;
		public IQueryable<Shop> GetAllShops()
		{
			var q = _shopCtx.Shops.Where(sh => sh.Id == sh.Id);
			return q;
		}

		public void AddNewShop(Shop receivedShopFromBLL)
		{
			Shop newShop = new Shop();
			newShop = receivedShopFromBLL;
			_shopCtx.Shops.Add(newShop);
			_shopCtx.SaveChanges();
		}

		public Shop GetShopById(int id)
		{
			_shopForEditing = new Shop();
			_shopForEditing = _shopCtx.Shops.Where(sh => sh.Id == id).Single();
			return _shopForEditing;
		}

		public void GetEditedShop(Shop editedShopFromBLL)
		{
			_shopForEditing = editedShopFromBLL;
			_shopCtx.SaveChanges();
		}

		public void DelShopById(int id)
		{
			_shopForDeleting = _shopCtx.Shops.Where(sh => sh.Id == id).Single();
			_shopCtx.Shops.Remove(_shopForDeleting);
			_shopCtx.SaveChanges();
		}
	}
}
