﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CF_FluentAPI_Anotat_Shop_Network_25._02._20.EntitiesConfigurations
{
    public class ProductConfiguration: EntityTypeConfiguration<Product>
    {
        public ProductConfiguration()
        {
            this.ToTable("Products").HasKey(pr => pr.Id)
            .Property(pr => pr.Id)
            .IsRequired()
            .HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            this.Property(pr => pr.Name).HasMaxLength(30)
               .IsRequired();
            this.Property(pr => pr.Price).IsRequired();
            this.Property(pr => pr.Discount).IsOptional();
        }
    }
}
