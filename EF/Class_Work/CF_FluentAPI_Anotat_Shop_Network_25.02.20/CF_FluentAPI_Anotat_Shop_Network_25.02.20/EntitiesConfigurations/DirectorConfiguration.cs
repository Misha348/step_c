﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CF_FluentAPI_Anotat_Shop_Network_25._02._20.EntitiesConfigurations
{
    public class DirectorConfiguration: EntityTypeConfiguration<Director>
    {
        public DirectorConfiguration()
        {
            this.ToTable("Directors").HasKey(d => d.Id)
               .Property(d => d.Id)
               .IsRequired()
               .HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            this.Property(c => c.FirstName).HasMaxLength(30).IsRequired();
            this.Property(c => c.LastName).HasMaxLength(30).IsRequired();
            this.Property(d => d.Education).IsOptional();

        }
    }
}
