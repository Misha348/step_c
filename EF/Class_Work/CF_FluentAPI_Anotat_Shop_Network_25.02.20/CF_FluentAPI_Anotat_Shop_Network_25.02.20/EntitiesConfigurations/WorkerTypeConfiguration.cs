﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CF_FluentAPI_Anotat_Shop_Network_25._02._20.EntitiesConfigurations
{
    public class WorkerTypeConfiguration: EntityTypeConfiguration<WorkerType>
    {
        public WorkerTypeConfiguration()
        {
            this.ToTable("WorkerType").HasKey(wt => wt.Id)
             .Property(wt => wt.Id)
             .IsRequired()
             .HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            this.Property(cat => cat.Name).HasMaxLength(30)
               .IsRequired();
            this.Property(wt => wt.IsStaticsalary).IsOptional();
            this.HasMany(wt => wt.Workers).WithRequired(w => w.WorkerType).HasForeignKey(w => w.WorkerTypeId);
        }
    }
}
