﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CF_FluentAPI_Anotat_Shop_Network_25._02._20.EntitiesConfigurations
{
    class CountryConfiguration: EntityTypeConfiguration<Country>
    {
        public CountryConfiguration()
        {
            this.ToTable("Countries").HasKey(co => co.Id)
                .Property(co => co.Id)
                .IsRequired()
                .HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);

            this.Property(c => c.Name).HasMaxLength(30)
                .IsRequired();

            this.HasMany(co => co.Cities).WithRequired(ci => ci.Country).HasForeignKey(ci => ci.CountryId);
        }
    }
}
