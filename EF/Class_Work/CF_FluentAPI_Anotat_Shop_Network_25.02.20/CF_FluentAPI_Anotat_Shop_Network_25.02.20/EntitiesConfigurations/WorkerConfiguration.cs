﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CF_FluentAPI_Anotat_Shop_Network_25._02._20.EntitiesConfigurations
{
    public class WorkerConfiguration: EntityTypeConfiguration<Worker>
    {
        public WorkerConfiguration()
        {
            this.ToTable("Workers").HasKey(w => w.Id)
              .Property(w => w.Id)
              .IsRequired()
              .HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            this.Property(w => w.FirstName).HasMaxLength(30).IsRequired();
            this.Property(w => w.LastName).HasMaxLength(30).IsRequired();       
           
        }
    }
}
