﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CF_FluentAPI_Anotat_Shop_Network_25._02._20.EntitiesConfigurations
{
    class CategoryConfiguration: EntityTypeConfiguration<Category>
    {
        public CategoryConfiguration()
        {
            this.ToTable("Categories").HasKey(cat => cat.Id)
              .Property(cat => cat.Id)
              .IsRequired()
              .HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            this.Property(cat => cat.Name).HasMaxLength(30)
               .IsRequired();
            this.HasMany(cat => cat.Products).WithRequired(pr => pr.Category).HasForeignKey(pr => pr.CategoryId);
            
        }
    }
}
