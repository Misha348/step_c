﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CF_FluentAPI_Anotat_Shop_Network_25._02._20.EntitiesConfigurations
{
    public class CityConfiguration: EntityTypeConfiguration<City>
    {
        public CityConfiguration()
        {
            this.ToTable("Cities").HasKey(c => c.Id)
                .Property(c => c.Id)
                .IsRequired()
                .HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            this.Property(c => c.Name).HasMaxLength(30)
                .IsRequired();

            //this.HasRequired(ci => ci.Country).WithMany(co => co.Cities);

            this.HasMany(ci => ci.Shops).WithRequired(sh => sh.City).HasForeignKey(sh => sh.CityId); //1 : *
        }
    }
}
