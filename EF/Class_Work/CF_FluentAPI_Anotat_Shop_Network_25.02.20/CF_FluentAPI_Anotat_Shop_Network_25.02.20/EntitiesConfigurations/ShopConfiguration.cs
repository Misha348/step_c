﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CF_FluentAPI_Anotat_Shop_Network_25._02._20.EntitiesConfigurations
{
    public class ShopConfiguration: EntityTypeConfiguration<Shop>
    {
        public ShopConfiguration()
        {
            this.ToTable("Shops").HasKey(sh => sh.Id)
                .Property(sh => sh.Id)
                .IsRequired()
                .HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);

            this.Property(sh => sh.Name).HasMaxLength(30)
                .IsRequired();

            this.Property(sh => sh.ParkingArea).IsOptional();

            this.HasRequired(sh => sh.Director).WithRequiredPrincipal(d => d.Shop);
            this.HasMany(sh => sh.Products).WithMany(pr => pr.Shops);
            
            this.HasMany(sh => sh.Workers).WithRequired(w => w.Shop).HasForeignKey(w => w.ShopId);
        }
    }
}
