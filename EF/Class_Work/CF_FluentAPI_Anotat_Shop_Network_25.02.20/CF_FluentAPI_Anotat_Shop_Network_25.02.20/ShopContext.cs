using CF_FluentAPI_Anotat_Shop_Network_25._02._20.EntitiesConfigurations;
using System;
using System.Data.Entity;
using System.Linq;

namespace CF_FluentAPI_Anotat_Shop_Network_25._02._20
{
    public class ShopContext : DbContext
    {       
        public ShopContext(): base("name=ShopContext")
        {
            Database.SetInitializer(new ShopInitializer());
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Configurations.Add(new CategoryConfiguration());
            modelBuilder.Configurations.Add(new CityConfiguration());
            modelBuilder.Configurations.Add(new CountryConfiguration());
            modelBuilder.Configurations.Add(new DirectorConfiguration());
            modelBuilder.Configurations.Add(new ProductConfiguration());
            modelBuilder.Configurations.Add(new ShopConfiguration());
            modelBuilder.Configurations.Add(new WorkerConfiguration());
            modelBuilder.Configurations.Add(new WorkerTypeConfiguration());
        }

        public virtual DbSet<Shop> Shops { get; set; }
        public virtual DbSet<City> Cities { get; set; }
        public virtual DbSet<Country> Countries { get; set; }
        public virtual DbSet<Director> Directors { get; set; }
        public virtual DbSet<WorkerType> WorkerTypes{ get; set; }
        public virtual DbSet<Worker> Workers { get; set; }
        public virtual DbSet<Category> Categories { get; set; }
        public virtual DbSet<Product> Products { get; set; }
    }

    public class ShopInitializer : CreateDatabaseIfNotExists<ShopContext>
    {
        protected override void Seed(ShopContext context)
        {
            base.Seed(context);
            context.Countries.Add(new Country() { Name = "Ukraine" });
            context.Countries.Add(new Country() { Name = "Poland" });
            context.SaveChanges();

            context.Cities.Add( new City() { Name = "Lviv", CountryId = 1} );
            context.Cities.Add (new City() { Name = "Rivne", CountryId = 1} );
            context.Cities.Add(new City() { Name = "Warshava", CountryId = 2 });
            context.Cities.Add(new City() { Name = "Krakiv", CountryId = 2 });
            context.SaveChanges();

            context.Categories.Add(new Category() {Name = "Meat" });
            context.Categories.Add(new Category() { Name = "Fish" });
            context.SaveChanges();
         

            context.WorkerTypes.Add(new WorkerType() { Name = "Administrator" });
            context.WorkerTypes.Add(new WorkerType() { Name = "Seller" });
            context.WorkerTypes.Add(new WorkerType() { Name = "Loader" });
            context.SaveChanges();

            context.Shops.Add(new Shop() { Name = "Fresh Meat", Address = "Warshawa, Mickevycha 23 str.", ParkingArea = 230, CityId = 3 });
            context.Shops.Add(new Shop() { Name = "Fresh Fish", Address = "Rivne, Pavlyuchenka 8 str.", ParkingArea = 130, CityId = 2 });
            context.SaveChanges();

            context.Workers.Add(new Worker() { FirstName = "Ostap", LastName = "Binchuk", Phone = "98786554435", ShopId = 1, WorkerTypeId = 1 });
            context.Workers.Add(new Worker() { FirstName = "Alla", LastName = "Popchuk", Phone = "2344657676", ShopId = 1, WorkerTypeId = 3 });
            context.SaveChanges();

            context.Products.Add(new Product { Name = "Pork", CategoryId = 1, Price = 180.5, Discount = 5.8, IsInStook = true });
            context.Products.Add(new Product { Name = "salmon", CategoryId = 2, Price = 250.2, Discount = 3.4, IsInStook = true });
            context.SaveChanges();
        }
    }
}