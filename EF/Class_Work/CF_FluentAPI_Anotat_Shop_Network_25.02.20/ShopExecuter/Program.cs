﻿using CF_FluentAPI_Anotat_Shop_Network_25._02._20;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopExecuter
{
    class Program
    {
        static void Main(string[] args)
        {
            DAL dal = new DAL();

            string country;
            Console.Write("Enter country: ");
            country = Console.ReadLine();
            dal.AddNewCountry(country);
            Console.WriteLine(new string('=', 30));

            dal.ShowAllCountries();
            Console.WriteLine(new string('=', 30));

            int id;
            Console.Write("Enter Id to delete: ");
            id = Convert.ToInt32(Console.ReadLine());
            dal.DelCountryById(id);
          
            dal.ShowAllCountries();
            Console.WriteLine(new string('=', 30));

            int ed_id;
            Console.Write("Enter Id to update: ");
            ed_id = Convert.ToInt32(Console.ReadLine());

            string editedCountryName;
            Console.Write("Enter country: ");
            editedCountryName = Console.ReadLine();

            dal.UpdateCountryById(ed_id, editedCountryName);

            dal.ShowAllCountries();
            Console.WriteLine(new string('=', 30));

        }
    }
}
