﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Intro_17._02._20
{
    class Program
    {
        static void Main(string[] args)
        {
            using (Library_ClassEntities lc_Ent = new Library_ClassEntities())
            {
                var query1 = lc_Ent.Books.Where(b => b.PagesCount > 100);
                foreach (var item in query1)
                {
                    Console.WriteLine($"{item.Id}\t{item.BookName}\t{item.PagesCount}\t");
                }
                Console.WriteLine(new string('=', 30));


                var query2 = lc_Ent.Books.Where(b => b.BookName.StartsWith("a") || b.BookName.StartsWith("A"));
                foreach (var item in query2)
                {
                    Console.WriteLine($"{item.Id}\t{item.BookName}\t{item.PagesCount}\t");
                }
                Console.WriteLine(new string('=', 30));


                var query3 = lc_Ent.Books.Where(b => b.BookName.Length > 10);
                foreach (var item in query3)
                {
                    Console.WriteLine($"{item.Id}\t{item.BookName}\t{item.PagesCount}\t");
                }
                Console.WriteLine(new string('=', 30));

                var query4 = lc_Ent.Authors.Where(a => a.FirstName == "William" && a.LastName == "Shakespeare");
                foreach (var item in query4)
                {
                    Console.WriteLine($"{item.Id}\t{item.FirstName}\t{item.LastName}\t");
                }
                Console.WriteLine(new string('=', 30));

                var query5 = from b in lc_Ent.Books
                             join l in lc_Ent.Languages on b.LanguageId equals l.Id
                             where l.LanguageName == "Ukrainian"
                             select b;

                foreach (var item in query5)
                {
                    Console.WriteLine($"{item.Id}\t{item.BookName}");
                }
                Console.WriteLine(new string('=', 30));
            }
        }
    }
}
