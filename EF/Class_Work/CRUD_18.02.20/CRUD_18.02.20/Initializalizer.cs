﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRUD_18._02._20

{
    class Initializalizer: DropCreateDatabaseIfModelChanges<UniversityEntities>
    {
        public override void InitializeDatabase(UniversityEntities ctx)
        {
            List<Specialization> specs = new List<Specialization>()
            {
               new Specialization() { Name = "History"},
               new Specialization() { Name = "Mathematic"},
               new Specialization() { Name = "SWDewelopment"},
               new Specialization() { Name = "Philology"},
               new Specialization() { Name = "Pholosophy"}
            };
            ctx.Specializations.AddRange(specs);
            ctx.SaveChanges();

            List<Subject> subjs = new List<Subject>()
            {
                new Subject() {  Name = "SHD", AmountHours = 250, SpecializationId = 1 },
                new Subject() {  Name = "East hist", AmountHours = 130, SpecializationId = 1 },

                new Subject() {  Name = "Math analitic", AmountHours = 190, SpecializationId = 2 },
                new Subject() {  Name = "Statistic", AmountHours = 140, SpecializationId = 2 },

                new Subject() {  Name = "C#", AmountHours = 200, SpecializationId = 3 },
                new Subject() {  Name = "ASP.NET", AmountHours = 180, SpecializationId = 3 },

                new Subject() {  Name = "Language", AmountHours = 170, SpecializationId = 4 },
                new Subject() {  Name = "Syntaxys", AmountHours = 160, SpecializationId = 4 },

                new Subject() {  Name = "West", AmountHours = 170, SpecializationId = 5 },
                new Subject() { Name = "East", AmountHours = 140, SpecializationId = 5 },
            };
            ctx.Subjects.AddRange(subjs);
            ctx.SaveChanges();

            List<Student> studs = new List<Student>()
            {
                new Student() { Name = "Jophn", SurName = "Johnovich", Address = "GB, Liverpool", Phone = "123456789", SpecializationId = 1 },
                new Student() { Name = "Jack", SurName = "Jacovich", Address = "GB, manchester", Phone = "846202618", SpecializationId = 2 },
                new Student() { Name = "Sarah", SurName = "Connor", Address = "GB, Wels", Phone = "7345264845", SpecializationId = 3 },
                new Student() { Name = "Julia", SurName = "Rors", Address = "GB, Glasgow", Phone = "634326474", SpecializationId = 4 },
                new Student() { Name = "Katty", SurName = "Parry", Address = "GB, Sassex", Phone = "8462343654", SpecializationId = 5 },
            };
            ctx.Students.AddRange(studs);

            ctx.SaveChanges();


            //base.InitializeDatabase(context);
        }
       
    }
}
