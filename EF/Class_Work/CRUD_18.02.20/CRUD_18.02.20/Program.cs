﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRUD_18._02._20
{
    class Program
    {
        public static void Init()
        {
            #region init
            //using (UniversityEntities ctx = new UniversityEntities())
            //{
            //    List<Specialization> specs = new List<Specialization>()
            //    {
            //        new Specialization() { Name = "History"},
            //        new Specialization() { Name = "Mathematic"},
            //        new Specialization() { Name = "SWDewelopment"},
            //        new Specialization() { Name = "Philology"},
            //        new Specialization() { Name = "Pholosophy"}
            //    };
            //    ctx.Specializations.AddRange(specs);
            //    ctx.SaveChanges();

            //    List<Subject> subjs = new List<Subject>()
            //    {
            //        new Subject() {  Name = "SHD", AmountHours = 250, SpecializationId = 1 },
            //        new Subject() {  Name = "East hist", AmountHours = 130, SpecializationId = 1 },

            //        new Subject() {  Name = "Math analitic", AmountHours = 190, SpecializationId = 2 },
            //        new Subject() {  Name = "Statistic", AmountHours = 140, SpecializationId = 2 },

            //        new Subject() {  Name = "C#", AmountHours = 200, SpecializationId = 3 },
            //        new Subject() {  Name = "ASP.NET", AmountHours = 180, SpecializationId = 3 },

            //        new Subject() {  Name = "Language", AmountHours = 170, SpecializationId = 4 },
            //        new Subject() {  Name = "Syntaxys", AmountHours = 160, SpecializationId = 4 },

            //        new Subject() {  Name = "West", AmountHours = 170, SpecializationId = 5 },
            //        new Subject() { Name = "East", AmountHours = 140, SpecializationId = 5 },
            //    };
            //    ctx.Subjects.AddRange(subjs);
            //    ctx.SaveChanges();

            //    List<Student> studs = new List<Student>()
            //    {
            //        new Student() { Name = "Jophn", SurName = "Johnovich", Address = "GB, Liverpool", Phone = "123456789", SpecializationId = 1 },
            //        new Student() { Name = "Jack", SurName = "Jacovich", Address = "GB, manchester", Phone = "846202618", SpecializationId = 2 },
            //        new Student() { Name = "Sarah", SurName = "Connor", Address = "GB, Wels", Phone = "7345264845", SpecializationId = 3 },
            //        new Student() { Name = "Julia", SurName = "Rors", Address = "GB, Glasgow", Phone = "634326474", SpecializationId = 4 },
            //        new Student() { Name = "Katty", SurName = "Parry", Address = "GB, Sassex", Phone = "8462343654", SpecializationId = 5 },
            //    };
            //    ctx.Students.AddRange(studs);

            //    ctx.SaveChanges();
            //}
            #endregion
        }

        public static void AddStudent()
        {
            using (UniversityEntities ctx = new UniversityEntities())
            {
                Console.Write("name: ");
                string name = Console.ReadLine();
                Console.Write("surname: ");
                string surname = Console.ReadLine();
                Console.Write("address: ");
                string addres = Console.ReadLine();
                Console.Write("phone: ");
                string phone = Console.ReadLine();
                Console.Write("spec: ");
                int spec = int.Parse( Console.ReadLine());


                var res = ctx.Students.FirstOrDefault(s => s.Name == name && s.SurName == surname);
                if (res == null)
                {
                    ctx.Students.Add(new Student() { Name = name, SurName = surname, Address = addres, Phone = phone, SpecializationId = spec } );
                    Console.WriteLine("student added.");
                    ctx.SaveChanges();
                }                    
            }
        }

        public static void Ordering() 
        {
            using (UniversityEntities ctx = new UniversityEntities())
            {
                var res = ctx.Subjects.OrderBy(s => s.Name);   // lazy loading
                foreach (var item in res)
                {
                    Console.WriteLine($"{item.Id}\t{item.Name}\t{item.AmountHours}\t{item.SpecializationId}\t{item.Specialization.Name}");
                }
                //var ob = ctx.Students.First(s => s.Name == "Bill").Name;
                
            }
        }

        public static void OrderingwithInclude() 
        {
            using (UniversityEntities ctx = new UniversityEntities())
            {
                //ctx.Subjects.Include("Specialization");       // eager loading
                //var res = ctx.Subjects.OrderBy(s => s.Name);
                //foreach (var item in res)
                //{
                //    Console.WriteLine($"{item.Id}\t{item.Name}\t{item.AmountHours}\t{item.SpecializationId}\t{item.Specialization.Name}");
                //}

                //ctx.Configuration.LazyLoadingEnabled = false; // turning off lazy loading 
                //var res = ctx.Subjects.OrderBy(s => s.Name);   // here will be mistake
                //foreach (var item in res)
                //{
                //    Console.WriteLine($"{item.Id}\t{item.Name}\t{item.AmountHours}\t{item.SpecializationId}\t{item.Specialization.Name}");
                //}

                ctx.Configuration.LazyLoadingEnabled = false;
                //var res = ctx.Subjects.OrderBy(s => s.Name);
                var subject = ctx.Subjects.FirstOrDefault();


                ctx.Entry(subject).Reference("Specialization").Load();
                Console.WriteLine($"{subject.Name}\t{subject.Specialization.Name}");

                var specialization = ctx.Specializations.FirstOrDefault();
                ctx.Entry(specialization).Collection("Subjects").Load();
                foreach (var item in specialization.Subjects)
                {
                    Console.WriteLine(item.Name);
                }
            }
        }

        //public static void GetStudentById(int id)
        //{
        //}


        public static void ShowSubjWithSpec()
        {
            using (UniversityEntities ctx = new UniversityEntities())
            {
                ctx.Subjects.Include("Specialization");       // eager loading
                var res = ctx.Subjects.Select(s => s);
                foreach (var item in res)
                {
                    Console.WriteLine($"{item.Id}\t{item.Name}\t{item.Specialization.Name}");
                }
            }
        }

        public static void ShowFirsSubjByAlpabet()
        {
            using (UniversityEntities ctx = new UniversityEntities())
            {
                //ctx.Subjects.Include("Specialization");       // eager loading
                var res = ctx.Subjects.OrderBy(s => s.Name).FirstOrDefault();
               
                Console.WriteLine($"{res.Name}\t{res.Specialization.Name}");
                
            }
        }

        public static void ShowSudentsBySpec()
        {
            Console.Write("SPECIALIZATIONS: History, Mathematic, SWDewelopment, Philology, Pholosophy\n  enter specialization:");
            string spec = Console.ReadLine();

            using (UniversityEntities ctx = new UniversityEntities())
            {
                var q = from s in ctx.Students
                        join sp in ctx.Specializations on s.SpecializationId equals sp.Id
                        where sp.Name == spec
                        select s;
                foreach (var item in q)
                {
                    Console.WriteLine($"{item.Name}\t{item.SurName}\t{item.Specialization.Name}");
                }
            }
        }

        public static void ShowTopSpec()
        {
            using (UniversityEntities ctx = new UniversityEntities())
            {
                var q = from sp in ctx.Specializations
                        join s in ctx.Students on sp.Id equals s.SpecializationId
                        group s by sp.Name into gr
                        select new { spec = gr.Key, count = gr.Count() };

                var topSp = q.OrderByDescending(s => s.count).Take(3);
                ctx.Specializations.OrderByDescending(s => s.Students.Count()).Take(3);

                foreach (var item in topSp)
                {                   
                    Console.WriteLine($"{item.spec}\t{item.count}");
                }

            }
        }



        static void Main(string[] args)
        {
            //Init();
            //AddStudent();
            //Ordering();
            //OrderingwithInclude();

            ShowSubjWithSpec();
            Console.WriteLine(new string('=', 40));

            ShowFirsSubjByAlpabet();
            Console.WriteLine(new string('=', 40));

            ShowSudentsBySpec();
            Console.WriteLine(new string('=', 40));

            ShowTopSpec();



        }
    }
}
