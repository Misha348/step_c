
			--1

--alter database SportShop set recovery full;

--select name, recovery_model_desc 
--from sys.databases  
--where name = 'SportShop'   
------------------------------------------------------
			--2

--backup database SportShop 
--to disk = 'E:\MSSQL\����� �����\MSSQL15.MSSQLSERVER\MSSQL\DATA\MishaBackup.bak' 
------------------------------------------------------
			--3
--insert into Clients(Name, Surname, Email, Tel, Discount)
--values('qqq', 'qqqq', 'qqqqq', '1111111111', 9), 
--	  ('qqqq', 'qqqq', 'qqqq', '222222222', 9 ), 
--	  ('qqqq', 'qqqq', 'qqqq', '333333333', 9);

--delete from Products
--where Name = 'apple'
------------------------------------------------------
			--4
--backup log SportShop
--to disk = 'E:\MSSQL\����� �����\MSSQL15.MSSQLSERVER\MSSQL\DATA\MishaLogBackup.trn'
------------------------------------------------------
			--5

--use master
--go

--ALTER DATABASE SportShop
--SET SINGLE_USER
----This rolls back all uncommitted transactions in the db.
--WITH ROLLBACK IMMEDIATE

--go
--restore database SportShop from disk='E:\MSSQL\����� �����\MSSQL15.MSSQLSERVER\MSSQL\DATA\MishaBackup.bak' with replace
------------------------------------------------------
			--6
--go
--restore log SportShop from disk='E:\MSSQL\����� �����\MSSQL15.MSSQLSERVER\MSSQL\DATA\MishaLogBackup.trn' WITH NORECOVERY


