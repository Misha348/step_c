			--1
--create view DoctorsNames as
--select Doc.Name 
--from Doctors as Doc 

--select * 
--from DoctorsNames 
--------------------------------------------------
			--2
--create view SurwaysDoctorsWards as
--select  Surveys.Name as SurvName,
--		Doctors.Name as DocName, 
--		Wards.Name as WardName, 
--		Wards.Places as WardPlaces
--from Surveys join Doctors on Surveys.DoctorId = Doctors.Id
--			 join Wards on Surveys.WardId = Wards.Id

--select *
--from SurwaysDoctorsWards
--------------------------------------------------
			--3
--create view WardsInfo as
--select Departments.Id, Wards.Name, Wards.DepartmentId,  Wards.Id as [WardID],  Wards.Places
--from Departments join Wards on Wards.DepartmentId = Departments.Id

--select *
--from WardsInfo
--where WardsInfo.DepartmentId = 1
--------------------------------------------------
			--4
--create or alter view DoctorsSurvays as
--select top(1) Doctors.Name, Doctors.Surname, count(*) as SurvCount
--from Surveys join Doctors on Surveys.DoctorId = Doctors.Id
--group by Doctors.Name, Doctors.Surname
--order by SurvCount desc

--select *
--from DoctorsSurvays

--------------------------------------------------
			--5
--create view TopDocSalary as
--select Doctors.Name, Doctors.Surname, Doctors.Premium
--from Doctors

--select top(3) TopDocSalary.Name, TopDocSalary.Surname, TopDocSalary.Premium from TopDocSalary order by Premium desc 

--------------------------------------------------
			--6

--create view DoctorsSalary as
--select Doctors.Name, Doctors.Surname, Doctors.Salary
--from Doctors


--declare @s int
--set @s = (select max(Salary) from Doctors)
--select  *
--from DoctorsSalary 
--where DoctorsSalary.Salary = @s
--------------------------------------------------
			--PROCEDURES

			--1
--go
--create procedure First as
--select Wards.Name as [Ward name], Wards.Places
--from Wards

--exec First
--------------------------------------------------
			--2
--go
--create or alter procedure SEC as
--select Surveys.Name, Doctors.Name, Doctors.Surname
--from Surveys, Doctors
--where Surveys.DoctorId = Doctors.Id and Doctors.Name = 'Andr�j'
--exec SEC
--------------------------------------------------
			--3
--go
--create or alter procedure Third as
--select avg(Salary) from Doctors

--exec Third
--------------------------------------------------
			--4
--go
--create or alter procedure Fourth as
--select top(3) Doctors.Name,  count(*) as SurvCount
--from Surveys join Doctors on Surveys.DoctorId = Doctors.Id
--group by Doctors.Name
--order by SurvCount desc

--exec Fourth



