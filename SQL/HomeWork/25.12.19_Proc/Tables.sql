
--create table Departments
--(
--	Id int identity(1, 1) primary key not null,
--	Building int not null check(Building >= 1 and Building <=5),	
--	Name nvarchar(100) not null unique
--)

--create table Doctors
--(
--	Id int identity(1, 1) not null primary key,
--	Name nvarchar(max) not null,
--	Surname nvarchar(max) not null,
--	Salary money not null default(0) check(Salary >= 0),
--	Premium money not null default(0) check(Premium >= 0)
--)

--create table Wards
--(
--	Id int identity(1, 1) not null primary key,
--	Name nvarchar(100) not null unique,
--	Places int not null check(Places >= 1 and Places <= 8),
--	DepartmentId int not null foreign key references Departments(Id)
--)

--create table Surveys
--(
--	Id int primary key identity not null,
--	Name nvarchar(100) not null unique default('noname'),
--	DayOfWeek int not null check(1<= DayOfWeek and 7 >= DayOfWeek),	
--	StartTime time not null  check(StartTime BETWEEN '8:00' AND '18:00'),
--	EndTime time not null,
--	check(StartTime < EndTime),
--	WardId int not null foreign key references Wards(Id),
--	DoctorId int not null foreign key references Doctors(Id)
--)