use Hospital_agregat

--insert into Departments(Name, Building  )
--values ( 'Traumatology', 2),
--	   ( 'Surgery',	1),
--	   ( 'Cardiology',	3),
--	   ( 'Neurological', 4),
--	   ( 'Rheumatology', 3),
--	   ( 'Therapeutic',	2),
--	   ( 'Dental',	3),
--	   ( 'Emergency',	5),
--	   ( 'Otolaryngology', 2),
--	   ( 'Laboratory',	3)

--insert into Wards(Name, Places, DepartmentId )
--values ('A', 5, 1 ),
--	   ('B', 5, 3 ),
--	   ('C', 4, 4 ),
--	   ('D', 7, 8 ),
--	   ('E', 8, 3 ),
--	   ('F', 6, 2 ),
--	   ('D', 2, 9 ),
--	   ('E', 4, 7 ),
--	   ('A', 3, 6 ),
--	   ('C', 8, 5),
--	   ('E', 8, 4 ),
--	   ('F', 6, 2 ),
--	   ('D', 2, 10 ),
--	   ('E', 4, 2 ),
--	   ('A', 3, 6 ),
--	   ('C', 8, 1);

--insert into Doctors(Name, Surname, Premium, Salary, {DepID})
--values ('Vasil', 'Petrenko', 1400, 14000),
--	   ('Andrew', 'Lev',  1000, 10000),
--	   ('Maksim', 'Kotov',  1600, 16000),
--	   ('�ra', 'Varish',  1100, 11000),
--	   ('Olexander', 'Holeryuk',  900, 9000),
--	   ('Roman', 'Perfekc�on',  1900, 19000),
--	   ('Olena', 'Zolota',  1800, 18000),
--	   ('Oksana', 'Gava',  1910, 19100),
--	   ('Lesya', 'Dodo',  1900, 19000),
--	   ('Lida', 'Goodwooman',   1900, 19000);

--insert into Suveys(Name)
--values  ('Gastric fluid analysis'),
--		('Kidney function test'),
--		('Liver function test'),
--		('Lumbar puncture'),
--		('Malabsorption test'),
--		('Pap smear'),
--		('Pregnancy test'),
--		('Urography'),
--		('Syphilis test'),
--		('Thoracentesis'),
--		('Toxicology test'),
--		('Tomography'),
--		('Prenatal testing'),
--		('Myelography'),
--		('Mammography'),
--		('Ultrasound');

--insert into DoctorsSurveys(DoctorID, WardID, SurveyID, StartTime, EndTime, {DepID} )
--values (2, 12, 4, '10:00', '14:00'),
--		(6, 13, 10, '9:00', '15:00'),
--		(1, 5, 11, '13:00', '17:00'),
--		(3, 6, 8, '8:00', '13:00'), 
--		(4, 3, 5, '15:00', '18:00' ), 
--		(7, 8, 2, '14:00', '18:00' ), 
--		(1, 7, 3, '13:00', '15:00' ), 
--		(8, 4, 1, '11:00', '17:00'), 
--		(10, 14, 16, '9:00', '16:00'), 
--		(4, 12, 12, '9:00', '18:00')
