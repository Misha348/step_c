use Hospital_agregat

			--1
--select count(*) as [wards count]
--from Wards as W
--where W.Places > 5
---------------------------------------------------

			--2
--select D.Building as [corp.], count(W.Id) as [wards count] 
--from Departments as D, Wards as W
--where D.Id = W.DepartmentId
--group by D.Building
---------------------------------------------------

			--3
--select D.Name as [corp.], count(W.Id) as [wards count] 
--from Departments as D, Wards as W
--where W.DepartmentId = D.Id 
--group by D.Name
---------------------------------------------------

			--4
--select Dep.Name, sum(Doc.Premium) as [total premium]
--from Departments as Dep, Doctors as Doc
--where Dep.Id = Doc.DepartmentId
--group by Dep.Name 
--order by [total premium]
---------------------------------------------------

			--5
--select Dep.Name, count(Doc.Name) as DoctorsCount
--from Departments as Dep, Doctors as Doc, DoctorsSurveys as DS
--where DS.DepartmentID = Dep.Id and DS.DoctorID = Doc.Id 
--group by Dep.Name 
--having count(Doc.Name) > 0
---------------------------------------------------

			--6
--select count(D.Id) as Doctors,  sum(D.Salary+D.Premium)
--from Doctors as D
---------------------------------------------------

			--7
--select  avg(D.Salary+D.Premium) as AverSalary
--from Doctors as D
---------------------------------------------------

			--8
--select W.Name, W.Places
--from Wards as W
--where W.Places = (  select min(W.Places)
--					from Wards as W )
---------------------------------------------------

			--9
--select Dep.Building, sum(W.Places)
--from Wards as W, Departments as Dep
--where W.DepartmentId = Dep.Id and W.Places > 4 and (Dep.Building = 1 or Dep.Building = 2 or Dep.Building = 5)
--group by Dep.Building
--having sum(W.Places) > 10