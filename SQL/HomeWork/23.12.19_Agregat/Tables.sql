--create table Departments
--(
--	Id int identity not null primary key,	
--	Name nvarchar(100) not null default('no name') unique,
--	Building int not null check (Building between 1 and 5)
--)

--create table Wards
--(
--	Id int identity not null primary key,
--	Name nvarchar(30) not null default('no name'),
--	Places int not null,
--	DepartmentId int not null foreign key references Departments(Id)
--)

--create table Doctors
--(
--	Id int identity not null primary key,
--	Name nvarchar(max) not null default('no name'),
--	Surname nvarchar(max) not null default('no name'),
--	Premium money not null default(0) check(Premium >= 0),
--	Salary money not null default(0),
--)

--create table Suveys
--(
--	Id int primary key identity not null,
--	Name nvarchar(100) not null unique default('noname'),
--)

--create table DoctorsSurveys
--(
--	Id int identity not null primary key,
--	DoctorID int not null foreign key references Doctors(Id),
--	WardID int not null foreign key references Wards(Id),
--	SurveyID int not null  foreign key references Suveys(Id),
--	StartTime time not null  check(StartTime BETWEEN '8:00' AND '18:00'),
--	EndTime time not null,
--	check(StartTime < EndTime)
--)