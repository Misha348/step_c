use ProductShop

			--1

-- cannot create more than one clustered index on view 'table_name'.
-- guess there's no necessity fristly drop already existing index
-- and than create similar one, init??
-----------------------------------------------------------------------------------------------
			--2

--create nonclustered index ind_cliens_name on Clients(Name)
--select C.Name
--from Clients as C

-----------------------------------------------------------------------------------------------
			--3

--create nonclustered index ind_name_producer_selfprice on Products(Name, Produser, Selfprice)
--where Selfprice > 100
--select P.Name, P.Produser, P.Selfprice
--from Products as P
--where P.Selfprice > 100

-----------------------------------------------------------------------------------------------
			--4

--create nonclustered index ind_name on Clients(Name) include(Surname)
--select C.Name, C.Surname, C.Tel
--from Clients as C
-------------------------

--create nonclustered index ind_total_price on Sells(Price, Quantity) include(ProductId)
--select S.ProductId, S.Price*S.Quantity as Totalprice
--from Sells as S
--order by Totalprice
-----------------------------------------------------------------------------------------------
			--5
--create nonclustered index ind_name_surname on SellerStuff(Name, Surname) include(Possition)
--select SS.Name, SS.Surname, SS.Possition
--from SellerStuff as SS
--where SS.Possition = 'seller'
-------------------------

--create nonclustered index ind_sells_dateSels on Sells(DateSell)
--select S.DateSell
--from Sells as S
--where (select DATEDIFF(month, S.DateSell,  getdate() )) > 6
-----------------------------------------------------------------------------------------------
			