use SportShop
		--1
--go
--create or alter trigger AddToHistoryTable
--on Sells after insert
--as
--insert into History(Message)
--select 'sold ' + S.Name  + ' (' + convert(nvarchar(20), S.Quantity) + ') to' +  C.Name--convert(nvarchar(10), ClientID)
--from inserted as I join Sells as S on S.Name = I.Name
--				   join Clients as C on C.Id = S.Id

--insert into Sells(Name, Price, Quantity, DateSell, ClientID)
--values('cherry', 320, 50, '2019.05.18', 3 )

--select *
--from History
-------------------------------------------------------------------------------
		--2
--go
--create or alter trigger AddToArchive
--on Sells after insert
--as 
--begin
--	declare @amount int
--	select @amount = I.Quantity
--	from inserted as I join Products as P on I.Name = P.Name
	
--	insert into Archive(Message)	
--	select 'product ' + P.Name + ' was completly sold'
--	from inserted as I join Products as P on I.Name = P.Name	
--	where P.AvailabilityCount < @amount 

--end

--insert into Sells(Name, Price, Quantity, DateSell, ClientID)
--values('cherry', 320, 480, '2019.04.28', 1 )

--select *
--from Archive
-------------------------------------------------------------------------------
			--3
--go 
--create or alter trigger ForbidingToCreateExistingClient
--on Clients after insert
--as
--begin
--	declare @email nvarchar(15)
--	select @email = I.Email
--	from inserted as I
--	if( (select count(*)
--		from Clients as C
--		where C.Email = @email) > 1)
--	print @email + ' already exist'
--	begin
--		rollback tran
--		return
--	end
--end;

--insert into Clients(Name, Surname, Email, Tel, Discount)
--values('Petya', 'Lipych', 'ppp@ppp.com', '097 333 3333', 8)

--insert into Clients(Name, Surname, Email, Tel, Discount)
--values('Kolya', 'Bogomaz', 'bbb@bbb.com', '098 111 1111', 18)
-------------------------------------------------------------------------------
			--4
--go 
--create or alter trigger BanToDeleteExistingClients
--on Clients after delete
--as
--begin
--	declare @email nvarchar(15)
--	select @email = d.Email
--	from deleted as d	
--	if(@email = any(select Cl.Email
--				    from Clients as Cl ))
--	print 'immpossible delete client'
--	begin	
--		rollback tran
--		return
--	end
--end;

--delete from Clients where Email = 'bbb@bbb.com'
-------------------------------------------------------------------------------
			--5
--go 
--create or alter trigger CheckTotalSum
--on Sells  after insert
--as
--begin
--	declare @Sum int
--	declare @CurrClientId int
	
--	select @Sum = I.Price * I.Quantity, @CurrClientId = I.ClientID
--	from inserted as I, Clients as C
--	where I.ClientID = C.Id	
	
--	if( (select count(*)
--		 from ClientsPurchases as PC
--		 where PC.ClientId = @CurrClientId) < 1)
--		 begin
--		 	insert into ClientsPurchases(ClientId, TotalSum)
--		 	values(@CurrClientId, @Sum)
--		 end
--	else
--		 begin
--			update	ClientsPurchases
--			set TotalSum = TotalSum + @Sum
--			where ClientsPurchases.ClientId = @CurrClientId
--		 end
	
--	declare @currSum int
--	select @currSum = CP.TotalSum
--	from ClientsPurchases as CP
--	where CP.ClientId = @CurrClientId

--	if(@currSum >= 50000)
--		begin
--			update Clients
--			set Discount = 15
--			where Clients.Id = @CurrClientId			
--			print ' total sum = ' + convert(nvarchar(20), @currSum) + '   discount = 15'
--		end 
--end

--insert into Sells(Name, Price, Quantity, DateSell, ClientID)
--values('peanut', 600, 40, '2019.02.02', 1)

--insert into Sells(Name, Price, Quantity, DateSell, ClientID)
--values('banana', 350, 170, '2019.10.19', 1)
-------------------------------------------------------------------------------
			--6
--go
--create or alter trigger BanToAddProduct
--on Products instead of insert
--as
--begin
--	insert into Products(Name, Produser, AvailabilityCount, Selfprice, Sellprice)
--	select P.Name, P.Produser, P.AvailabilityCount, P.Selfprice, P.Sellprice
--	from Products as P
--	where P.Produser = 'fresh meat' or P.Produser = 'fresh fish'
--	print 'forbidden produser for inserting'
--end
	
--insert into Products(Name, Produser, AvailabilityCount, Selfprice, Sellprice)
--values('pork', 'fresh meat', 200, 180, 250) 
-------------------------------------------------------------------------------
			--7
--go 
--create or alter trigger CheckAmountOfProducts
--on Sells after insert
--as
--begin
--	declare @sellProdName nvarchar(20)
--	declare @sellProdQuant int
	
--	select @sellProdName = I.Name 
--	from inserted as I, Products as P
--	where I.Name = P.Name

--	select @sellProdQuant = I.Quantity 
--	from inserted as I
--	where I.Name = @sellProdName
-----
--	declare @prodQuantInStorage int

--	select @prodQuantInStorage = P.AvailabilityCount
--	from Products as P
--	where P.Name = @sellProdName

--	if(@prodQuantInStorage < @sellProdQuant)
--		begin
--			print 'there is less ' + @sellProdName  + ' in storage '
--			print ' deal - canceled'
--			delete Sells
--			where Sells.Name = @sellProdName and Sells.Quantity = @sellProdQuant
--		end
--	else
--	print 'AAAA, ' + @sellProdName + convert(nvarchar(20), @sellProdQuant)	
--end

--insert into Sells(Name, Price, Quantity, DateSell, ClientID)
--values('strawberry', 150, 500, '2019.01.01', 3) 
-------------------------------------------------------------------------------