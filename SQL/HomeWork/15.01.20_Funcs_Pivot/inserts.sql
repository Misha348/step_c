use ProductShop

--insert into Products(Name, Produser, AvailabilityCount, Selfprice, Sellprice)
--values('apple', 'Garden & co', 500, 250, 400), 
--	    ('peanut', 'Somali & co', 300, 400, 600), 
--	    ('strawberry', 'Polacs', 450, 150, 300), 
--	    ('cherry', 'Ukrainer', 400, 180, 320), 
--	    ('banana', 'Ecuador', 450, 200, 350),		
--		('pineapple', 'Garden & co', 300, 150, 300), 
--		('orange', 'Somali & co', 600, 300, 700),
--		('lemon', 'Polacs', 550, 250, 400), 
--		('plum', 'Ukrainer', 700, 180, 320), 
--		('cacao', 'Ecuador', 850, 400, 950);		

--insert into Clients(Name, Surname, Email, Tel, Discount)
--values('Petya', 'Lipych', 'ppp@ppp.com', '097 111 1111', 8), 
--		('Vasya', 'Papezhuk', 'vvv@vvv.com', '095 222 2222', 12 ), 
--		('Vadym', 'Spiridos', 'sss@sss.com', '096 333 3333', 12),
--		('Igor', 'Dedera', 'iii@iii.com', '096 222 2222', 10 ),
--		('Vladislav', 'Gotovkin', 'ggg@ggg.com', '093 333 3333', 6);

--insert into SellerStuff(Name, Surname, Possition, StartEmployment, Salary)
--values ('Dima', 'Shevchuk', 'beginer-seller', '2019.12.12', 10000), 
--		 ('Yura', 'shpak', 'seller', '2018.12.12', 15000), 
--		 ('Kola', 'Gay', 'seller', '2017.08.23', 14000), 
--		 ('Galya', 'Fed', 'beginer-seller', '2019.11.18', 9000), 
--		 ('Vadym', 'Bilous', 'super-visor', '2016.05.10', 20000), 
--		 ('Yulya', 'Fedyna', 'seller', '2018.01.02', 12000); 	   

--insert into Sells(Price, Quantity, DateSell, ProductId, ClientId, SellerStuffId)
--values(600, 80, '2019.01.01', 2, 1, 2 ), 
--		(350, 170, '2019.10.19', 5, 3, 3), 
--		(320, 40, '2019.12.12', 4, 4, 5 ), 
--		(600, 40, '2019.04.27', 2, 3, 6 ), 
--		(300, 150, '2019.01.01', 3, 3, 1), 
--		(300, 20, '2019.03.30', 6, 5, 2 ), 
--		(700, 100, '2019.08.27', 7, 2, 4), 
--		(400, 70, '2019.03.18', 8, 1, 5 ), 
--		(320, 120, '2019.04.19', 9, 4, 3), 
--		(950, 200, '2019.05.10', 10, 5, 1);
