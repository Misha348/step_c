use ProductShop

			--1
--create user Dima without login
--deny select on dbo.SellerStuff to Dima

-----------------------------------------

			--2
--create user Sashko without login
--grant select, update on dbo.SellerStuff(Name, Surname) to Sashko
-----------------------------------------

			--3
--revert
--create user Mariya without login
--grant select, insert, delete, update on dbo.Products to Mariya
--grant select, insert, delete, update on dbo.Clients to Mariya
--grant select, insert, delete, update on dbo.SellerStuff to Mariya
--grant select, insert, delete, update on dbo.Sells to Mariya
-----------------------------------------

			--4
--revert
--create user Pavlo without login
--grant select, insert, delete, update on dbo.Sells to Pavlo
--grant select on dbo.Products to Pavlo
-----------------------------------------

			--5
--create user Nazar without login
--grant insert, delete on dbo.Clients to Nazar
-----------------------------------------

			--6
--create user Tolyan without login
--deny insert, delete, update on dbo.Products to Tolyan
--deny insert, delete, update on dbo.Clients to Tolyan
--deny insert, delete, update on dbo.SellerStuff to Tolyan
--deny insert, delete, update on dbo.Sells to Tolyan
-----------------------------------------

			--7
--create user Bohdan without login
--grant create table  to Bohdan
--grant create function to Bohdan
--grant create view  to Bohdan
----grant create trigger to Bohdan  ?? 
-----------------------------------------

			--8
--create user Misha without login
--create user Anna without login

--grant select on dbo.Products to Misha, Anna
--grant update on dbo.Products(Name, Produser, AvailabilityCount ) to Misha, Anna









