use BarberShop
			--1
--go
--create or alter function F1()
--returns table
--as
--return (select *
--		from Barbers as B
--		where B.EmploymentDate = (select min(B.EmploymentDate)
--								  from Barbers as B))
--select * from dbo.F1()
-------------------------------------------------------------------
			--2
--go
--create or alter function F2 (@from date, @till date)
--returns table
--as
--return(select top(1) B.Name, B.Surname, count(*) as maxCountOfservicedClients
--		from Barbers as B join VisitingArchive as VA on B.Id = VA.BarberId and VA.Date between @from and @till
--		group by B.Name, B.Surname
--		order by maxCountOfservicedClients desc)

--select * from dbo.F2('2019.01.01', '2019.03.27') 
-------------------------------------------------------------------
			--3

--go
--create or alter function F3 ()
--returns table
--as
--return(select C.Name, C.Surname, C.Patronymic, count(*) as max_Count_Of_Visiting
--		from Customers as C join VisitingArchive as VA on C.Id = VA.CustomerId 
--		group by C.Name, C.Surname, C.Patronymic
--		having count(*) > 1 )

--select * from dbo.F3() 
-------------------------------------------------------------------
			--4
--go
--create or alter function F4 ()
--returns table
--as
--	return (select top(1) C.Name, C.Surname, C.Patronymic, sum(TotalPrice) as TotalSum
--			from Customers as C join VisitingArchive as VA on C.Id = VA.CustomerId
--			group by C.Name, C.Surname, C.Patronymic
--			order by TotalSum desc  )

--select * from dbo.F4() 
-------------------------------------------------------------------
			--5
--go
--create or alter function F5 ()
--returns table
--as
--	return (select top(1) *
--			from [Services] as S
--			order by S.Duration desc)

--select * from dbo.F5() 
-------------------------------------------------------------------
-------------------------------------------------------------------
			--1
--go
--create or alter function F6 ()
--returns table
--as
--return(select top(1) B.Name, B.Surname, count(*) as CountOfservicedClients
--		from Barbers as B join VisitingArchive as VA on B.Id = VA.BarberId
--		group by B.Name, B.Surname
--		order by CountOfservicedClients desc)

--select * from dbo.F6()
-------------------------------------------------------------------
			--2
--go
--create or alter function F7 (@month int)
--returns table
--as
--	return( select top(1)  B.Name, B.Surname, sum(TotalPrice) as TotalSum
--			from VisitingArchive as VA join Barbers as B on VA.BarberId = B.Id 
--			where @month = month(VA.Date)
--			group by B.Name, B.Surname
--			order by TotalSum desc)

--select * from dbo.F7(4)

-------------------------------------------------------------------
			--3
--go
--create or alter function F8 (@estimType int)
--returns table
--as
--	return(select top(3)  B.Name, B.Surname, count(*) as Count_Of_Serviced_Clients
--			from Barbers as B join VisitingArchive as VA on B.Id = VA.BarberId
--								join Estimations as E on B.Id = E.BarberId 
--			where E.TypeId = @estimType
--			group by B.Name, B.Surname		
--			having count(*) > 1)

--select * from dbo.F8(3)
-------------------------------------------------------------------
			--4
--go
--create or alter function F9 (@barbName nvarchar(15))
--returns table
--as
--	return (select B.Name, B.Surname, S.Day, S.StartTime, S.EndTime
--			from Barbers as B join Schedule as S on B.Id = S.BarberId
--			where B.Name = @barbName)

--select * from dbo.F9('Andrii')
-------------------------------------------------------------------
			--5 (dont work)
--go
--create or alter function F10 (@barId int, @day int)
--returns nvarchar(200)
--as
--begin
--	declare @begFreeTime time
--	declare @endFreeTime time
--	declare @bName nvarchar(20)	

--		  (select @bName = B.Name
--		   from Barbers as B
--		   where B.Id = @barId)

--		   select @begFreeTime = 
--		   from Schedule as S, Records as R
--end

--select * from dbo.F10(3, 2)
-------------------------------------------------------------------
			--6
--go 
--create or alter procedure RemoveNotesFroRecordsToHistory
--as
--	declare @notesIdCount int
--	select @notesIdCount = count(R.Id)
--	from Records as R
--	-------------------------
--	declare @notesDate date
--	declare @minId int

--	if(@notesIdCount > 0)
--	begin
--		while @notesIdCount > 0
--			begin
--				select @minId = min(R.Id)
--				from Records as R

--				select @notesDate = R.Date
--				from Records as R
--				where R.Id = @minId				

--				if(@notesDate <  getdate())
--					begin
--						declare @customerId int
--						declare @barberId int
--						declare @date date
--						declare @time time

--						select @customerId = R.CustomerId, @barberId = R.BarberId, @date = R.Date, @time = R.Time
--						from Records as R
--						where R.Date = @notesDate

--						delete from Records where Id = @minId
--						insert into History(CustomerId, BarberId, [Date], [Time])
--						values(@customerId, @barberId, @date, @time)

--						set @notesIdCount = @notesIdCount - 1
--					end
--				else
--					set @notesIdCount = @notesIdCount - 1
--			end
--	end

-- (	WARNING !!!  executing this proc might broke work of another funcs!! execute it  eventually !!!)
--exec RemoveNotesFroRecordsToHistory 
-------------------------------------------------------------------
			--7
--go 
--create or alter trigger ProhibitionToAddCustomer
--on Records after insert
--as
--begin
--	declare @barId int
--	declare @orddate date
--	declare @ordTime time
		
--		select @barId = I.BarberId, @orddate = I.Date, @ordTime = I.Time
--		from inserted as I 

--		if( (select count(*)
--			from Records as R
--			where R.BarberId = @barId and
--					R.Date = @orddate and
--					R.Time = @ordTime) > 1 )
--		print 'already ordered'
--		begin
--			rollback tran
--			return
--		end
--end

--insert into Records(CustomerId, BarberId, [Date], [Time])
--values(4, 1, '2020.02.01', '10:00') 
-------------------------------------------------------------------
			--8
--go
--create or alter trigger ProhibitionToAddJunior
--on Barbers after insert
--as
--begin
--	declare @junCount int

--	select @junCount = count(B.Id)
--	from Barbers as B, Positions as P
--	where B.PositionId = P.Id and P.Name = 'Junior Barber'

--	if(  (select I.PositionId
--	      from inserted as I 
--	      where I.PositionId = (select P.Id
--								from Positions as P
--								where P.Name = 'Junior Barber')) =(select P.Id 
--																  from Positions as P
--																  where P.Name = 'Junior Barber') and @junCount >= 2 )
						
--	print 'already work at least 2 juniors'
--	begin
--		rollback tran
--		return
--	end	
--end

--INSERT INTO Barbers([Name], [Surname], Sex, Phone, Email, [Birth Date], [EmploymentDate], [PositionId])
--VALUES('Jimm', 'Kastrel', 'man', '+380966515380', 'Kastrel@gmail.com', '1988-05-15', '2016-01-24', 3)
-------------------------------------------------------------------
			--9
--go
--create or alter function F11 ()
--returns table
--as
--	return ( select C.Name, C.Surname
--			 from Customers as C join  Estimations as E on C.Id = E.CustomerId
--								 join Feedbacks as F on C.Id = F.CustomerId
--			where E.CustomerId = 0 and F.CustomerId = 0			 )
			

--select * from dbo.F11()
-------------------------------------------------------------------
			--10

--go
--create or alter function F12 ()
--returns table
--as
--	return ( select C.Name, C.Surname, VA.Date
--			 from Customers as C join VisitingArchive as VA on C.Id = VA.CustomerId
--			 where datediff(MONTH, VA.Date, getdate()) >= 12 )

--select * from dbo.F12()
-------------------------------------------------------------------