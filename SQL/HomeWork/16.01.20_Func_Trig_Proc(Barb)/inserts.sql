--insert into Positions([Name])
--values('Chief Barber'),
--		('Signor Barber'),
--		('Junior Barber')

--insert into EstimationTypes
--values('Worst'),
--		('Dislike'),
--		('Neutral'),
--		('Like it'),
--		('Best');

--INSERT INTO Barbers([Name], [Surname], Sex, Phone, Email, [Birth Date], [EmploymentDate], [PositionId])
--VALUES('Bob', 'Bobenko', 'Man', '+380964512387', 'BobBobenko@gmail.com', '1978-02-25', '2015-03-14', 1),
--		('Max', 'Maxsymenko', 'Man', '+38063698741', 'MaxsymenkoMax@gmail.com', '1988-08-15', '2018-04-24', 2),
--		('Andrii', 'Andrienko', 'Man', '+38068124785', 'Andrii@gmail.com', '1986-01-20', '2017-05-13', 2),
--		('Mariia', 'Ponomarenko', 'Woman', '+380681257963', 'Ponomarenko@gmail.com', '1995-01-23', '2019-08-10', 3),
--		('Petro', 'Petrenko', 'Man', '+380954123584', 'Petrenko@gmail.com', '1997-01-23', '2019-11-14', 3)


--insert into Schedule (BarberId, [Day], [StartTime], [EndTime] )
--values(1, 4, '10:00', '18:00'),
--		(2, 1, '11:00', '15:30'),
--		(3, 2, '9:00', '18:00'), 
--		(4, 6, '8:00', '20:00'),
--		(5, 2, '10:00', '17:00')

--insert into Records(CustomerId, BarberId, [Date], [Time])
--values(1, 1, '2020.02.01', '10:00'), 
--		(2, 2, '2020.02.03', '12:00'),
--		(3, 3, '2020.02.05', '9:00'),
--		(4, 4, '2020.02.03', '13:00'),
--		(5, 5, '2020.01.30', '12:00')

--INSERT Customers 
--VALUES('Dmitro','Gorobchik','Artemovich','+3805553535','unichtojitel@mega.net'),
--		('Robert', 'Gorobchik', 'Dmitrovich', '+3805979798','unichtojitel2@mega.net'),
--		('Herald', 'Rivnenskii', 'NONE','+38034623456','Plotva@gmail.net'),
--		('Albert', 'Maximovich','Georgievich','+38023214122','fish@new.net'),
--		('Maxim','Konopko', 'Robertovich','+7324554323','hesab@gmail.com'),
--		('Ivan', 'Kvitnickii', 'Olegovich','+482345432134','grush@gmail.com')

--insert into [Services]([Name],[Price],[Duration]) 
--VALUES('haircut beard', 150, '00:20'),
--	    ('semi-box', 300, '00:40'),
--	    ('box', 300, '00:35'),
--	    ('hair styling', 90, '00:15'),
--	    ('crop', 350, '00:50'),
--	    ('UNDERCUT', 290, '00:30')

--insert into VisitingArchive(CustomerId, BarberId, ServiceId, [Date], TotalPrice)
--values(1, 2, 3, '2019.01.12', 250), 
--		(1, 2, 2, '2019.02.12', 300), 
--		(2, 1, 5, '2019.03.12', 350), 
--		(2, 3, 6, '2019.04.10', 400), 
--		(3, 5, 1, '2019.03.23', 245), 
--		(3, 1, 4, '2019.04.20', 200), 
--		(4, 2, 2, '2019.11.21', 230), 
--		(4, 5, 6, '2019.05.19', 120) 
--insert into VisitingArchive(CustomerId, BarberId, ServiceId, [Date], TotalPrice)
--values(5, 3, 1, '2019.04.15', 200)

--insert into Feedbacks([Message], [CustomerId], [BarberId])
--values('bla-bla, not bad', 1, 3), 
--		('bla-bla, good', 2, 1), 
--		('bla-bla, wonderful', 3, 5), 
--		('bla-bla, might be better', 4, 2),
--		('bla-bla, norm', 5, 1);

--insert into Estimations(BarberId, CustomerId, TypeId)
--values(1, 6, 5),
--		(2, 5, 2),
--		(3, 4, 1),
--		(4, 3, 4),
--		(5, 2, 3),
--		(1, 1, 3);

--insert into BarbersServices([ServiceId], [BarberId] )
--values(1, 1),
--		(2, 1),
--		(3, 2),
--		(4, 2),
--		(5, 3),
--		(6, 3),
--		(1, 4),
--		(1, 4),
--		(4, 5),
--		(6, 5);