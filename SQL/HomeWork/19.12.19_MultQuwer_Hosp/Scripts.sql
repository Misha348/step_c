			--1
--select D.Name, D.Surname, S.Name
--from Doctors as D, Specializations as S, DoctorsSpecialization as DS
--where DS.DoctorId = D.Id and DS.SpecializationId = S.Id
------------------------------------------------------------------------------------
			--2
--select D.Surname,  D.Salary+D.Premium as [salary]
--from Doctors as D, Vacations as V
--where D.Id = V.DoctorId and V.EndDate < '2020.01.01'
------------------------------------------------------------------------------------
			--3
--select W.Name, D.Name as [Depart]
--from Departments as D, Wards as W
--where D.Id = W.DepartmentId  and  D.Name = 'Neurological'
------------------------------------------------------------------------------------
			--4
--select distinct D.Name
--from Sponsors as S, Departments as D, Donations as Don
--where D.Id = Don.DepartmentId and Don.SponsorId = S.Id and S.Name = 'Honesty'
------------------------------------------------------------------------------------

			--5
--SELECT DateAdd(month, -1, Convert(date, GetDate()));
--select Dep.Name as [Dep], Sp.Name as [Sponsor], Don.Amount, Don.Date
--from Departments as Dep, Sponsors as Sp, Donations as Don
--where Dep.Id = Don.DepartmentId and Sp.Id = Don.SponsorId and Don.Date > DateAdd(year, -2, Convert(date, GetDate())) 

--SELECT CONVERT(VARCHAR(10), getdate(), 102);
------------------------------------------------------------------------------------

			--6
--select Dep.Name, Don.Amount
--from Donations as Don, Departments as Dep 
--where Don.DepartmentId = Dep.Id and Don.Amount > 15000
------------------------------------------------------------------------------------

			--7
--select W.Name as [Ward], W.DepartmentId
--from Wards as W
--where W.DepartmentId = (select Dep.Id
--						from Doctors as D, Departments as Dep
--						where Dep.Id =  D.DepartmentID and D.Name = 'Luiza')
------------------------------------------------------------------------------------
			
			--8
--select  Dep.Name, D.Name, Don.Amount
--from Donations as Don, Departments as Dep, Doctors as D
--where Dep.Id = Don.DepartmentId and Dep.Id = D.DepartmentID and Don.Amount > 15000
------------------------------------------------------------------------------------
			--9
--select Dep.Name
--from Doctors as Doc, Departments as Dep
--where Dep.Id = Doc.DepartmentID and Doc.Premium between 1000 and 1500
------------------------------------------------------------------------------------

			--10
--select Spec.Name
--from Specializations as Spec
--where Spec.DeseasSeverityCover > ( select D.DeseasSeverity
--									 from Deseases as D
--									 where D.DeseasSeverity <= 3
--								   )
------------------------------------------------------------------------------------

			--11
--declare @d1 date
--set @d1 = CONVERT(VARCHAR(10), getdate(), 102)

--select Dep.Name, D.Name, S.Date
--from Surveys as S, Departments as Dep, Deseases as D
--where Dep.Id = S.DepartmentID and D.Id = S.DeseasesID and S.Date between '2016.01.01' and @d1
------------------------------------------------------------------------------------
			--12

--select Dep.Name as [Dep], W.Name as [Ward]
--from Surveys as S, Deseases as D, Departments as Dep, Wards as W
--where Dep.Id = S.DepartmentID and W.Id = S.WardID and S.DeseasesID = D.Id and D.Type = 'infectious'


																	
