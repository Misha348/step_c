use Hosp_Mult_Query2

--create table Buildings
--(
--	Id int identity not null primary key,
--	Name nvarchar(100) not null default('no name') unique
--)

--create table Departments
--(
--	Id int identity not null primary key,	
--	Name nvarchar(100) not null default('no name') unique,
--	BuildingID int not null foreign key references Buildings(Id)
--)

--create table Wards
--(
--	Id int identity not null primary key,
--	Name nvarchar(30) not null unique default('no name'),
--	DepartmentId int not null foreign key references Departments(Id)
--)
-------------------------------------------------------------------------------
--create table Doctors
--(
--	Id int identity not null primary key,
--	Name nvarchar(max) not null default('no name'),
--	Surname nvarchar(max) not null default('no name'),
--	Premium money not null default(0) check(Premium >= 0),
--	Salary money not null default(0),
--	DepartmentID int not null foreign key references Departments(Id)
--)

--create table Specializations
--(
--	Id int identity not null primary key,
--	Name nvarchar(100) not null unique,
--	DeseasSeverityCover int not null
--)

--create table DoctorsSpecialization
--(
--	Id int identity not null primary key,
--	DoctorId int not null foreign key references Doctors(Id),
--	SpecializationId int not null foreign key references Specializations(Id)
--)

--create table Vacations
--(
--	Id int identity not null primary key,
--	StartDate date not null,
--	EndDate date not null,
--	DoctorId int not null foreign key references Doctors(Id),
--	check(EndDate > StartDate)
--)
-------------------------------------------------------------------------------
--create table Deseases
--(
--	Id int identity not null primary key,
--	Name nvarchar(25) not null unique,
--	Type nvarchar(25) not null unique,
--	DeseasSeverity int not null default(1) check(DeseasSeverity >=1)
--)

--create table Surveys
--(
--	Id int primary key identity not null,
--	Name nvarchar(100) not null unique default('noname'),
--	Date date not null default('2020.01.01'),
--	DayOfWeek int not null check(1<= DayOfWeek and 7 >= DayOfWeek),
--	DeseasesID int not null foreign key references Deseases(Id),
--	DepartmentID int not null foreign key references Departments(Id),
--	WardID int not null foreign key references Wards(Id),
--	DoctorID int not null foreign key references Doctors(Id)	
--)
-------------------------------------------------------------------------------

--create table Sponsors
--(
--	Id int identity not null primary key,
--	Name nvarchar(100) not null unique
--)

--create table Donations
--(
--	Id int identity primary key,
--	Amount money not null check(Amount >= 0),
--	Date date not null default('2020.01.01'),
--	DepartmentId int not null foreign key references Departments(Id),
--	SponsorId int not null foreign key references Sponsors(Id)
--)
-------------------------------------------------------------------------------




