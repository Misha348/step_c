--create table Countries
--(
--	Id int primary key identity not null,
--	Name nvarchar(10) not null,
--	PhoneCode int not null
--)

--create table Groups
--(
--	Id int primary key identity not null,
--	Name nvarchar(10) not null,
--	YearOfStuding int not null	

--)

--create table Students
--(
--	Id int primary key identity not null,
--	Name nvarchar(10) not null check(Name LIKE '[a-z,A-Z]'),
--	Surname nvarchar(10) not null check(Surname LIKE '[A-Z]%'),
--	Birthdate date not null,
--	Age int not null,
--	GroupId int not null foreign key references Groups(Id),
--	CountryId int not null foreign key references Countries(Id)
--)


create table Subjects
(
	Id int primary key identity not null,
	Name nvarchar(15) not null check(Name LIKE '[a-z,A-Z]%'),
)