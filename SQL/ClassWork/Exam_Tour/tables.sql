﻿use TravelAgency_Polishchuk_and_Company

		--POSITIONS
--CREATE TABLE Positions  
--(
--	Id int identity(1,1) primary key,
--	Name nvarchar(50) not null
--)​


--		EMPLOYEES
--create table Employees
--(
--	Id int identity(1,1) primary key,
--	FirstName nvarchar(25) not null check(FirstName!=''),
--	LastSurname nvarchar(25) not null check(LastSurname!=''),
--	Patronymic nvarchar(25) not null check(Patronymic!=''),	
--	PositionId int references Positions(Id),
--	Phone nvarchar(50) not null,
--	Email nvarchar(50) not null,
--	EmploymentDate date not null check( EmploymentDate <= getdate() )
--)
--------------------------------------------------------------------------------

		--COUNTRY
--create table Coutries
--(
--	Id int identity(1,1) primary key,
--	CountryName nvarchar(25) not null
--)

		--CITIES
--create table Cities
--(
--	Id int identity(1,1) primary key,
--	CityName nvarchar(25) not null, 
--	CoutrysId int not null references Coutries(Id)
--)

			--TRANSPORTTYPE
--create table TransportType
--(
--	Id int identity(1,1) primary key,
--	TransportName nvarchar(30) not null
--)

--create table Tours
--(
--	Id int identity(1,1) primary key,
--	TourName nvarchar(25) not null,
--	Price money not null check(Price > 1000), 
--	TransportTypeId int references TransportType(Id), 
--	StartDate date not null, 
--	EndDate date not null, 		
--	MaxTouristsQuantity int not null check(MaxTouristsQuantity between 50 and 80), 
--	IsDeleted bit not null, 	
--	check(StartDate < EndDate)
--)

		--VISITING
--create table Visitings
--(
--	Id int identity(1,1) primary key,	
--	CityId int references Cities(Id),
--	TourId int references Tours(Id), 
--	VisitingDate date not null,	
--)

--create table Attractions
--(
--	Id int identity(1,1) primary key,
--	AttractionName nvarchar(35) not null,
--	VisitingId int references Visitings(Id),
--	ExtraPrice nvarchar(5) not null, 	
--)

--create table AttractionImages
--(
--	Id int identity(1,1) primary key,
--	Name nvarchar(250) not null, 
--	AttractionId int not null references Attractions(Id)
--)
-------------------
--create table Hotels
--(
--	Id int identity(1,1) primary key,
--	HotelName nvarchar(35) not null,
--	StarRating int not null check (StarRating between 2 and 5), 	
--	CityId int references Cities(Id),	
--)

--create table HotelImages
--(
--	Id int identity(1,1) primary key,
--	Name nvarchar(250) not null, 
--	HotelId int not null references Hotels(Id)
--)

-------------------------------------------------------

--create table EmployeeTourResponsibilities
--(
--	Id int identity(1,1) primary key,
--	EmployeeId int references Employees(Id), 
--	TourId int references Tours(Id)
--)

--create table EmployeeCountryResponsibilities
--(
--	Id int identity(1,1) primary key,
--	EmployeeId int references Employees(Id), 
--	CountryId int references Coutries(Id), 
--)
-----------------------------------------------------
		--CLIENTS
--create table Clients
--(
--	Id int identity(1,1) primary key,
--	FirstName nvarchar(25) not null check(FirstName!=''),
--	LastSurname nvarchar(25) not null check(LastSurname!=''),
--	Patronymic nvarchar(25) not null check(Patronymic!=''),	
--	Phone nvarchar(50) not null,
--	Email nvarchar(50) not null,
--	BirthDate date not null, check(BirthDate < getdate())
--)

--create table TourPayments
--(
--	Id int identity(1,1) primary key,
--	TourId int not null references Tours(Id),
--	ClientId int not null references Clients(Id),
--)

--create table Interesteds
--(
--	Id int identity(1,1) primary key,
--	ClientId int not null references Clients(Id), 
--	TourId int not null references Tours(Id)
--)

--create table ClientFutureTours
--(
--	Id int identity(1,1) primary key,
--	ClientId int not null references Clients(Id), 
--	TourId int not null references Tours(Id)
--)

--create table ArchiveTours
--(
--	Id int identity(1,1) primary key	
--)
--go
--alter table ArchiveTours
--add TourName nvarchar(25) not null

--go
--alter table ArchiveTours
--add StartDate date not null

--go
--alter table ArchiveTours
--add	EndDate date not null

--go
--alter table ArchiveTours
--add TouristsQuantity int not null


--create table ClientsPastTours
--(
--	Id int identity(1,1) primary key,
--	ClientId int not null references Clients(Id), 
--	ArchiveTourId int not null references ArchiveTours(Id)
--)


--('To Germany', 1250, 2, '2020.01.31', '2020.02.03', 70, 0), 
--('To Cairo', 1350, 1, '2020.02.01', '2020.02.06', 80, 0), 
--('To Baltim', 1450, 1, '2020.02.11', '2020.02.18', 80, 0), 
--('To Port-Said', 1880, 1, '2020.02.14', '2020.02.28', 80, 0),
--('To Rashid', 1780, 1, '2020.02.24', '2020.03.01', 70, 0),
--('To Austria', 1550, 2, '2020.02.21', '2020.02.28', 80, 0),
--('To London', 1900, 1, '2020.04.01', '2020.04.07', 50, 0),
--('To Oslo', 1750, 1, '2020.03.11', '2020.03.16', 50, 0),
--('To France', 1650, 1, '2020.03.22', '2020.03.26', 50, 0)