use TravelAgency_Polishchuk_and_Company
					--- INDEXES ---

					--1
--create nonclustered index TourName_Date_Price on Tours(TourName, StartDate, EndDate, Price )
-------------------------------------------------------------

					--2
--create nonclustered index Clients_Tour_Payments on TourPayments(ClientId) include(TourId)
-------------------------------------------------------------

					--3
--create nonclustered index Hotels on Hotels(HotelName, CityId) include(StarRating)
-------------------------------------------------------------

					--4
--create nonclustered index Tour_Interesteds on Interesteds(TourId, ClientId)
-------------------------------------------------------------

					--5
--create nonclustered index ToursArchives on ArchiveTours(TourId)



--------------------------------------------------------------------------------------
					--- ROLES ---

					--1
----create user Oxana_Managing_Director without login
--alter role  db_owner add member Oxana_Managing_Director
-------------------------------------------------------------
					--2
----create user Yuliya_Advisor without login

--grant select on dbo.Coutries to Yuliya_Advisor
--grant select on dbo.Tours to Yuliya_Advisor
-------------------------------------------------------------
					--3
----create user Katya_Bookkeeper without login
--grant select, insert, update on dbo.Employees to Katya_Bookkeeper
-------------------------------------------------------------

					--4
----create user Luca_Sysadmin without login

--go
--exec sp_addrolemember 'db_backupoperator', 'Luca_Sysadmin'
-------------------------------------------------------------

					--5

----create user Ostap_Sysadmin_Asistant without login

--go
--exec sp_addrolemember 'db_accessadmin', 'Ostap_Sysadmin_Asistant'

