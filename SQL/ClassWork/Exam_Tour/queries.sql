use TravelAgency_Polishchuk_and_Company

					--1
--go
--create view ActualTours as
--select * 
--from Tours

--select *
--from ActualTours
------------------------------------------------------------------
					--2
--go
--create function F1(@from date, @till date)
--returns table
--as
--	return ( select *
--			 from Tours as T
--			 where T.StartDate between @from and @till )

--select * from dbo.F1('2020.02.01', '2020.03.01')
------------------------------------------------------------------
					--3

--go
--create or alter function F2( @Country nvarchar(20) )
--returns table
--as
--	return (select T.TourName, T.Price, T.StartDate, T.TransportTypeId
--			from Tours as T join Visitings as V on T.Id = V.TourId
--							join Cities as Cit on V.CityId = Cit.Id
--							join Coutries as Cou on Cit.CoutrysId = Cou.Id
--							where Cou.CountryName = @Country)

--select * from dbo.F2('Egypt')
------------------------------------------------------------------
					--4
--go
--create or alter function F3()
--returns table
--as
--	return (select top(1) C.CountryName, count(*) as Max_Popularity
--			from Coutries as C join Cities as Cit on C.Id = Cit.CoutrysId
--								join Visitings as V on Cit.Id = V.CityId
--								join Tours as T on V.TourId = T.Id
--			group by C.CountryName
--			order by Max_Popularity desc )

--select * from F3()
------------------------------------------------------------------
					--5
--go
--create or alter function F4()
--returns table
--as
--	return (select top(1) T.TourName, count(*) as Max_Sold_Tickets
--			from Tours as T join TourPayments as TP on T.Id = TP.TourId							
--			group by T.TourName
--			order by  Max_Sold_Tickets desc)

--select * from dbo.F4()
------------------------------------------------------------------
					--6
--go
--create or alter function F6()
--returns table
--as
--	return ( select top(1) T.TourName, sum(T.TouristsQuantity) as TotSoldTickets
--			 from ArchiveTours as T
--			 group by T.TourName
--			 order by TotSoldTickets desc )

--select * from dbo.F6()

------------------------------------------------------------------
					--7
--go
--create or alter function F5()
--returns table
--as
--	return (select top(1) T.TourName, count(*) as Min_Sold_Tickets
--			from Tours as T join TourPayments as TP on T.Id = TP.TourId							
--			group by T.TourName
--			order by  Min_Sold_Tickets )

--select * from dbo.F5()									 
------------------------------------------------------------------
					--8
--go
--create or alter function F8(@name nvarchar(20), @surname nvarchar(20), @patronimic nvarchar(20))
--returns table
--as 
--	return ( select  T.TourName, T.StartDate, T.EndDate
--			 from Tours as T join TourPayments as TPay on T.Id = TPay.TourId
--							 join Clients as C on C.Id = TPay.ClientId
--							 join ClientsPastTours as CPT on C.Id = CPT.ClientId --
--							 join ArchiveTours as TA on TA.Id = CPT.ArchiveTourId
--							 join Clients as Cl on Cl.Id = CPT.ClientId
--							 where C.FirstName = @name and C.LastSurname = @surname and C.Patronymic = @patronimic )	

--select * from dbo.F8('Uuuuuu', 'Uuuuuu', 'Uuuuu')	

------------------------------------------------------------------
					--9
--go
--create or alter function F9(@name nvarchar(20), @surname nvarchar(20), @patronimic nvarchar(20))
--returns table
--as 
--	return ( select C.FirstName, C.LastSurname, T.TourName
--			 from Clients as C join TourPayments as TPay on C.Id = TPay.ClientId  
--							   join Tours as T on TPay.TourId = T.Id
--			 where C.FirstName = @name and C.LastSurname = @surname and C.Patronymic = @patronimic and getdate() between T.StartDate and T.EndDate)

--select * from dbo.F9('Uuuuuu', 'Uuuuuu', 'Uuuuu')  
------------------------------------------------------------------
					--11

--go 
--create or alter function F11()
--returns table
--as
--	return ( select top(1) C.FirstName, C.LastSurname, count(*) as Max_Bought_Tickets
--			 from Clients as C join TourPayments as TP on C.Id = TP.ClientId
--			group by C.FirstName, C.LastSurname
--			order by Max_Bought_Tickets desc)

--select * from dbo.F11()	
------------------------------------------------------------------
					--12
--go
--create or alter function F12(@transp nvarchar(10))
--returns table
--as
--	return ( select T.TourName, TT.TransportName
--			 from Tours as T join TransportType as TT on T.TransportTypeId = TT.Id
--			 where TT.TransportName = @transp)

--select * from dbo.F12('Plain')
------------------------------------------------------------------
					--13
--go
--create or alter trigger ForbidingToAddExistingClient
--on Clients after insert
--as
--begin	
--	declare @phone nvarchar(15);

--	select @phone = I.Phone
--	from inserted as I

--	if( (select count(*)
--		from Clients as C
--		where C.Phone = @phone) > 1)
--		print 'client with phone - ' + @phone + ' already exist'
--		begin
--			rollback tran
--			return
--		end
--end;

--insert into Clients(FirstName, LastSurname, Patronymic, Phone, Email, BirthDate)
--values('������', '����', '����', '7754345678', '���@gmail.com', '1984.09.09') 
------------------------------------------------------------------

								