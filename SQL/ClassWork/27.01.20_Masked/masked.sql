use Users_Polishchuk

--create table Users
--(
--	Id int masked with(function='default()') identity(1,1) primary key,
--	Name nvarchar(20) masked with( function='partial(1, "xxx", 2)' )  not null,
--	Email nvarchar(50) masked with( function='email()' ) not null,
--	Phone nvarchar(50) masked with( function='partial(3, "xxxxx", 2)' )  not null,
--	BirthDate date masked with(function='default()') not null,
--	Salary int not null, 
--	Adress nvarchar(40) masked with (function='default()')
--)

--insert into Users(Name, Email, Phone, BirthDate, Salary, Adress)
--values  ('Kolya', 'mikola@gmail.com', '050 111 1111', '1995.01.01', 10000,  'Lviv, Prokopovycha str.'), 
--		('Ostap', 'ostap@gmail.com', '066 222 2222', '1995.11.11', 12000,  'Rivne, Pushkina str.'),
--		('Dima', 'dmitro@gmail.com', '073 333 3333', '1995.03.12', 11000,  'Lutsk, Balabana str.'), 
--		('Oleksandr', 'sasha@gmail.com', '077 444 4444', '1996.06.15', 16000,  'Uzhgorod, Masharyka str.'), 
--		('Yaroslav', 'yaroslav@gmail.com', '068 555 5555', '1997.02.20', 14000,  'Ternopil, Shuhevycha str.') 

--select *
--from Users
--------------

--create user SomeUserName without login
--grant select on Users to SomeUserName
--------------
--execute as user = 'SomeUserName'
--select *
--from Users
--------------
--revert

--alter table Users alter column Phone add masked with(function='partial(3, ".....", 2)')
--alter table Users alter column Salary add masked with(function='random(1, 100)')
--------------
--execute as user = 'SomeUserName'
--select *
--from Users

--revert
--------------
--grant unmask to SomeUserName
--execute as user = 'SomeUserName'
--select *
--from Users
--------------
--revert
--revoke unmask from SomeUserName
--execute as user = 'SomeUserName'
--select *
--from Users
--------------



