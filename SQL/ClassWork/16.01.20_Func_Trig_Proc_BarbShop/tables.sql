﻿
-- POSITIONS
--CREATE TABLE Positions  @@
--(
--	Id int identity(1,1) primary key,
--	[Name] nvarchar(100) not null
--)​

-- BARBERS
--CREATE TABLE Barbers
--(
--	Id int identity(1,1) primary key,
--	[Name] nvarchar(100) not null,
--	[Surname] nvarchar(100) not null,
--	[Patronymic] nvarchar(100) not null,
--	Sex nvarchar(50) not null,
--	Phone nvarchar(50) not null,
--	Email nvarchar(50) not null,
--	[Birth Date] date not null,
--	[EmploymentDate] date not null,
--	[PositionId] int references Positions(Id),​
--)
​

-- CUNSTOMERS
--CREATE TABLE Customers
--(
--	Id INT NOT NULL IDENTITY PRIMARY KEY,
--	[Name] NVARCHAR(40) NOT NULL CHECK([Name]!=''),
--	Surname NVARCHAR(40) NOT NULL CHECK(Surname!=''),
--	Patronymic NVARCHAR(40) NOT NULL CHECK(Patronymic!='') DEFAULT'NONE',
--	Phone NVARCHAR(40) NOT NULL,
--	Email NVARCHAR(60) NOT NULL
--)
​

-- SCHEDULE
--create table Schedule 
--(
--	Id int identity(1,1) primary key,
--	BarberId int not null FOREIGN KEY REFERENCES Barbers(Id),
--	[Day] int not null check([Day] between 1 and 7),
--	[StartTime] time not null,
--	[EndTime] time not null,
--	check([StartTime] < [EndTime])
--)

--create table Records 
--(
--	Id int identity(1,1) primary key,
--	CustomerId int not null FOREIGN KEY REFERENCES Customers(Id),
--	BarberId int not null FOREIGN KEY REFERENCES Barbers(Id),
--	[Date] date not null check([Date] >= GETDATE()),
--	[Time] time not null
--)
​
​
-- SERVICES​
--CREATE TABLE [Services]
--(
--	Id int identity(1,1) primary key,
--	[Name] nvarchar(100) not null,
--	Price money not null,
--	Duration time not null
--)
​

--CREATE TABLE [BarbersServices]
--(
--	Id int identity(1,1) primary key,
--	[ServiceId] int References [Services](Id),
--	[BarberId] int References [Barbers](Id)
--)
​

-- FEEDBACKS
--CREATE TABLE [Feedbacks]
--(
--	Id int identity(1,1) primary key,
--	[Message] nvarchar(100) not null,
--	[CustomerId] int References [Customers](Id),
--	[BarberId] int References [Barbers](Id)
--)
​
-- ESTIMATIONS
--CREATE TABLE EstimationTypes
--(
--	Id INT PRIMARY KEY IDENTITY(1,1),
--	[Type] nvarchar(50)
--);
​
--CREATE TABLE Estimations
--(
--	Id INT PRIMARY KEY IDENTITY(1,1),
--	BarberId INT REFERENCES Barbers(Id),
--	CustomerId INT REFERENCES Customers(Id),
--	TypeId INT REFERENCES EstimationTypes(Id)
--); 
​

-- ARCHIVE
--create table VisitingArchive
--(
--	Id int identity(1,1) not null primary key,
--	CustomerId int not null foreign key references Customers(Id),
--	BarberId int not null foreign key references Barbers(Id) , 
--	ServiceId int not null foreign key references [Services](Id), 
--	[Date] date not null check (Date <= getdate()),	
--	TotalPrice money not null check (TotalPrice >= 0), 
--	@ EstimationId int foreign key references Estimations(Id), 
--	@ FeedbackId int foreign key references Feedbacks(Id)
--)