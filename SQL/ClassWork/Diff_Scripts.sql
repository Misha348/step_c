use Polishchuk_Hospital_2

--go
--create procedure FirstP
--as	
--	declare @time time
--	set @time = convert(varchar, getdate(), 8);
--	print @time
--	go
--exec FirstP
-------------------------------------------------------------

--go
--create procedure SecondP
--as	
--	declare @date date
--	set @date = convert(varchar, getdate(), 102);
--	print @date
--	go
--exec SecondP
-------------------------------------------------------------
--go
--alter procedure ThirdP
--as	
--	declare @date date
--	declare @time time
--	set @time = convert(nvarchar(8), getdate(), 8);
--	--set @time = convert(Time, GetDate())
--	set @date = convert(varchar, getdate(), 102);
--	print @time
--	print @date
-------------------------------------------------------------

--go 
--alter procedure Fourth(@a int, @b int, @c int)
--as
--	return @a + @b + @C
--go
--exec Fourth 5, 6, 7

--declare @res int
--exec @res = Fourth 10, 20, 20
--print @res
-------------------------------------------------------------

--go 
--create procedure Fifth(@a int, @b int, @c int)
--as
--	return (@a + @b + @c)/3
--go
--exec Fifth 5, 10, 6

--declare @res int
--exec @res = Fifth 10, 20, 20
--if(@res > 0)
--	print @res
--else
--print 'null'
-------------------------------------------------------------

--go
--create or alter procedure Sixth(@a int, @b int, @c int)
--as
--	if(@a > @b and @a > @c)
--		return @a
--	else if (@b > @a and @b > @c)
--		return @b
--	else if (@c > @a and @c > @b)
--	return @c
--go

--declare @res int
--exec @res = Sixth 5, 10, 6;
--print @res

-------------------------------------------------------------
--go 
--alter procedure Seventh(@a int, @b char)
--as
--declare @line nvarchar(20)	 
--	while(@a > 0)
--		begin			
--			set @a = @a - 1
--			set @line = CONCAT(@line, @b)
--		end
--		print @line
--go
--exec Seventh 10, '#'
-------------------------------------------------------------

--go
--create or alter procedure Eighth(@a int, @b int, @c int, @min int output, @max int output)
--as
--	if(@a > @b and @a > @c)
--		set @max = @a
--	else if (@b > @a and @b > @c)
--		set @max = @b
--	else if (@c > @a and @c > @b)
--		set @max = @c

--		if(@a < @b and @a < @c)
--		set @min = @a
--	else if (@b < @a and @b < @c)
--		set @min = @b
--	else if (@c < @a and @c < @b)
--		set @min = @c

--go
--declare @min int
--declare @max int
--exec Eighth 3, 10, 7, @min output, @max output
--Print @min
--Print @max
-------------------------------------------------------------

--go
--create or alter procedure Nineth
--as
--	select *
--	from Wards

--go
--exec Nineth
--exec Nineth with recompile
-------------------------------------------------------------

--go
--create TRIGGER Tr_1
--on Surveys
--after insert
--as
--	print 'Survey added'

--go
--insert into Surveys(Name, DayOfWeek, StartTime, EndTime, WardId, DoctorId)
--values('AAAA', 5, '8:00', '12:00', 44, 30)

--go
--disable TRIGGER Tr_1 on Surveys

--go
--enable TRIGGER Tr_1 on Surveys
-------------------------------------------------------------
--create table History
--(
--	Id int primary key identity not null,
--	Message nvarchar(100) not null 
--)

--go
--create or alter TRIGGER Tr_2 on Surveys
--after insert
--as
--	insert into History(Message)
	
--	select name + 'was inserted at' + convert(varchar, getdate(), 8)
--	from inserted 
	--values('new survey was addad at ' + convert(varchar, getdate(), 8))

--go
--insert into Surveys(Name, DayOfWeek, StartTime, EndTime, WardId, DoctorId)
--values('juhfgyutf', 4, '12:00', '13:00', 46, 30),
--('kjghj', 4, '12:00', '13:00', 46, 30),
--('blagkfyug', 4, '12:00', '13:00', 46, 30)
--------------------
--go
--create or alter TRIGGER Tr_3 on Surveys
--after delete
--as
--	insert into History(Message)

--	select name + 'was deleted at ' + convert(varchar, getdate(), 8)
--	from deleted 

--delete from Surveys
--where Id = 22
--------------------
--go
--create or alter TRIGGER Tr_4 on Surveys
--after update
--as

--	insert into History(Message)
--	select name + 'was updated at ' + convert(varchar, getdate(), 8)
--	from inserted

--update Surveys
--set Name = 'hjfhj'
--where id = 29

