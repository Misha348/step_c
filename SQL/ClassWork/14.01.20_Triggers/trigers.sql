use Product_Polishchuk

--go
--create or alter trigger CheckExistClient
--on Clients
--after insert
--as
--begin
--	declare @email nvarchar(20)
--	select @email = I.email
--	from inserted as I
--	if( (select count(*)
--		from Clients as C
--		where C.email = @email) > 1)
--	begin
--		rollback tran
--		return
--	end
--end;


--go
--insert into Clients(Name, Surname, email)
--values('Ostap', 'Bender', 'ooo@ooo.com')
-------------------------------------------------------------

--go
--create or alter trigger CheckExistClient_1
--on Clients
--instead of insert
--as
--begin
--	insert into Clients(Name, Surname, email)
--	select Name, Surname,  email
--	from inserted as I
--	where not exists  (select *  
--					   from Clients as C
--					   where C.email = I.email)
--end;

--go
--insert into Clients(Name, Surname, email)
--values('Stepan', 'Bandera', 'uuu@uuu.com')
-------------------------------------------------------------

--go 
--create or alter trigger NotNullCountProd
--on Product
--after insert 
--as
--begin
--	declare @amount int
--	select @amount = I.Amount
--	from inserted as I
--	if(@amount = 0 )
--	begin
--		rollback tran
--		return
--	end
--end;



--go
--insert into Product(Name, [Cost Price], Amount, Price, Producer)
--values('prod_7', 333, 2, 222, 'producer_18')
-------------------------------------------------------------

--go
--create or alter trigger ForbidenDeleteClients
--on Clients
--instead of delete
--as
--	raiserror('ERROR', 0, 0);



--go
--delete from Clients where id = 1
-------------------------------------------------------------

--go
--create or alter trigger ForbiddenDeleteProducts
--on Product
--instead of delete
--as
--begin
--	--delete from Product
--	--where id = ANY (select d.id from deleted as d where d.Amount < 10)

--	declare @id int

--	select @id = d.id
--	from deleted as d
--	where d.Amount < 10
--		delete from Product
--		where id = @id


--	--delete from Product
--	--where id = 
--	--selec
--	--from deleted as del	
--	--if( select *
--	--	from Product as P
--	--	where P.Amount >= 10 )
--	--raiserror('ERROR', 0, 0)
--end;

--go
--create or alter trigger ForbiddenDeleteProducts
--on Product
--after delete
--as
--begin
--	declare @c int

--	select @c = d.Amount
--	from deleted as d
--	if (@c >= 10)
--	begin
--		raiserror('ERROR', 3, 0)
--		rollback tran
--	end
--end;

--go
--disable trigger ForbiddenDeleteProducts on Product

go
delete from Product
		where id = 8