create table Surveys
(
	Id int primary key identity not null,
	Name nvarchar(100) not null unique default('noname'),
	DayOfWeek int not null check(1<= DayOfWeek and 7 >= DayOfWeek),
	--check(StartTime >= '8:00' and StartTime <= '18:00')
	StartTime time not null  check(StartTime BETWEEN '8:00' AND '18:00'),
	EndTime time not null,
	check(StartTime < EndTime)
)

create table Departments
(
	Id int identity(1, 1) primary key not null,
	Building int not null check(Building >= 1 and Building <=5),
	Financing money not null check(Financing >= 0) default(0),
	Name nvarchar(100) not null unique
)

create table Diseases
(
	Id int identity not null primary key,
	Name nvarchar(100) not null unique,
	Suverity int not null default(1) check(Suverity >=1)
)

create table Doctors
(
	Id int identity(1, 1) not null primary key,
	Name nvarchar(max) not null,
	Phone char(10),
	Salary money not null
);