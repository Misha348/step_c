insert into Departments (Building, Financing, Name)
values (2,1000,'Traumatology'),
	   (1,1000,'Surgery'),
	   (3,5000,'Cardiology'),
	   (4,7000,'Neurological'),
	   (3,6000,'Rheumatology'),
	   (2,5000,'Therapeutic'),
	   (3,3000,'Dental'),
	   (5,7000,'Burning'),
	   (2,2000,'Otolaryngology'),
	   (3,5000,'Laboratory')