-- from base Product_Polishchuk (CW 14.01.20_Triggers)

--select P.Name, avg(P.[Cost Price]) as 'AverPrice'
--from Product as P
--group by P.Name
----------------------------------------------------
--select 'AverPrice', 
--		[PRODUCER_12] as '1_producer', 
--		[PRODUCER_08] as '2_producer', 
--		[PRODUCER_10] as '3_producer', 
--		[PRODUCER_55] as '4_producer'
--from (  select P.[Cost Price], P.Producer
--		from Product as P) as sourceTable

--pivot 
--( 
--	AVG([Cost Price]) for [Producer] in ([PRODUCER_12], [PRODUCER_08], [PRODUCER_10], [PRODUCER_55])
--) as crossTable
----------------------------------------------------

select 'SalesCount', 
		[prod_1] as '1 prod', 
		[prod_2] as '2 prod', 
		[prod_3] as '3 prod', 
		[prod_4] as '4 prod' 
from (  select S.Amount, P.Name
		from Product as P, Sales as S
		where S.ProductId = P.Id ) as sourceTable

pivot
(
	count(Amount) for [Name] in ([prod_1], [prod_2], [prod_3], [prod_4])
) as crossTable
----------------------------------------------------