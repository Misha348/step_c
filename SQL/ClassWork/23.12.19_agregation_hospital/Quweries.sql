--select count (Name) as [count of wards]
--from Wards

--select count (*) as [count of wards]
--from Wards

--select count (distinct Name) as [count of wards]
--from Wards

--select AVG(Salary)
--from Doctors

--select MAX(Salary)
--from Doctors

--select MAX(Salary)
--from Doctors
--where Premium > 1000

--select *
--from Wards, Departments,
--	Wards as W, Departments as D
--where W.DepartmentId = D.Id

--select D.Name, W.Name
--from Wards, Departments,
--	Wards as W, Departments as D
--where W.DepartmentId = D.Id

--select D.Name, count(W.Name)
--from Wards as W, Departments as D
--where W.DepartmentId = D.Id
--group by D.Name 

--select D.Name, D.Building, count(W.Name)
--from Wards as W, Departments as D
--where W.DepartmentId = D.Id
--group by D.Name, D.Building 

--select D.Name, D.Building, count(W.Name)
--from Wards as W, Departments as D
--where W.DepartmentId = D.Id
--group by D.Name, D.Building 
--having count(W.Name) > 1

(select min(Places)
from Wards) 


select D.Name, D.Building, count(W.Name)
from Wards as W, Departments as D
where W.DepartmentId = D.Id and W.Places = (select min(Places)
											from Wards) 
group by D.Name, D.Building 

