﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Security_11._02._20
{
    public partial class Form1 : Form
    {
        private SqlConnection conn = null;
        SqlDataAdapter da = null;
        DataSet set = null;
        SqlCommandBuilder cmd = null;
        private DataTable table;
        string cs = "";

        public Form1()
        {
            InitializeComponent();
            conn = new SqlConnection();

            cs = ConfigurationManager.
                    ConnectionStrings["ConnStr"].
                    ConnectionString;
            conn.ConnectionString = cs;
        }
        // 
        private void button2_Click(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection(cs);
            set = new DataSet();
            string sqlExp = "select * from UserRoles";
            da = new SqlDataAdapter(sqlExp, conn);
            listBox1.DataSource = null;
            cmd = new SqlCommandBuilder(da);
            da.Fill(set);

            foreach (DataRow r in set.Tables[0].Rows)
            {
                listBox1.Items.Add($"{r[0]} \t{r[1]}\t{r[2]}\t{r[3]}\t{r[4]}\t{r[5]}");
            }           
           
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form2 form2 = new Form2();
            if (form2.ShowDialog() == DialogResult.OK)
            {
                DataRow row = set.Tables[0].NewRow();
                row[1] = form2.login;
                row[2] = form2.passwd;
                row[3] = form2.address;
                row[4] = form2.phone;
                row[5] = form2.IsAdmin == "1";

                set.Tables[0].Rows.Add(row);
                da.Update(set);
                foreach (DataRow r in set.Tables[0].Rows)
                {
                    listBox1.Items.Add($"{r[0]} \t{r[1]}\t{r[2]}\t{r[3]}\t{r[4]}\t{r[5]}");
                }


               


            }
          
            

        }
    }
}
