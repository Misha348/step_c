﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Security_11._02._20
{
    public partial class Form2 : Form
    {
        public string login;
        public string passwd;
        public string address;
        public string phone;
        public string IsAdmin;

        public Form2()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            login = textBox1.Text;
            passwd = textBox2.Text;
            address = textBox3.Text;
            phone = textBox4.Text;
            IsAdmin = textBox5.Text;
            this.Close();
        }
    }
}
