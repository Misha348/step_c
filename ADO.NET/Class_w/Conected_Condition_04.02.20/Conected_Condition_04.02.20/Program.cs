﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Conected_Condition_04._02._20
{
    class Program
    {      
        public static void ShowAllBooksByLanguage(string language, string conStr)
        {
            using (SqlConnection connection = new SqlConnection(conStr))
            {
                connection.Open();
                string sqlexpress = $"select * " +
                    $"                  from Books as B, Languages as L" +
                    $"                  where B.LanguageId = L.Id and L.LanguageName = @LanName ";
                SqlCommand command = new SqlCommand(sqlexpress, connection);
                command.Parameters.AddWithValue(@"LanName", language);                
               

                SqlDataReader reader;
                reader = command.ExecuteReader();
                while (reader.Read())
                {
                    Console.WriteLine($"{reader[0]}\t{reader[1]}\t{reader[2]}\t{reader[3]}\t{reader[4]}");
                }
            }
        }

        public static void ShowAllBooksByAuthorNameSurname(string conStr, string name, string surname)
        {
            using (SqlConnection connection = new SqlConnection(conStr))
            {
                connection.Open();
                string sqlexpress = $"select * " +
                    $"                  from Books as B, Authors as A" +
                    $"                  where B.AuthorId = A.Id and A.FirstName = @name and A.LastName = @surname ";
                SqlCommand command = new SqlCommand(sqlexpress, connection);
                command.Parameters.AddWithValue(@"name", name);
                command.Parameters.AddWithValue(@"surname", surname);

                SqlDataReader reader;
                reader = command.ExecuteReader();
                while (reader.Read())
                {
                    Console.WriteLine($"{reader[0]}\t{reader[1]}\t{reader[2]}\t{reader[3]}\t{reader[4]}");
                }
            }
        }

        //where(select DATEDIFF(month, S.DateSell, getdate() )) > 6
        public static void ShowAuthorsByAge(string conStr, int age)
        {
            using (SqlConnection connection = new SqlConnection(conStr))
            {
                connection.Open();
                string sqlexpress = $"select * " +
                    $"                from Authors as A" +
                    $"                where(select DATEDIFF(year, A.BirthDate, getdate() )) = @age ";

                SqlCommand command = new SqlCommand(sqlexpress, connection);
                command.Parameters.AddWithValue(@"age", age);
               

                SqlDataReader reader;
                reader = command.ExecuteReader();
                while (reader.Read())
                {
                    Console.WriteLine($"{reader[0]}\t{reader[1]}\t{reader[2]}\t{reader[3]}");
                }
            }
        }



        static void Main(string[] args)
        {
            string connectStr = @"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=Library;Integrated Security=True";
            #region inserts
            //using (SqlConnection connection = new SqlConnection(connectStr))
            //{
            //    connection.Open();
            //    SqlCommand command = new SqlCommand();
            //    command.CommandText = "insert into Countries(CountryName)" +
            //        "                  values('Poland'), " +
            //        "                        ('Ukraine'), " +
            //        "                        ('Cheh Republic'), " +
            //        "                        ('Germany'),  " +
            //        "                        ('France')";
            //    command.Connection = connection;
            //    command.ExecuteNonQuery();
            //}  

            //using (SqlConnection connection = new SqlConnection(connectStr))
            //{
            //    connection.Open();
            //    SqlCommand command = new SqlCommand();
            //    command.CommandText = "insert into Languages(LanguageName, CountryId)" +
            //        "                  values('Polish', 1), " +
            //        "                        ('Ukrainian', 2), " +
            //        "                        ('Czech', 3), " +
            //        "                        ('Germanys', 4),  " +
            //        "                        ('Franch', 5)";
            //    command.Connection = connection;
            //    command.ExecuteNonQuery();
            //}          

            //using (SqlConnection connection = new SqlConnection(connectStr))
            //{
            //    connection.Open();
            //    SqlCommand command = new SqlCommand();
            //    command.CommandText = "insert into Authors(FirstName, LastName, BirthDate)" +
            //        "                  values('Qqqqqqqq', 'Qqqqqq',  '1980.10.23'), " +
            //        "                        ('Wwwwwwww', 'Wwwwww',  '1982.04.20'), " +
            //        "                        ('Eeeeeeee', 'Eeeeee',  '1983.01.12'), " +
            //        "                        ('Rrrrrrrr', 'Rrrrrr',  '1978.03.15'),  " +
            //        "                        ('Tttttttt', 'Tttttt',  '1973.04.25')";
            //    command.Connection = connection;
            //    command.ExecuteNonQuery();
            //}          

            //using (SqlConnection connection = new SqlConnection(connectStr))
            //{
            //    connection.Open();
            //    SqlCommand command = new SqlCommand();
            //    command.CommandText = "insert into Books(BookName, PagesCount, AuthorId, LanguageId)" +
            //        "                  values('Aaaaaaaaaa', 234, 1, 1), " +
            //        "                        ('Sssssssssss', 333, 1, 1), " +
            //        "                        ('Dssssssssss', 434, 2, 2), " +
            //        "                        ('Fffffffffff', 163, 2, 2),  " +
            //        "                        ('Ggggggggggg', 268, 3, 3), " +
            //        "                        ('Hhhhhhhhhhh', 484, 4, 4), " +
            //        "                        ('Jjjjjjjjjjj', 210, 4, 4), " +
            //        "                        ('Kkkkkkkkkkk', 520, 5, 5), " +
            //        "                        ('Llllllllll', 300, 5, 5)";                                              
            //    command.Connection = connection;
            //    command.ExecuteNonQuery();
            //}
            #endregion

            Console.Write("option: ");
            int userOption = Convert.ToInt32(Console.ReadLine());

            switch (userOption)
            {
                case 1:
                    Console.WriteLine("enter language: ");
                    string language = Console.ReadLine();
                    ShowAllBooksByLanguage(language, connectStr);
                    break;
                case 2:
                    Console.WriteLine("enter author name: ");
                    string name = Console.ReadLine();
                    Console.WriteLine("enter author surname: ");
                    string surname = Console.ReadLine();
                    ShowAllBooksByAuthorNameSurname(connectStr, name, surname);
                    break;
                case 3:
                    Console.Write("enter age: ");
                    int age = Convert.ToInt32(Console.ReadLine());
                    ShowAuthorsByAge(connectStr, age);
                    break;
                case 4:
                    break;
                case 5:
                    break;
                case 6:
                    break;
            }











            Console.ReadKey();
        }
    }
}


