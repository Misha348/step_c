﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data.Common;

namespace Introduction
{
    class Program
    {


        static void Main(string[] args)
        {
            string conectString = @"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=TEST_DB_03.02.20;Integrated Security=True";

            SqlConnection sqlcon = new SqlConnection(conectString);
            sqlcon.Open();

            SqlCommand sqlComand = new SqlCommand()
            {
                CommandText = "insert into [GROUP](Name)" +
                "                 values(21), " +
                "                       ( 31), " +
                "                       ( 41)",

                Connection = sqlcon
            };
            //Console.WriteLine(sqlComand.ExecuteNonQuery());

            sqlComand.CommandText = "insert into Student(NAME, AGE, GroupId )" +
                "                   values('Dima', 20, 1), " +
                "                       ( 'Yulya', 19, 1), " +
                "                       ( 'Kolya', 20, 2), " +
                "                       ('Valya', 19, 2), " +
                "                       ('Sasha', 20, 3 ), " +
                "                       ('Petya', 20, 3)";
            // Console.WriteLine(sqlComand.ExecuteNonQuery());
            sqlComand.CommandText = "select * from Student";


            DbDataReader reader;
            reader = sqlComand.ExecuteReader();


            for (int i = 0; i < reader.FieldCount; i++)
            {
                Console.Write($"{reader.GetName(i)}  ");
               
            }
            Console.WriteLine();
            while (reader.Read())
            {
                Console.WriteLine($"{reader[0]}   {reader[1]}  {reader[2]}\t{reader[3]}");               
            }
            //==============================================================================
            //string name;
            //name = Console.ReadLine();
            //sqlComand.CommandText = $"select * from Student where NAME = '{name}'";
            //reader.Close();
            //reader = sqlComand.ExecuteReader();

            //while (reader.Read())
            //{
            //    Console.WriteLine($"{reader[0]}   {reader[1]}  {reader[2]}\t{reader[3]}");
            //}
            //==============================================================================

            //string name;
            //name = Console.ReadLine();

            //sqlComand.Parameters.AddWithValue("@name", name);
            //sqlComand.CommandText = $"select * from Student where NAME = @name";

            //reader.Close();
            //reader = sqlComand.ExecuteReader();

            //while (reader.Read())
            //{
            //    Console.WriteLine($"{reader[0]}   {reader[1]}  {reader[2]}\t{reader[3]}");
            //}
            //==============================================================================

            Console.Write("name: ");
            string name1 = Console.ReadLine();

            Console.Write("age: ");
            int age = Convert.ToInt32(Console.ReadLine());

            Console.Write("groupId: ");
            int groupId = Convert.ToInt32(Console.ReadLine());

           
            sqlComand.Parameters.AddWithValue("@name", name1);
            sqlComand.Parameters.AddWithValue("@age", age);
            sqlComand.Parameters.AddWithValue("@groupId", groupId);


            sqlComand.CommandText = $"insert into Student(NAME, AGE, GroupId )" +
            $"                                   values('{name1}', '{age}', '{groupId}') ";

            sqlComand.CommandText = "select * from Student";
            reader.Close();
            reader = sqlComand.ExecuteReader();

            while (reader.Read())
            {
                Console.WriteLine($"{reader[0]}   {reader[1]}  {reader[2]}\t{reader[3]}");
            }
            //==============================================================================



            //Console.Write("from: ");
            //int from = Convert.ToInt32(Console.ReadLine());
            //Console.Write("till: ");
            //int till = Convert.ToInt32(Console.ReadLine());

            //sqlComand.Parameters.AddWithValue("@from", from);
            //sqlComand.Parameters.AddWithValue("@till", till);

            //sqlComand.CommandText = $"select * from Student where AGE >= {from} and AGE <= {till}";

            //reader.Close();
            //reader = sqlComand.ExecuteReader();

            //while (reader.Read())
            //{
            //    Console.WriteLine($"{reader[0]}   {reader[1]}  {reader[2]}\t{reader[3]}");
            //}

            //Console.WriteLine(sqlComand.ExecuteNonQuery());
            sqlcon.Close();

        }
    }
}
