﻿using System;
using System.Collections.Generic;
using System.Linq;

using System.Text;
using System.Threading.Tasks;

namespace Linq_12._02._20_hw
{
    class Program
    {
        static void Main(string[] args)
        {
            DataClasses1DataContext ctx = new DataClasses1DataContext();

            var query1 = ctx.Books.Where(b => b.PagesCount > 100);
            foreach (var item in query1)
            {
                Console.WriteLine($"{item.Id}\t{item.BookName}\t{item.PagesCount}\t");
            }
            Console.WriteLine(new string('=', 30));

            var query2 = ctx.Books.Where(b => b.BookName.StartsWith("a") || b.BookName.StartsWith("A"));
            foreach (var item in query2)
            {
                Console.WriteLine($"{item.Id}\t{item.BookName}\t{item.PagesCount}\t");
            }
            Console.WriteLine(new string('=', 30));

            var query3 = ctx.Authors.Where(a => a.FirstName == "William" && a.LastName == "Shakespeare");
            foreach (var item in query3)
            {
                Console.WriteLine($"{item.Id}\t{item.FirstName}\t{item.LastName}\t");
            }
            Console.WriteLine(new string('=', 30));

            var query4 = from b in ctx.Books
                         join l in ctx.Languages on b.LanguageId equals l.Id
                         where l.LanguageName == "Ukrainian"
                         select b;

            foreach (var item in query4)
            {
                Console.WriteLine($"{item.Id}\t{item.BookName}");
            }
            Console.WriteLine(new string('=', 30));

            var query5 = ctx.Books.Where(b => b.BookName.Length > 10);
            foreach (var item in query5)
            {
                Console.WriteLine($"{item.Id}\t{item.BookName}\t{item.PagesCount}\t");
            }
            Console.WriteLine(new string('=', 30));


            var query6 = from b in ctx.Books
                         join l in ctx.Languages on b.LanguageId equals l.Id
                         where l.LanguageName != "Franch"
                         orderby b.PagesCount descending
                         select b;
            foreach (var item in query6)
            {
                Console.WriteLine($"{item.Id}\t{item.BookName}\t{item.PagesCount}\t");
            }
            Console.WriteLine(new string('=', 30));
            //https://www.csharp-examples.net/linq-max/
        }
    }
}
