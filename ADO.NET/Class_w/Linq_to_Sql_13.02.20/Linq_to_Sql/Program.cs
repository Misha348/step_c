﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Linq_to_Sql
{
    class Program
    {
        static void Menu()
        {
            Console.WriteLine("\t\t[ 1 - add book ]\n\t\t[ 2 - show all books ]\n\t\t[ 3 - edit book by Id ]\n\t\t[ 4 - delete book by Id ]\n\t\t[ 0 - exit ]");
        }

        static void Main(string[] args)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString;
          

            DataClasses1DataContext ctx = new DataClasses1DataContext();
            int choise = 0;
            bool notSucces = true;
            Menu();

            while (notSucces)
            {
                Console.Write("\ninput choise: ");
                choise = int.Parse(Console.ReadLine());
                switch (choise)
                {
                    case 1:
                        string bName = "";
                        int pCount = 0;
                        int authId = 0;
                        int langId = 0;

                        Console.Write("name: ");
                        bName = Console.ReadLine();

                        Console.Write("pages count: ");
                        pCount = int.Parse( Console.ReadLine());

                        Console.Write("author Id: ");
                        authId = int.Parse(Console.ReadLine());

                        Console.Write("language Id: ");
                        langId = int.Parse(Console.ReadLine());

                        Book newBook = new Book()
                        { BookName = bName, PagesCount = pCount, AuthorId = authId, LanguageId = langId };
                        ctx.Books.InsertOnSubmit(newBook);
                        ctx.SubmitChanges();
                        break;
                    case 2:
                        var query = from c in ctx.Books                                                                   
                                    select c;
                        foreach (var item in query)
                        {
                            Console.WriteLine($"{item.Id}\t{item.BookName}\t{item.PagesCount}\t{item.AuthorId}\t{item.LanguageId}");
                        }
                        break;
                    case 3:
                        int bookId = 0;
                        Console.Write("Id: ");
                        bookId = int.Parse(Console.ReadLine());

                        Book editBook = ctx.Books.Where(b => b.Id == bookId).First(); // try catch
                        // firstOrDef - return null

                        Console.Write("name: ");
                        bName = Console.ReadLine();
                        editBook.BookName = bName;

                        Console.Write("pages count: ");
                        pCount = int.Parse(Console.ReadLine());
                        editBook.PagesCount = pCount;

                        Console.Write("author Id: ");
                        authId = int.Parse(Console.ReadLine());
                        editBook.AuthorId = authId;

                        Console.Write("language Id: ");
                        langId = int.Parse(Console.ReadLine());
                        editBook.LanguageId = langId;
                        ctx.SubmitChanges();
                        break;
                    case 4: 
                        int bId = 0;
                        Console.Write("Id: ");
                        bId = int.Parse(Console.ReadLine());

                        Book delBook = ctx.Books.Where(b => b.Id == bId).First();
                        ctx.Books.DeleteOnSubmit(delBook);
                        ctx.SubmitChanges();

                        break;
                    case 0:
                        notSucces = false;
                        break;
                }
            }












        }
    }
}

//Console.Write("Product ID: ");
//		bool notSucces = true;
//var _prodID = 0;
//		while (notSucces)
//		{
//			if (int.TryParse(Console.ReadLine(), out _prodID && _prodID > 0 ))
//				notSucces = false;
//			else
//				Console.WriteLine("error");
//		}