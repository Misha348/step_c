﻿using MoreLinq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LINQ_SQL_12._02._20
{
    public class Good
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public double Price { get; set; }
        public string Category { get; set; }

        public override string ToString()
        {
            return $"{Id}\t{Title}\t{Price}\t{Category}";
        }

    }

    class Program
    {
        static void Main(string[] args)
        {
            //string cs = "";
            //cs = ConfigurationManager.
            //       ConnectionStrings["ConnStr"].
            //       ConnectionString;

            List<Good> goods1 = new List<Good>()
            {
                new Good() {Id = 1, Title = "Nokia 1100", Price = 450.99, Category = "Mobile" },
                new Good() { Id = 2, Title = "Iphone 4", Price = 5000, Category = "Mobile" },
                new Good() { Id = 3, Title = "Refregirator 5000", Price = 2555, Category = "Kitchen" },
                new Good() { Id = 4, Title = "Mixer", Price = 150, Category = "Kitchen" },
                new Good() { Id = 5, Title = "Magnitola", Price = 1499, Category = "Car" },
                new Good() { Id = 6, Title = "Samsung Galaxy", Price = 3100, Category = "Mobile" },
                new Good() { Id = 7, Title = "Auto Cleaner", Price = 2300, Category = "Car" },
                new Good() { Id = 8, Title = "Owen", Price = 700, Category = "Kitchen" },
                new Good() { Id = 9, Title = "Siemens Turbo", Price = 3199, Category = "Mobile" },
                new Good() { Id = 10, Title = "Lighter", Price = 150, Category = "Car" }
            };

            var query1 = from good in goods1
                         where good.Category == "Mobile" && good.Price > 1000
                         select good;

            foreach (var item in query1)
            {                
                Console.WriteLine(item.ToString());
            }
            Console.WriteLine(new string('=', 38));

            var query2 = goods1.Where(g => g.Category != "Kitchen" && g.Price > 1000);
            foreach (var item in query2)
            {
                Console.WriteLine(item.ToString());
            }
            Console.WriteLine(new string('=', 38));

            var query3 = goods1.MaxBy(g => g.Price);
            foreach (var item in query3)
            {
                Console.WriteLine($"{item.Title}\t{item.Category}\t{item.Price}");
            }
            Console.WriteLine(new string('=', 38));

            var query4 = goods1.Average(g => g.Price);
            Console.WriteLine($"average price = {query4}");
            Console.WriteLine(new string('=', 38));

            var query5 = goods1.DistinctBy(g => g.Category);
            foreach (var item in query5)
            {
                Console.WriteLine($"{item.Category}");
            }
            Console.WriteLine(new string('=', 38));


            var query6 = goods1.GroupBy(g => g.Price).Where(g => g.Count() > 1);
            foreach (var group in query6)
            {
                Console.WriteLine(group.Key);
                foreach (var i in group)
                {
                    Console.WriteLine(i);
                }
            }
            Console.WriteLine(new string('=', 38));

            var query7 = goods1.OrderBy(g => g.Title);
            foreach (var item in query7)
            {            
                Console.WriteLine($"{item.Title}");
            }
            Console.WriteLine(new string('=', 38));

            var query8 = from g in goods1
                         where g.Category == "Car" && (g.Price > 1000 && g.Price < 2000)
                         select g;

            foreach (var item in query8)
            {
                Console.WriteLine($"{item.Category}\t{item.Title}");
            }
            Console.WriteLine(new string('=', 38));




            //  var query1 = from good in goods1
            //where good.Category == "Mobile" && good.Price > 1000
            //select good;



            Console.ReadKey();
        }
    }
}
