﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Configuration;

namespace Adapt_Table_Set
{
    public partial class Form1 : Form
    {
        private SqlConnection conn = null;
        SqlDataAdapter da = null;
        DataSet set = null;
        SqlCommandBuilder cmd = null;
        private DataTable table;
        string cs = "";

        public Form1()
        {
            InitializeComponent();
            conn = new SqlConnection();

            cs = ConfigurationManager.
                    ConnectionStrings["ConnStr"].
                    ConnectionString;
            conn.ConnectionString = cs;
        }

        private void button1_Click(object sender, EventArgs e)
        {          
            SqlConnection conn = new SqlConnection(cs);
            set = new DataSet();
            table = new DataTable();

            string sql = textBox1.Text;
            da = new SqlDataAdapter(sql, conn);
            cmd = new SqlCommandBuilder(da);

            da.Fill(set);
            
            for (int i = 0; i < set.Tables.Count; i++ )
            {
                TabPage page = new TabPage();
                DataGridView dataGridView = new DataGridView();

                dataGridView.DataSource = null;
                dataGridView.DataSource = set.Tables[i];

                page.Controls.Add(dataGridView);
                tabControl1.TabPages.Add(page);
            }          
          
        }

        private void button2_Click(object sender, EventArgs e)
        {

        }
    }
}
