﻿CREATE TABLE [dbo].[Books] (
    [Id]        INT           IDENTITY (1, 1) NOT NULL,
    [BookName]  NVARCHAR (20) NOT NULL,
    [AuthorId]  INT           NOT NULL,
    [VisitorId] INT           NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    FOREIGN KEY ([AuthorId]) REFERENCES [dbo].[Authors] ([Id]),
    FOREIGN KEY ([VisitorId]) REFERENCES [dbo].[Visitors] ([Id])
);

