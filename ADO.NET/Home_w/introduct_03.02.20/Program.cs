﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace introduct_03._02._20
{
	class Program
	{
		public static void InsertDataToTable(string conectStr, string sqlExpres)
		{
			using (SqlConnection connection = new SqlConnection(conectStr))
			{
				connection.Open();
				SqlCommand command = new SqlCommand(sqlExpres, connection);
				command.ExecuteNonQuery();
			}
		}

		public static void ShowAllDebters(string conectStr)
		{
			using (SqlConnection connection = new SqlConnection(conectStr))
			{
				connection.Open();
				string sqlExp = "select * from Visitors as V where V.IsDebtor = 1 ";
				SqlCommand command = new SqlCommand(sqlExp, connection);
				SqlDataReader reader = command.ExecuteReader();

				Console.ForegroundColor = ConsoleColor.Green;
				Console.WriteLine($"{reader.GetName(0)}\t{reader.GetName(1)}\t{reader.GetName(2)}\t{reader.GetName(3)}");
				Console.ForegroundColor = ConsoleColor.White;
				while (reader.Read())
				{
					Console.WriteLine($"{reader[0]} \t{reader[1]} \t\t{reader[2]} \t{reader[3]}");
				}
			}			
		}
		
		select * from Books

		public static void ShowAllAuthors(string conectStr, int bookId )
		{
			using (SqlConnection connection = new SqlConnection(conectStr))
			{
				connection.Open();
				string sqlExp = $"select A.Authorname, A.AuthorSurname " +
					"			 from Authors as A join Books as B on A.BookId = B.Id and B.Id = @Id";

				SqlCommand command = new SqlCommand(sqlExp, connection);
				command.Parameters.AddWithValue(@"Id", bookId);

				SqlDataReader reader;
				reader = command.ExecuteReader();

				Console.ForegroundColor = ConsoleColor.Green;
				Console.WriteLine($"{reader.GetName(0)}\t{reader.GetName(1)}");
				Console.ForegroundColor = ConsoleColor.White;
				while (reader.Read())
				{
					Console.WriteLine($"{reader[0]} \t\t{reader[1]}");
				}
			}				
		}

		public static void ShowAllFreeBooks(string conectStr)
		{
			using (SqlConnection connection = new SqlConnection(conectStr))
			{
				connection.Open();
				string sqlExp = "select * from Books as B where B.VisitorId is null "; 
				SqlCommand command = new SqlCommand(sqlExp, connection);
				//command.Parameters.AddWithValue(@"null", DBNull.Value);

				SqlDataReader reader = command.ExecuteReader();
				Console.ForegroundColor = ConsoleColor.Green;
				Console.WriteLine($"{reader.GetName(0)}\t{reader.GetName(1)}\t{reader.GetName(2)}\t{reader.GetName(3)}");
				Console.ForegroundColor = ConsoleColor.White;
				while (reader.Read())
				{
					Console.WriteLine($"{reader[0]} \t{reader[1]} \t\t{reader[2]} \t{reader[3]}");
				}
			}				
		}

		public static void ShowBooksWithinSpecifiedVisitor(string conectStr, int visitotId)
		{
			using (SqlConnection connection = new SqlConnection(conectStr))
			{
				connection.Open();
				string sqlExp = "select * from Books as B where B.VisitorId = @visitorId ";
				SqlCommand command = new SqlCommand(sqlExp, connection);
				command.Parameters.AddWithValue(@"visitorId", visitotId);

				SqlDataReader reader = command.ExecuteReader();
				Console.ForegroundColor = ConsoleColor.Green;
				Console.WriteLine($"{reader.GetName(0)}\t{reader.GetName(1)}\t{reader.GetName(2)}\t{reader.GetName(3)}");
				Console.ForegroundColor = ConsoleColor.White;
				while (reader.Read())
				{
					Console.WriteLine($"{reader[0]} \t{reader[1]} \t\t{reader[2]} \t{reader[3]}");
				}
			}
		}

		public static void FreeAllDebtors(string conectStr)
		{
			using (SqlConnection connection = new SqlConnection(conectStr))
			{
				connection.Open();
				string sqlExp = "update Visitors set IsDebtor = 0 where IsDebtor = 1  ";
				SqlCommand command = new SqlCommand(sqlExp, connection);
				command.ExecuteNonQuery();

				//SqlDataReader reader = command.ExecuteReader();

				//Console.ForegroundColor = ConsoleColor.Green;
				//Console.WriteLine($"{reader.GetName(0)}\t{reader.GetName(1)}\t{reader.GetName(2)}\t{reader.GetName(3)}");
				//Console.ForegroundColor = ConsoleColor.White;
				//while (reader.Read())
				//{
				//	Console.WriteLine($"{reader[0]} \t{reader[1]} \t\t{reader[2]} \t{reader[3]}");
				//}
			}
		}
		static void Main(string[] args)
		{
			string conectStr = @"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=Library;Integrated Security=True";

			#region inserts
			//string InsertToVisitorExpres = "insert into Visitors(FirstName, LastName, IsDebtor)" +
			//	"													values('Bob', 'Bobchuk', 0)," +
			//	"														  ('Bill', 'Billchuk', 1)," +
			//	"														  ('Jack', 'Jackchuk', 1)," +
			//	"														  ('Stanley', 'Stanlichuk', 0)," +
			//	"														  ('Sarrah', 'Odin', 0)," +
			//	"														  ('Julia', 'Ostin', 1)";
			//InsertDataToTable(conectStr, InsertToVisitorExpres);

			//string InsertIntoAuthorsExpres = "insert into Authors(AuthorName, AuthorSurname, BookId)" +
			//	"												 values('Qqqqqq', 'Qqqq', 1), " +
			//	"														('Wwwwww', 'Wwww', 2), " +
			//	"														('Eeeeee', 'Eeee', 3), " +
			//	"														('Rrrrrr', 'Rrrr', 4), " +
			//	"														('Tttttt', 'Tttt', 5)";
			//InsertDataToTable(conectStr, InsertIntoAuthorsExpres);

			//string InsertIntoBooksExpres = "insert into Books(BookName, AuthorId, VisitorId)" +
			//"												 values('Zzzzzz', 1, 1), " +
			//"														('Xxxxxxx', 2, 2), " +
			//"														('Cccccccc', 3, 3), " +
			//"														('Vvvvvvvv', 4, 4), " +
			//"														('Bbbbbbbb', 5, 5), " +
			//"														('Nnnnnnn', 1, 3), " +
			//"														('Mmmmmmm', 2, 3), " +
			//"														('Aaaaaaa', 3, 5), " +
			//"														('Sssssss', 4, 2), " +
			//"														('Ffffff', 5, 3), " +
			//"														('Oooooo', 3, null), " +
			//"														('Llllll', 3, null)";
			//InsertDataToTable(conectStr, InsertIntoBooksExpres);
			#endregion

			Console.WriteLine("\t\tDEBTERS");
			ShowAllDebters(conectStr);

			Console.WriteLine("\n\t\tBook's Authors");
			ShowAllAuthors(conectStr, 3);			

			Console.WriteLine("\n\t\tFREE BOOKS");
			ShowAllFreeBooks(conectStr);

			Console.WriteLine("\n\t\tVisitors Books");
			ShowBooksWithinSpecifiedVisitor(conectStr, 2);		

			Console.WriteLine("\t\tDEBTERS");
			//FreeAllDebtors(conectStr);
			ShowAllDebters(conectStr);







		}
	}
}
