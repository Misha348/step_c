﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADO_CRUD_LINQ_13._02._20
{
	class Program
	{
		public static void Menu()
		{
			Console.WriteLine("\n 1 - create new data.\n 2 - show data.\n" +
				" 3 - udate data\n 4 - delete data\n 5 - change user\n 0 - exit");
		}


		static void Main(string[] args)
		{
			bool notSuccess = true;

			bool currUsIsAdminStatus = false;
			Authorization newAuthorization = new Authorization();
			currUsIsAdminStatus = newAuthorization.GetUsersRole();

			CRUD_operations operation = new CRUD_operations(currUsIsAdminStatus);

			string login;
			string password;
			string address;
			string phone;
			bool isAdmin;

			Menu();
			while (notSuccess)
			{
				Console.Write("\nselect operation: ");
				int choise = int.Parse(Console.ReadLine());
				switch (choise)
				{
					case 1:
						Console.Write("\nlogin: ");
						login = Console.ReadLine();

						Console.Write("password: ");
						password = Console.ReadLine();

						Console.Write("\naddress: ");
						address = Console.ReadLine();

						Console.Write("\nphone: ");
						phone = Console.ReadLine();

						Console.Write("\nis admin? : ");
						isAdmin = bool.Parse(Console.ReadLine());

						operation.CreateNewRecord(login, password, address,
												  phone, isAdmin, currUsIsAdminStatus);
						break;
					case 2:
						Console.WriteLine("1 - admins.\t\t2 - not admins");						
						int choice1 = int.Parse(Console.ReadLine());
						operation.ShowData(choice1);
						break;
					case 3:
						Console.Write("user id to edit:");
						int choice2 = int.Parse(Console.ReadLine());
						operation.UptadeData(choice2, currUsIsAdminStatus);
						break;
					case 4:
						Console.Write("user id to delete:");
						int choice3 = int.Parse(Console.ReadLine());
						operation.DeleteData(choice3, currUsIsAdminStatus);
						break;
					case 5:
						currUsIsAdminStatus = newAuthorization.GetUsersRole();
						operation = new CRUD_operations(currUsIsAdminStatus);
						break;
					case 0:
						notSuccess = false;
						break;

				}
			}


			

			
			


		}
	}
}
