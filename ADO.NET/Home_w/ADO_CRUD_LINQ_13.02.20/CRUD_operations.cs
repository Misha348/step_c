﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADO_CRUD_LINQ_13._02._20
{
	public class CRUD_operations
	{
		private bool _currUsAdminStatus { get; set; }
		private DataClasses1DataContext ctx { get; set; }
		public CRUD_operations(bool adminStatus)
		{
			ctx = new DataClasses1DataContext();
			_currUsAdminStatus = adminStatus;
		}

		private void CreateNewRecordCByNotAdmin(string login, string password, string address,
									string phone, bool isAdmin, bool statusIsadmin)
		{
			UsersTable ut = new UsersTable()
			{
				UsLogin = login,
				UsPasswdord = password,
				UsAddress = address,
				UsPhone = phone,
				UsRole = isAdmin
			};
			ctx.UsersTables.InsertOnSubmit(ut);
			ctx.SubmitChanges();
		}

		public void CreateNewRecord(string login, string password, string address,
									string phone, bool isAdmin, bool statusIsadmin)
		{
			if ( (_currUsAdminStatus == true) &&  (statusIsadmin == true) )
			{
				CreateNewRecordCByNotAdmin(login, password, address,
									 phone, isAdmin, statusIsadmin);
			}
			else if((_currUsAdminStatus == false) && (statusIsadmin == false))
			{
				isAdmin = false;
				CreateNewRecordCByNotAdmin(login, password, address,
									 phone, isAdmin, statusIsadmin);			
			}
		}

		private void ShowDatahelper(IQueryable<UsersTable>q)
		{
			foreach (var item in q)
			{
				Console.WriteLine($"{item.Id}\t{item.UsLogin}\t{item.UsPasswdord}\t{item.UsAddress}\t" +
					$"{item.UsPhone}\t{item.UsRole}");
			}
		}

		public void ShowData(int choice)
		{
			
			switch (choice)
			{				
				case 1:
					var q = ctx.UsersTables.Where(ur => ur.UsRole == true);
					ShowDatahelper(q);
					break;
				case 2:
					var q1 = ctx.UsersTables.Where(ur => ur.UsRole == false);
					ShowDatahelper(q1);
					break;
			}
			
		}

		private void DataEditor(UsersTable q, bool currUsIsAdminStatus) 
		{
			bool notSuccess = true;
			Console.WriteLine("select statement to edit:\n1 - Id.\n2 - login\n" +
				"3 - password\n4 - address\n5 - phone\n6 - admin status\n7 - finish");
			while (notSuccess)
			{
				Console.Write("choice: ");
				var choise = int.Parse(Console.ReadLine());
				switch (choise)
				{
					case 1:
						Console.Write("Id: ");
						var id = int.Parse(Console.ReadLine());
						q.Id = id;
						ctx.SubmitChanges();
						break;
					case 2:
						Console.Write("login: ");
						var login = Console.ReadLine();
						q.UsLogin = login;
						ctx.SubmitChanges();
						break;
					case 3:
						Console.Write("password: ");
						var password = Console.ReadLine();
						q.UsPasswdord = password;
						ctx.SubmitChanges();
						break;
					case 4:
						Console.Write("address: ");
						var address = Console.ReadLine();
						q.UsAddress = address;
						ctx.SubmitChanges();
						break;
					case 5:
						Console.Write("phone: ");
						var phone = Console.ReadLine();
						q.UsPhone = phone;
						ctx.SubmitChanges();
						break;
					case 6:
						if (currUsIsAdminStatus == true)
						{
							Console.Write("is admin?: ");
							var admin = Console.ReadLine();
							q.UsRole = bool.Parse(admin);
							ctx.SubmitChanges();
						}	
						else
							Console.WriteLine("not admin can't edit admin status");
						break;
					case 7:
						notSuccess = false;
						break;
				}
			}
			
		}

		public void UptadeData(int choise, bool currUsIsAdminStatus)
		{
			if (currUsIsAdminStatus == true)
			{
				var q = ctx.UsersTables.First(id => id.Id == choise);
				DataEditor(q, currUsIsAdminStatus);
			}
			if (currUsIsAdminStatus == false)
			{
				var q = ctx.UsersTables.First(id => id.Id == choise );
				if(q.UsRole == true)
					Console.WriteLine("not admin can't edit admin");
				else
					DataEditor(q, currUsIsAdminStatus);
			}
		}	

		public void DeleteData(int choise, bool currUsIsAdminStatus)
		{
			if (currUsIsAdminStatus == true)
			{
				var q = ctx.UsersTables.First(id => id.Id == choise);
				ctx.UsersTables.DeleteOnSubmit(q);
				ctx.SubmitChanges();

			}
			if (currUsIsAdminStatus == false)
			{
				var q = ctx.UsersTables.First(id => id.Id == choise);
				if (q.UsRole == true)
					Console.WriteLine("not admin can't delete admins");
				else
				{
					ctx.UsersTables.DeleteOnSubmit(q);
					ctx.SubmitChanges();
				}
					
			}

		}

	}
}
