﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Conected_Condition_04._02._20
{
    class Program
    {
        static void GetDataCallback(IAsyncResult result)
        {
            SqlDataReader reader = null;
            try
            {                
                SqlCommand command = (SqlCommand)result.AsyncState;               
                reader = command.EndExecuteReader(result);

                while (reader.Read())
                {
                    for (int i = 0; i < reader.FieldCount; i++)
                    {
                        Console.Write($"{reader[i]} | ");
                    }
                    Console.WriteLine();
                }
                Console.WriteLine();
            }
            catch (Exception ex)
            {
                Console.WriteLine("From Callback 1:" + ex.Message);
            }
            finally
            {
                try
                {
                    if (!reader.IsClosed)
                        reader.Close();
                }
                catch (Exception ex)
                {
                    Console.WriteLine("From Callback 2" + ex.Message);
                }
            }
        }

        static IAsyncResult AsyncQuery(string cs, string query)
        {            
            const string AsyncEnabled = "Asynchronous Processing=true";
            if (!cs.Contains(AsyncEnabled))
            {
                cs = String.Format($"{cs}; {AsyncEnabled}");
            }

            var conn = new SqlConnection(cs);
            SqlCommand comm = new SqlCommand();
            comm.Connection = conn;
           
            comm.CommandText = query;
            comm.CommandType = CommandType.Text;
            comm.CommandTimeout = 30; 
          
            conn.Open();
           
            AsyncCallback callback = new AsyncCallback(GetDataCallback);
            Console.WriteLine("Added thread.");
            return comm.BeginExecuteReader(callback, comm);           
        }     
                                            
        static void Main(string[] args)
        {
            string connectStr = @"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=Library;Integrated Security=True";              
            
            string expresString1 = "WAITFOR DELAY '00:00:01' SELECT * FROM Books;";
            string expresString2 = "WAITFOR DELAY '00:00:01' SELECT * FROM Authors;";
            string expresString3 = "WAITFOR DELAY '00:00:01' SELECT * FROM Visitors;";

            IAsyncResult iar1 = AsyncQuery(connectStr, expresString1);
            WaitHandle handler1 = iar1.AsyncWaitHandle;

            IAsyncResult iar2 = AsyncQuery(connectStr, expresString2);
            WaitHandle handler2 = iar2.AsyncWaitHandle;

            IAsyncResult iar3 = AsyncQuery(connectStr, expresString3);
            WaitHandle handler3 = iar3.AsyncWaitHandle;



            //WaitHandle[] waitHandles = new WaitHandle[] { handler1, handler2, handler3 };
            if (WaitHandle.WaitAll(new WaitHandle[] { handler1, handler2, handler3 }));
            {
                Console.WriteLine("All threads done.");
            }

            Console.ReadKey();
        }
    }
}




