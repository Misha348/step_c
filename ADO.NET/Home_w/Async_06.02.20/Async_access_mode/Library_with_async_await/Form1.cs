﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Library_with_async_await
{
    public partial class Form1 : Form
    {
        readonly DbConnection conn = null;
        readonly string connectionString = null;

        public Form1()
        {
            InitializeComponent();
            button1.Enabled = false;
            connectionString = ConfigurationManager.ConnectionStrings["LibraryConnStr"].ConnectionString;
            conn = new SqlConnection(connectionString);
        }      
       
        private async void button1_Click(object sender, EventArgs e)
        {
            button1.Enabled = false;
            await conn.OpenAsync(); // 1

            DbCommand comm = conn.CreateCommand();
            comm.CommandText = "WAITFOR DELAY '00:00:03';";
            comm.CommandText += textBox1.Text.ToString();

            DataTable table = new DataTable();
            using (DbDataReader reader = await comm.ExecuteReaderAsync()) // 2
            {               
                for (int i = 0; i < reader.FieldCount; i++)
                {
                    table.Columns.Add(reader.GetName(i));
                }
                do
                {
                    while (await reader.ReadAsync())    // 3
                    {                        
                        DataRow row = table.NewRow();
                        for (int i = 0; i < reader.FieldCount; i++)
                        {
                            row[i] = await reader.GetFieldValueAsync<Object>(i); // 4
                        }
                        table.Rows.Add(row);
                    }
                } while (await reader.NextResultAsync());
            }         
                       
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = table;                
            
            string str = "";
            string authorSurname = "";
            int AuthorIndex = 0;
            foreach (DataGridViewRow row in dataGridView1.Rows)
            {               
                str = (string)row.Cells["AuthorId"].Value;
                AuthorIndex = ConvertValue(str);
                authorSurname = await GetDataToComboboxById(AuthorIndex);
                FillComboBox(authorSurname);
            }                
            
            conn.Close();
            button1.Enabled = true;
        }

        private async Task <string> GetDataToComboboxById(int id)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                await connection.OpenAsync();
                string sqlExp = "select A.AuthorSurname " +
                                "from Authors as A join Books as B on B.AuthorId = A.Id and B.AuthorId = @Id ";
                SqlCommand comm = new SqlCommand(sqlExp, connection);
                comm.Parameters.AddWithValue(@"Id", id);

                SqlDataReader reader;
                reader = await comm.ExecuteReaderAsync();

                while (reader.Read())
                {
                    string authorSurname = (string)reader[0];
                    return authorSurname;
                }
                return "";
               
            }    
        }

        private int ConvertValue(string str)
        {
            int valFromAuthorsIdColumn = 0;
            if (str != null)
            {
                valFromAuthorsIdColumn = int.Parse(str);
                return valFromAuthorsIdColumn;
            }
            return 0;
        }

        private  void FillComboBox(string authorSurname)
        {
            if (!comboBox1.Items.Contains(authorSurname))
            {
                 comboBox1.Items.Add(authorSurname);
            }
        }

        ///<summary>
        ///bunnto enable manage
        ///</summary>
        ///<param name="sender"></param>
        ///<param name="e"></param>
        void textBox1_TextChanged(object sender, EventArgs e)
        {
            if (textBox1.Text.Length > 3)
                button1.Enabled = true;
            else
                button1.Enabled = false;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            string selectedAutorSurname = comboBox1.SelectedItem.ToString();
            GetAllBooksByAuthorsSurname(selectedAutorSurname);
        }

        private async void GetAllBooksByAuthorsSurname(string authorSurname) ////
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                await connection.OpenAsync();
                string sqlExp = "select count(*)" +
                                "from Books as B join Authors as A on B.AuthorId = A.Id and A.AuthorSurname = @AuthorSurname";
                              
                SqlCommand comm = new SqlCommand(sqlExp, connection);
                comm.Parameters.AddWithValue(@"AuthorSurname", authorSurname);

                SqlDataReader reader;
                reader = await comm.ExecuteReaderAsync(); /////

                while ( await reader.ReadAsync()) /////
                {
                    int countOfBooksByAuthor = (int)reader[0];
                    textBox2.Text = countOfBooksByAuthor.ToString();
                }
            }
        }

       
    }
}