﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Users_Table_Encription_11._02._20
{
	public partial class Form3 : Form
	{
		public string Login_f3 { get; set; }
		public string Password_f3 { get; set; }
		public string Address_f3 { get; set; }
		public string Phone_f3 { get; set; }
		public string IsAdmin_f3 { get; set; }
		public Form3(string arg1, string arg2, string arg3, string arg4, string arg5)
		{
			InitializeComponent();
			textBox1.Text = arg1;
			textBox2.Text = arg2;
			textBox3.Text = arg3;
			textBox4.Text = arg4;
			textBox5.Text = arg5;
		}

		private void button1_Click(object sender, EventArgs e)
		{
			Login_f3 = textBox1.Text;
			Password_f3 = textBox2.Text;
			Address_f3 = textBox3.Text;
			Phone_f3 = textBox4.Text;
			IsAdmin_f3 = textBox5.Text;
			this.Close();
			DialogResult = DialogResult.OK;
		}
	}
}
