﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

using System.Windows.Forms;

namespace Users_Table_Encription_11._02._20
{
	public partial class Form1 : Form
	{
		
		private SqlConnection conn = null;
		SqlDataAdapter da = null;
		DataSet set = null;	
		SqlCommandBuilder cmd = null;		
		string cs = "";

		public List<string> Logins { get; set; }
		public string Login { get; set; }
		public string Password { get; set; }
		public string Address { get; set; }
		public string Phone { get; set; }
		public string IsAdmin { get; set; }

		public Form1()
		{
			InitializeComponent();
			conn = new SqlConnection();
			cs = ConfigurationManager.ConnectionStrings["connStr"].ConnectionString;
			

			EncryptConnSettings("connectionStrings");
			cs = ConfigurationManager.ConnectionStrings["connStr"].ConnectionString;
			conn.ConnectionString = cs;

			set = new DataSet();
            string sqlExpr = "select * from UsersTable";
          
            da = new SqlDataAdapter(sqlExpr, conn);
			
			cmd = new SqlCommandBuilder(da);
            da.AcceptChangesDuringUpdate = true;
            da.AcceptChangesDuringFill = true;
			da.Fill(set);			
			
			Logins = new List<string>();
			foreach (DataRow dataRow in set.Tables[0].Rows)
			{
				string str = dataRow.Field<string>("UsLogin");
				Logins.Add(str);
			}
		}

        private static void EncryptConnSettings(string section)
        {
            Configuration objConfig = ConfigurationManager.
                OpenExeConfiguration(GetAppPath() + "Users_Table_Encription_11.02.20.exe");

            ConnectionStringsSection conSringSection = (ConnectionStringsSection)objConfig.
                GetSection(section);

            if (!conSringSection.SectionInformation.IsProtected)
            {
                conSringSection.SectionInformation.ProtectSection("RsaProtectedConfigurationProvider");
                conSringSection.SectionInformation.ForceSave = true;
                objConfig.Save(ConfigurationSaveMode.Modified);
            }
        }

        private static string GetAppPath()
		{
			System.Reflection.Module[] modules = System.Reflection.Assembly.
				GetExecutingAssembly().GetModules();
			string location = System.IO.Path.GetDirectoryName(modules[0].FullyQualifiedName);
			if ((location != "") && (location[location.Length - 1] != '/')) 
				location += '/';
			return location;
		}
		private void button1_Click(object sender, EventArgs e)
		{
            set = new DataSet();
            string sqlExpr = "select * from UsersTable";
            
            da = new SqlDataAdapter(sqlExpr, conn);
            listBox1.DataSource = null;
            cmd = new SqlCommandBuilder(da);

            da.Fill(set);
            foreach ( DataRow dataRow in set.Tables[0].Rows)
			{				
				listBox1.Items.Add(String.Format("{0, -10}{1, -30}{2, -20:0}{3, -30:0}{4, -35}{5, -30}", dataRow[0], dataRow[1], dataRow[2], dataRow[3], dataRow[4], dataRow[5]) );
			}
		}

		private void button2_Click(object sender, EventArgs e)
		{
			Form2 form2 = new Form2();
			if (form2.ShowDialog() == DialogResult.OK)
			{
				DataRow newRow = set.Tables[0].NewRow();
				newRow[1] = form2.Login;
				newRow[2] = form2.Password;
				newRow[3] = form2.Address;
				newRow[4] = form2.Phone;
				newRow[5] = form2.IsAdmin == "yes" ? "True" : "False";

				set.Tables[0].Rows.Add(newRow);
				da.Update(set);

				foreach (DataRow dataRow in set.Tables[0].Rows)
				{
					string str = dataRow.Field<string>("UsLogin");
					if (!Logins.Contains(str))
						Logins.Add(str);
				}

				listBox1.Items.Clear();

                set = new DataSet();
                string sqlExpr = "select * from UsersTable";
               
                da = new SqlDataAdapter(sqlExpr, conn);
                listBox1.DataSource = null;
                cmd = new SqlCommandBuilder(da);

                da.Fill(set);
				foreach (DataRow dataRow in set.Tables[0].Rows)
				{					
					listBox1.Items.Add(String.Format("{0, -10}{1, -30}{2, -20:0}{3, -30:0}{4, -35}{5, -30}", dataRow[0], dataRow[1], dataRow[2], dataRow[3], dataRow[4], dataRow[5]));
				}
			}
		}

		private void button3_Click(object sender, EventArgs e)
		{
			listBox1.Items.Clear();
		}

		private void listBox1_MouseDoubleClick(object sender, MouseEventArgs e)
		{
			int index = this.listBox1.IndexFromPoint(e.Location);
			if (index != ListBox.NoMatches) // -1
			{
				MessageBox.Show(index.ToString());					
			
				Login = (set.Tables[0].Rows[index][1]).ToString();
				Password = (set.Tables[0].Rows[index][2]).ToString();
				Address = (set.Tables[0].Rows[index][3]).ToString();
				Phone = (set.Tables[0].Rows[index][4]).ToString();
				IsAdmin = (set.Tables[0].Rows[index][5]).ToString();
				Form3 form3 = new Form3(Login, Password, Address, Phone, IsAdmin);
				//form3.Show();

				if (form3.ShowDialog() == DialogResult.OK)
				{
					(set.Tables[0].Rows[index][1]) = form3.Login_f3;
					(set.Tables[0].Rows[index][2]) = form3.Password_f3;
					(set.Tables[0].Rows[index][3]) = form3.Address_f3;
					(set.Tables[0].Rows[index][4]) = form3.Phone_f3;
					(set.Tables[0].Rows[index][5]) = form3.IsAdmin_f3 == "yes" ? "True" : "False";
					da.Update(set);
					listBox1.Items.Clear();

                    set = new DataSet();
                    string sqlExpr = "select * from UsersTable";
                   
                    da = new SqlDataAdapter(sqlExpr, conn);
                    listBox1.DataSource = null;
                    cmd = new SqlCommandBuilder(da);

                    da.Fill(set);
					foreach (DataRow dataRow in set.Tables[0].Rows)
					{
						listBox1.Items.Add(String.Format("{0, -10}{1, -30}{2, -20:0}{3, -30:0}{4, -35}{5, -30}", dataRow[0], dataRow[1], dataRow[2], dataRow[3], dataRow[4], dataRow[5]));
					}
				}
			}
		}

		private void button4_Click(object sender, EventArgs e)
		{
            MessageBox.Show(cmd.GetDeleteCommand().CommandText);
			DataRow row = set.Tables[0].Rows[listBox1.SelectedIndex];
			set.Tables[0].Rows.Remove(row);		 // ???????????????????????
			da.Update(set);						 // ???????????????????????

			listBox1.Items.Clear();
		
			foreach (DataRow dataRow in set.Tables[0].Rows)
			{
				listBox1.Items.Add(String.Format("{0, -10}{1, -30}{2, -20:0}{3, -30:0}{4, -35}{5, -30}", dataRow[0], dataRow[1], dataRow[2], dataRow[3], dataRow[4], dataRow[5]));
			}			
		}
	}
}
