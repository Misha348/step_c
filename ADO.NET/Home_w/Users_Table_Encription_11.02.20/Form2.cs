﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Users_Table_Encription_11._02._20
{
	public partial class Form2 : Form
	{
		public Encription Encr;
		public string Login { get; set; }
		public string Password { get; set; }
		public string Address { get; set; }
		public string Phone { get; set; }
		public string IsAdmin  { get; set; }

		public Form2()
		{
			InitializeComponent();
			Encr = new Encription();
		}

		private void button1_Click(object sender, EventArgs e)
		{
			Form1 form1 = new Form1();

			Login = textBox1.Text;
			if (!form1.Logins.Contains(Login))
			{
				Password = textBox2.Text;
				Password = Encr.GetHashString(Password);
				Address = textBox3.Text;
				Phone = textBox4.Text;
				IsAdmin = textBox5.Text;
				this.Close();
				DialogResult = DialogResult.OK;
			}
			else
			{
				MessageBox.Show("such login already exist.");
				DialogResult = DialogResult.Cancel;
			}

				
			
			
		}
	}
}
