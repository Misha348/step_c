﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MoreLinq;

namespace Linq_To_Sql_12._02._20
{
	class Program
	{
		static void Main(string[] args)
		{			
			DataClasses1DataContext ctx = new DataClasses1DataContext();
			
			var query1 = ctx.Books.Where(b => b.PagesCount > 100);
			foreach (var item in query1)
			{
				Console.WriteLine($"{item.Id}\t{item.BookName}\t{item.PagesCount}\t");
			}
			Console.WriteLine(new string('=', 30));


            var query2 = ctx.Books.Where(b => b.BookName.StartsWith("a") || b.BookName.StartsWith("A"));
            foreach (var item in query2)
            {
                Console.WriteLine($"{item.Id}\t{item.BookName}\t{item.PagesCount}\t");
            }
            Console.WriteLine(new string('=', 30));


            var query3 = ctx.Authors.Where(a => a.FirstName == "William" && a.LastName == "Shakespeare");
            foreach (var item in query3)
            {
                Console.WriteLine($"{item.Id}\t{item.FirstName}\t{item.LastName}\t");
            }
            Console.WriteLine(new string('=', 30));

            var query4 = from b in ctx.Books
                         join l in ctx.Languages on b.LanguageId equals l.Id
                         where l.LangName == "Ukrainian"
                         select b;
            foreach (var item in query4)
            {
                Console.WriteLine($"{item.Id}\t{item.BookName}");
            }
            Console.WriteLine(new string('=', 30));



            var query5 = ctx.Books.Where(b => b.BookName.Length > 10);
            foreach (var item in query5)
            {
                Console.WriteLine($"{item.Id}\t{item.BookName}\t{item.PagesCount}\t");
            }
            Console.WriteLine(new string('=', 30));


            var query6 = from b in ctx.Books
                         join l in ctx.Languages on b.LanguageId equals l.Id
                         where l.LangName != "English"
                         orderby b.PagesCount descending
                         select b;

            var q = query6.MaxBy(p => p.PagesCount);
            foreach (var item in q)
            {
                Console.WriteLine($"{item.Id}\t{item.BookName}\t{item.PagesCount}");
                
            }
            Console.WriteLine(new string('=', 30));


            var query7 = from b in ctx.Books
                         join a in ctx.Authors on b.AuthorId equals a.Id
                         group a by a.FirstName into gr
                         select new { name = gr.Key, count = gr.Count() };
            int min = query7.Min(c => c.count);
            foreach (var item in query7)
            {
                if(item.count == min)
                Console.WriteLine($"{item.name} \t {item.count}");
            }
            Console.WriteLine(new string('=', 30));

            
            var query8 = ctx.Authors.OrderBy(a => a.FirstName).Where(a => (a.FirstName != "Mark" && a.FirstName != "Jim"));
            foreach (var item in query8)
            {
                Console.WriteLine($"{item.FirstName}");
            }
            Console.WriteLine(new string('=', 30));
            


            var query9 = from b in ctx.Books                       
                         join l in ctx.Languages on b.LanguageId equals l.Id
                         group l by l.LangName into gr
                         select new { name = gr.Key, count = gr.Count() };
            int max = query9.Max(c => c.count);
            foreach (var item in query9)
            {
                if (item.count == max)
                    Console.WriteLine($"{item.name} \t {item.count}");
            }
            Console.WriteLine(new string('=', 30));





        }
    }
}
