﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hash_with_getHashCode
{
    class Program
    {
        static string GetHashString(string s)
        {
            // get hash code from string
            string hashCode = s.GetHashCode().ToString();
            return hashCode;
        }

        static void Main(string[] args)
        {
            string password;
            password = Console.ReadLine();
            Console.WriteLine("Original str: " + password);

            password = GetHashString(password);
            Console.WriteLine("Hashed str: " + password);

            string password_compare = Console.ReadLine();
            password_compare = GetHashString(password_compare);
            Console.WriteLine("Hashed str: " + password_compare);

            if (password == password_compare)
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("Password is correct!");
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Password invalid!");
            }

            Console.ResetColor();
            Console.ReadKey();
        }
    }
}
