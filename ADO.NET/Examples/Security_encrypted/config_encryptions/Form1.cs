﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace config_encryptions
{
    public partial class Form1 : Form
    {
        //не шифрованная строка подключения
        string connStringNotEncrypted = "";
        //шифрованная строка подключения
        string connStringWithEncryption = "";
        public Form1()
        {
            InitializeComponent();
            //читаем строку подключения до шифрования
            connStringNotEncrypted = ConfigurationManager.
                ConnectionStrings["LibraryConnStr"].ConnectionString;
            //отображаем строку подключения до шифрования
            textBox1.Text = connStringNotEncrypted;
            //выполняем шифрование
            EncryptConnSettings("connectionStrings");
            //читаем строку подключения после шифрования
            connStringWithEncryption = ConfigurationManager.
                ConnectionStrings["LibraryConnStr"].ConnectionString;
            //отображаем строку подключения после шифрования
            textBox2.Text = connStringWithEncryption;
        }
        private static void  EncryptConnSettings(string section)
        {
            //создаем объект нашего конфигурационного файла
            //AppConfigDecrypt.exe — это имя выполняемого
            //файла вашего приложения если у вас оно
            //другое, то учтите этот момент
            Configuration objConfig = ConfigurationManager.
                OpenExeConfiguration(GetAppPath() + "config_encryptions.exe");
            //получаем доступ к разделу ConnectionStrings
            //нашего конфигурационного файла
            ConnectionStringsSection conSringSection = (ConnectionStringsSection)objConfig.
                GetSection(section);
            //если раздел не зашифрован — шифруем его
            if (!conSringSection.SectionInformation.IsProtected)
            {
                conSringSection.SectionInformation.ProtectSection("RsaProtectedConfigurationProvider");
                conSringSection.SectionInformation.ForceSave = true;
                objConfig.Save(ConfigurationSaveMode.Modified);
            }
        }
        //получаем путь к папке, в которой лежит
        //конфигурационный файл
        private static string GetAppPath()
        {
            System.Reflection.Module[] modules = System.Reflection.Assembly.
                GetExecutingAssembly().GetModules();
            string location = System.IO.Path.GetDirectoryName(modules[0].FullyQualifiedName);
            if ((location != "") && (location[location.Length - 1] != '/')) // /\\/
                location += '/';
            return location;
        }
        private void button1_Click(object sender, EventArgs e)
        {
            SqlConnection conn = null;
            SqlDataAdapter da = null;
            DataSet set = null;
            SqlCommandBuilder cmd = null;
            string connString = "";
            //повторно читаем строку подключения
            //из зашифрованной секции
            connString = ConfigurationManager.
                ConnectionStrings["LibraryConnStr"].ConnectionString;
            //повторно выводим строку подключения
            //в окно приложения
            textBox2.Text = connString;
            try
            {
                conn = new SqlConnection(connString);
                set = new DataSet();
                string sql = "select * from Books";
                da = new SqlDataAdapter(sql, conn);
                dataGridView1.DataSource = null;
                cmd = new SqlCommandBuilder(da);
                da.Fill(set, "Books");
                dataGridView1.DataSource = set.Tables["Books"];
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
