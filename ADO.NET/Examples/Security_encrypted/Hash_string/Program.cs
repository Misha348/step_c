﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;

namespace Hash_string
{
    class Program
    {
        static string GetHashString(string s)
        {
            // conver to byte array
            byte[] bytes = Encoding.Unicode.GetBytes(s);

            //create encrypt obj
            MD5CryptoServiceProvider CSP = new MD5CryptoServiceProvider();

            // calc hash-encrypt from bytes
            byte[] byteHash = CSP.ComputeHash(bytes);

            string hash = string.Empty;

            // make string from hash
            foreach (byte b in byteHash)
                hash += string.Format("{0:x2}", b);

            return hash;
        }

        static void Main(string[] args)
        {
            string password;
            password = Console.ReadLine();
            Console.WriteLine("Original str: " + password);

            password = GetHashString(password);
            Console.WriteLine("Hashed str: " + password);

            string password_compare = Console.ReadLine();
            password_compare = GetHashString(password_compare);
            Console.WriteLine("Hashed str: " + password_compare);

            if (password == password_compare)
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("Password is correct!");
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Password invalid!");
            }

            Console.ResetColor();
            Console.ReadKey();
        }
    }
}
