﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Library_with_async_await
{
    public partial class Form1 : Form
    {
        DbConnection conn = null;
        string connectionString = null;

        public Form1()
        {
            InitializeComponent();
            button1.Enabled = false;
            connectionString = ConfigurationManager.ConnectionStrings["LibraryConnStr"].ConnectionString;
            conn = new SqlConnection(connectionString);
        }

        private async void button1_Click(object sender, EventArgs e)
        {
            button1.Enabled = false;
            await conn.OpenAsync(); // 1

            DbCommand comm = conn.CreateCommand();
            comm.CommandText = "WAITFOR DELAY '00:00:05';";
            comm.CommandText += textBox1.Text.ToString();

            DataTable table = new DataTable();
            using (DbDataReader reader = await comm.ExecuteReaderAsync()) // 2
            {               
                for (int i = 0; i < reader.FieldCount; i++)
                {
                    table.Columns.Add(reader.GetName(i));
                }
                do
                {
                    while (await reader.ReadAsync())    // 3
                    {                        
                        DataRow row = table.NewRow();
                        for (int i = 0; i < reader.FieldCount; i++)
                        {
                            row[i] = await reader.GetFieldValueAsync<Object>(i); // 4
                        }
                        table.Rows.Add(row);
                    }
                } while (await reader.NextResultAsync());
            }

            // show query result
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = table;

            conn.Close();
            button1.Enabled = true;
        }
       
        ///<summary>
        ///bunnto enable manage
        ///</summary>
        ///<param name="sender"></param>
        ///<param name="e"></param>
        void textBox1_TextChanged(object sender, EventArgs e)
        {
            if (textBox1.Text.Length > 3)
                button1.Enabled = true;
            else
                button1.Enabled = false;
        }
    }
}