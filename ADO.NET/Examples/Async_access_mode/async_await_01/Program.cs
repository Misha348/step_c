﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace async_await_01
{
    class AsyncBase
    {
        async Task<int> Method1Async()
        {
            int result = 0;
            // Your actions
            return result;
        }
        async Task Method2Async()
        {
            // Your actions
        }

        async void CallAsyncMethod()
        {
            // Keyword await used with a method that returnsa Task<int>
            int result = await Method1Async();
            // Keyword await used with a method that returns a Task
            await Method2Async();
        }
        async void CallExpandedAsyncMethod()
        {
            //Calls to Method1Async
            Task<int> returnedTaskInt = Method1Async();           
            int result = await returnedTaskInt;
            //Calls to Method1Async
            Task returnedTaskVoid = Method2Async();
            await returnedTaskVoid;
        }
    }
    class AsyncTest
    {       
        async public Task<int> GetDataAsync(string filename)
        {           
            byte[] data = null;
           
            using (FileStream fs = File.Open(filename, FileMode.Open))
            {
                data = new byte[fs.Length];

                await fs.ReadAsync(data, 0, (int)fs.Length);
            }           
            string result = System.Text.Encoding.UTF8.GetString(data);
            await Task.Delay(2000);
            Console.WriteLine("Read was finished!");
            return 0;
        }

        public async void CallAsyncMethod()
        {            
            Console.WriteLine("Before async call");
            int n = await GetDataAsync(@"1.txt");
            Console.WriteLine(n);
            Console.WriteLine("After async call");
        }
    }

    class Program
    {              
        static void Main(string[] args)
        {
            AsyncTest test = new AsyncTest();
            test.CallAsyncMethod();

            while (!Console.KeyAvailable)
            {
                Console.WriteLine("Press any key...");
                Thread.Sleep(200);
            }
            //Console.ReadKey();
        }
    }
}