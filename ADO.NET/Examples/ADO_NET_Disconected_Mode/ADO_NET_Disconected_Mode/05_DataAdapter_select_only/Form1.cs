﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _05_DataAdapter_select_only
{
    public partial class Form1 : Form
    {
        private SqlConnection conn = null;
        SqlDataAdapter da = null;
        DataSet set = null;
        SqlCommandBuilder cmd = null;
        string cs = "";

        public Form1()
        {
            InitializeComponent();
            conn = new SqlConnection();

            cs = ConfigurationManager.
                    ConnectionStrings["LibraryConnStr"].
                    ConnectionString;
            conn.ConnectionString = cs;
        }

        private void btnFill_Click(object sender, EventArgs e)
        {
            try
            {
                SqlConnection conn = new SqlConnection(cs);
                set = new DataSet();
                string sql = textBox1.Text;
                da = new SqlDataAdapter(sql, conn);
                dataGridView1.DataSource = null;
                cmd = new SqlCommandBuilder(da);
                da.Fill(set, "mybook");                   // da.Fill(set);
                dataGridView1.DataSource = set.Tables[0]; // dataGridView1.DataSource = set.Tables[0];
            }
            catch (Exception)
            {
            }
            finally
            {
            }
        }
        private void btnUpdate_Click(object sender, EventArgs e)
        {
            da.Update(set, "mybook");
        }
    }
}