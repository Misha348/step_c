﻿using System.Collections.Generic;
using System.Linq;
using static System.Console;

namespace _07_join
{
    class Student
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int GroupId { get; set; }
    }

    class Group
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
   
    class Program
    {
        static void Main(string[] args)
        {
            //var anonimousType = new
            //{
            //    UserName = "ewfr",
            //    LastName = "ergarg",
            //    esrgrsthr = 3464634
            //};

            //System.Console.WriteLine(anonimousType.UserName);

            List<Group> groups = new List<Group>
            {
                new Group { Id = 1, Name = "27PPS11" },
                new Group { Id = 2, Name = "27PPS12" }
            };
            List<Student> students = new List<Student>
            {
                new Student { FirstName = "John", LastName = "Miller", GroupId = 2 },
                new Student { FirstName = "Candice", LastName = "Leman", GroupId = 1 },
                new Student { FirstName = "Joey", LastName = "Finch", GroupId = 1 },
                new Student { FirstName = "Nicole", LastName = "Taylor", GroupId = 3 }
            };

            var query = from g in groups join st in students on g.Id equals st.GroupId into res
                        from r in res
                        select new { FirstName = r.FirstName, LastName = r.LastName, Name = g.Name };

            //WriteLine("\tStudents in groups:"); 
            //foreach (Student item in query)
            //{
            //    WriteLine($"Surname: { item.LastName}, " +

            //        $"Name: { item.FirstName}, " +
            //        $"Group:{ groups.First(g => g.Id == item.GroupId).Name}");
            //}

            //foreach (var item in query)
            //{
            //    WriteLine($"Surname: { item.FirstName}, " +
            //
            //        $"Name: { item.FirstName}, " +
            //        $"Group:{ item.Name}");
            //}

            var result = groups.Join(students,
                                        g => g.Id,
                                        st => st.GroupId,
                                        (g, st) => new { StudentName = st.FirstName, GroupName = g.Name});

            foreach (var item in result)
            {
                System.Console.WriteLine(
                $"Student: { item.StudentName}, " +
                    $"Group:{ item.GroupName}");
            }
        }
    }
}
