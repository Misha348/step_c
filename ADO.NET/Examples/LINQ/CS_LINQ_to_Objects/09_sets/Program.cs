﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _09_sets
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] soft = { "Blue", "Grey", "Yellow", "Cyan", "Grey", "Yellow" };
            string[] hard = { "Yellow", "Magenta", "White", "Blue" };
            IEnumerable<string> result;

            // Except -----------------------        
            // разность множеств
            //result = soft.Except(hard);           
           
            // Intersect ---------------------------
            // пересечение множеств
            //result = soft.Intersect(hard);           

            // Union ---------------------------
            // объединение множеств
            //result = soft.Union(hard);
           
            // Concat -------------
            // объединение двух
            //result = soft.Concat(hard);

            // Distinct ----------------
            // удаления дублей
            result = soft.Distinct();

            foreach (string s in result)
                Console.WriteLine(s);
        }
    }
}
