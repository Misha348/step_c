﻿using System.Collections.Generic;
using System.Linq;
using static System.Console;

namespace _04_group_by
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arrayInt = { 5, 34, 65, 12, 94, 42, 365 };

            IEnumerable<IGrouping<int, int>> query =
                                                    from i in arrayInt
                                                    group i by i % 10;

            WriteLine("Forming groups of criteria:");
            foreach (IGrouping<int, int> key in query)
            {
                Write($"Key: {key.Key}\nValue:");
                foreach (int item in key)
                {
                    Write($"\t{item}");
                }
                WriteLine();
            }

            // Метод розширення
            var result = arrayInt.GroupBy(i => i % 10);
        }
    }
}
