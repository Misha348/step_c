﻿using System.Collections.Generic;
using System.Linq;
using static System.Console;

namespace _02_Where
{
    
    // Where: определяет фильтр выборки
    class Program
    {
        static void Main(string[] args)
        {
            int[] arrayInt = { 5, 34, 67, 12, 94, 42 };
            //IEnumerable<int> query;

            var query = from i in arrayInt
                        where i % 2 == 0
                        select i;
            
            WriteLine("Only the even elements:");
            foreach (int item in query)
            {
                Write($"{item}\t");
            }
            WriteLine();

            

            // Метод розширення
            query = arrayInt.Where(item => item % 2 == 0);

            WriteLine("Only the even elements:");
            foreach (int item in query)
            {
                Write($"{item}\t");
            }
            WriteLine();
        }
    }
}
