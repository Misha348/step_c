USE [master]
GO
/****** Object:  Database [AutoLot]    Script Date: 17.07.2017 12:19:49 ******/
CREATE DATABASE [AutoLot]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'AutoLot', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\AutoLot.mdf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'AutoLot_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\AutoLot_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [AutoLot] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [AutoLot].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [AutoLot] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [AutoLot] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [AutoLot] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [AutoLot] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [AutoLot] SET ARITHABORT OFF 
GO
ALTER DATABASE [AutoLot] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [AutoLot] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [AutoLot] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [AutoLot] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [AutoLot] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [AutoLot] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [AutoLot] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [AutoLot] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [AutoLot] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [AutoLot] SET  DISABLE_BROKER 
GO
ALTER DATABASE [AutoLot] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [AutoLot] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [AutoLot] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [AutoLot] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [AutoLot] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [AutoLot] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [AutoLot] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [AutoLot] SET RECOVERY FULL 
GO
ALTER DATABASE [AutoLot] SET  MULTI_USER 
GO
ALTER DATABASE [AutoLot] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [AutoLot] SET DB_CHAINING OFF 
GO
ALTER DATABASE [AutoLot] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [AutoLot] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [AutoLot] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'AutoLot', N'ON'
GO
USE [AutoLot]
GO
/****** Object:  Table [dbo].[Customers]    Script Date: 17.07.2017 12:19:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Customers](
	[CustID] [int] NOT NULL,
	[FirstName] [varchar](50) NULL,
	[LastName] [varchar](50) NULL,
 CONSTRAINT [PK_Customers] PRIMARY KEY CLUSTERED 
(
	[CustID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Inventory]    Script Date: 17.07.2017 12:19:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Inventory](
	[CarID] [int] NOT NULL,
	[Make] [nvarchar](50) NULL,
	[Color] [nvarchar](50) NULL,
	[PetName] [nchar](10) NULL,
 CONSTRAINT [PK_Inventory] PRIMARY KEY CLUSTERED 
(
	[CarID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Orders]    Script Date: 17.07.2017 12:19:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Orders](
	[OrderID] [int] NOT NULL,
	[CarID] [int] NOT NULL,
	[CustID] [int] NOT NULL,
 CONSTRAINT [PK_Orders] PRIMARY KEY CLUSTERED 
(
	[OrderID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
INSERT [dbo].[Customers] ([CustID], [FirstName], [LastName]) VALUES (1, N'Dave', N'Brenner
')
INSERT [dbo].[Customers] ([CustID], [FirstName], [LastName]) VALUES (2, N'Matt
', N'Wafton
')
INSERT [dbo].[Customers] ([CustID], [FirstName], [LastName]) VALUES (3, N'Steve
', N'Hagen')
INSERT [dbo].[Customers] ([CustID], [FirstName], [LastName]) VALUES (4, N'Pat
', N'Walton')
INSERT [dbo].[Inventory] ([CarID], [Make], [Color], [PetName]) VALUES (12, N'Honda                                             ', N'Зелений                                           ', N'Acord     ')
INSERT [dbo].[Inventory] ([CarID], [Make], [Color], [PetName]) VALUES (23, N'BMW', N'Blue', N'Bummer    ')
INSERT [dbo].[Inventory] ([CarID], [Make], [Color], [PetName]) VALUES (32, N'VW', N'Black
', N'Zippy
    ')
INSERT [dbo].[Inventory] ([CarID], [Make], [Color], [PetName]) VALUES (83, N'Ford', N'Rust', N'Rusty     ')
INSERT [dbo].[Inventory] ([CarID], [Make], [Color], [PetName]) VALUES (123, N'asdf                                              ', N'asdf                                              ', N'asdfsdf   ')
INSERT [dbo].[Inventory] ([CarID], [Make], [Color], [PetName]) VALUES (333, N'Honda                                             ', N'Білий                                             ', N'Бумер     ')
INSERT [dbo].[Inventory] ([CarID], [Make], [Color], [PetName]) VALUES (872, N'Saab', N'Black', N'Mel       ')
INSERT [dbo].[Inventory] ([CarID], [Make], [Color], [PetName]) VALUES (888, N'Yugo', N'Yellow', N'Chunko    ')
INSERT [dbo].[Inventory] ([CarID], [Make], [Color], [PetName]) VALUES (1000, N'BMW', N'Black', N'Bimmer    ')
INSERT [dbo].[Inventory] ([CarID], [Make], [Color], [PetName]) VALUES (1011, N'BMV', N'Green', N'Hank      ')
INSERT [dbo].[Inventory] ([CarID], [Make], [Color], [PetName]) VALUES (1233, N'sdfsdf                                            ', N'sdfsdf                                            ', N'sdfsdf    ')
INSERT [dbo].[Inventory] ([CarID], [Make], [Color], [PetName]) VALUES (2911, N'BMW', N'Pink', N'Pinky     ')
INSERT [dbo].[Inventory] ([CarID], [Make], [Color], [PetName]) VALUES (10999, N'Renault                                           ', N'Жовтий                                            ', N'Кобайн    ')
INSERT [dbo].[Orders] ([OrderID], [CarID], [CustID]) VALUES (1000, 1000, 1)
INSERT [dbo].[Orders] ([OrderID], [CarID], [CustID]) VALUES (1002, 888, 3)
INSERT [dbo].[Orders] ([OrderID], [CarID], [CustID]) VALUES (1003, 2911, 4)
INSERT [dbo].[Orders] ([OrderID], [CarID], [CustID]) VALUES (10001, 32, 2)
ALTER TABLE [dbo].[Orders]  WITH CHECK ADD  CONSTRAINT [FK_Orders_Customers] FOREIGN KEY([CustID])
REFERENCES [dbo].[Customers] ([CustID])
GO
ALTER TABLE [dbo].[Orders] CHECK CONSTRAINT [FK_Orders_Customers]
GO
ALTER TABLE [dbo].[Orders]  WITH CHECK ADD  CONSTRAINT [FK_Orders_Inventory] FOREIGN KEY([CarID])
REFERENCES [dbo].[Inventory] ([CarID])
GO
ALTER TABLE [dbo].[Orders] CHECK CONSTRAINT [FK_Orders_Inventory]
GO
/****** Object:  StoredProcedure [dbo].[GetPetName]    Script Date: 17.07.2017 12:19:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetPetName]
@carID int,
@petName char(10) output
AS
SELECT @petName = PetName from Inventory where CarID = @carID


GO
USE [master]
GO
ALTER DATABASE [AutoLot] SET  READ_WRITE 
GO
