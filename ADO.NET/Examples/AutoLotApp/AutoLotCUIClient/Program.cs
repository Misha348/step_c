﻿using DAL;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;

namespace AutoLotCUIClient
{
    class Program
    {
        static void Main(string[] args)
        {


            //string connectionString = @"Data Source=.\SQLEXPRESS;Initial Catalog=usersdb;Integrated Security=True";
            //                                   Data Source = DESKTOP - EPN9CP6; Initial Catalog = AutoLot; Integrated Security = True
            //SqlConnection connection = new SqlConnection(connectionString);
            //try
            //{
            //    connection.Open();
            //    Console.WriteLine("Подключение открыто");
            //}
            //catch (SqlException ex)
            //{
            //    Console.WriteLine(ex.Message);
            //}
            //finally
            //{
            //    connection.Close();
            //    Console.WriteLine("Подключение закрыто...");
            //}





            Car car = new Car
            {
                Color = "Black",
                Make = "Buick",
                PetName = "Bobick"
            };
            InsertAuto(car);

            //string login = "Saab";
            //string password = "Black";
            //
            //
            //bool isLogin = LoginParam(login, password);
            //Console.WriteLine(isLogin);
            //
            //Console.WriteLine(GetPetById(11));

            //Console.WriteLine("---------------");
            //foreach (var el in GetAllCars())
            //{
            //    Console.WriteLine($"{el}");
            //}
            


        }

        private static string GetPetById(int id)
        {
            var providerDb = new InventoryDal();
            var con = ConfigurationManager.ConnectionStrings["MyAutoLotConn"].ConnectionString;
            providerDb.OpenConnection(con);
            string res = providerDb.GetPetById(11);
            providerDb.CloseConnection();
            return res;
        }
        private static bool Login(string login, string password)
        {
            InventoryDal providerDB = new InventoryDal();
            string con = ConfigurationManager.ConnectionStrings["MyAutoLotConn"].ConnectionString;
            providerDB.OpenConnection(con);
            bool isLogin = providerDB.Login(login, password);
            providerDB.CloseConnection();
            return isLogin;
        }
        private static bool LoginParam(string login, string password)
        {
            var providerDb = new InventoryDal();
            var con = ConfigurationManager.ConnectionStrings["MyAutoLotConn"].ConnectionString;
            providerDb.OpenConnection(con);
            var isLogin = providerDb.LoginParam(login, password);
            providerDb.CloseConnection();
            return isLogin;
        }
        private static void InsertAuto(Car car)
        {
            var providerDb = new InventoryDal();
            var con = ConfigurationManager.ConnectionStrings["MyAutoLotConn"].ConnectionString;
            providerDb.OpenConnection(con);

            providerDb.InsertAuto(car);

            providerDb.CloseConnection();
        }

        private static List<Car> GetAllCars()
        {
            var providerDb = new InventoryDal();
            var con = ConfigurationManager.ConnectionStrings["MyAutoLotConn"].ConnectionString;
            providerDb.OpenConnection(con);
            var tmpList = providerDb.GetAllCars();
    
            providerDb.CloseConnection();
            return tmpList;
        }
    }
}
