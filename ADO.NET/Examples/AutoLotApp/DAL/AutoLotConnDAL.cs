﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class Car
    {
        public int CarId { get; set; }
        public string Color { get; set; }
        public string Make { get; set; }
        public string PetName { get; set; }
        public override string ToString() => $"\t{CarId}\t{Color}\t{Make}\t{PetName}";

    } // entity or DataModel
    public class InventoryDal
    {

        private SqlConnection _sqlCn = null; //current connection

        public void OpenConnection(string connectionString)
        {
            _sqlCn = new SqlConnection {ConnectionString = connectionString};
            _sqlCn.Open();
        }

        public void CloseConnection()
        {
            _sqlCn.Close();
        }


        #region Insert method (no param-query)
        public void InsertAuto(Car car)
        {
            var sql = "USE master; DROP DATABASE AutoLot;Insert Into Inventory (Make, Color, PetName) Values" +
                         $"('{car.Make}','{car.Color}','{car.PetName}');" +
                      $"Insert Into Inventory (Make, Color, PetName) Values" +
                      $"('{car.Make}','{car.Color}','{car.PetName}')";
            using (var cmd = new SqlCommand(sql, this._sqlCn))
            {
                Console.WriteLine($"{cmd.ExecuteNonQuery()}");
            }
        }
        #endregion
        #region Login method (no param-query)
        public bool Login(string make, string color)
        {
            var sql = "SELECT TOP 1 * FROM [dbo].[Inventory] as i " +
                      $"WHERE i.Make = '{make}' AND i.Color = '{color}'";
            using (var cmd = new SqlCommand(sql, this._sqlCn))
            {
                var reader = cmd.ExecuteReader();
                if(reader.Read())
                {
                    return true;
                }
            }
            return false;
        }
        #endregion
        #region Login method (param-query)
        public bool LoginParam(string make, string color)
        {
            var sql = string.Format("SELECT TOP 1 *" +
                                "FROM [dbo].[Inventory] as i " +
                                "WHERE i.Make = @Login AND i.Color = @Password",
                                make, color);
            using (var cmd = new SqlCommand(sql, this._sqlCn))
            {
                var param = new SqlParameter
                {
                    ParameterName = "@Login",
                    Value = make,
                    SqlDbType = SqlDbType.Char,
                    Size = 10
                };
                cmd.Parameters.Add(param);

                param = new SqlParameter
                {
                    ParameterName = "@Password",
                    Value = color,
                    SqlDbType = SqlDbType.Char,
                    Size = 10
                };
                cmd.Parameters.Add(param);

                var reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    return true;
                }
            }
            return false;
        }
        #endregion
        #region Look up pet name
        public string GetPetById(int carId)
        {
            string carPetName = null;
            using (var cmd = new SqlCommand("GetPetName", this._sqlCn))
            {
                cmd.CommandType = CommandType.StoredProcedure;

                // Input param.
                var param = new SqlParameter
                {
                    ParameterName = "@carID",
                    SqlDbType = SqlDbType.Int,
                    Value = carId,
                    Direction = ParameterDirection.Input
                };
                cmd.Parameters.Add(param);

                // Output param.
                param = new SqlParameter
                {
                    ParameterName = "@petName",
                    SqlDbType = SqlDbType.NVarChar,
                    Size = -1,
                    Direction = ParameterDirection.Output
                };
                cmd.Parameters.Add(param);

                // Execute the stored proc.
                cmd.ExecuteNonQuery();

                // Return output param.
                carPetName = (string)cmd.Parameters["@petName"].Value;
            }
            return carPetName;
        }

        public List<Car> GetAllCars()
        {

            var tmpList = new List<Car>();
            const string sqlExpression = "SELECT * FROM Inventory";
            using (var cmd = new SqlCommand(sqlExpression, this._sqlCn))
            {
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        //Console.WriteLine($"{reader.GetName(0)}\t{reader.GetName(1)}\t{reader.GetName(2)}\t{reader.GetName(3)}");

                        while (reader.Read())
                        {
                            Car tmpCar = new Car
                            {
                                CarId = (int)reader.GetValue(0),
                                Make = (string)reader.GetValue(1),
                                Color = (string)reader.GetValue(2),
                                PetName = (string)reader.GetValue(3)
                            };
                           // Console.WriteLine(tmpCar.ToString());//c
                            //Console.WriteLine($"{tmpCar}");
                            tmpList.Add(tmpCar);
                        }
                    }
                }
            }
           return tmpList;
        }

        #endregion
    }
}
